from django.apps import AppConfig


class ApiFaceRecogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api_face_recog'
