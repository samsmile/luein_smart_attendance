from random import choices
from rest_framework import serializers
from django.forms import ClearableFileInput

from .models import FaceRecogModel


training_choices = (
    ("incremental","INCREMENTAL"),
    ("from_scratch","FROM_SCRATCH")
)

class FaceRecogInferenceModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = FaceRecogModel
        fields = (
            'input_file',
        )


class FaceRecogTrainingModelSerializer(serializers.ModelSerializer):
    training_choices = serializers.ChoiceField(choices=training_choices)
    class Meta:
        model = FaceRecogModel
        fields = (
            'training_choices',
        )
