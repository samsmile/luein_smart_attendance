import asyncio
import json
import logging as log
import os.path
from django.core.files.storage import FileSystemStorage
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime
# comment start
from api_face_recog.src.recognition_v2_deepface.train import train_recognition_model
from api_face_recog.src.recognition_v2_deepface.inference import inference_image, inference_video, extract_images_for_inference
# comment end
from .models import q_uuid_generate, FaceRecogModel
from .serializers import FaceRecogInferenceModelSerializer, FaceRecogTrainingModelSerializer
from rest_framework.permissions import AllowAny

def setup_custom_logger(name):
    formatter = log.Formatter(fmt='%(asctime)s - %(process)d - %(levelname)s - %(message)s')
    handler = log.StreamHandler()
    handler.setFormatter(formatter)
    logger = log.getLogger(name)
    logger.setLevel(log.INFO)
    logger.addHandler(handler)
    return logger


logger = setup_custom_logger("api_face_recog")


class FaceRecognitionInferenceViews(APIView):
    queryset = FaceRecogModel
    serializer_class = FaceRecogInferenceModelSerializer

    def get(request, *args, **kwargs):
        # todos = UserDataModel.objects.filter()
        # serializer = UserDataInferenceModelSerializer(todos, many=True)
        # return Response(serializer.data, status=status.HTTP_200_OK)

        json_response = {
            "ERROR": "invalid request",
        }
        return Response(json_response)

    def post(self, request):
        start = datetime.now()
        API_URL = request.api_url
        print("API_URL: ", API_URL)
        # print(request.data)

        data = []
        status = "failure"
        message = "Invalid Data!!!"
        is_valid = False

        if request.data.get('input_file'):
            request_file = request.FILES['input_file']
            # file_path = str(request_file.temporary_file_path())
            filename = request_file.name
            fs = FileSystemStorage()

            file_path = "media/"
            if len(os.path.splitext(filename)) > 1 and os.path.splitext(filename)[-1]:
                if os.path.splitext(filename)[1].strip('.') in ['jpg','jpeg','png']:
                    file_path_without_media = fs.save("api_face_recog/inference/image/" + filename, request_file)
                    file_path = "media/" + file_path_without_media
                elif os.path.splitext(filename)[1].strip('.') in ['mp4','webm','wmv','avi','flv','mkv']:
                    file_path_without_media = fs.save("api_face_recog/inference/video/" + filename, request_file)
                    file_path = "media/" + file_path_without_media
            else:
                file_path_without_media = fs.save("api_face_recog/inference/blob_video/" + filename, request_file)
                print("file_path_without_media: ",file_path_without_media)
                file_path = "media/" + file_path_without_media
            # print(file_path)
            try:
                db_path = "media/api_face_recog/attendence/trained"
                if os.path.exists(db_path):
                    logger.info("DB_path selected --- %s",db_path)
                    if "image" in file_path:
                        logger.info(" IMAGE INFERENCE STARTED ")
                        data = inference_image(file_path,db_path)[1]
                    else:
                        logger.info(" VIDEO INFERENCE STARTED ")
                        frames_folder = extract_images_for_inference(file_path)                
                        data = inference_video(frames_folder,db_path)
                        log.debug('INFERENCE RESPONSE:')
                        print("api data: \n",data)
                else:
                    raise Exception("Training Folder doesn't exsist!!!")
            except Exception as e:
                logger.exception("Exception occurred!!")
            else:
                logger.info(" INFERENCE FINISHED ")
                if 'faces_identified' in data and data['faces_identified']:
                    message = "Face Recognised!!!"
                    status = "success"
                    is_valid = True
                else:
                    message = "No face recognised"
        else:
            message = "No file/data uploaded"
        json_response = {
            "status": status,
            "message": message,
            'is_valid': is_valid,
            "data": data
        }
        print("API JSON RESPONSE: \n",json_response)
        end = datetime.now()
        logger.info(" --- INFERENCE TIME TAKEN : %s ----",end-start)
        return Response(json_response)


class FaceRecognitionTrainingViews(APIView):
    permission_classes = [AllowAny]
    queryset = FaceRecogModel
    serializer_class = FaceRecogTrainingModelSerializer

    def get(request, *args, **kwargs):
        # todos = FaceRecogModel.objects.filter()
        # serializer = FaceRecogTrainingModelSerializer(todos, many=True)
        # return Response(serializer.data, status=status.HTTP_200_OK)

        json_response = {
            "ERROR": "invalid request",
        }
        return Response(json_response)

    def post(self, request):
        start = datetime.now()
        API_URL = request.api_url
        print("API_URL: ", API_URL)

        id = q_uuid_generate()
        status = "failure"
        message = "Invalid Data!!!"
        is_valid = False
        log = {}

        file_path_without_media = ""
        choice = request.data.get('training_choices')
        if choice == "incremental":
            training_folder = "media/api_face_recog/attendence/waiting_folder"
        else:
            training_folder = "media/api_face_recog/attendence/trained"
        
        if training_folder:
            try:
                response_data = train_recognition_model(training_folder)
                logger.info("WAITING_FOLDER: %s",training_folder)
            except Exception as e:
                log['exception'] = str(e)
                logger.exception("Exception Occurred in Training API!!!")
                message = "Training failed!!!"
                if log['exception']:
                    message = log['exception']
            else:
                logger.info("TRAINING COMPLETED")
                status = response_data['status']
                is_valid = response_data['is_valid']
                message = response_data['message']
                FaceRecogModel.objects.create(
                    user_id=id,
                    status=status,
                    is_valid=is_valid,
                    message=message,
                    logs=log,
                    input_file=file_path_without_media,
                    metadata={}
                )
        else:
            message = "No path sepcified"

        json_response = {
            "response_id": id,
            "status": status,
            "message": message,
            'is_valid': is_valid
        }
        end = datetime.now()
        logger.info(" --- TRAINING TIME TAKEN : %s ----",end-start)
        return Response(json_response)
