import logging
import shutil
from api_face_recog.src.recognition_v2_deepface.inference_utils import find
from django.conf import settings
import cv2
import os,time
import concurrent.futures
import time
from django.conf import settings
from api_face_recog.src.utils import extract_images_from_blobs

log = logging.getLogger("api_face_recog")

def inference_image(image_path,db_path):
    df,results = find(img_path = image_path,
        db_path = db_path, 
        model_name = settings.RECOGNIION_MODEL_NAME,
        model=settings.RECOGNIION_MODEL,
        detector_backend = settings.RECOGNIION_DETECTOR,
        distance_metric = settings.RECOGNIION_METRICS,
        enforce_detection = settings.RECOGNITION_DETECTION_ENFORCE
    )
    response = {"faces_identified":results,"set_of_classes_identified":[x['class'] for x in results]}
    return results,response

"""
    ---- video inference ---
"""
def extract_images_for_inference(video_file_path):
    file_name = os.path.splitext(video_file_path)[0]
    # if "\\" in video_file_path:
    #     video_folder = "/".join(video_file_path.split("\\")[:-1])
    #     file_name = file_name.split("\\")[-1]
    # else:
    video_folder = "/".join(video_file_path.split("/")[:-1])
    file_name = file_name.split("/")[-1]

    temp_folder_name = "temp_"+time.strftime("%Y%m%d%H%M%S")
    temp_folder = os.path.join(video_folder,temp_folder_name)
    # print("temp_folder: ",temp_folder)
    if not os.path.exists(temp_folder):
        os.mkdir(temp_folder)
    temp_folder = os.path.join(temp_folder,file_name)
    if not os.path.exists(temp_folder):
        os.mkdir(temp_folder)
        
    log.info('EXTRACTING IMAGES FROM INFERENCE')
    extract_images_from_blobs(train_folder=temp_folder,video_file=video_file_path)
    log.info('EXTRACTING IMAGES FROM INFERENCE >>> COMPLETED')

    return temp_folder


def call_inference_api(count,frames_folder,set_of_images,db_path):
    results = []
    for x in set_of_images:
        results.append(
            inference_image(image_path=os.path.join(frames_folder, x), db_path=db_path)[0]
        )
        log.info('List Count: %s --- image: %s',count,x)
        time.sleep(1)    
    return results

def inference_video(frame_folder,db_path):
    faces_identified = []
    set_of_classes_identified = set()

    frames_extracted = os.listdir(frame_folder)
    sublists = list()
    chunk_size = 10

    for i in range(0,len(frames_extracted),chunk_size):
        sublists.append(frames_extracted[i:i+chunk_size])
    
    # print(sublists)
    response = list()
    with concurrent.futures.ThreadPoolExecutor(max_workers=30) as executor:
        futures = {executor.submit(call_inference_api, count,frame_folder,lst,db_path) for count,lst in enumerate(sublists)}
        for future in concurrent.futures.as_completed(futures):
            response = future.result()
        # print(response)
    
    if response:
        for lst in response:
            if lst:
                for x in lst:
                    if x['class'] not in set_of_classes_identified:
                        faces_identified.append(x)
                        set_of_classes_identified.add(x['class'])

    
    if not settings.SAVE_TEMP_FOLDER_INFERENCE:
        if os.path.exists(frame_folder):
            shutil.rmtree(frame_folder)
            if '\\' in frame_folder:
                os.rmdir('\\'.join(frame_folder.split('\\')[:-1]))
            else:
                os.rmdir('/'.join(frame_folder.split('/')[:-1]))

    return {"faces_identified":faces_identified,"set_of_classes_identified":list(set_of_classes_identified)}