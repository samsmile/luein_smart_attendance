import time
from api_face_recog.src.recognition_v2_deepface.train_utils import update_database
import os
from django.conf import settings
import glob,shutil
import logging

log = logging.getLogger("api_face_recog")

def train_recognition_model(classes_to_train_folder_path):
    status = "failure"
    message = ""
    is_valid = False
    db_path = "/".join(classes_to_train_folder_path.split('/')[:-1])
    data_folder_path = classes_to_train_folder_path
    retrain_model_from_scratch = False
    representation_file = "representations_%s.pkl".replace("-", "_").lower() % (settings.RECOGNIION_MODEL_NAME)
    log.info("--- available representation_file : %s ---",representation_file)

    if "waiting_folder" in classes_to_train_folder_path:
        if len(os.listdir(classes_to_train_folder_path)) == 0:
            raise ValueError("No data in 'waiting_folder'!!!")
        if "trained" not in os.listdir(db_path):
            db_path = os.path.join(db_path,"trained")
            os.mkdir(db_path)
        else:            
            db_path = os.path.join(db_path,"trained")
    elif "trained" in classes_to_train_folder_path:
        db_path = classes_to_train_folder_path
        data_folder_path = classes_to_train_folder_path
        print("From scratch impl. ")
        if os.path.exists(os.path.join(os.getcwd(),data_folder_path,representation_file)):
            print("path: ",os.path.join(os.getcwd(),db_path,representation_file))
            os.rename(src=os.path.join(os.getcwd(),db_path,representation_file),dst=os.path.join(os.getcwd(),db_path,representation_file.split('.')[0]+"_backup_{}.pkl".format(time.strftime("%Y%m%d%H%M%S"))))
        retrain_model_from_scratch = True
    else:
        raise Exception("!!! --- INVALID PATH SPECIFIED: all files/folders for: \n attendence register should be under 'media/api_face_recog/attendence/waiting_folder' or for \n testing : 'media/api_face_recog/attendence/trained'---- !!!")
    
    log.info("--- coping files to db_path : %s started ---",db_path)
    list_of_images = []
    if not retrain_model_from_scratch:  # if choice seleccted is "incremental" then create folder in train else don't
        for x in os.listdir(data_folder_path):
            if not os.path.exists(os.path.join(db_path,x)):
                os.mkdir(os.path.join(db_path,x))
            for file in os.listdir(os.path.join(data_folder_path,x)):
                shutil.copy(os.path.join(data_folder_path,x,file),os.path.join(db_path,x,file),follow_symlinks=True)
                list_of_images.append(os.path.join(db_path,x,file))
    else:
        #  check if data in waiting _ folder or not 
        if len(os.listdir(classes_to_train_folder_path.replace("trained","waiting_folder"))) > 0:
            for x in os.listdir(classes_to_train_folder_path.replace("trained","waiting_folder")):
                if not os.path.exists(os.path.join(db_path,x)):
                    os.mkdir(os.path.join(db_path,x))
                for file in os.listdir(os.path.join(data_folder_path.replace("trained","waiting_folder"),x)):
                    shutil.copy(os.path.join(data_folder_path.replace("trained","waiting_folder"),x,file),os.path.join(db_path,x,file),follow_symlinks=True)
        for folder in os.listdir(data_folder_path):
            if '.pkl' not in folder:
                for file in os.listdir(os.path.join(data_folder_path,folder)):
                    list_of_images.append(os.path.join(db_path,folder,file))
    log.info("--- coping files to db_path : %s  completed ---",db_path)
    
    log.info("--- training started ---")
    try:
        print("total list_of_images: ",len(list_of_images))
        update_database(list_of_images=list_of_images,trained_model_path = db_path,model_name=settings.RECOGNIION_MODEL_NAME,detector_backend=settings.RECOGNIION_DETECTOR,model=settings.RECOGNIION_MODEL,
        distance_metric = settings.RECOGNIION_METRICS,disable_prog_bar=False)
    except Exception as e:
        log.exception("EXCEPTION OCCURRED during train!!! ")
        message = str(e)
    else:
        status = "success"
        message = "Model updated Successfully!!!"
        is_valid = True   
    log.info("--- training started completed---")

    log.info("--- removing files in waiting folder---")
    if "waiting_folder" not in data_folder_path:
        data_folder_path = data_folder_path.replace("trained","waiting_folder")
    for f in glob.glob(os.path.join(data_folder_path,'*')):
        shutil.rmtree(f)
    
    return {"status":status,"message":message,"is_valid":is_valid}
