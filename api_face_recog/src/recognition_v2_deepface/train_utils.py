import json
import warnings
warnings.filterwarnings("ignore")
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ["CUDA_VISIBLE_DEVICES"]="0"
from tqdm import tqdm
import pickle
from deepface.basemodels import Boosting
from deepface.DeepFace import build_model,represent
import concurrent.futures
import time,logging
from django.conf import settings

log = logging.getLogger("api_face_recog")

def find_representations(
	count,
	list_of_images,
	model_names,
	models,
	model_name,
	detector_backend,
	align,
	normalization,
	disable_prog_bar):

	representations = list()
	log.info("Processing batch: %s",count)
	pbar = tqdm(range(0,len(list_of_images)), desc='Finding representations...', disable = disable_prog_bar)

	for index in pbar:
		# time.sleep(1)
		image = list_of_images[index]

		instance = []
		instance.append(image)

		for j in model_names:
			custom_model = models[j]

			representation = represent(
					img_path = image,
					model_name = model_name, 
					model = custom_model,
					enforce_detection = settings.RECOGNITION_DETECTION_ENFORCE, 
					detector_backend = detector_backend,
					align = align,
					normalization = normalization
			)
			
			if representation:
				instance.append(representation)
			else:
				instance.append(list())

		if instance[1]:
			representations.append(instance)
	return representations


def update_database(list_of_images,trained_model_path, model_name ='VGG-Face', distance_metric = 'cosine', model = None, detector_backend = 'opencv', align = True, disable_prog_bar = True, normalization = 'base', silent=False):

	if os.path.isdir(trained_model_path) == True:

		# Load or build model
		if model == None:

			if model_name == 'Ensemble':
				if not silent: print("Ensemble learning enabled")
				models = Boosting.loadModel()

			else: #model is not ensemble
				model = build_model(model_name)
				models = {}
				models[model_name] = model

		else: #model != None
			if not silent: print("Already built model is passed")

			if model_name == 'Ensemble':
				Boosting.validate_model(model)
				models = model.copy()
			else:
				models = {}
				models[model_name] = model

		#---------------------------------------

		if model_name == 'Ensemble':
			model_names = ['VGG-Face', 'Facenet', 'OpenFace', 'DeepFace']
			metric_names = ['cosine', 'euclidean', 'euclidean_l2']
		elif model_name != 'Ensemble':
			model_names = []; metric_names = []
			model_names.append(model_name)
			metric_names.append(distance_metric)

		#---------------------------------------

		file_name = "representations_%s.pkl" % (model_name)
		file_name = file_name.replace("-", "_").lower()

		if len(list_of_images) == 0:
			raise ValueError("'list_of_images' list is empty.........No image available for training!!!")
		
		# # generate representations
		representations = list()
		sublists = list()
		chunk_size = 20

		for i in range(0,len(list_of_images),chunk_size):
			sublists.append(list_of_images[i:i+chunk_size])
		
		# """
		# 		PARALLEL PROCESSING VIA THREADING
		# """

		# # print(sublists)
		# response = list()
		# with concurrent.futures.ThreadPoolExecutor(max_workers=50) as executor:
		# 	futures = {executor.submit(find_representations, count,lst,model_names,models,model_name,detector_backend,align,normalization,enforce_detection,disable_prog_bar) for count,lst in enumerate(sublists)}
		# 	for future in concurrent.futures.as_completed(futures):
		# 		response.append(future.result())
		
		# if response:
		# 	for x in response:
		# 		if x:
		# 			for y in x:
		# 				representations.append(y)
		# """
		# 		PARALLEL PROCESSING VIA THREADING END
		# """

		"""
				SERIAL PROCESSING
		"""

		for count,lst in enumerate(sublists):
			res = find_representations(count,lst,model_names,models,model_name,detector_backend,align,normalization,disable_prog_bar)
			if res:
				for x in res:
					representations.append(x)

		if representations:
			# print(json.dumps(representations,indent=4))
			prev_representations = list()
			if os.path.exists(os.path.join(trained_model_path,file_name)):
				with open(os.path.join(trained_model_path,file_name),'rb+') as f:			
					prev_representations = pickle.load(f)

			with open(os.path.join(trained_model_path,file_name),'wb') as f:
				combined_data = prev_representations
				for x in representations:
					combined_data.append(x)
				pickle.dump(combined_data, f)

			if not silent: print("Representations stored in ",trained_model_path,"/",file_name," file. This file has been updated!")
		else:
			raise ValueError("No response representations returned for image!!!")
	else:
		raise ValueError("Passed trained_model_path does not exist!")