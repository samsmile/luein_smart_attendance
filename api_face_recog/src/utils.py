import base64
import logging
import os
import time,sys,django
from django.conf import settings
import cv2
from django.core.files.base import ContentFile

# LUEIN_SM_ATT_PROJ_PATH = os.getenv("LUEIN_SM_ATT_PROJ_PATH")
# print("LUEIN_SM_ATT_PROJ_PATH : ", LUEIN_SM_ATT_PROJ_PATH)
# sys.path.append(LUEIN_SM_ATT_PROJ_PATH)
# os.environ['DJANGO_SETTINGS_MODULE'] = 'luein_smart_attendance.settings'
# django.setup()

log = logging.getLogger("api_face_recog")

def base64_file(data, name=None):
    _format, _img_str = data.split(';base64,')
    _name, ext = _format.split('/')
    if not name:
        name = _name.split(":")[-1]
    return ContentFile(base64.b64decode(_img_str), name='{}.{}'.format(name, ext))


def base64_decode(data):
    format, imgstr = data.split(';base64,')
    return imgstr.decode('base64')


def base64_encode(data):
    if data:
        return 'data:image/png;base64,' + data


def save_frames(frame_data_lst,train_folder):
    const_time = time.strftime("%Y%m%d%H%M%S")
    updated_frames = dict()
    counts = list(frame_data_lst.keys())
    print("len(counts) -- total frames extract : ",len(counts)) 

    if len(counts) <= settings.MAX_FRAMES_TO_BE_EXTRACTED:
        updated_frames = frame_data_lst
    
    print("len(updated_frames) 1 : ",len(updated_frames))    
    COUNT_IDX = 0
    for ct in range(0,len(counts)):
        if ct % settings.FRAME_COUNTER == 0:
            if len(updated_frames.keys()) >= settings.MAX_FRAMES_TO_BE_EXTRACTED:
                break
            updated_frames[counts[ct]] = frame_data_lst[counts[ct]]
            COUNT_IDX += 1
    
    print("len(updated_frames) 2 : ",len(updated_frames))    
    if COUNT_IDX < settings.MAX_FRAMES_TO_BE_EXTRACTED:
        for ctx,img in frame_data_lst.items():
            if len(updated_frames.keys()) >= settings.MAX_FRAMES_TO_BE_EXTRACTED:
                break
            updated_frames[ctx] = img

    print("len(updated_frames) 3 : ",len(updated_frames))      

    for count,img in updated_frames.items():        
        cv2.imwrite(os.path.join(train_folder,"%s_frame_%d.jpg") % (const_time, count), img)


def extract_images_from_blobs(train_folder, video_file):
    frames = {}
    vidcap = cv2.VideoCapture(video_file)
    every_n_frame = settings.FRAME_EXTRACT_RATE
    fps = vidcap.get(cv2.CAP_PROP_FPS)
    log.warn("FPS : %s",fps)

    after_n_frame = int(every_n_frame * fps)
    print('---every_n_frame---',every_n_frame)
    print('---fps---',fps)
    success, image = vidcap.read()
    count = 0
    counter = 0
    while success:
        if count % after_n_frame == 0:
            frames[count] = image
            counter += 1
        success, image = vidcap.read()
        count += 1
    # print(frames)
    save_frames(frame_data_lst=frames,train_folder=train_folder)

# if __name__ == "__main__":
#     # extract_images_from_blobs("E:\\PERSONAL\\LUEIN\\codebases\\facial_recog\\main_app\\luein_smart_attendance\\media\\api_face_recog\\attendence\\waiting_folder","D:\\Downloads\\blobs\\register__blob_wgQo0Rw")
#     extract_images_from_blobs("E:\\PERSONAL\\LUEIN\\codebases\\facial_recog\\main_app\\luein_smart_attendance\\media\\api_face_recog\\attendence\\waiting_folder","C:\\Users\\Kaustav\\Desktop\\attendance__blob_LDxqmfU.mp4")