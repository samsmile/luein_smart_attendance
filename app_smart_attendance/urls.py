from django.urls import path

from . import views

app_name = 'app_smart_attendance'

urlpatterns = [
    # template view

    path('registration/', views.student_register_view, name='student_register'),
    path('student/', views.students_view, name='students'),
    path('student/<slug:profile_id>/', views.StudentsDetailView.as_view(), name='students_detail'),

    path('mark_attendance/', views.take_attendance_view, name='take_attendance'),
    path('attendance/', views.attendance_view, name='attendance'),

    path('timesheet/overview/', views.timesheet_custom_view, name='timesheet_view'),
    path('timesheet/overview/<slug:date_text>/', views.timesheet_custom_view, name='timesheet_custom_view'),
    path('timesheet/overview/<slug:date_text>/<slug:date_range_type>/', views.timesheet_custom_view,
         name='timesheet_custom_all_view'),

    path('timesheet/overview-self/', views.timesheet_self_view, name='timesheet_self_view'),
    path('timesheet/overview-self/<slug:date_text>/', views.timesheet_self_view, name='timesheet_self_custom_view'),
    path('timesheet/overview-self/<slug:date_text>/<slug:date_range_type>/', views.timesheet_self_view,
         name='timesheet_self_custom_all_view'),

    path('timesheet/overview-class/', views.timesheet_class_view, name='timesheet_class_view'),
    path('timesheet/overview-class/<slug:date_text>/', views.timesheet_class_view, name='timesheet_class_date_view'),
    path('timesheet/overview-class/<slug:date_text>/<slug:date_range_type>/',
         views.timesheet_class_view, name='timesheet_class_date_type_view'),
    path('timesheet/overview-class/<slug:date_text>/<slug:date_range_type>/<slug:class_name_section>',
         views.timesheet_class_view, name='timesheet_class_custom_all_view'),


    path('timesheet/<slug:date_text>/<slug:profile_id>/detail', views.timesheet_detail_view,
         name='timesheet_detail_view'),


    path('reports/', views.report_view, name='report'),
    path('classes/', views.classes_view, name='classes'),
    path('settings/', views.settings_view, name='settings'),
    path('announcement/', views.announcement_view, name='announcement_view'),
    path('people/', views.people_view, name='people'),
    path('period/list/', views.period_list_view, name='period_list'),

    path('camera/list/', views.camera_list_view, name='camera_list'),

    path('staff/', views.teacher_list_view, name='teacher_list'),
    path('staff/<slug:profile_id>/', views.TeacherDetailView.as_view(), name='teacher_detail'),

    path('locations/manage/', views.location_manage_view, name='location_manage_view'),

    # form view

    path('register_user/', views.register_user_view, name='register_user_view'),
    path('register_face/', views.register_face_view, name='register_face_view'),
    # old
    path('take_attendance_class/', views.take_attendance_class_view, name='take_attendance_class_view'),
    path('take_attendance_other/', views.take_attendance_other_view, name='take_attendance_other_view'),
    # new
    path('take_attendance_self/', views.take_attendance_self_view, name='take_attendance_self_view'),
    path('take_attendance_class_preselected/', views.take_attendance_class_preselected_view,
         name='take_attendance_class_preselected'),
    path('take_attendance_cctv/', views.take_attendance_cctv_view, name='take_attendance_cctv'),

    path('take_attendance_other_preselected/', views.take_attendance_other_preselected_view,
         name='take_attendance_other_preselected'),

    path('profile/<slug:profile_id>/update/', views.profile_update_view, name='profile_update'),
    path('teacher/<slug:profile_id>/update/', views.teacher_update_view, name='teacher_update'),
    path('student/<slug:profile_id>/update/', views.student_update_view, name='student_update'),
    path('period/create/', views.period_create_view, name='period_create'),
    path('period/<slug:period_id>/update/', views.period_update_view, name='period_update'),
    path('period/<slug:period_id>/delete/', views.period_delete_view, name='period_delete'),

    path('camera/create/', views.camera_create_view, name='camera_create'),
    path('camera/<slug:camera_id>/update/', views.camera_update_view, name='camera_update'),
    path('camera/<slug:camera_id>/delete/', views.camera_delete_view, name='camera_delete'),
    path('camera/<slug:camera_id>/', views.CameraDetailView.as_view(), name='camera_detail'),

    path('announcement/create/', views.announcement_create_view, name='announcement_create'),
    path('announcement/<slug:announcement_id>/update/', views.announcement_update_view, name='announcement_update'),
    path('announcement/<slug:announcement_id>/delete/', views.announcement_delete_view, name='announcement_delete'),

    # working - change_password_v1
    path('password/', views.change_password, name='change_password'),


    # ajax view
    # path('ajax/student_register/', views.student_register_view_ajax, name='student_register_ajax'),
    path('ajax/user_register/', views.user_register_ajax, name='user_register_ajax'),
    path('ajax/role_profiles/', views.get_profiles_by_role_ajax, name='get_profiles_by_role_ajax'),
    path('ajax/get_attendance/', views.get_list_of_all_students_attendance_by_class_name_section_date_ajax,
         name='get_list_of_all_students_attendance_by_class_name_section_date_ajax'),
    path('ajax/get_attendance_status/', views.get_attendance_status_by_profile_id_ajax,
         name='get_attendance_status_by_profile_id_ajax'),

    path('ajax/camera_details/', views.get_camera_details_ajax, name='get_camera_details_ajax'),
    path('ajax/check_in_out/', views.get_check_in_out_by_profile_ajax, name='get_check_in_out_by_profile_ajax'),
    path('ajax/take_attendance/', views.take_attendance_view_ajax, name='take_attendance_ajax'),
    path('ajax/take_attendance_class/', views.take_attendance_class_view_ajax, name='take_attendance_class_view_ajax'),
    path('ajax/take_attendance_class_/', views.take_attendance_class_manual_view_ajax,
         name='take_attendance_class_manual_view_ajax'),
    path('ajax/take_attendance_self/', views.take_attendance_self_view_ajax, name='take_attendance_self_view_ajax'),
    path('ajax/take_attendance_other/', views.take_attendance_other_view_ajax, name='take_attendance_other_view_ajax'),

    path('ajax/attendance_class/', views.get_attendance_class_ajax, name='get_attendance_class_ajax'),
    path('ajax/attendance_self/', views.get_attendance_self_ajax, name='get_attendance_self_ajax'),
    path('ajax/attendance_other/', views.get_attendance_other_ajax, name='get_attendance_other_ajax'),
    path('ajax/attendance_cctv/', views.get_attendance_cctv_ajax, name='get_attendance_cctv_ajax'),

    path('ajax/load_section/', views.get_section_view_ajax, name='ajax_load_section'),
    path('ajax/report/', views.get_report_view_ajax, name='get_report_ajax'),

    # demo
    path('demo/', views.demo_view, name='demo'),

]
