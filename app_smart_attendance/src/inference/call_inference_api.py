

"""
    ################ api_face_recog/ calling ####################
"""
import requests


def get_face_recognition_response(request,file):
    # PRODUCTION URL
    # url = "http://216.48.177.111:8084/api/inference/"
    url = request.api_url + "/" + "api/inference/"
    header = {
        "accept": "application/json;",
    }
    payload = {'csrfmiddlewaretoken': ""}
    try:
        r = requests.post(url, headers=header, data=payload, files={'input_file': open(file, 'rb')})
        # print(r)
        if r.status_code == 200:
            data = r.json()
            final_response_json = data
        else:
            raise Exception(f"STATUS CODE: {r.status_code} --- Message: {r.reason}")
        return final_response_json
    except Exception as e:
        raise Exception(str(e))