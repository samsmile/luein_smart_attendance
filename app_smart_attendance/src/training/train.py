import time

from deepface import DeepFace

from .train_utils import update_database
import os
from django.conf import settings
import glob, shutil
import logging
import traceback

log = logging.getLogger("redis_call")


def train_recognition_model(classes_to_train_folder_path, pickle_folder):
    status = "failure"
    message = ""
    waiting_data_folder_path = classes_to_train_folder_path

    list_of_images = []
    
    # -------------REPRESENTATIONS FILE------------
    # representation_file = "representations_%s.pkl".replace("-", "_").lower() % settings.RECOGNIION_MODEL_NAME
    representation_file = 'representations.pkl'
    log.info("--- available representation_file : %s ---", representation_file)
    # ---------------------------------------------
    
    print("waiting_data_folder_path: ",waiting_data_folder_path)

    if len(os.listdir(classes_to_train_folder_path)) == 0:
        raise ValueError("No data in 'waiting_folder'!!!")
    training_images_path = classes_to_train_folder_path.replace('waiting_folder','training_folder')

    log.info("--- coping files to training_images_path : %s started ---", training_images_path)
    for file in os.listdir(waiting_data_folder_path):
        shutil.copy(os.path.join(waiting_data_folder_path, file), os.path.join(training_images_path, file),
                    follow_symlinks=True)
        list_of_images.append(os.path.join(training_images_path, file))
    log.info("--- coping files to training_images_path : %s  completed ---", training_images_path)

    log.info("--- training started ---")
    print("total list_of_images: ", len(list_of_images))
    try:
        update_database(list_of_images=list_of_images, training_images_path=training_images_path, pickle_folder=pickle_folder,
                        model_name=settings.RECOGNIION_MODEL_NAME, detector_backend=settings.RECOGNIION_DETECTOR,
                        model=settings.RECOGNIION_MODEL,
                        distance_metric=settings.RECOGNIION_METRICS, disable_prog_bar=False)
    except Exception as e:
        traceback.print_exc()
        log.exception("EXCEPTION OCCURRED during train!!! ")
        message = str(e)
    else:
        status = "success"
        message = "Model updated Successfully!!!"
    log.info("--- training started completed---")

    log.info("--- removing files in waiting folder---")
    if '\\' in waiting_data_folder_path:
        waiting_data_folder_path = '/'.join(waiting_data_folder_path.split("\\")[:-1])
    else:
        waiting_data_folder_path = '/'.join(waiting_data_folder_path.split("/")[:-1])
    for f in glob.glob(os.path.join(waiting_data_folder_path, '*')):
        shutil.rmtree(f)
    return {"status": status, "message": message}
