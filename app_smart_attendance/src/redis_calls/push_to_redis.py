from walrus import Database
import logging

log = logging.getLogger("app_smart_attendance")

db = Database(port=6379)


def push_request_for_face_recog(request_data):
    log.info("---- Sending TRAINING request ----")
    print(request_data)
    stream_keys = ['face_recog']
    cg_current = db.consumer_group('face_recog-cg', stream_keys)
    cg_current.face_recog.add(request_data)

# if __name__ == "__main__":
#     push_request_for_face_recog({
#         "request_type": "train",
#         "mode": "incremental",
#         "sender_uuid": str(sender_uuid),
#         "notification_type": notification_type,
#         "description": description,
#         "action_object_uuid": str(action_object_uuid),
#         "class_name": class_name,
#         "section": section,
#         "media_type": media_type,
#         "school_id": str(school_id),
#         "recipient_role": json.dumps(recipient_role),
#         "wait_folder": db_path,
#         "role": role
#     })