import json
import concurrent.futures
import os, sys
import django
from django.core import files
from walrus import Database
import logging as log_print
from webpush.utils import _send_notification
import traceback


"""
    ----------------------
    pacakge import segment
    ----------------------
"""
SRC_DIRECTORY = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print("SRC_DIRECTORY: ", SRC_DIRECTORY)
sys.path.append(SRC_DIRECTORY)

from training.train import train_recognition_model
from inference.inference import extract_images_for_inference, inference_video

from camera.camera_init import get_stream_save_frame_or_video

"""
    --------------------------
    pacakge import segment end
    --------------------------
"""

"""
    --------------------------
    django setup load
    --------------------------
"""
LUEIN_SM_ATT_PROJ_PATH = os.getenv("LUEIN_SM_ATT_PROJ_PATH")
print("LUEIN_SM_ATT_PROJ_PATH: ", LUEIN_SM_ATT_PROJ_PATH)
sys.path.append(LUEIN_SM_ATT_PROJ_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = 'luein_smart_attendance.settings'
django.setup()

from app_root.models import SchoolModel, Profile, AttendanceModel, PeriodModel, MediaCapturedModel, \
    StaffAttendanceModel, CameraResponseModel
from app_smart_attendance.views import set_attendance_under_process
from notifications.signals import notify
from django.conf import settings
from django.utils.dateparse import parse_datetime

"""
    --------------------------
    django setup load end
    --------------------------
"""

"""
    --------------------------
        logger segment
    --------------------------
"""


def setup_custom_logger(name):
    formatter = log_print.Formatter(fmt='%(asctime)s - %(process)d - %(levelname)s - %(message)s')
    # handler = log_print.StreamHandler()
    fh = log_print.FileHandler(settings.LOGS_DIR + '/redis.log')
    # handler.setFormatter(formatter)
    fh.setFormatter(formatter)
    logger = log_print.getLogger(name)
    logger.setLevel(log_print.INFO)
    # logger.addHandler(handler)
    logger.addHandler(fh)
    return logger


log = setup_custom_logger("redis")

"""
    --------------------------
        logger segment end
    --------------------------
"""

# Global
db = Database(port=6379)

"""
    --------------------------
    Delete key from redis stream
    --------------------------
"""


def delete_key_from_stream(id):
    stream_keys = ['face_recog']
    cg_current = db.consumer_group('face_recog-cg', stream_keys)
    cg_current.face_recog.delete(id)


"""
    --------------------------
    Create row in attendance model
    --------------------------
"""


def create_staff_attendance_row(staff_attendance_type, attempt_status, logged_in_user, taken_by, media_id,
                                attendance_date, taken_for, attendance_status, staff_attendance_geolocation):
    staff_attendance_get_qs = StaffAttendanceModel.objects.create(
        attendance_type=staff_attendance_type,
        attempt_status=attempt_status,
        loggedin_user=logged_in_user,
        taken_by=taken_by,
        media_id=media_id,
        attendance_date=attendance_date,

        taken_for=taken_for,
        attendance_status=attendance_status,
        staff_attendance_geolocation=staff_attendance_geolocation,
    )
    return staff_attendance_get_qs


def create_attendance_row(user, attendance_taker, attempt_status, period_id, media_id, attendance_status,
                          attendance_date, attendance_choice):
    attendance_get_qs = AttendanceModel.objects.create(
        loggedin_user=user,
        attendance_taker=attendance_taker,
        attempt_status=attempt_status,
        period_id=period_id,
        media_id=media_id,
        attendance_status=attendance_status,
        attendance_date=attendance_date,
        attendance_choice=attendance_choice
    )
    return attendance_get_qs


def create_camera_response_row(user, attendance_taker, attempt_status, period_id, media_id, attendance_status,
                               attendance_date):
    attendance_get_qs = AttendanceModel.objects.create(
        loggedin_user=user,
        attendance_taker=attendance_taker,
        attempt_status=attempt_status,
        period_id=period_id,
        media_id=media_id,
        attendance_status=attendance_status,
        attendance_date=attendance_date
    )
    return attendance_get_qs


def update_camera_response_row(camera_response_id, media_id, cctv_attempt_status,
                               cctv_attendance_status, attendance_date):
    camera_response_get_qs = CameraResponseModel.objects.filter(camera_response_id=camera_response_id)
    camera_response_get_qs.update(
        media_id=media_id,
        cctv_attempt_status=cctv_attempt_status,
        cctv_attendance_status=cctv_attendance_status,
        attendance_date=attendance_date,
    )

    return camera_response_get_qs[0]


"""
    ----------------------------------------------------
    Delete previous media file on fail attendance cases
    ----------------------------------------------------
"""


def delete_media_file(prev_media_id):
    if settings.REMOVE_ORPHAN_MEDIA:
        print("prev_media_id: ", prev_media_id)
        if prev_media_id.media_file and os.path.isfile(
                os.path.join(os.getcwd(), prev_media_id.media_file.url.lstrip("/"))):
            os.remove(os.path.join(os.getcwd(), prev_media_id.media_file.url.lstrip("/")))
        prev_media_id.delete()


"""
    ----------------------------------------------------
    Attendance handler method
    ----------------------------------------------------
"""


def recognise_face(logged_in_user_uuid, taken_by_user_uuid, taken_for_user_uuid, period_number, section, class_name,
                   day, school_id, attendance_date, pickle_folder, file_path,
                   media_id, role, staff_attendance_type, attendance_type, staff_attendance_geolocation,
                   camera_response_id, attendance_choice):
    is_completed = False

    log.info(" ATTENDANCE STARTED ")
    frames_folder = extract_images_for_inference(file_path)

    logged_in_user = Profile.objects.none()
    attendance_get_qs = AttendanceModel.objects.none()
    staff_attendance_get_qs = StaffAttendanceModel.objects.none()
    taken_by = Profile.objects.none()
    taken_for = Profile.objects.none()

    media_id = MediaCapturedModel.objects.get(media_id=media_id)
    message = ""

    print('---attendance_type---', attendance_type)

    if attendance_type == "class":
        period_id = PeriodModel.objects.get(period_number=period_number, section=section,
                                            class_name=class_name,
                                            day=day, school_id=school_id)
        attendance_list = AttendanceModel.objects.filter(period_id=period_id,
                                                         attendance_date=attendance_date)
        logged_in_user = Profile.objects.get(profile_id=logged_in_user_uuid)
        students = Profile.objects.filter(role=role, class_name=class_name, section=section, school_id=school_id)
        taken_by = Profile.objects.get(profile_id=taken_by_user_uuid)

        # if attendance list is empty i.e. no attendance is taken yet
        if attendance_list and attendance_list[0].attempt_status == "success":
            message = "Attendance Already taken for given period and date!!!"
        else:
            attempt_status = "failure"
            if students:
                print('---frames_folder---', frames_folder)
                print('---pickle_folder---', pickle_folder)
                print('---role---', role)
                video_data = []  # sample response data
                try:
                    video_data = inference_video(frames_folder, pickle_folder, role)
                except:
                    traceback.print_exc()
                    log.exception("Exception occurred during Attendence")

                attendance_status = []
                if video_data and video_data['set_of_classes_identified']:
                    for user in students:
                        if str(user.profile_id) in video_data['set_of_classes_identified']:
                            attendance_status.append({
                                "status": "present",
                                "profile_id": str(user.profile_id)
                            })
                        else:
                            attendance_status.append({
                                "status": "absent",
                                "profile_id": str(user.profile_id)
                            })
                    if attendance_list:
                        log.info("---- UPDATE ----")
                        attendance_obj = attendance_list[0]
                        prev_media_id = attendance_obj.media_id
                        attendance_obj.media_id = media_id
                        attendance_obj.attempt_status = "success"
                        attendance_obj.attendance_status = attendance_status
                        attendance_obj.save()
                        attendance_get_qs = attendance_obj
                        delete_media_file(prev_media_id)
                    else:
                        log.info("---- CREATING ATTENDANCE ROW ----")
                        attendance_get_qs = create_attendance_row(
                            user=logged_in_user,
                            attendance_taker=taken_by,
                            attempt_status="success",
                            period_id=period_id,
                            media_id=media_id,
                            attendance_status=attendance_status,
                            attendance_date=attendance_date,
                            attendance_choice=attendance_choice)
                        log.info("---- CREATING ATTENDANCE ROW COMPLETED ----")
                    message = "Attendance Completed"
                    is_completed = True
                    attempt_status = "success"
                else:
                    message = "No faces identified"
                    # Handle case when all students absent/ No face recognised / Mass bunk
                    if attendance_list:
                        log.info("---- FAILURE MEDIA UPDATE ----")
                        attendance_obj = attendance_list[0]
                        prev_media_id = attendance_obj.media_id
                        attendance_obj.media_id = media_id
                        attendance_obj.save()
                        attendance_get_qs = attendance_obj

                        delete_media_file(prev_media_id)
                    else:
                        log.info("---- FAILURE ROW CREATE ----")
                        attendance_get_qs = create_attendance_row(
                            user=logged_in_user,
                            attendance_taker=taken_by,
                            attempt_status="failure",
                            period_id=period_id,
                            media_id=media_id,
                            attendance_status=attendance_status,
                            attendance_date=attendance_date,
                            attendance_choice=attendance_choice)
                        log.info("---- FAILURE ROW CREATE COMPLETED----")
            else:
                message = "No student registered"

            log.info("---- CREATE STAFF ATTENDANCE ROW ----")
            StaffAttendanceModel.objects.create(
                attendance_type=staff_attendance_type,
                attempt_status=attempt_status,
                loggedin_user=logged_in_user,
                taken_by=taken_by,
                media_id=media_id,
                # attendance_status=attendance_status,
                attendance_date=attendance_date
            )


            log.info("---- CREATE STAFF ATTENDANCE ROW COMPLETED----")
    elif attendance_type == "cctv":
        logged_in_user = Profile.objects.get(profile_id=logged_in_user_uuid)
        taken_by = Profile.objects.get(profile_id=taken_by_user_uuid)
        print('---frames_folder---', frames_folder)
        print('---pickle_folder---', pickle_folder)
        print('---role---', role)

        log.info("--- TAKING ATTENDANCE ---")
        video_data = []
        try:
            video_data = inference_video(frames_folder, pickle_folder, role)
        except:
            traceback.print_exc()
            log.exception("Exception occurred during %s Attendance", attendance_type)

        attendance_status = []
        attempt_status = "failure"

        # video_data = {
        # 	'faces_identified': [{
        # 		'class': '7ee74383-9eb9-4eb4-a20b-858f860ac00a',
        # 		'role': 'school_admin',
        # 		'bbox': {
        # 			'x_min': 555,
        # 			'y_min': 329,
        # 			'x_max': 746,
        # 			'y_max': 563
        # 		},
        # 		'match': True
        # 	}],
        # 	'set_of_classes_identified': ['7ee74383-9eb9-4eb4-a20b-858f860ac00a']
        # }
        print('---video_data---', video_data)

        if video_data and video_data['faces_identified']:

            for item in video_data['faces_identified']:
                profile_id = item["class"]
                recognised_role = item["role"]
                recognised_profile_qs = Profile.objects.filter(profile_id=profile_id, role=recognised_role)[0]

                print('---recognised_profile_qs---', recognised_profile_qs)
                attendance_status.append({
                    "status": "present",
                    "profile_id": str(recognised_profile_qs.profile_id)
                })
                attempt_status = "success"

            attendance_get_qs = update_camera_response_row(camera_response_id=camera_response_id,
                                                           media_id=media_id,
                                                           cctv_attempt_status="success",
                                                           cctv_attendance_status=attendance_status,
                                                           attendance_date=attendance_date)
        else:
            attendance_get_qs = update_camera_response_row(camera_response_id=camera_response_id,
                                                           media_id=media_id,
                                                           cctv_attempt_status="failure",
                                                           cctv_attendance_status=attendance_status,
                                                           attendance_date=attendance_date)

        print("--- attendance_status ---", attendance_status)

        log.info("--- TAKING ATTENDANCE COMPLETED ---")
    else:
        if attendance_type == "self":
            logged_in_user = Profile.objects.get(profile_id=logged_in_user_uuid)
            taken_by = logged_in_user
            taken_for = logged_in_user
        elif attendance_type == "other":
            logged_in_user = Profile.objects.get(profile_id=logged_in_user_uuid)
            taken_by = Profile.objects.get(profile_id=taken_by_user_uuid)
            taken_for = Profile.objects.get(profile_id=taken_for_user_uuid)

        print('---taken_for---', taken_for)
        print('---attendance_date---', attendance_date)
        print('---staff_attendance_type---', staff_attendance_type)

        # UNCOMMENT START #
        # staff_attendance_list = StaffAttendanceModel.objects.filter(
        #     taken_for=taken_for,
        #     attendance_date__date=attendance_date,
        #     attempt_status="success",
        #     attendance_type=staff_attendance_type
        # )
        # print('---staff_attendance_list---', staff_attendance_list)
        #
        # if staff_attendance_list:
        #     log.info("---- STAFF ATTENDANCE ALREADY TAKEN FOR GIVEN DATE ----")
        #     message = "Attendance already taken for given date: " + attendance_date.strftime("%d-%m-%Y")
        #     staff_attendance_get_qs = staff_attendance_list
        # else:
        log.info("--- TAKING ATTENDANCE ---")
        video_data = []
        try:
            video_data = inference_video(frames_folder, pickle_folder, role)
        except:
            traceback.print_exc()
            log.exception("Exception occurred during %s Attendance", attendance_type)

        print('---video_data---', video_data)

        attendance_status = []
        attempt_status = "failure"
        if video_data and video_data['set_of_classes_identified']:
            if taken_for_user_uuid in video_data['set_of_classes_identified']:
                attendance_status.append({
                    "status": "present",
                    "profile_id": str(taken_for.profile_id)
                })
                attempt_status = "success"
                set_attendance_under_process(taken_for.profile_id, False, staff_attendance_type)
            else:
                attendance_status.append({
                    "status": "absent",
                    "profile_id": str(taken_for.profile_id)
                })
                set_attendance_under_process(taken_for.profile_id, False)

            message = f"{role} attendance completed"
            is_completed = True

        else:
            set_attendance_under_process(taken_for.profile_id, False)

        log.info("--- TAKING ATTENDANCE COMPLETED ---")


        log.info("---- CREATE STAFF ATTENDANCE ROW ----")
        staff_attendance_get_qs = StaffAttendanceModel.objects.create(
            attendance_type=staff_attendance_type,
            attempt_status=attempt_status,
            loggedin_user=logged_in_user,
            taken_by=taken_by,
            taken_for=taken_for,
            media_id=media_id,
            attendance_status=attendance_status,
            attendance_date=attendance_date,
            staff_attendance_geolocation=staff_attendance_geolocation,
        )
        log.info("---- CREATE STAFF ATTENDANCE ROW COMPLETED----")



    # if self/other --- staff,non-staff attendance
    if staff_attendance_get_qs:
        return is_completed, message, staff_attendance_get_qs
    else:  # else class attendance
        return is_completed, message, attendance_get_qs


"""
    ---------------------------------------------------------------
        TRAINING HANDLER METHOD
    ---------------------------------------------------------------
"""


def train_face_recognition_model(wait_folder, pickle_folder):
    is_completed = False
    profile_id = ""
    training_folder = os.path.join(LUEIN_SM_ATT_PROJ_PATH, wait_folder)
    if os.path.exists(training_folder):
        if "\\" in training_folder:
            profile_id = training_folder.split("\\")[-1]
        else:
            profile_id = training_folder.split("/")[-1]
        try:
            log.info("WAITING_FOLDER: %s", training_folder)
            response_data = train_recognition_model(training_folder, pickle_folder)
        except:
            traceback.print_exc()
            log.exception("Exception Occurred in Training API!!!")
        else:
            print("---- response_data--message: ", response_data['message'])
            if response_data['status'] == "success":
                log.info("TRAINING COMPLETED")
                is_completed = True
                print("----- profile_id : ", profile_id)
                profile_obj = Profile.objects.get(pk=profile_id)
                print("----- profile_obj : ", profile_obj)
                profile_obj.is_registered = is_completed
                profile_obj.save()
            else:
                log.error("TRAINING FAILED....CHECK LOGS FOR MORE DETAILS")
    else:
        log.error("Either Waiting Folder or Training folder doesn't exists yet!!!")
    return is_completed, profile_id, response_data


"""
    ---------------------------------------------------------------
        Send notifications method
    ---------------------------------------------------------------
"""


def create_push_notification_body(request_type, registration_type, verb, recipient, action_object, profile,
                                  profile_role, sender, date_str, taken_for, taken_by, class_name, section, period):
    body = ""
    if request_type == "train":
        if registration_type == "new_user":
            if verb == "new_user_registration_completed":
                body = f"Hi {recipient.first_name + ' ' + recipient.last_name}.\n" \
                       "Registration Successful \n"
                if profile_role == "student":
                    body += f"Name: {action_object.first_name + ' ' + action_object.last_name} \nClass: {profile.class_name} --- Section: {profile.section.capitalize()} \nTaken By: {sender.first_name + ' ' + sender.last_name}"
                else:
                    body += f"Name: {action_object.first_name + ' ' + action_object.last_name} \nTaken By: {sender.first_name + ' ' + sender.last_name}"
            elif verb == "new_user_registration_failed":
                body = f"Hi {recipient.first_name + ' ' + recipient.last_name}. \n " \
                       f"New User Registration Failed!!! \n"
                if profile_role == "student":
                    body += f"Name: {action_object.first_name + ' ' + action_object.last_name} " \
                            f"\nClass: {profile.class_name} --- Section: {profile.section.capitalize()} " \
                            f"\nTaken By: {sender.first_name + ' ' + sender.last_name}"
                else:
                    body += f"Name: {action_object.first_name + ' ' + action_object.last_name} " \
                            f"\nTaken By: {sender.first_name + ' ' + sender.last_name}"
        elif registration_type == "face":
            if verb == "face_registration_completed":
                body = f"Hi {recipient.first_name + ' ' + recipient.last_name}. \n"
                if profile_role == "student":
                    body += f"Name: {action_object.first_name + ' ' + action_object.last_name} \nClass: {profile.class_name} \nSection: {profile.section.capitalize()} \nTaken By: {sender.first_name + ' ' + sender.last_name}"
                else:
                    body += f"Name: {action_object.first_name + ' ' + action_object.last_name} \nTaken By: {sender.first_name + ' ' + sender.last_name}"
            elif verb == "face_registration_failed":
                body = f"Hi {recipient.first_name + ' ' + recipient.last_name} \n"
                if profile_role == "student":
                    body += f"Name: {action_object.first_name + ' ' + action_object.last_name} " \
                            f"\nClass: {profile.class_name} \nSection: {profile.section.capitalize()} " \
                            f"\nTaken By: {sender.first_name + ' ' + sender.last_name}"
                else:
                    body += f"Name: {action_object.first_name + ' ' + action_object.last_name} " \
                            f"\nTaken By: {sender.first_name + ' ' + sender.last_name}"
    elif request_type == "attendance":
        # if attendance type == self/other
        if taken_for:
            body = f"Date: {date_str}" \
                   f"\nTaken By: {taken_by.user.first_name + ' ' + taken_by.user.last_name}" \
                   f"\nTaken For: {taken_for.user.first_name + ' ' + taken_for.user.last_name}"
        else:
            body = f"Date: {date_str}" \
                   f"\nTaken By: {taken_by.user.first_name + ' ' + taken_by.user.last_name}" \
                   f"\nTaken for class : \nClass: {class_name} \nSection: {section.capitalize()} \nPeriod: {period}"
    return body


def send_notifications(request_type, all_recipient, sender, verb, description, notification_type, target, action_object,
                       class_name, section, media_type, recipient_role, school_id, registration_type, attendance_type,
                       period=None,
                       date=None, date_str=None,
                       profile_role=None,
                       taken_by_user_uuid=None, taken_for_user_uuid=None):
    print("-- request_type: ", request_type)
    print("-- all_recipient: ", all_recipient)
    print("-- classs: ", class_name)
    print("-- section : ", section)
    print("-- verb: ", verb)
    print("-- profile_role: ", profile_role)
    print("-- action_object: ", action_object)
    print("-- taken_by_user_uuid: ", taken_by_user_uuid)
    print("-- taken_for_user_uuid: ", taken_for_user_uuid)
    print("-- attendance_date: ", date)
    print("-- registration_type: ", registration_type)

    taken_by = Profile.objects.none()
    taken_for = Profile.objects.none()
    profile = Profile.objects.none()
    if taken_by_user_uuid:
        taken_by = Profile.objects.get(pk=taken_by_user_uuid)
    else:
        log.warning("No taken_by_user_uuid passed")

    if taken_for_user_uuid:
        taken_for = Profile.objects.get(pk=taken_for_user_uuid)
    else:
        log.warning("No taken_for_user_uuid passed")

    try:
        for assignee in all_recipient:
            title = ""
            recipient = assignee.user
            if request_type == "train":
                profile = Profile.objects.get(user=action_object)
                print("-- profile: ", profile)
                title = verb

                notify.send(
                    sender,
                    recipient=recipient,
                    verb=verb,
                    description=description,
                    notification_type=notification_type,
                    target=target,
                    action_object=action_object,
                    class_name=profile.class_name,
                    section=profile.section,
                    media_type=media_type)
            elif request_type == "attendance":
                notify.send(
                    sender,
                    recipient=recipient,
                    verb=verb,
                    description=description,
                    notification_type=notification_type,
                    target=target,
                    action_object=action_object,
                    class_name=class_name,
                    section=section,
                    period=period,
                    date=date,
                    media_type=media_type,
                    attendance_type=attendance_type)

                if verb == "attendance_completed_v1":
                    if attendance_type == "class":
                        title = "class_attendance_taken"
                    if attendance_type == "cctv":
                        title = "cctv_attendance_taken"
                    if attendance_type == "self":
                        title = "self_attendance_taken"
                    if attendance_type == "other":
                        title = "other_attendance_taken"
                elif verb == "attendance_failed_v1":
                    if attendance_type == "class":
                        title = "class_attendance_failed"
                    if attendance_type == "cctv":
                        title = "cctv_attendance_failed"
                    if attendance_type == "self":
                        title = "self_attendance_failed"
                    if attendance_type == "other":
                        title = "other_attendance_failed"

            body = create_push_notification_body(
                request_type=request_type,
                registration_type=registration_type,
                verb=title,
                recipient=recipient,
                action_object=action_object,
                profile=profile,
                profile_role=profile_role,
                sender=sender,
                date_str=date_str,
                taken_for=taken_for,
                taken_by=taken_by,
                class_name=class_name,
                section=section,
                period=period)
            send_device_notification(verb=title, body=body, recipient_roles=recipient_role, school_id=school_id,
                                     recipient=recipient)
    except:
        log.exception("Exception Occurred During send notification")

    # body = ""
    # if request_type == "train":
    #     if "completed" in verb:
    #         body = "New User Got Successfully Registered!!!"
    #     elif "failed" in verb:
    #         body = "Registration Failed!!!"
    #
    #     body += f"Name: {action_object.first_name + ' ' + action_object.last_name} \n \t Class: {class_name} \n \t Section: {section.capitalize()} \n\t Taken By: {sender.first_name + ' ' + sender.last_name}"
    # elif request_type == "attendance":
    #     if "completed" in verb:
    #         body = "Attendance Taken for "
    #     elif "failed" in verb:
    #         body = "Attendance Failed for "
    #     body += f"{date} \n \t Class: {class_name} \n \t Section: {section.capitalize()} \n\t Period: {period} \n\t Taken By: {sender.first_name + sender.last_name}"
    # send_device_notification(verb=verb, body=body, recipient_roles=recipient_role, school_id=school_id, recipient=recipient)


"""
    Group notification utility
"""


def send_notification_to_group(group_names, school_id, payload, ttl=0):
    # Get all the subscription related to the group
    payload = json.dumps(payload)

    users = Profile.objects.filter(role__in=group_names, school_id=school_id)
    print("users: ", users)

    for usr in users:
        push_infos = usr.user.webpush_info.select_related("subscription")
        print("push_infos: ", push_infos)
        for push_info in push_infos:
            _send_notification(push_info.subscription, payload, ttl)


def send_device_notification(verb, body, recipient_roles, school_id, recipient):
    from webpush import send_user_notification

    log.info("Sending device notification")
    try:
        body = body
        head = " ".join(wrd.capitalize() for wrd in verb.split('_'))

        payload = {'head': head, 'body': body}
        print(payload)
        # # Single user
        send_user_notification(user=recipient, payload=payload, ttl=1000)
        # # Group notification
        # send_notification_to_group(recipient_roles, school_id, payload, ttl=1000)
    except:
        log.exception("Exception occurred during push notification")
    else:
        log.info("Sending device notification completed")


"""
    ---------------------------------------------------------------
        REDIS WRAPPER METHOD
        - HANDLE ALL REQUESTS IN POOL
    ---------------------------------------------------------------
"""


def wrapper_function(request_dict):
    """
        ---- CHECK IF RESOURCES AND SUB DIRECTORIES EXISTS ---
    """
    log.info("--- MODEL/REPRESENTATION VALIDATION CHECK IN PROGRESS ---")
    settings.FACE_RECOG_UTILS.check_resources_exists(root_folder=LUEIN_SM_ATT_PROJ_PATH)
    log.info("--- MODEL/REPRESENTATION VALIDATION CHECK COMPLETED ---")

    request_id = request_dict[0]
    request_data = request_dict[1]
    encoding = 'utf-8'

    request_id_decoded = request_id.decode(encoding)
    request_type = request_data[b'request_type']
    request_type = request_type.decode(encoding)

    sender_uuid = request_data[b'sender_uuid']
    sender_uuid = sender_uuid.decode(encoding)
    notification_type = request_data[b'notification_type']
    notification_type = notification_type.decode(encoding)
    description = request_data[b'description']
    description = description.decode(encoding)
    media_type = request_data[b'media_type']
    media_type = media_type.decode(encoding)
    school_id = request_data[b'school_id']
    school_id = school_id.decode(encoding)
    recipient_role = request_data[b'recipient_role']
    recipient_role = recipient_role.decode(encoding)
    recipient_role = json.loads(recipient_role)

    period_number = ""
    action_object = None
    attendance_date = None
    attendance_date_str = None
    section = ""
    class_name = ""
    day = ""
    profile_role = ""
    taken_by_user_uuid = ""
    taken_for_user_uuid = ""
    staff_attendance_geolocation = None
    registration_type = ""
    attendance_type = ""
    camera_response_id = ""
    attendance_choice = ""

    pickle_folder = settings.FACE_RECOG_UTILS.create_pickle_folder(school_id=str(school_id),
                                                                   root_folder=LUEIN_SM_ATT_PROJ_PATH)
    print("---pickle_folder---: ", pickle_folder)

    sender_profile_qs = Profile.objects.get(profile_id=sender_uuid)
    sender = sender_profile_qs.user
    target = SchoolModel.objects.get(school_id=school_id)
    all_recipient_profile = Profile.objects.none()

    try:
        log.info("Started JOB: %s", request_id_decoded)
        verb = ""
        if request_type == "train":
            log.info("Registration inprogress JOB-ID: %s", request_id_decoded)

            action_object_uuid = request_data[b'action_object_uuid']
            action_object_uuid = action_object_uuid.decode(encoding)
            action_object = Profile.objects.get(profile_id=action_object_uuid).user
            wait_folder = request_data[b'wait_folder']
            wait_folder = wait_folder.decode(encoding)

            registration_type = request_data[b'registration_type']
            registration_type = registration_type.decode(encoding)

            is_completed, profile_id, response_data = train_face_recognition_model(wait_folder, pickle_folder)
            if is_completed:
                if registration_type == "new_user":
                    verb = "new_user_registration_completed"
                else:
                    verb = "face_registration_completed"
                delete_key_from_stream(request_id)
                log.info("Finished JOB: %s", request_id_decoded)
            else:
                if registration_type == "new_user":
                    verb = "new_user_registration_failed"
                else:
                    verb = "face_registration_failed"
                log.warning("Execution Failed -- JOB: %s", request_id_decoded)
            all_recipient_profile = Profile.objects.filter(school_id=school_id,
                                                           profile_id__in=[profile_id, sender_uuid])
            profile_role = Profile.objects.get(pk=profile_id).role
        elif request_type == "attendance":
            log.info("Attendance inprogress -- JOB-ID: %s", request_id_decoded)

            attendance_type = request_data[b'attendance_type']
            attendance_type = attendance_type.decode(encoding)

            staff_attendance_type = request_data[b'staff_attendance_type']
            staff_attendance_type = staff_attendance_type.decode(encoding)

            attendance_date = request_data[b'attendance_date']
            attendance_date_str = attendance_date.decode(encoding)
            attendance_date = parse_datetime(attendance_date_str)

            file_path = request_data[b'file_path']
            file_path = file_path.decode(encoding)

            media_id = request_data[b'media_id']
            media_id = media_id.decode(encoding)

            role = request_data[b'role']
            role = role.decode(encoding)

            logged_in_user_uuid = sender_uuid

            if attendance_type == 'class':
                period_number = request_data[b'period']
                period_number = period_number.decode(encoding)

                class_name = request_data[b'class_name']
                class_name = class_name.decode(encoding)

                section = request_data[b'section']
                section = section.decode(encoding)

                day = request_data[b'day']
                day = day.decode(encoding)

                taken_by_user_uuid = request_data[b'taken_by_user_uuid']
                taken_by_user_uuid = taken_by_user_uuid.decode(encoding)

                attendance_choice = request_data[b'attendance_choice']
                attendance_choice = attendance_choice.decode(encoding)

                all_recipient_profile = Profile.objects.filter(school_id=school_id,
                                                               profile_id__in=[logged_in_user_uuid])
            elif attendance_type == "cctv":
                path = request_data[b'path']
                path = path.decode(encoding)

                url = request_data[b'url']
                url = url.decode(encoding)

                taken_by_user_uuid = request_data[b'taken_by_user_uuid']
                taken_by_user_uuid = taken_by_user_uuid.decode(encoding)

                extra_data = {
                    "request_type": "attendance",
                    "attendance_type": attendance_type,
                    "sender_uuid": sender_uuid,
                    'taken_by_user_uuid': taken_by_user_uuid,
                    "notification_type": notification_type,
                    "description": description,
                    "media_type": media_type,
                    "recipient_role": json.dumps(recipient_role),
                    "attendance_date": attendance_date_str,
                    "role": role
                }

                file_key = "media_file"
                path_plus_file_name_plus_extension = get_stream_save_frame_or_video(path=path, url=url, user=None,
                                                                                    password=None)

                camera_response_id = request_data[b'camera_response_id']
                camera_response_id = camera_response_id.decode(encoding)

                if path_plus_file_name_plus_extension:
                    media_path = path_plus_file_name_plus_extension.split(settings.BROWSE_FOLDER_ROOT)[1]

                    print('---path_plus_file_name_plus_extension---', path_plus_file_name_plus_extension)
                    filename = "file_name"
                    # override file name with prefixes
                    filename = media_type + "__" + filename

                    obj = MediaCapturedModel.objects.create(
                        media_type=media_type,
                        extra_data=extra_data
                    )

                    obj.media_file = media_path
                    obj.save()

                    file_path = path_plus_file_name_plus_extension
                    media_id = obj.media_id
                else:
                    logged_in_user = Profile.objects.get(profile_id=logged_in_user_uuid)
                    taken_by = Profile.objects.get(profile_id=taken_by_user_uuid)

                    attendance_get_qs = update_camera_response_row(camera_response_id=camera_response_id,
                                                                   media_id=None,
                                                                   cctv_attempt_status="failure",
                                                                   cctv_attendance_status=[],
                                                                   attendance_date=attendance_date)

                    action_object = attendance_get_qs
            elif attendance_type == "self":
                taken_by_user_uuid = logged_in_user_uuid
                taken_for_user_uuid = logged_in_user_uuid
                all_recipient_profile = Profile.objects.filter(school_id=school_id,
                                                               profile_id__in=[taken_for_user_uuid])

                staff_attendance_geolocation = request_data[b'staff_attendance_geolocation']
                staff_attendance_geolocation = staff_attendance_geolocation.decode(encoding)
                staff_attendance_geolocation = json.loads(staff_attendance_geolocation)
            elif attendance_type == "other":
                taken_by_user_uuid = request_data[b'taken_by_user_uuid']
                taken_by_user_uuid = taken_by_user_uuid.decode(encoding)
                taken_for_user_uuid = request_data[b'taken_for_user_uuid']
                taken_for_user_uuid = taken_for_user_uuid.decode(encoding)
                all_recipient_profile = Profile.objects.filter(school_id=school_id,
                                                               profile_id__in=[logged_in_user_uuid,
                                                                               taken_for_user_uuid])

                # todo uncomment when other geolocation enable
                # staff_attendance_geolocation = request_data[b'staff_attendance_geolocation']
                # staff_attendance_geolocation = staff_attendance_geolocation.decode(encoding)
                # staff_attendance_geolocation = json.loads(staff_attendance_geolocation)

            if media_id:
                is_completed, message, action_object = recognise_face(logged_in_user_uuid=logged_in_user_uuid,
                                                                      taken_by_user_uuid=taken_by_user_uuid,
                                                                      taken_for_user_uuid=taken_for_user_uuid,
                                                                      period_number=period_number,
                                                                      section=section,
                                                                      class_name=class_name,
                                                                      day=day,
                                                                      school_id=school_id,
                                                                      attendance_date=attendance_date,
                                                                      pickle_folder=pickle_folder,
                                                                      file_path=file_path,
                                                                      media_id=media_id,
                                                                      role=role,
                                                                      staff_attendance_type=staff_attendance_type,
                                                                      attendance_type=attendance_type,
                                                                      staff_attendance_geolocation=staff_attendance_geolocation,
                                                                      camera_response_id=camera_response_id,
                                                                      attendance_choice=attendance_choice
                                                                      )
            else:
                is_completed = False
                message = "Camera is not working, no stream found"
                # action_object = ""

            log.info(" MESSAGE: %s", message)
            if is_completed:
                verb = "attendance_completed_v1"
                delete_key_from_stream(request_id)
                log.info("Finished JOB: %s", request_id_decoded)
            else:
                verb = 'attendance_failed_v1'
                log.warning("Execution Failed -- JOB: %s", request_id_decoded)

        school_admin_qs = Profile.objects.none()
        if target.organisation_type == "school":
            school_admin_qs = Profile.objects.filter(school_id=school_id, role="school_admin")
        elif target.organisation_type == "company":
            school_admin_qs = Profile.objects.filter(school_id=school_id, role="admin")

        all_recipient = all_recipient_profile | school_admin_qs
        all_recipient = all_recipient.distinct()

        log.info("SENDING NOTIFICATIONS")
        send_notifications(request_type=request_type,
                           all_recipient=all_recipient,
                           sender=sender,
                           verb=verb,
                           description=description,
                           notification_type=notification_type,
                           target=target,
                           action_object=action_object,
                           class_name=class_name,
                           section=section,
                           media_type=media_type,
                           recipient_role=recipient_role,
                           school_id=school_id,
                           period=period_number,
                           date=attendance_date,
                           date_str=attendance_date_str,
                           profile_role=profile_role,
                           taken_by_user_uuid=taken_by_user_uuid,
                           taken_for_user_uuid=taken_for_user_uuid,
                           registration_type=registration_type,
                           attendance_type=attendance_type)
        log.info("SENDING NOTIFICATIONS COMPLETED")
    except:
        log.exception("Exception occurred during redis call")


def main():
    stream_keys = ['face_recog']
    cg_current = db.consumer_group('face_recog-cg', stream_keys)
    jobs = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
        while True:
            result = cg_current.read(block=0, count=1)
            # print("result: ", result)
            job = executor.map(wrapper_function, [result[0][1][0]])
            jobs[job] = result
            keys = [k for k, v in jobs.items()]
            for x in keys:
                del jobs[x]


if __name__ == "__main__":
    main()
