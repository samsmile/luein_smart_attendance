import json
from datetime import datetime
import logging as log_print
from pathlib import Path
from subprocess import Popen, PIPE

import boto3
from botocore.exceptions import ClientError
import requests
import os
import sys

from tqdm import tqdm

LUEIN_SM_ATT_PROJ_PATH = os.getenv("LUEIN_SM_ATT_PROJ_PATH")
print("LUEIN_SM_ATT_PROJ_PATH: ", LUEIN_SM_ATT_PROJ_PATH)
sys.path.append(LUEIN_SM_ATT_PROJ_PATH)

LOGS_DIR = LUEIN_SM_ATT_PROJ_PATH + "/logs"

if os.path.exists(LOGS_DIR) and os.path.isdir(LOGS_DIR):
    print("LOGS_DIR: ", LOGS_DIR)
else:
    os.mkdir(LOGS_DIR)

if os.path.exists("db_backup") and os.path.isdir("db_backup"):
    print("db_backup: ", os.path.join(os.getcwd(),"db_backup"))
else:
    os.mkdir("db_backup")

def setup_custom_logger(name):
    formatter = log_print.Formatter(fmt='%(asctime)s - %(process)d - %(levelname)s - %(message)s')
    # handler = log_print.StreamHandler()
    fh = log_print.FileHandler(LOGS_DIR + '/s3_backup.log')
    # handler.setFormatter(formatter)
    fh.setFormatter(formatter)
    logger = log_print.getLogger(name)
    logger.setLevel(log_print.INFO)
    # logger.addHandler(handler)
    logger.addHandler(fh)
    return logger


log = setup_custom_logger("s3_backup")


def create_presigned_post(s3_client, bucket_name, object_name, fields=None, conditions=None, expiration=3600):
    """Generate a presigned URL S3 POST request to upload a file

    :param bucket_name: string
    :param object_name: string
    :param fields: Dictionary of prefilled form fields
    :param conditions: List of conditions to include in the policy
    :param expiration: Time in seconds for the presigned URL to remain valid
    :return: Dictionary with the following keys:
        url: URL to post to
        fields: Dictionary of form fields and values to submit with the POST
    :return: None if error.
    """
    try:
        response = s3_client.generate_presigned_post(bucket_name,
                                                     object_name,
                                                     Fields=fields,
                                                     Conditions=conditions,
                                                     ExpiresIn=expiration)
    except ClientError as e:
        log.error(e)
        return None

    return response


def upload_file(s3_client, source_file, destination_url, BUCKET_NAME):
    response = create_presigned_post(s3_client, BUCKET_NAME, destination_url)
    # print(response)
    if response is None:
        return None

    with open(source_file, 'rb') as f:
        files = {'file': (source_file, f)}
        http_response = requests.post(response['url'].replace('https', 'http'), data=response['fields'], files=files)
    # If successful, returns HTTP status code 204
    s3_file_url = ""
    if http_response.status_code in [200, 201, 204]:
        log.info("File Uploaded successfully!!! Returned Status: %s", http_response.status_code)
        # print(response['url'] + response['fields']['key'])
        s3_file_url = response['url'] + response['fields']['key']
        is_upload_successful = True
    else:
        log.error("Upload Failed !!!! Status: %s", http_response.status_code)
        is_upload_successful = False

    return s3_file_url, is_upload_successful


def upload_representations_files(s3_client, bucket, timestamp):
    db_path = ""
    if os.path.exists(os.path.join(LUEIN_SM_ATT_PROJ_PATH, "resources")):
        if os.path.exists(os.path.join(LUEIN_SM_ATT_PROJ_PATH, "resources", "attendance_prod_models")):
            if os.path.exists(
                    os.path.join(LUEIN_SM_ATT_PROJ_PATH, "resources", "attendance_prod_models", "face_recog_db")):
                db_path = os.path.join(LUEIN_SM_ATT_PROJ_PATH, "resources", "attendance_prod_models", "face_recog_db")
            else:
                log.error("'face_recog_db' doesn't exists!!!")
        else:
            log.error("'attendance_prod_models' doesn't exists!!!")
    else:
        log.error("'resources' doesn't exists!!!")

    if db_path:
        pbar = tqdm(os.listdir(db_path))
        for school_folder in pbar:
            for file in os.listdir(os.path.join(db_path, school_folder)):
                backup_file = f"{timestamp}_{file}"
                source_file = os.path.join(db_path, school_folder, file)
                destination_url = os.path.join(db_path, school_folder, backup_file)
                if "\\" in destination_url:
                    destination_url = "/".join(destination_url.split("\\")[-2:])
                else:
                    destination_url = "/".join(destination_url.split("/")[-2:])
                destination_url = "representations/" + destination_url
                response = upload_file(s3_client=s3_client, source_file=source_file, destination_url=destination_url,
                                       BUCKET_NAME=bucket)
                # print("response: ", response[0])
                pbar.set_description(f'Uploaded {source_file} to {destination_url}')
                # update_s3_upload_json(school_id=school_folder, url=response[0])


def upload_config_folder_to_s3(s3_client, bucket, timestamp):
    input_dir = os.path.join(LUEIN_SM_ATT_PROJ_PATH, "resources", "api_face_recog")
    s3_path = "api_face_recog"
    pbar = tqdm(os.walk(input_dir))
    for path, subdirs, files in pbar:
        for file in files:
            dest_path = path.replace(input_dir, "").replace(os.sep, '/')
            s3_file = f'{s3_path}/{dest_path}/{file}'.replace('//', '/')
            local_file = os.path.join(path, file)
            # s3_client.upload_file(local_file, s3bucket, s3_file)
            response = upload_file(s3_client=s3_client, source_file=local_file, destination_url=s3_file,
                                   BUCKET_NAME=bucket)
            pbar.set_description(f'Uploaded {local_file} to {s3_file}')


"""
    ---- DB s3 upload -----
"""


def upload_backup_file_to_s3(s3_client, bucket, backup_file):
    s3_file = backup_file.replace(os.sep, '/')
    local_file = backup_file
    response = upload_file(s3_client=s3_client, source_file=local_file, destination_url=s3_file,
                           BUCKET_NAME=bucket)
    log.info(f'Uploaded {local_file} to {s3_file}')


def dump_table(db_config, s3_client, bucket):
    user_name = db_config['user_name']
    host_name = db_config['host_name']
    database_name = db_config['database_name']
    database_password = db_config['database_password']
    port = db_config['port']
    db_backup_file = db_config['db_backup_file']

    # command = ""
    # if platform.system() == "Windows":
    #     command = "pg_dump -U {0} -p {1} -d {2} -W -f {3}"\
    #     .format(user_name,port,database_name,db_backup_file)
    # else:
    #     command = 'pg_dump -U {0} {1} -h {2} -p {3} -c -Ft -f {4}'\
    #     .format(user_name,database_name,host_name,port,db_backup_file)

    # # log.info("COMMAND: %s", command)

    command = 'pg_dump --dbname=postgresql://{}:{}@{}:{}/{} > {}'.format(user_name, database_password, host_name, port,
                                                                         database_name, db_backup_file)
    log.info("COMMAND: %s", command)
    try:
        proc = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        proc.wait()
    except:
        log.exception('Exception happened during backup')
    else:
        try:
            upload_backup_file_to_s3(s3_client=s3_client, bucket=bucket, backup_file=db_backup_file)
        except:
            log.exception("Exception occurred during s3 upload")
        else:
            log.info('Backup successful')


"""
    ---- DB s3 upload end-----
"""


def cron_job():
    TIMESTAMP = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
    print("TIMESTAMP: ", TIMESTAMP)

    # Generate a presigned S3 POST URL
    session = boto3.Session(profile_name="dev")
    s3_client = session.client('s3')

    # todo : uncomment line 131 and comment 132 when deploying for production
    # BUCKET_NAME = "lueinsmartattendance"
    BUCKET_NAME = "testlueinsmartattendance"

    db_config = {
        "host_name": "localhost",
        "port": 5432,
        "user_name": "luein_user",
        "database_password": "Luein2018",
        "database_name": "luein_smart_attendance_ui_db_dev",
        "db_backup_file": f"db_backup/luein_smart_attendance_ui_db_dev_{TIMESTAMP}.tar",
    }

    # upload_config_folder_to_s3(s3_client=s3_client, bucket=BUCKET_NAME, timestamp=TIMESTAMP)
    # upload_representations_files(s3_client=s3_client, bucket=BUCKET_NAME, timestamp=TIMESTAMP)

    dump_table(db_config=db_config, s3_client=s3_client, bucket=BUCKET_NAME)


if __name__ == "__main__":
    cron_job()
