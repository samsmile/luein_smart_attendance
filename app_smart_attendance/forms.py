from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms import NumberInput
from django.utils.translation import ugettext

from app_root.models import PeriodModel, Profile, CLASS_NAME_CHOICE, AnnouncementModel, ROLE_CHOICE, AttendanceModel, \
    StaffAttendanceModel, CameraModel, CameraResponseModel


class DateInput(forms.DateInput):
    input_type = 'date'


class AnnouncementCreateForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = AnnouncementModel
        fields = (
            'title',
            'content',
            # 'extra',
            'is_active',
            'expire_date',
        )
        widgets = {
            'expire_date': DateInput(),
            'content': forms.Textarea(),
        }

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(AnnouncementCreateForm, self).__init__(*args, **kwargs)
        profile = user.profile

        self.fields['title'].required = True
        self.fields['content'].required = True
        self.fields['expire_date'].required = True
        self.fields['is_active'].label = "Is active (Visibility)"


class AnnouncementUpdateForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = AnnouncementModel
        fields = (
            'title',
            'content',
            # 'extra',
            'is_active',
            'expire_date',
        )
        widgets = {
            'expire_date': DateInput(),
            'content': forms.Textarea(),
        }

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(AnnouncementUpdateForm, self).__init__(*args, **kwargs)
        profile = user.profile

        self.fields['title'].required = True
        self.fields['content'].required = True
        self.fields['expire_date'].required = True
        self.fields['is_active'].label = "Is active (Visibility)"

    def clean(self):
        cleaned_data = super(AnnouncementUpdateForm, self).clean()
        # return self.cleaned_data
        return cleaned_data


class PeriodCreateForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = PeriodModel
        fields = (
            'day',
            'class_name',
            'section',
            'period_number',
            'subject',
            'teacher_id',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(PeriodCreateForm, self).__init__(*args, **kwargs)
        profile = user.profile
        school_id = profile.school_id

        self.fields['day'].required = True
        self.fields['class_name'].required = True
        self.fields['section'].required = True
        self.fields['period_number'].required = True
        self.fields['subject'].required = True
        self.fields['teacher_id'].required = True

        teacher_filter_qs = Profile.objects.filter(school_id=school_id, role="teacher")
        self.fields['teacher_id'] = forms.ModelChoiceField(queryset=teacher_filter_qs)


class PeriodUpdateForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = PeriodModel
        fields = (
            'day',
            'class_name',
            'section',
            'period_number',
            'subject',
            'teacher_id',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(PeriodUpdateForm, self).__init__(*args, **kwargs)
        profile = user.profile
        school_id = profile.school_id
        self.fields['teacher_id'] = forms.ModelChoiceField(
            queryset=Profile.objects.filter(school_id=school_id, role="teacher"))
        self.fields['day'].required = True
        self.fields['class_name'].required = True
        self.fields['section'].required = True
        self.fields['period_number'].required = True
        self.fields['subject'].required = True
        self.fields['teacher_id'].required = True

    def clean(self):
        cleaned_data = super(PeriodUpdateForm, self).clean()
        # return self.cleaned_data
        return cleaned_data


class CameraCreateForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = CameraModel
        fields = (
            'title',
            'url',
            'ip_address',
            'port',

            'user',
            'password',
            'type',
            'mac_address',
            'resolution',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(CameraCreateForm, self).__init__(*args, **kwargs)
        profile = user.profile
        school_id = profile.school_id

        self.fields['title'].required = True
        self.fields['url'].required = True
        self.fields['ip_address'].required = True
        self.fields['port'].required = True


class CameraUpdateForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = CameraModel
        fields = (
            'title',
            'url',
            'ip_address',
            'port',

            'user',
            'password',
            'type',
            'mac_address',
            'resolution',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(CameraUpdateForm, self).__init__(*args, **kwargs)
        profile = user.profile
        school_id = profile.school_id

        self.fields['title'].required = True
        self.fields['url'].required = True
        self.fields['ip_address'].required = True
        self.fields['port'].required = True

    def clean(self):
        cleaned_data = super(CameraUpdateForm, self).clean()
        # return self.cleaned_data
        return cleaned_data


#  RegisterFace / add Face
class AddRegisterFaceUserForm(UserCreationForm):
    required_css_class = 'required'
    email = forms.EmailField(max_length=254, required=False)
    first_name = forms.CharField(required=False)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(AddRegisterFaceUserForm, self).__init__(*args, **kwargs)

        for field_name in ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

        self.fields['username'].widget.attrs['readonly'] = True
        self.fields['email'].widget.attrs['readonly'] = True
        self.fields['first_name'].widget.attrs['readonly'] = True
        self.fields['last_name'].widget.attrs['readonly'] = True

        self.fields['username'].required = False

    def clean_username(self):
        user_data = self.cleaned_data['username']
        if User.objects.filter(username=user_data).exists():
            raise ValidationError("A user with this username already exists.")
        if User.objects.filter(email=user_data).exists():
            raise ValidationError("A user with this username already exists.")
        return user_data

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise ValidationError("A user with this email already exists.")
        return email


#  RegisterFace / add Face
class AddRegisterFaceProfileForm(forms.ModelForm):
    contact_number = forms.CharField(required=False, label='Contact Number (With Country Code)', widget=forms.TextInput(
        attrs={"autocomplete": "off"}))
    person = forms.ChoiceField(label='Person', required=True, )
    required_css_class = 'required'

    class Meta:
        model = Profile
        fields = (
            'gender',
            'blood_group',
            'role',
            'class_name',
            'section',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(AddRegisterFaceProfileForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)
        school_id = profile.school_id

        if profile.school_id.organisation_type == "school":
            if profile.role == "school_admin":
                FORM_ROLE_CHOICE = (
                    ('school_admin', 'School Admin'),
                    ('teacher', 'Teacher'),
                    ('non_teacher', 'Non Teacher'),
                    ('student', 'Student'),
                )
                self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                        widget=forms.RadioSelect)
            else:
                FORM_ROLE_CHOICE = (
                    ('student', 'Student'),
                )
                self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                        widget=forms.RadioSelect)

            self.fields['class_name'] = forms.ChoiceField(label='Class Name', required=False, )
            self.fields['section'] = forms.ChoiceField(label='Section', required=False, )
            self.fields['class_name'].widget.attrs.update({'class': 'dn'})
            self.fields['section'].widget.attrs.update({'class': 'dn'})
            self.fields['class_name'].widget.attrs['readonly'] = True
            self.fields['section'].widget.attrs['readonly'] = True
            self.fields['class_name'].required = False
            self.fields['section'].required = False
        elif profile.school_id.organisation_type == "company":
            FORM_ROLE_CHOICE = (
                ('admin', 'Admin'),
                ('staff', 'Staff'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                    widget=forms.RadioSelect)
        elif profile.school_id.organisation_type == "anganwadi":
            FORM_ROLE_CHOICE = (
                ('admin', 'Admin'),
                ('staff', 'Staff'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                    widget=forms.RadioSelect)

        self.fields['gender'].widget.attrs['readonly'] = True
        self.fields['blood_group'].widget.attrs['readonly'] = True

    def clean_class_name(self):
        class_name = self.cleaned_data['class_name']
        if not PeriodModel.objects.filter(class_name=class_name).exists():
            raise ValidationError("Given Class name is not available, please correct and try again...!!!")
        return class_name

    def clean_section(self):
        section = self.cleaned_data['section']
        if not PeriodModel.objects.filter(section=section).exists():
            raise ValidationError("Given Section is not available, please correct and try again...!!!")
        return section

    def clean(self):
        role = self.cleaned_data['role']

        if role == 'student':
            class_name = self.cleaned_data['class_name']
            section = self.cleaned_data['section']
            if class_name is None:
                raise ValidationError('Class name is required for "Student" user group')
            if section is None:
                raise ValidationError('Section is required for "Student" user group')


#  RegisterUser / add User
class AddRegisterUserUserForm(UserCreationForm):
    required_css_class = 'required'
    email = forms.EmailField(max_length=254, required=True)
    first_name = forms.CharField(required=True)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2'
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(AddRegisterUserUserForm, self).__init__(*args, **kwargs)

        for field_name in ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

    def clean_username(self):
        user_data = self.cleaned_data['username']
        if User.objects.filter(username=user_data).exists():
            raise ValidationError("A user with this username already exists.")
        if User.objects.filter(email=user_data).exists():
            raise ValidationError("A user with this username already exists.")
        return user_data

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise ValidationError("A user with this email already exists.")
        return email


#  RegisterUser / add User
class AddRegisterUserProfileForm(forms.ModelForm):
    contact_number = forms.CharField(required=False, label='Contact Number (With Country Code)', widget=forms.TextInput(
        attrs={"autocomplete": "off"}))
    required_css_class = 'required'

    face_registration = forms.BooleanField(required=False, label="Register face now")

    class Meta:
        model = Profile
        fields = (
            'father_name',
            'gender',
            'blood_group',
            'role',
            'class_name',
            'section',
            'contact_number',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(AddRegisterUserProfileForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)
        school_id = profile.school_id

        self.fields['face_registration'].initial = True

        # Class name dropdown
        period_filter_qs = PeriodModel.objects.filter(school_id=school_id)
        FORM_CLASS_NAME_CHOICE = sorted(
            set([(item.class_name, item.get_class_name_display()) for item in period_filter_qs]))
        FORM_CLASS_NAME_CHOICE.insert(0, ('', '---Select---'))
        self.fields['class_name'] = forms.ChoiceField(choices=FORM_CLASS_NAME_CHOICE, label='Class Name',
                                                      required=False, )

        # Section dropdown
        current_class_name = profile.class_name
        print('---current_class_name---', current_class_name)
        period_filter_qs.filter(class_name=current_class_name)
        FORM_SECTION_CHOICE = sorted(
            set([(item.section, item.get_section_display()) for item in period_filter_qs]))
        FORM_SECTION_CHOICE.insert(0, ('', '---Select---'))
        self.fields['section'] = forms.ChoiceField(choices=FORM_SECTION_CHOICE, label='Section', required=False, )

        if profile.school_id.organisation_type == "school":
            if profile.role == "school_admin":
                if period_filter_qs:
                    FORM_ROLE_CHOICE = (
                        ('teacher', 'Teacher'),
                        ('non_teacher', 'Non Teacher'),
                        ('student', 'Student'),
                    )
                else:
                    FORM_ROLE_CHOICE = (
                        ('teacher', 'Teacher'),
                        ('non_teacher', 'Non Teacher'),
                    )

                self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                        widget=forms.RadioSelect)
            else:
                FORM_ROLE_CHOICE = (
                    ('student', 'Student'),
                )
                self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                        widget=forms.RadioSelect, initial=1)
            self.fields['class_name'].widget.attrs.update({'class': 'dn'})
            self.fields['section'].widget.attrs.update({'class': 'dn'})
        elif profile.school_id.organisation_type == "company":
            FORM_ROLE_CHOICE = (
                ('staff', 'Staff'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                    widget=forms.RadioSelect)
        elif profile.school_id.organisation_type == "anganwadi":
            FORM_ROLE_CHOICE = (
                ('staff', 'Staff'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                    widget=forms.RadioSelect)

    def clean(self):
        role = self.cleaned_data['role']

        if role == 'student':
            class_name = self.cleaned_data['class_name']
            section = self.cleaned_data['section']
            if class_name is None:
                raise ValidationError('Class name is required for "Student" user group')
            if section is None:
                raise ValidationError('Section is required for "Student" user group')


#  Register school_admin user
class AddSchoolAdminUserForm(UserCreationForm):
    required_css_class = 'required'
    email = forms.EmailField(max_length=254, required=True)
    first_name = forms.CharField(required=True)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2'
        )

    def __init__(self, *args, **kwargs):
        organisation = kwargs.pop("organisation", None)
        super(AddSchoolAdminUserForm, self).__init__(*args, **kwargs)

        for field_name in ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

    def clean_username(self):
        user_data = self.cleaned_data['username']
        if User.objects.filter(username=user_data).exists():
            raise ValidationError("A user with this username already exists.")
        if User.objects.filter(email=user_data).exists():
            raise ValidationError("A user with this username already exists.")
        return user_data

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise ValidationError("A user with this email already exists.")
        return email


#  Register school_admin profile
class AddSchoolAdminProfileForm(forms.ModelForm):
    contact_number = forms.CharField(required=False, label='Contact Number (With Country Code)', widget=forms.TextInput(
        attrs={"autocomplete": "off"}))
    required_css_class = 'required'

    class Meta:
        model = Profile
        fields = (
            'gender',
            'blood_group',
            'role',
        )

    def __init__(self, *args, **kwargs):
        organisation = kwargs.pop("organisation", None)
        super(AddSchoolAdminProfileForm, self).__init__(*args, **kwargs)
        print('organisation--', organisation)

        if organisation.organisation_type == "school":
            FORM_ROLE_CHOICE = (
                ('school_admin', 'school_admin'),
            )
        elif organisation.organisation_type == "company" or organisation.organisation_type == "anganwadi":
            FORM_ROLE_CHOICE = (
                ('admin', 'admin'),
            )

        self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                widget=forms.RadioSelect)

    def clean(self):
        role = self.cleaned_data['role']
        if role == 'student':
            raise ValidationError('Class name is required for "Student" user group')


class TakeAttendanceForm(forms.ModelForm):
    # type = forms.ChoiceField(label='Type', required=True, )
    person = forms.ChoiceField(label='Person', required=True, )

    required_css_class = 'required'

    class Meta:
        model = AttendanceModel
        fields = (
            'loggedin_user',
            'period_id',
            'attendance_date',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(TakeAttendanceForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)
        school_id = profile.school_id

        # ATTENDANCE_FOR_CHOICE = (
        #     ("class_attendance", "Class attendance"),
        #     ("self", "Self"),
        #     ("others", "Others"),
        # )
        # self.fields['type'] = forms.ChoiceField(choices=ATTENDANCE_FOR_CHOICE, label='Attendance for', required=True,
        #                                         widget=forms.RadioSelect)

        if profile.role == "school_admin":
            FORM_ROLE_CHOICE = (
                ('school_admin', 'School Admin'),
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance taker', required=True,
                                                    widget=forms.RadioSelect)
        else:
            FORM_ROLE_CHOICE = (
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance taker', required=True,
                                                    widget=forms.RadioSelect, initial=1)

    #     self.fields['class_name'] = forms.ChoiceField(label='Class Name', required=False, )
    #     self.fields['section'] = forms.ChoiceField(label='Section', required=False, )
    #
    #     self.fields['class_name'].widget.attrs.update({'class': 'dn'})
    #     self.fields['section'].widget.attrs.update({'class': 'dn'})
    #
    #
    # def clean_class_name(self):
    #     class_name = self.cleaned_data['class_name']
    #     if not PeriodModel.objects.filter(class_name=class_name).exists():
    #         raise ValidationError("Given Class name is not available, please correct and try again...!!!")
    #     return class_name
    #
    # def clean_section(self):
    #     section = self.cleaned_data['section']
    #     if not PeriodModel.objects.filter(section=section).exists():
    #         raise ValidationError("Given Section is not available, please correct and try again...!!!")
    #     return section

    def clean(self):
        role = self.cleaned_data['role']

        # if role == 'student':
        #     class_name = self.cleaned_data['class_name']
        #     section = self.cleaned_data['section']
        #     if class_name is None:
        #         raise ValidationError('Class name is required for "Student" user group')
        #     if section is None:
        #         raise ValidationError('Section is required for "Student" user group')


class TakeClassAttendanceFormPreselected(forms.ModelForm):
    # person = forms.ChoiceField(label='Attendance taker', required=True, )
    class_name = forms.ChoiceField(label='Class', required=True, )
    section = forms.ChoiceField(label='Section', required=True, )
    period_id = forms.ChoiceField(label='Period', required=True, )
    check_in = forms.ChoiceField(label='Check in', required=False, )
    check_out = forms.ChoiceField(label='Check out', required=False, )

    required_css_class = 'required'

    class Meta:
        model = AttendanceModel
        fields = (
            'attendance_date',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        attendance_taker_profile_qs = kwargs.pop('attendance_taker_profile_qs', None)

        super(TakeClassAttendanceFormPreselected, self).__init__(*args, **kwargs)
        profile = user.profile
        school_id = profile.school_id

        print('loggedin user--', user)
        print('loggedin user profile--', profile)
        print('frontend send profile--', attendance_taker_profile_qs)

        ATTENDANCE_TYPE_IN_CHOICE = (
            ('in', 'In'),
        )
        ATTENDANCE_TYPE_OUT_CHOICE = (
            ('out', 'Out'),
        )
        self.fields['check_in'] = forms.ChoiceField(choices=ATTENDANCE_TYPE_IN_CHOICE, label='Check in', required=False,
                                                    widget=forms.RadioSelect)
        self.fields['check_out'] = forms.ChoiceField(choices=ATTENDANCE_TYPE_OUT_CHOICE, label='Check out',
                                                     required=False,
                                                     widget=forms.RadioSelect)

        # Class name dropdown
        period_filter_qs = PeriodModel.objects.filter(school_id=school_id)
        FORM_CLASS_NAME_CHOICE = sorted(
            set([(item.class_name, item.get_class_name_display()) for item in period_filter_qs]))
        FORM_CLASS_NAME_CHOICE.insert(0, ('', '---Select---'))
        self.fields['class_name'] = forms.ChoiceField(choices=FORM_CLASS_NAME_CHOICE, label='Class Name',
                                                      required=True, )

        print('FORM_CLASS_NAME_CHOICE--', FORM_CLASS_NAME_CHOICE)

        # Section dropdown
        current_class_name = profile.class_name
        print('---current_class_name---', current_class_name)
        period_filter_qs.filter(class_name=current_class_name)
        FORM_SECTION_CHOICE = sorted(
            set([(item.section, item.get_section_display()) for item in period_filter_qs]))
        FORM_SECTION_CHOICE.insert(0, ('', '---Select---'))
        self.fields['section'] = forms.ChoiceField(choices=FORM_SECTION_CHOICE, label='Section', required=True, )
        print('FORM_SECTION_CHOICE--', FORM_SECTION_CHOICE)

        # if profile.role == "school_admin":
        #     FORM_ROLE_CHOICE = (
        #         ('school_admin', 'School Admin'),
        #         ('teacher', 'Teacher'),
        #         ('non_teacher', 'Non Teacher'),
        #     )
        #     self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance taker role',
        #                                             required=True,
        #                                             widget=forms.RadioSelect)
        # else:
        #     FORM_ROLE_CHOICE = (
        #         ('teacher', 'Teacher'),
        #         ('non_teacher', 'Non Teacher'),
        #     )
        #     self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance taker role',
        #                                             required=True,
        #                                             widget=forms.RadioSelect, initial=1)

        self.fields['attendance_date'] = forms.CharField(widget=forms.TextInput(attrs={'type': 'date'}))

        # self.fields['class_name'] = forms.ChoiceField(label='Class Name', required=False, )
        # self.fields['section'] = forms.ChoiceField(label='Section', required=False, )
        #
        # self.fields['class_name'].widget.attrs.update({'class': 'dn'})
        # self.fields['section'].widget.attrs.update({'class': 'dn'})

        if school_id.is_manual_entry_allowed:
            ATTENDANCE_CHOICE = [
                ('face_recognition', 'By Face Recognition'),
                ('manual', 'By Manual Entry'),
            ]
        else:
            ATTENDANCE_CHOICE = [
                ('face_recognition', 'By Face Recognition'),
            ]

        # ATTENDANCE_CHOICE.insert(0, ('', '---Select---'))
        self.fields['attendance_choice'] = forms.ChoiceField(choices=ATTENDANCE_CHOICE, label='Attendance choice',
                                                             required=True)

    def clean_class_name(self):
        class_name = self.cleaned_data['class_name']
        if not PeriodModel.objects.filter(class_name=class_name).exists():
            raise ValidationError("Given Class name is not available, please correct and try again...!!!")
        return class_name

    def clean_section(self):
        section = self.cleaned_data['section']
        if not PeriodModel.objects.filter(section=section).exists():
            raise ValidationError("Given Section is not available, please correct and try again...!!!")
        return section

    def clean(self):
        role = self.cleaned_data['role']

        # if role == 'student':
        #     class_name = self.cleaned_data['class_name']
        #     section = self.cleaned_data['section']
        #     if class_name is None:
        #         raise ValidationError('Class name is required for "Student" user group')
        #     if section is None:
        #         raise ValidationError('Section is required for "Student" user group')


class TakeClassAttendanceForm(forms.ModelForm):
    # type = forms.ChoiceField(label='Type', required=True, )
    person = forms.ChoiceField(label='Attendance taker', required=True, )
    class_name = forms.ChoiceField(label='Class', required=True, )
    section = forms.ChoiceField(label='Section', required=True, )
    period_id = forms.ChoiceField(label='Period', required=True, )

    required_css_class = 'required'

    class Meta:
        model = AttendanceModel
        fields = (
            'attendance_date',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(TakeClassAttendanceForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)
        school_id = profile.school_id
        print('school_id--', school_id)

        # Class name dropdown
        period_filter_qs = PeriodModel.objects.filter(school_id=school_id)
        FORM_CLASS_NAME_CHOICE = sorted(
            set([(item.class_name, item.get_class_name_display()) for item in period_filter_qs]))
        FORM_CLASS_NAME_CHOICE.insert(0, ('', '---Select---'))
        self.fields['class_name'] = forms.ChoiceField(choices=FORM_CLASS_NAME_CHOICE, label='Class Name',
                                                      required=True, )

        print('FORM_CLASS_NAME_CHOICE--', FORM_CLASS_NAME_CHOICE)

        # Section dropdown
        current_class_name = profile.class_name
        print('---current_class_name---', current_class_name)
        period_filter_qs.filter(class_name=current_class_name)
        FORM_SECTION_CHOICE = sorted(
            set([(item.section, item.get_section_display()) for item in period_filter_qs]))
        FORM_SECTION_CHOICE.insert(0, ('', '---Select---'))
        self.fields['section'] = forms.ChoiceField(choices=FORM_SECTION_CHOICE, label='Section', required=True, )
        print('FORM_SECTION_CHOICE--', FORM_SECTION_CHOICE)

        if profile.role == "school_admin":
            FORM_ROLE_CHOICE = (
                ('school_admin', 'School Admin'),
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance taker role',
                                                    required=True,
                                                    widget=forms.RadioSelect)
        else:
            FORM_ROLE_CHOICE = (
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance taker role',
                                                    required=True,
                                                    widget=forms.RadioSelect, initial=1)

        self.fields['attendance_date'] = forms.CharField(widget=forms.TextInput(attrs={'type': 'date'}))

        # self.fields['class_name'] = forms.ChoiceField(label='Class Name', required=False, )
        # self.fields['section'] = forms.ChoiceField(label='Section', required=False, )
        #
        # self.fields['class_name'].widget.attrs.update({'class': 'dn'})
        # self.fields['section'].widget.attrs.update({'class': 'dn'})

    def clean_class_name(self):
        class_name = self.cleaned_data['class_name']
        if not PeriodModel.objects.filter(class_name=class_name).exists():
            raise ValidationError("Given Class name is not available, please correct and try again...!!!")
        return class_name

    def clean_section(self):
        section = self.cleaned_data['section']
        if not PeriodModel.objects.filter(section=section).exists():
            raise ValidationError("Given Section is not available, please correct and try again...!!!")
        return section

    def clean(self):
        role = self.cleaned_data['role']

        # if role == 'student':
        #     class_name = self.cleaned_data['class_name']
        #     section = self.cleaned_data['section']
        #     if class_name is None:
        #         raise ValidationError('Class name is required for "Student" user group')
        #     if section is None:
        #         raise ValidationError('Section is required for "Student" user group')


class TakeCCTVAttendanceForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = CameraResponseModel
        fields = (
            'camera_id',
            'role',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(TakeCCTVAttendanceForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)
        school_id = profile.school_id
        print('school_id--', school_id)

        camera_qs = CameraModel.objects.filter(school_id=school_id)
        self.fields['camera_id'] = forms.ModelChoiceField(queryset=camera_qs)
        self.fields['camera_id'].label_from_instance = self.label_from_instance

        # FORM_CAMERA_CHOICE = sorted(
        #     set([(item.camera_id, item.title) for item in camera_qs]))
        # FORM_CAMERA_CHOICE.insert(0, ('', '---Select---'))
        # self.fields['camera_id'] = forms.ChoiceField(choices=FORM_CAMERA_CHOICE,
        #                                              label='Camera',
        #                                              required=True, )

        if profile.school_id.organisation_type == "school":
            FORM_ROLE_CHOICE = [
                ('school_admin', 'School Admin'),
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
                ('student', 'Student'),
            ]
            FORM_ROLE_CHOICE.insert(0, ('', '---------'))
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE,
                                                    label='Role',
                                                    required=False, )
        elif profile.school_id.organisation_type == "company":
            FORM_ROLE_CHOICE = [
                ('admin', 'Admin'),
                ('staff', 'Staff'),
            ]
            FORM_ROLE_CHOICE.insert(0, ('', '---------'))
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE,
                                                    label='Role',
                                                    required=False, )
        elif profile.school_id.organisation_type == "anganwadi":
            FORM_ROLE_CHOICE = [
                ('admin', 'Admin'),
                ('staff', 'Staff'),
            ]
            FORM_ROLE_CHOICE.insert(0, ('', '---------'))
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE,
                                                    label='Role',
                                                    required=False, )

    @staticmethod
    def label_from_instance(obj):
        return "%s" % obj.title


# Attendance self
class TakeStaffSelfAttendanceForm(forms.ModelForm):
    required_css_class = 'required'
    check_in = forms.ChoiceField(label='Check in', required=False, )
    check_out = forms.ChoiceField(label='Check out', required=False, )

    class Meta:
        model = StaffAttendanceModel
        fields = (
            'attendance_type',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        is_check_in = kwargs.pop('is_check_in', None)
        is_check_out = kwargs.pop('is_check_out', None)
        super(TakeStaffSelfAttendanceForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)
        school_id = profile.school_id

        ATTENDANCE_TYPE_IN_CHOICE = (
            ('in', 'In'),
        )
        ATTENDANCE_TYPE_OUT_CHOICE = (
            ('out', 'Out'),
        )
        self.fields['check_in'] = forms.ChoiceField(choices=ATTENDANCE_TYPE_IN_CHOICE, label='Check in', required=False,
                                                    widget=forms.RadioSelect)
        self.fields['check_out'] = forms.ChoiceField(choices=ATTENDANCE_TYPE_OUT_CHOICE, label='Check out',
                                                     required=False,
                                                     widget=forms.RadioSelect)

    def clean(self):
        role = self.cleaned_data['role']
        # write validations

        # if role == 'student':
        #     class_name = self.cleaned_data['class_name']
        #     section = self.cleaned_data['section']
        #     if class_name is None:
        #         raise ValidationError('Class name is required for "Student" user group')
        #     if section is None:
        #         raise ValidationError('Section is required for "Student" user group')


class TakeStaffOtherAttendanceFormPreselected(forms.ModelForm):
    required_css_class = 'required'
    check_in = forms.ChoiceField(label='Check in', required=False, )
    check_out = forms.ChoiceField(label='Check out', required=False, )
    person = forms.ChoiceField(label='Attendance for', required=True, )

    class Meta:
        model = StaffAttendanceModel
        fields = (
            'attendance_type',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        attendance_for_profile_qs = kwargs.pop('attendance_for_profile_qs', None)

        is_check_in = kwargs.pop('is_check_in', None)
        is_check_out = kwargs.pop('is_check_out', None)
        super(TakeStaffOtherAttendanceFormPreselected, self).__init__(*args, **kwargs)
        profile = user.profile
        school_id = profile.school_id

        print('loggedin user--', user)
        print('loggedin user profile--', profile)
        print('frontend send profile--', attendance_for_profile_qs)

        ATTENDANCE_TYPE_IN_CHOICE = (
            ('in', 'In'),
        )
        ATTENDANCE_TYPE_OUT_CHOICE = (
            ('out', 'Out'),
        )
        self.fields['check_in'] = forms.ChoiceField(choices=ATTENDANCE_TYPE_IN_CHOICE, label='Check in', required=False,
                                                    widget=forms.RadioSelect)
        self.fields['check_out'] = forms.ChoiceField(choices=ATTENDANCE_TYPE_OUT_CHOICE, label='Check out',
                                                     required=False,
                                                     widget=forms.RadioSelect)

        if profile.role == "school_admin":
            FORM_ROLE_CHOICE = (
                ('school_admin', 'School Admin'),
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance for role',
                                                    required=True,
                                                    widget=forms.RadioSelect)
        else:
            FORM_ROLE_CHOICE = (
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance for role',
                                                    required=True,
                                                    widget=forms.RadioSelect, initial=1)

    def clean(self):
        role = self.cleaned_data['role']
        # write validations

        # if role == 'student':
        #     class_name = self.cleaned_data['class_name']
        #     section = self.cleaned_data['section']
        #     if class_name is None:
        #         raise ValidationError('Class name is required for "Student" user group')
        #     if section is None:
        #         raise ValidationError('Section is required for "Student" user group')


class TakeStaffOtherAttendanceForm(forms.ModelForm):
    required_css_class = 'required'
    check_in = forms.ChoiceField(label='Check in', required=False, )
    check_out = forms.ChoiceField(label='Check out', required=False, )
    person = forms.ChoiceField(label='Attendance for', required=True, )

    class Meta:
        model = StaffAttendanceModel
        fields = (
            'attendance_type',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        is_check_in = kwargs.pop('is_check_in', None)
        is_check_out = kwargs.pop('is_check_out', None)
        super(TakeStaffOtherAttendanceForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)
        school_id = profile.school_id

        ATTENDANCE_TYPE_IN_CHOICE = (
            ('in', 'In'),
        )
        ATTENDANCE_TYPE_OUT_CHOICE = (
            ('out', 'Out'),
        )
        self.fields['check_in'] = forms.ChoiceField(choices=ATTENDANCE_TYPE_IN_CHOICE, label='Check in', required=False,
                                                    widget=forms.RadioSelect)
        self.fields['check_out'] = forms.ChoiceField(choices=ATTENDANCE_TYPE_OUT_CHOICE, label='Check out',
                                                     required=False,
                                                     widget=forms.RadioSelect)

        if profile.role == "school_admin":
            FORM_ROLE_CHOICE = (
                ('school_admin', 'School Admin'),
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance for role',
                                                    required=True,
                                                    widget=forms.RadioSelect)
        else:
            FORM_ROLE_CHOICE = (
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance for role',
                                                    required=True,
                                                    widget=forms.RadioSelect, initial=1)

    def clean(self):
        role = self.cleaned_data['role']
        # write validations

        # if role == 'student':
        #     class_name = self.cleaned_data['class_name']
        #     section = self.cleaned_data['section']
        #     if class_name is None:
        #         raise ValidationError('Class name is required for "Student" user group')
        #     if section is None:
        #         raise ValidationError('Section is required for "Student" user group')


class TakeStaffAttendanceForm(forms.Form):
    type = forms.ChoiceField(label='Type', required=True, )
    person = forms.ChoiceField(label='Person', required=True, )

    required_css_class = 'required'

    class Meta:
        model = StaffAttendanceModel
        fields = (
            'staff_attendance_id',
            'attendance_id',
            'attendance_type',
            'attempt_status',
            'loggedin_user',
            'taken_by',
            'media_id',
            'attendance_status',
            'attendance_date',
            'date_time',
            'updated_at',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(TakeStaffAttendanceForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)
        school_id = profile.school_id

        if profile.role == "school_admin":
            FORM_ROLE_CHOICE = (
                ('school_admin', 'School Admin'),
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance taker', required=True,
                                                    widget=forms.RadioSelect)
        else:
            FORM_ROLE_CHOICE = (
                ('teacher', 'Teacher'),
                ('non_teacher', 'Non Teacher'),
            )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Attendance taker', required=True,
                                                    widget=forms.RadioSelect, initial=1)

        ATTENDANCE_FOR_CHOICE = (
            ("class_attendance", "Class attendance"),
            ("self", "Self"),
            ("others", "Others"),
        )
        self.fields['type'] = forms.ChoiceField(choices=ATTENDANCE_FOR_CHOICE, label='Attendance for', required=True,
                                                widget=forms.RadioSelect)

        # self.fields['class_name'] = forms.ChoiceField(label='Class Name', required=False, )
        # self.fields['section'] = forms.ChoiceField(label='Section', required=False, )
        #
        # self.fields['class_name'].widget.attrs.update({'class': 'dn'})
        # self.fields['section'].widget.attrs.update({'class': 'dn'})
        #
        # self.fields['gender'].widget.attrs['readonly'] = True
        # self.fields['blood_group'].widget.attrs['readonly'] = True
        # self.fields['class_name'].widget.attrs['readonly'] = True
        # self.fields['section'].widget.attrs['readonly'] = True
        #
        # self.fields['class_name'].required = False
        # self.fields['section'].required = False

    # def clean_class_name(self):
    #     class_name = self.cleaned_data['class_name']
    #     if not PeriodModel.objects.filter(class_name=class_name).exists():
    #         raise ValidationError("Given Class name is not available, please correct and try again...!!!")
    #     return class_name
    #
    # def clean_section(self):
    #     section = self.cleaned_data['section']
    #     if not PeriodModel.objects.filter(section=section).exists():
    #         raise ValidationError("Given Section is not available, please correct and try again...!!!")
    #     return section

    def clean(self):
        role = self.cleaned_data['role']

        if role == 'student':
            class_name = self.cleaned_data['class_name']
            section = self.cleaned_data['section']
            if class_name is None:
                raise ValidationError('Class name is required for "Student" user group')
            if section is None:
                raise ValidationError('Section is required for "Student" user group')


class UpdateTeacherUserForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = User
        fields = (
            # 'username',
            # 'email',
            'first_name',
            'last_name',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(UpdateTeacherUserForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)

        for field_name in ['first_name', 'last_name']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

        self.fields['first_name'].required = True

        # if instance and instance.pk:
        #     self.fields['username'].widget.attrs['readonly'] = True
        #     self.fields['email'].widget.attrs['readonly'] = True




class UpdateTeacherProfileForm(forms.ModelForm):
    contact_number = forms.CharField(required=False, label='Contact Number (With Country Code)', widget=forms.TextInput(
        attrs={"autocomplete": "off"}))
    required_css_class = 'required'

    class Meta:
        model = Profile
        fields = (
            'gender',
            'blood_group',
            'role',
            'contact_number',
            'father_name',
            # 'class_name',
            # 'section',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(UpdateTeacherProfileForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)

        school_id = profile.school_id
        if profile.school_id.organisation_type == "school":
            if kwargs["instance"].role == "school_admin":
                FORM_ROLE_CHOICE = (
                    ('school_admin', 'School Admin'),
                )
            else:
                FORM_ROLE_CHOICE = (
                    ('teacher', 'Teacher'),
                    ('non_teacher', 'Non Teacher'),
                )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='Staff Group', required=True,
                                                    widget=forms.RadioSelect)
        elif profile.school_id.organisation_type == "company":
            if kwargs["instance"].role == "admin":
                FORM_ROLE_CHOICE = (
                    ('admin', 'Admin'),
                )
            else:
                FORM_ROLE_CHOICE = (
                    ('staff', 'Staff'),
                )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                    widget=forms.RadioSelect)
        elif profile.school_id.organisation_type == "anganwadi":
            if kwargs["instance"].role == "admin":
                FORM_ROLE_CHOICE = (
                    ('admin', 'Admin'),
                )
            else:
                FORM_ROLE_CHOICE = (
                    ('staff', 'Staff'),
                )
            self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
                                                    widget=forms.RadioSelect)

        # if profile.school_id.organisation_type == "school":
        #     if profile.role == "school_admin":
        #         if period_filter_qs:
        #             FORM_ROLE_CHOICE = (
        #                 ('teacher', 'Teacher'),
        #                 ('non_teacher', 'Non Teacher'),
        #                 ('student', 'Student'),
        #             )
        #         else:
        #             FORM_ROLE_CHOICE = (
        #                 ('teacher', 'Teacher'),
        #                 ('non_teacher', 'Non Teacher'),
        #             )
        #
        #         self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
        #                                                 widget=forms.RadioSelect)
        #     else:
        #         FORM_ROLE_CHOICE = (
        #             ('student', 'Student'),
        #         )
        #         self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
        #                                                 widget=forms.RadioSelect, initial=1)
        #     self.fields['class_name'].widget.attrs.update({'class': 'dn'})
        #     self.fields['section'].widget.attrs.update({'class': 'dn'})
        # elif profile.school_id.organisation_type == "company":
        #     FORM_ROLE_CHOICE = (
        #         ('staff', 'Staff'),
        #     )
        #     self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
        #                                             widget=forms.RadioSelect)
        # elif profile.school_id.organisation_type == "anganwadi":
        #     FORM_ROLE_CHOICE = (
        #         ('staff', 'Staff'),
        #     )
        #     self.fields['role'] = forms.ChoiceField(choices=FORM_ROLE_CHOICE, label='User group', required=True,
        #                                             widget=forms.RadioSelect)


class UpdateStudentUserForm(forms.ModelForm):
    required_css_class = 'required'
    first_name = forms.CharField(required=True)

    class Meta:
        model = User
        fields = (
            # 'username',
            # 'email',
            'first_name',
            'last_name',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(UpdateStudentUserForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)

        for field_name in ['first_name', 'last_name']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

        self.fields['first_name'].required = True
        # self.fields['username'].required = False

        # if instance and instance.pk:
            # self.fields['username'].widget.attrs['readonly'] = True
            # self.fields['email'].widget.attrs['readonly'] = True



class UpdateStudentProfileForm(forms.ModelForm):
    contact_number = forms.CharField(required=False, label='Contact Number (With Country Code)', widget=forms.TextInput(
        attrs={"autocomplete": "off"}))
    required_css_class = 'required'

    class Meta:
        model = Profile
        fields = (
            'father_name',
            'gender',
            'blood_group',
            'class_name',
            'section',
            'contact_number',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(UpdateStudentProfileForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)
        school_id = profile.school_id

        # Class name dropdown
        period_filter_qs = PeriodModel.objects.filter(school_id=school_id)
        FORM_CLASS_NAME_CHOICE = sorted(
            set([(item.class_name, item.get_class_name_display()) for item in period_filter_qs]))
        FORM_CLASS_NAME_CHOICE.insert(0, ('', '---Select---'))
        self.fields['class_name'] = forms.ChoiceField(choices=FORM_CLASS_NAME_CHOICE, label='Class Name', required=True)

        # Section dropdown
        current_class_name = profile.class_name
        print('---current_class_name---', current_class_name)
        period_filter_qs.filter(class_name=current_class_name)
        FORM_SECTION_CHOICE = sorted(
            set([(item.section, item.get_section_display()) for item in period_filter_qs]))
        FORM_SECTION_CHOICE.insert(0, ('', '---Select---'))
        self.fields['section'] = forms.ChoiceField(choices=FORM_SECTION_CHOICE, label='Section', required=True)

    def clean_class_name(self):
        class_name = self.cleaned_data['class_name']
        if not PeriodModel.objects.filter(class_name=class_name).exists():
            raise ValidationError("Given Class name is not available, please correct and try again...!!!")
        return class_name

    def clean_section(self):
        section = self.cleaned_data['section']
        if not PeriodModel.objects.filter(section=section).exists():
            raise ValidationError("Given Section is not available, please correct and try again...!!!")
        return section


class UpdateProfileUserForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(UpdateProfileUserForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)

        for field_name in ['first_name', 'last_name']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

        self.fields['first_name'].required = True

        # self.fields['username'].required = False
        # if instance and instance.pk:
        #     self.fields['username'].widget.attrs['readonly'] = True
        #     self.fields['email'].widget.attrs['readonly'] = True


class UpdateProfileProfileForm(forms.ModelForm):
    contact_number = forms.CharField(required=False, label='Contact Number (With Country Code)', widget=forms.TextInput(
        attrs={"autocomplete": "off"}))
    required_css_class = 'required'

    class Meta:
        model = Profile
        fields = (
            'profile_picture',
            'father_name',
            'gender',
            'blood_group',
            'contact_number',
            'address',
            'city',
            'state',
            'country',
            'postal_code',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(UpdateProfileProfileForm, self).__init__(*args, **kwargs)
        print('user--', user)
        profile = user.profile
        print('profile--', profile)
        school_id = profile.school_id
