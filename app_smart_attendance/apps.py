from django.apps import AppConfig


class AppSmartAttendanceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_smart_attendance'
