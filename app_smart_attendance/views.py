import datetime
import json
import logging as log_print
import os
import re
import time
from itertools import groupby
# from luein_smart_attendance.settings import REMOVE_ORPHAN_MEDIA
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User, Group
from django.contrib.postgres.aggregates import StringAgg, JSONBAgg
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction
from django.db.models import F, Func, Value, CharField, Q
from django.db.models.functions import Concat, JSONObject
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.dateparse import parse_datetime, parse_date
from django.views.generic import DetailView

from api_face_recog.src.utils import extract_images_from_blobs
from app_root.models import Profile, MediaCapturedModel, AttendanceModel, PeriodModel, SchoolModel, CLASS_NAME_CHOICE, \
    SECTION_CHOICE, PERIOD_CHOICE, AnnouncementModel, BLOOD_GROUP_CHOICE, GENDER_CHOICE, StaffAttendanceModel, \
    CameraModel, CameraResponseModel, ROLE_CHOICE, PROFILE_ATTENDANCE_STATUS_CHOICE
from app_smart_attendance import forms
from app_smart_attendance.code_base.attendance_data_formatter import data_processor
from app_smart_attendance.code_base.attendance_data_formatter_2 import get_date_range_presence_by_profile_class
from app_smart_attendance.code_base.datetime_utils import get_date_range
from app_smart_attendance.forms import PeriodCreateForm, PeriodUpdateForm, CameraCreateForm, CameraUpdateForm
from app_smart_attendance.forms import PeriodCreateForm, PeriodUpdateForm, AnnouncementCreateForm
from app_smart_attendance.src.camera.camera_init import get_stream_save_frame_or_video
from app_smart_attendance.src.redis_calls.push_to_redis import push_request_for_face_recog
from luein_smart_attendance import settings
from luein_smart_attendance.code_base.utility import querydict_to_dict

logger = log_print.getLogger(__name__)


@login_required
def student_register_view(request):
    template_name = "app_smart_attendance/html/student_register.html"
    print('request.api_url---', request.api_url)

    role = request.user.profile.role
    if role == "admin" or role == "staff" or role == "school_admin" or role == "teacher" or role == "non_teacher":
        school_id = request.user.profile.school_id
        class_name_section_dict, _, _, _ = create_dynamic_class_name_section(school_id)
        # print('class_name_section_dict---', class_name_section_dict)
        class_choice = dict(CLASS_NAME_CHOICE)
        section_choice = dict(SECTION_CHOICE)
        blood_group_choice = dict(BLOOD_GROUP_CHOICE)
        gender_choice = dict(GENDER_CHOICE)

        old_students_qs = Profile.objects.none()
        # old_students_qs = Profile.objects.filter(school_id=school_id, role="student").order_by("class_name", "section",
        #                                                                                        "user__first_name")

        old_students_qs = Profile.objects.filter(school_id=school_id, role="student").extra(
            select={'int_class_name': 'CAST(class_name AS CHAR)'},
            order_by=['int_class_name', "section", "user__first_name"])

        # print('---old_students_qs---', old_students_qs)

        context_payload = {
            'class_name_section_dict': class_name_section_dict,
            'class_choice': class_choice,
            'section_choice': section_choice,
            'blood_group_choice': blood_group_choice,
            'gender_choice': gender_choice,
            'old_students_qs': old_students_qs,
        }

        return render(request, template_name, context_payload)
    else:
        return render(request, 'access_denied/access-denied-page.html', {})


@login_required
def students_view(request):
    template_name = "app_smart_attendance/html/student_list.html"
    print('request.api_url---', request.api_url)
    role = request.user.profile.role
    if role == "admin" or role == "school_admin" or role == "teacher":
        school_id = request.user.profile.school_id
        profile_filter_qs = Profile.objects.filter(role="student", school_id=school_id)
        school_id = request.user.profile.school_id
        class_name_section_dict, _, _, _ = create_dynamic_class_name_section(school_id)
        # print('class_name_section_dict---', class_name_section_dict)
        class_choice = dict(CLASS_NAME_CHOICE)
        section_choice = dict(SECTION_CHOICE)

        context_payload = {
            'class_name_section_dict': class_name_section_dict,
            'class_choice': class_choice,
            'section_choice': section_choice,
            "profile_filter_qs": profile_filter_qs,
        }

        return render(request, template_name, context_payload)
    else:
        return render(request, 'access_denied/access-denied-page.html', {})


@login_required
def take_attendance_view(request):
    template_name = "app_smart_attendance/html/take_attendance.html"
    print('request.api_url---', request.api_url)
    role = request.user.profile.role
    profile = request.user.profile
    return_data = get_today_check_in_check_out_attendance_success(request, profile)
    is_check_in = return_data["is_check_in"]
    is_check_out = return_data["is_check_out"]
    staff_at_qs = return_data["staff_at_qs"]

    # if role in ["admin", "staff", "school_admin", "teacher"]:
    if role == "admin" or role == "staff" or role == "school_admin" or role == "teacher":
        school_id = request.user.profile.school_id
        class_name_section_dict, class_name_section_period_dict, period_id_map_dict, _ = create_dynamic_class_name_section(
            school_id)
        class_choice = dict(CLASS_NAME_CHOICE)
        section_choice = dict(SECTION_CHOICE)
        period_choice = dict(PERIOD_CHOICE)
        blood_group_choice = dict(BLOOD_GROUP_CHOICE)
        gender_choice = dict(GENDER_CHOICE)

        context_payload = {
            'class_name_section_dict': class_name_section_dict,
            'class_name_section_period_dict': class_name_section_period_dict,
            'period_id_map_dict': period_id_map_dict,
            'class_choice': class_choice,
            'section_choice': section_choice,
            'period_choice': period_choice,
            'blood_group_choice': blood_group_choice,
            'gender_choice': gender_choice,
            'is_check_in': is_check_in,
            'is_check_out': is_check_out,
            'staff_at_qs': staff_at_qs,
        }

        return render(request, template_name, context_payload)
    else:
        return render(request, 'access_denied/access-denied-page.html', {})


@login_required
def attendance_view(request):
    template_name = "app_smart_attendance/html/attendance.html"
    context_payload = {}

    if request.user.profile.role == "student":
        profile_id = request.user.profile
        username = request.user.username
        # todo change logic to show student attendance to student
        attendance_filter_qs = AttendanceModel.objects.filter(
            attendance_status__contains=[{'username': username}]).values()
        attendances_json = json.loads(json.dumps(list(attendance_filter_qs), cls=DjangoJSONEncoder))

        print("---attendance_filter_qs---", attendances_json)
        context_payload["attendance_filter_qs"] = attendances_json

    else:
        school_id = request.user.profile.school_id
        class_name_section_dict, class_name_section_period_dict, period_id_map_dict, _ = create_dynamic_class_name_section(
            school_id)
        class_choice = dict(CLASS_NAME_CHOICE)
        section_choice = dict(SECTION_CHOICE)
        period_choice = dict(PERIOD_CHOICE)

        context_payload = {
            'class_name_section_dict': class_name_section_dict,
            'class_name_section_period_dict': class_name_section_period_dict,
            'period_id_map_dict': period_id_map_dict,
            'class_choice': class_choice,
            'section_choice': section_choice,
            'period_choice': period_choice,
        }

    return render(request, template_name, context_payload)


@login_required
def timesheet_view(request):
    template_name = "app_smart_attendance/html/timesheet_overview.html"
    school_id = request.user.profile.school_id
    role_list = ['teacher', 'non_teacher', 'school_admin']

    all_profile_qs = Profile.objects.filter(school_id=school_id, role__in=role_list).exclude(
        profile_id=request.user.profile.profile_id)

    return_data = get_timesheet_by_date_range(request=request, all_profile_qs=all_profile_qs)

    context_payload = return_data

    return render(request, template_name, context_payload)


@login_required
def timesheet_self_view(request, **kwargs):
    date_range_type = kwargs.get('date_range_type')
    date_text = kwargs.get('date_text')

    is_date_range_type_valid = True
    is_date_valid = True
    is_next_url_valid = True

    # check if "date_range_type" and its format is correct or not
    default_date_range_type = 'week'
    if not date_range_type in DATE_RANGE_TYPE:
        date_range_type = default_date_range_type
        is_date_range_type_valid = False

    # check if "date" and its format is correct or not
    date_from_date_text = None
    try:
        date_from_date_text = datetime.datetime.strptime(date_text, '%Y-%m-%d')
    except Exception as e:
        print('--date Exception occurred--', e)
        is_date_valid = False

    # set a valid date to now
    if not is_date_valid:
        now = datetime.datetime.now()
    else:
        now = date_from_date_text

    # finding and setting first date of any type of range
    if date_range_type == 'day':
        # set today
        now = now
        date_text = now.strftime('%Y-%m-%d')
    elif date_range_type == 'week':
        # set first day of week
        now = now + datetime.timedelta(days=-now.weekday())
        date_text = now.strftime('%Y-%m-%d')
    elif date_range_type == 'month':
        # set first day of month
        now = now.replace(day=1)
        date_text = now.strftime('%Y-%m-%d')

    current_range, previous_date, next_date = get_date_range(now, date_range_type)
    start_date = current_range[0]
    end_date = current_range[1]

    kwargs_val = {
        'date_text': date_text,
        'date_range_type': date_range_type
    }
    redirect_url = reverse('app_smart_attendance:timesheet_self_custom_all_view', kwargs=kwargs_val)

    print('redirect_url---', redirect_url)
    if not is_date_valid or not is_date_range_type_valid:
        print('PAGE REDIRECTED----')
        return redirect(redirect_url)

    ####################################

    template_name = "app_smart_attendance/html/timesheet_overview_self.html"
    school_id = request.user.profile.school_id

    all_profile_qs = Profile.objects.filter(profile_id=request.user.profile.profile_id)

    return_data = get_timesheet_by_date_range(
        request=request,
        all_profile_qs=all_profile_qs,
        start_date=start_date,
        end_date=end_date,
        date_range_type=date_range_type
    )

    today = datetime.datetime.now()

    today_text = today.strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': today_text,
        'date_range_type': 'day'
    }
    day_url = reverse('app_smart_attendance:timesheet_self_custom_all_view', kwargs=kwargs_val)

    today_text = (today + datetime.timedelta(days=-today.weekday())).strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': today_text,
        'date_range_type': 'week'
    }
    week_url = reverse('app_smart_attendance:timesheet_self_custom_all_view', kwargs=kwargs_val)

    today_text = (today.replace(day=1)).strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': today_text,
        'date_range_type': 'month'
    }
    month_url = reverse('app_smart_attendance:timesheet_self_custom_all_view', kwargs=kwargs_val)

    previous_date_text = previous_date.strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': previous_date_text,
        'date_range_type': date_range_type
    }
    previous_url = reverse('app_smart_attendance:timesheet_self_custom_all_view', kwargs=kwargs_val)

    if end_date > today:
        is_next_url_valid = False

    next_url = ""
    if is_next_url_valid:
        next_date_text = next_date.strftime('%Y-%m-%d')
        kwargs_val = {
            'date_text': next_date_text,
            'date_range_type': date_range_type
        }
        next_url = reverse('app_smart_attendance:timesheet_self_custom_all_view', kwargs=kwargs_val)

    return_data['is_next_url_valid'] = is_next_url_valid
    return_data['date_range_type'] = date_range_type
    return_data['day_url'] = day_url
    return_data['week_url'] = week_url
    return_data['month_url'] = month_url
    return_data['previous_url'] = previous_url
    return_data['next_url'] = next_url

    context_payload = return_data

    return render(request, template_name, context_payload)


@login_required
def timesheet_class_view(request, **kwargs):
    if request.organisation_type != "school":
        template_name = "access_denied/access-denied-page.html"
        context_payload = {
            "side_nav_bar": True
        }
        return render(request, template_name, context_payload)

    date_range_type = kwargs.get('date_range_type')
    date_text = kwargs.get('date_text')
    class_name_section = kwargs.get('class_name_section')

    school_id = request.user.profile.school_id
    class_name_section_dict, _, _, class_name_section_list = create_dynamic_class_name_section(school_id)
    # print('---class_name_section_list---', class_name_section_list)

    is_date_range_type_valid = True
    is_date_valid = True
    is_next_url_valid = True
    is_class_name_section = True

    # check if "date_range_type" and its format is correct or not
    default_date_range_type = 'week'
    if not date_range_type in DATE_RANGE_TYPE:
        date_range_type = default_date_range_type
        is_date_range_type_valid = False

    # check if "date" and its format is correct or not
    date_from_date_text = None
    try:
        date_from_date_text = datetime.datetime.strptime(date_text, '%Y-%m-%d')
    except Exception as e:
        print('--date Exception occurred--', e)
        is_date_valid = False

    # set a valid date to now
    if not is_date_valid:
        now = datetime.datetime.now()
    else:
        now = date_from_date_text

    # finding and setting first date of any type of range
    if date_range_type == 'day':
        # set today
        now = now
        date_text = now.strftime('%Y-%m-%d')
    elif date_range_type == 'week':
        # set first day of week
        now = now + datetime.timedelta(days=-now.weekday())
        date_text = now.strftime('%Y-%m-%d')
    elif date_range_type == 'month':
        # set first day of month
        now = now.replace(day=1)
        date_text = now.strftime('%Y-%m-%d')

    current_range, previous_date, next_date = get_date_range(now, date_range_type)
    start_date = current_range[0]
    end_date = current_range[1]

    # check if "class_name_section" and its format is correct or not
    # print('---class_name_section--1--', class_name_section)
    class_name = None
    section = None
    class_name_section_list_url = []
    if class_name_section in class_name_section_list:
        is_class_name_section = True
        class_name, section = get_class_name_section(class_name_section)
        # print('---input class_name---', class_name)
        # print('---input section---', section)
    elif class_name_section == "None":
        is_class_name_section = True
    else:
        is_class_name_section = False
        class_name_section = "None"
    # print('---class_name_section--2--', class_name_section)

    # create class_name dropdown url list
    url_kwargs = {
        'date_text': date_text,
        'date_range_type': date_range_type,
        'class_name_section': None
    }
    class_name_section_url = reverse('app_smart_attendance:timesheet_class_custom_all_view', kwargs=url_kwargs)
    class_name_section_list_url.append({
        "class_name_section": "None",
        "url": class_name_section_url,
    })
    for item in class_name_section_list:
        url_kwargs = {
            'date_text': date_text,
            'date_range_type': date_range_type,
            'class_name_section': item
        }
        class_name_section_url = reverse('app_smart_attendance:timesheet_class_custom_all_view', kwargs=url_kwargs)
        cn, s = get_class_name_section(item)
        class_name_section_list_url.append({
            "class_name_section": item,
            "class_name": cn,
            "section": s,
            "url": class_name_section_url,
        })

    kwargs_val = {
        'date_text': date_text,
        'date_range_type': date_range_type,
        'class_name_section': class_name_section
    }
    redirect_url = reverse('app_smart_attendance:timesheet_class_custom_all_view', kwargs=kwargs_val)

    # print('redirect_url---', redirect_url)
    if not is_date_valid or not is_date_range_type_valid or not is_class_name_section:
        print('PAGE REDIRECTED----')
        return redirect(redirect_url)

    ####################################

    template_name = "app_smart_attendance/html/timesheet_overview_class.html"
    role_list = ['student']

    all_profile_qs = Profile.objects.filter(
        school_id=school_id,
        role__in=role_list,
        class_name=class_name,
        section=section
    )

    return_data = get_timesheet_class_by_date_range(
        request=request,
        role_list=role_list,
        all_profile_qs=all_profile_qs,
        date_range_type=date_range_type,
        start_date=start_date,
        end_date=end_date,
        class_name=class_name,
        section=section
    )

    today = datetime.datetime.now()

    today_text = today.strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': today_text,
        'date_range_type': 'day',
        'class_name_section': class_name_section
    }
    day_url = reverse('app_smart_attendance:timesheet_class_custom_all_view', kwargs=kwargs_val)

    today_text = (today + datetime.timedelta(days=-today.weekday())).strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': today_text,
        'date_range_type': 'week',
        'class_name_section': class_name_section
    }
    week_url = reverse('app_smart_attendance:timesheet_class_custom_all_view', kwargs=kwargs_val)

    today_text = (today.replace(day=1)).strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': today_text,
        'date_range_type': 'month',
        'class_name_section': class_name_section
    }
    month_url = reverse('app_smart_attendance:timesheet_class_custom_all_view', kwargs=kwargs_val)

    previous_date_text = previous_date.strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': previous_date_text,
        'date_range_type': date_range_type,
        'class_name_section': class_name_section
    }
    previous_url = reverse('app_smart_attendance:timesheet_class_custom_all_view', kwargs=kwargs_val)

    if end_date > today:
        is_next_url_valid = False

    next_url = ""
    if is_next_url_valid:
        next_date_text = next_date.strftime('%Y-%m-%d')
        kwargs_val = {
            'date_text': next_date_text,
            'date_range_type': date_range_type,
            'class_name_section': class_name_section
        }
        next_url = reverse('app_smart_attendance:timesheet_class_custom_all_view', kwargs=kwargs_val)

    return_data['is_next_url_valid'] = is_next_url_valid
    return_data['date_range_type'] = date_range_type
    return_data['day_url'] = day_url
    return_data['week_url'] = week_url
    return_data['month_url'] = month_url
    return_data['previous_url'] = previous_url
    return_data['next_url'] = next_url
    return_data['class_name_section'] = class_name_section

    return_data['class_name_section_dict'] = class_name_section_dict
    return_data['class_name_section_list_url'] = class_name_section_list_url

    context_payload = return_data

    return render(request, template_name, context_payload)


DATE_RANGE_TYPE = [
    'day',
    'week',
    'month',
]


def get_class_name_section(class_name_section):
    ### input: 10a (string)
    ### output: class_name = 10, section = a
    # class_name = int(re.search(r'\d+', class_name_section).group())
    # section = class_name_section.replace(str(class_name), '')

    # LOGIC 2
    # input: 10-a (string)
    # output: class_name = 10, section = a

    class_name = None
    section = None
    class_name_section_list = class_name_section.split('-')
    class_name = class_name_section_list[0]
    if class_name_section.find('-') > 0:
        section = class_name_section_list[1]

    return class_name, section


# use for showing "Timesheet Other"
@login_required
def timesheet_custom_view(request, **kwargs):
    date_range_type = kwargs.get('date_range_type')
    date_text = kwargs.get('date_text')

    is_date_range_type_valid = True
    is_date_valid = True
    is_next_url_valid = True

    # check if "date_range_type" and its format is correct or not
    default_date_range_type = 'week'
    if not date_range_type in DATE_RANGE_TYPE:
        date_range_type = default_date_range_type
        is_date_range_type_valid = False

    # check if "date" and its format is correct or not
    date_from_date_text = None
    try:
        date_from_date_text = datetime.datetime.strptime(date_text, '%Y-%m-%d')
    except Exception as e:
        print('--date Exception occurred--', e)
        is_date_valid = False

    # set a valid date to now
    if not is_date_valid:
        now = datetime.datetime.now()
    else:
        now = date_from_date_text

    # finding and setting first date of any type of range
    if date_range_type == 'day':
        # set today
        now = now
        date_text = now.strftime('%Y-%m-%d')
    elif date_range_type == 'week':
        # set first day of week
        now = now + datetime.timedelta(days=-now.weekday())
        date_text = now.strftime('%Y-%m-%d')
    elif date_range_type == 'month':
        # set first day of month
        now = now.replace(day=1)
        date_text = now.strftime('%Y-%m-%d')

    current_range, previous_date, next_date = get_date_range(now, date_range_type)
    start_date = current_range[0]
    end_date = current_range[1]

    kwargs_val = {
        'date_text': date_text,
        'date_range_type': date_range_type
    }
    redirect_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)

    print('redirect_url---', redirect_url)
    if not is_date_valid or not is_date_range_type_valid:
        print('PAGE REDIRECTED----')
        return redirect(redirect_url)

    #######################################

    template_name = "app_smart_attendance/html/timesheet_overview.html"
    school_id = request.user.profile.school_id
    role_list = ['admin', 'staff', 'teacher', 'non_teacher', 'school_admin']

    all_profile_qs = Profile.objects.filter(school_id=school_id, role__in=role_list).exclude(
        profile_id=request.user.profile.profile_id)

    userprofile = request.user.profile
    all_profile_qs = [userprofile] + list(all_profile_qs)

    return_data = get_timesheet_by_date_range(
        request=request,
        all_profile_qs=all_profile_qs,
        start_date=start_date,
        end_date=end_date,
        date_range_type=date_range_type
    )

    """
    day = OVERVIEW_URL/CURRENT_DATE/day
    week = OVERVIEW_URL/CURRENT_DATE/week
    month = OVERVIEW_URL/CURRENT_DATE/month

    previous = OVERVIEW_URL/previous_first_date_of_range/CURRENT_TYPE
    next = OVERVIEW_URL/next_first_date_of_range/CURRENT_TYPE		if next_first_date_of_range > TODAY ---- disable this button -- NO_URL


    OVERVIEW_URL=http://127.0.0.1:8001/app/timesheet/overview/
    CURRENT_DATE=current_first_date_of_range
    CURRENT_TYPE=date_range_type
    """

    today = datetime.datetime.now()

    today_text = today.strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': today_text,
        'date_range_type': 'day'
    }
    day_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)

    today_text = (today + datetime.timedelta(days=-today.weekday())).strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': today_text,
        'date_range_type': 'week'
    }
    week_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)

    today_text = (today.replace(day=1)).strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': today_text,
        'date_range_type': 'month'
    }
    month_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)

    previous_date_text = previous_date.strftime('%Y-%m-%d')
    kwargs_val = {
        'date_text': previous_date_text,
        'date_range_type': date_range_type
    }
    previous_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)

    if end_date > today:
        is_next_url_valid = False

    next_url = ""
    if is_next_url_valid:
        next_date_text = next_date.strftime('%Y-%m-%d')
        kwargs_val = {
            'date_text': next_date_text,
            'date_range_type': date_range_type
        }
        next_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)

    return_data['is_next_url_valid'] = is_next_url_valid
    return_data['date_range_type'] = date_range_type
    return_data['day_url'] = day_url
    return_data['week_url'] = week_url
    return_data['month_url'] = month_url
    return_data['previous_url'] = previous_url
    return_data['next_url'] = next_url

    context_payload = return_data

    return render(request, template_name, context_payload)


def get_timesheet_by_date_range(request, role_list=None, all_profile_qs=None, date_range_type=None, start_date=None,
                                end_date=None):
    # print('---request---', request)
    # print('---all_profile_qs---', all_profile_qs)
    # print('---start_date---1---', start_date)
    # print('---end_date---1---', end_date)
    # print('----------')

    school_id = request.user.profile.school_id
    if not role_list:
        role_list = ['teacher', 'non_teacher', 'school_admin']

    if not all_profile_qs:
        all_profile_qs = Profile.objects.filter(school_id=school_id, role__in=role_list)

    today = datetime.date.today()
    today = datetime.datetime.combine(today, datetime.datetime.min.time())

    dt = today
    if not start_date:
        # weekday (Monday =0 Sunday=6)
        # isoweekday (Monday =1 Sunday=6)
        start_date = dt - datetime.timedelta(days=dt.weekday())

    if not end_date:
        end_date = start_date + datetime.timedelta(days=6)

    # create date_range start
    day_full_name = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    day_abbr_name = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    day_init_name = ['M', 'T', 'W', 'T', 'F', 'S', 'S']
    date_range = []
    date_list = []
    date_modified = start_date
    nbDaysbtw2dates = 1
    while date_modified <= end_date:
        day_init = day_init_name[date_modified.weekday()]
        date_number = date_modified.day
        date_range.append({
            "day": day_init,
            "date": date_number
        })
        date_list.append(date_modified)

        date_modified += datetime.timedelta(days=nbDaysbtw2dates)
    # create date_range end

    # create people start
    people = []
    if all_profile_qs:
        for item in all_profile_qs:

            profile_picture = None
            if item.profile_picture:
                profile_picture = item.profile_picture.url

            data = get_date_range_check_in_out_by_profile_old(request,
                                                              single_profile_qs=item,
                                                              start_date=start_date,
                                                              end_date=end_date,
                                                              date_list=date_list)

            # todo - ON HOLD
            # data = get_date_range_check_in_out_by_profile_new(request,
            #                                               single_profile_qs=item,
            #                                               start_date=start_date,
            #                                               end_date=end_date,
            #                                               date_list=date_list)

            people.append({
                "profile_id": str(item.profile_id),
                "first_name": item.user.first_name,
                "last_name": item.user.last_name,
                "father_name": item.father_name,
                "profile_picture": profile_picture,
                "username": item.user.username,
                "data": data
            })
    else:
        pass
    # create people end

    # sample data
    timesheet_data = {
        "start_date": "5-dec-2022",
        "end_date": "11-dec-2022",
        "date_range": [
            {
                "day": "M",
                "date": 5
            }, {
                "day": "T",
                "date": 6
            }, {
                "day": "W",
                "date": 7
            }, {
                "day": "T",
                "date": 8
            }, {
                "day": "F",
                "date": 9
            }, {
                "day": "S",
                "date": 10
            }, {
                "day": "S",
                "date": 11
            }],
        "people": [
            {
                "profile_id": "alskdjflaskjdf",
                "first_name": "sumit",
                "last_name": "sumit",
                "profile_picture": "/media/profile_picture/pp_1_vKeMv12.jpg",

                "data": [
                    {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm",
                        "date": "5-dec"
                    },
                    {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm",
                        "date": "6-dec"
                    }, {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm",
                        "date": "7-dec"
                    }, {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm",
                        "date": "8-dec"
                    }, {
                        "first_check_in": "",
                        "last_check_out": "",
                        "date": "9-dec"
                    }, {
                        "first_check_in": "",
                        "last_check_out": "",
                        "date": "10-dec"
                    }, {
                        "first_check_in": "",
                        "last_check_out": "",
                        "date": "11-dec"
                    }]
            },
            {
                "profile_id": "alskdjflaskjdf",
                "first_name": "sumit",
                "last_name": "sumit",
                "profile_picture": "",

                "data": [
                    {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm"
                    }, {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm"
                    }, {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm"
                    }, {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm"
                    }, {
                        "first_check_in": "",
                        "last_check_out": ""
                    }, {
                        "first_check_in": "",
                        "last_check_out": ""
                    }, {
                        "first_check_in": "",
                        "last_check_out": ""
                    }]
            }
        ],
        "date_range_type": "daily/weekly/monthly"
    }

    date_range_type = "weekly"
    timesheet_data = {
        "start_date": start_date,
        "end_date": end_date,
        "date_range": date_range,
        "people": people,
        "date_range_type": date_range_type
    }
    # print('---timesheet_data---', timesheet_data)

    return timesheet_data


def get_timesheet_class_by_date_range(request,
                                      role_list=None,
                                      all_profile_qs=None,
                                      date_range_type=None,
                                      start_date=None,
                                      end_date=None,
                                      class_name=None,
                                      section=None
                                      ):
    # print('---request---', request)
    # print('---all_profile_qs---', all_profile_qs)
    # print('---start_date---1---', start_date)
    # print('---end_date---1---', end_date)
    # print('----------')

    school_id = request.user.profile.school_id
    if not role_list:
        role_list = ['student']

    if not all_profile_qs:
        all_profile_qs = Profile.objects.filter(
            school_id=school_id,
            role__in=role_list,
            class_name=class_name,
            section=section
        )

    today = datetime.date.today()
    today = datetime.datetime.combine(today, datetime.datetime.min.time())

    dt = today
    if not start_date:
        # weekday (Monday =0 Sunday=6)
        # isoweekday (Monday =1 Sunday=6)
        start_date = dt - datetime.timedelta(days=dt.weekday())

    if not end_date:
        end_date = start_date + datetime.timedelta(days=6)

    # create date_range start
    day_full_name = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    day_abbr_name = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    day_init_name = ['M', 'T', 'W', 'T', 'F', 'S', 'S']
    date_range = []
    date_list = []
    date_modified = start_date
    nbDaysbtw2dates = 1
    while date_modified <= end_date:
        day_init = day_init_name[date_modified.weekday()]
        date_number = date_modified.day
        date_range.append({
            "day": day_init,
            "date": date_number
        })
        date_list.append(date_modified)

        date_modified += datetime.timedelta(days=nbDaysbtw2dates)
    # create date_range end

    people = []

    attendance_filter_qs = AttendanceModel.objects.filter(
        attendance_taker__school_id=school_id,
        period_id__class_name=class_name,
        period_id__section=section,
        attendance_date__in=date_list,
        attempt_status="success"
    ).values(
        'attendance_date',
        'attendance_status'
    ).annotate(
        formatted_date=Func(
            F('attendance_date'),
            Value('dd-mm-YYYY'),
            function='to_char',
            output_field=CharField()
        )
    )
    # print('-- attendance_filter_qs --', attendance_filter_qs)

    # start = datetime.datetime.now()
    # print('start-------1----',start)

    """
    # LOGIC 1 - by sam - create people start
    people = []
    if all_profile_qs:
        for item in all_profile_qs:

            profile_url = None
            if item.profile_picture:
                profile_url = item.profile_picture.url

            # get consolidated data of given date range for each profile
            data = get_date_range_presence_by_profile_class(request,
                                                            single_profile_qs=item,
                                                            attendance_filter_qs=attendance_filter_qs,
                                                            date_list=date_list,
                                                            )

            people.append({
                "profile_id": str(item.profile_id),
                "first_name": item.user.first_name,
                "last_name": item.user.last_name,
                "father_name": item.father_name,
                "profile_url": profile_url,
                "data": data
            })
    else:
        pass
    # LOGIC 1 - by sam - create people end
    """

    # print('end-------1----', datetime.datetime.now()-start)

    # start = datetime.datetime.now()
    # print('start-------2----', start)

    print('---')
    # sample data
    # LOGIC 1 SAMPLE OUTPUT
    # people = [{
    # 	'profile_id': '10936583-57e8-45cd-abb0-3834807d7918',
    # 	'first_name': 'labhya',
    # 	'last_name': 'mittal',
    # 	'father_name': 'himanshu',
    # 	'profile_url': '/media/profile_picture/labhya.png',
    # 	'data': [{
    # 		'status': 'present',
    # 		'date': '2023-01-02'
    # 	}, {
    # 		'status': 'present',
    # 		'date': '2023-01-03'
    # 	}, {
    # 		'status': '',
    # 		'date': '2023-01-04'
    # 	}, {
    # 		'status': '',
    # 		'date': '2023-01-05'
    # 	}, {
    # 		'status': '',
    # 		'date': '2023-01-06'
    # 	}, {
    # 		'status': '',
    # 		'date': '2023-01-07'
    # 	}, {
    # 		'status': '',
    # 		'date': '2023-01-08'
    # 	}]
    # }, {
    # 	'profile_id': '0760a904-5098-4553-bd48-ac958b1aea37',
    # 	'first_name': 'rishu',
    # 	'last_name': 'mittal',
    # 	'father_name': 'himanshu',
    # 	'profile_url': '/media/profile_picture/rishu_1GcgLdR.png',
    # 	'data': [{
    # 		'status': 'absent',
    # 		'date': '2023-01-02'
    # 	}, {
    # 		'status': 'present',
    # 		'date': '2023-01-03'
    # 	}, {
    # 		'status': '',
    # 		'date': '2023-01-04'
    # 	}, {
    # 		'status': '',
    # 		'date': '2023-01-05'
    # 	}, {
    # 		'status': '',
    # 		'date': '2023-01-06'
    # 	}, {
    # 		'status': '',
    # 		'date': '2023-01-07'
    # 	}, {
    # 		'status': '',
    # 		'date': '2023-01-08'
    # 	}]
    # }]

    # print('---date_list---', date_list)
    print('---')

    # """
    # LOGIC 2 - by harendra - start
    all_profile_list = list(all_profile_qs.values_list('profile_id', flat=True))
    all_profile_list = [str(item) for item in all_profile_list]
    # print('---all_profile_list---', all_profile_list)

    attendance_filter_dict = list(attendance_filter_qs)
    people = data_processor(attendance_filter_dict, date_list, all_profile_list)
    # LOGIC 2 - by harendra - end
    # """
    # print('end-------2----', datetime.datetime.now()-start)

    # print('---attendance_filter_dict---', attendance_filter_dict)

    print('---')
    # sample data
    # LOGIC 2 SAMPLE OUTPUT
    # people = [{
    #     'first_name': 'labhya',
    #     'last_name': 'mittal',
    #     'father_name': 'himanshu',
    #     'profile_url': '/media/profile_picture/labhya.png',
    #     'profile_id': '10936583-57e8-45cd-abb0-3834807d7918',
    #     'data': [{
    #         'status': 'present',
    #         'formatted_date': '02-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 2)
    #     }, {
    #         'status': 'present',
    #         'formatted_date': '03-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 3)
    #     }, {
    #         'status': '',
    #         'formatted_date': '04-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 4)
    #     }, {
    #         'status': '',
    #         'formatted_date': '05-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 5)
    #     }, {
    #         'status': '',
    #         'formatted_date': '06-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 6)
    #     }, {
    #         'status': '',
    #         'formatted_date': '07-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 7)
    #     }, {
    #         'status': '',
    #         'formatted_date': '08-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 8)
    #     }]
    # }, {
    #     'first_name': 'rishu',
    #     'last_name': 'mittal',
    #     'father_name': 'himanshu',
    #     'profile_url': '/media/profile_picture/rishu_1GcgLdR.png',
    #     'profile_id': '0760a904-5098-4553-bd48-ac958b1aea37',
    #     'data': [{
    #         'status': 'absent',
    #         'formatted_date': '02-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 2)
    #     }, {
    #         'status': 'present',
    #         'formatted_date': '03-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 3)
    #     }, {
    #         'status': '',
    #         'formatted_date': '04-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 4)
    #     }, {
    #         'status': '',
    #         'formatted_date': '05-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 5)
    #     }, {
    #         'status': '',
    #         'formatted_date': '06-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 6)
    #     }, {
    #         'status': '',
    #         'formatted_date': '07-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 7)
    #     }, {
    #         'status': '',
    #         'formatted_date': '08-01-2023',
    #         'attendance_date': datetime.date(2023, 1, 8)
    #     }]
    # }]

    timesheet_data = {
        "start_date": "5-dec-2022",
        "end_date": "11-dec-2022",
        "date_range": [
            {
                "day": "M",
                "date": 5
            }, {
                "day": "T",
                "date": 6
            }, {
                "day": "W",
                "date": 7
            }, {
                "day": "T",
                "date": 8
            }, {
                "day": "F",
                "date": 9
            }, {
                "day": "S",
                "date": 10
            }, {
                "day": "S",
                "date": 11
            }],
        "people": [
            {
                "profile_id": "alskdjflaskjdf",
                "first_name": "sumit",
                "last_name": "sumit",
                "profile_picture": "/media/profile_picture/pp_1_vKeMv12.jpg",

                "data": [
                    {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm",
                        "date": "5-dec"
                    },
                    {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm",
                        "date": "6-dec"
                    }, {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm",
                        "date": "7-dec"
                    }, {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm",
                        "date": "8-dec"
                    }, {
                        "first_check_in": "",
                        "last_check_out": "",
                        "date": "9-dec"
                    }, {
                        "first_check_in": "",
                        "last_check_out": "",
                        "date": "10-dec"
                    }, {
                        "first_check_in": "",
                        "last_check_out": "",
                        "date": "11-dec"
                    }]
            },
            {
                "profile_id": "alskdjflaskjdf",
                "first_name": "sumit",
                "last_name": "sumit",
                "profile_picture": "",

                "data": [
                    {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm"
                    }, {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm"
                    }, {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm"
                    }, {
                        "first_check_in": "8:00 am",
                        "last_check_out": "4:00 pm"
                    }, {
                        "first_check_in": "",
                        "last_check_out": ""
                    }, {
                        "first_check_in": "",
                        "last_check_out": ""
                    }, {
                        "first_check_in": "",
                        "last_check_out": ""
                    }]
            }
        ],
        "date_range_type": "daily/weekly/monthly"
    }

    date_range_type = "weekly"
    timesheet_data = {
        "start_date": start_date,
        "end_date": end_date,
        "date_range": date_range,
        "people": people,
        "date_range_type": date_range_type
    }
    # print('---timesheet_data---', timesheet_data)

    return timesheet_data


def get_date_range_check_in_out_by_profile_old(request, single_profile_qs, start_date=None, end_date=None,
                                               date_list=None):
    start_date = start_date
    end_date = end_date

    # to fix __range or __get_date_range_check_in_out_by_profile gte/__lte : due to not include end_date so add one day
    new_end = end_date + datetime.timedelta(days=1)

    # LOGIC 0
    # staff_at_qs = StaffAttendanceModel.objects.filter(taken_for=single_profile_qs,
    #                                                   attendance_type__in=['in', 'out'],
    #                                                   attempt_status='success',
    #                                                   ).annotate(hour=F('attendance_date')) \
    #     .order_by('-attendance_date')
    # print('--staff_at_qs == == -- ----- --',staff_at_qs.values())

    # LOGIC 1
    staff_at_qs = StaffAttendanceModel.objects.filter(taken_for=single_profile_qs,
                                                      attendance_type__in=['in', 'out'],
                                                      attempt_status='success',
                                                      attendance_date__range=[start_date, new_end]
                                                      ).order_by('-attendance_date')
    # print('--staff_at_qs == == -- range --', staff_at_qs.values())

    # LOGIC 2
    # staff_at_qs = StaffAttendanceModel.objects.filter(taken_for=single_profile_qs,
    #                                                   attendance_type__in=['in', 'out'],
    #                                                   attempt_status='success',
    #                                                   attendance_date__gte=start_date,
    #                                                   attendance_date__lte=new_end
    #                                                   ).order_by('-attendance_date')
    # print('--staff_at_qs == == -- gte -- lte --',staff_at_qs.values())

    data = []

    less_than_date = start_date
    previous_staff_at_qs = StaffAttendanceModel.objects.filter(taken_for=single_profile_qs,
                                                               attendance_type__in=['in', 'out'],
                                                               attempt_status='success',
                                                               attendance_date__lte=less_than_date
                                                               ).order_by('attendance_date')

    previous_staff_at_qs_entry = previous_staff_at_qs.last()

    today = datetime.datetime.now()
    is_previous_check_out = False
    is_previous_check_in = False
    previous_last_check_in_date = None
    if previous_staff_at_qs_entry and previous_staff_at_qs_entry.attendance_type == "in":
        is_previous_check_in = True
        previous_last_check_in_date = previous_staff_at_qs_entry.attendance_date
        print("previous_last_check_in_date--", previous_last_check_in_date)

    for date_ in date_list:
        date_str = str(date_.date())
        date_only = date_.date()
        staff_at_qs_date = staff_at_qs.filter(attendance_date__date=date_only)
        staff_at_qs_date_for_check_in = staff_at_qs_date.filter(attendance_type='in').order_by('attendance_date')
        staff_at_qs_date_for_check_out = staff_at_qs_date.filter(attendance_type='out').order_by('attendance_date')

        first_check_in = ""
        last_check_out = ""

        is_continuous = False
        if date_ < today:
            if is_previous_check_in:
                is_continuous = True

        # for check in
        first_check_in_attendance_date = None
        last_check_in_attendance_date = None
        if staff_at_qs_date_for_check_in:
            first_check_in_attendance_date = staff_at_qs_date_for_check_in.first().attendance_date
            first_check_in_attendance_date = first_check_in_attendance_date.astimezone()
            check_in_str_time = first_check_in_attendance_date.strftime("%I:%M %p")
            first_check_in = check_in_str_time
            is_previous_check_in = True
            last_check_in_attendance_date = staff_at_qs_date_for_check_in.last().attendance_date
        else:
            pass

        # for check out
        if staff_at_qs_date_for_check_out:
            last_check_out_attendance_date = staff_at_qs_date_for_check_out.last().attendance_date
            last_check_out_attendance_date = last_check_out_attendance_date.astimezone()
            check_out_str_time = last_check_out_attendance_date.strftime("%I:%M %p")
            ### old logic
            last_check_out = check_out_str_time
            is_previous_check_out = True

            # new logic to hide same day multi check-out
            if first_check_in and first_check_in_attendance_date > last_check_out_attendance_date:
                pass
            else:
                # last_check_out = check_out_str_time
                is_previous_check_in = False

            if last_check_in_attendance_date and last_check_in_attendance_date > last_check_out_attendance_date:
                is_previous_check_in = True

        data.append({
            "first_check_in": first_check_in,
            "last_check_out": last_check_out,
            "date": date_str,
            "is_continuous": is_continuous,
            "previous_last_check_in_date": previous_last_check_in_date
        })

        if last_check_in_attendance_date:
            previous_last_check_in_date = last_check_in_attendance_date

    return_data = data
    # print('---return_data---',return_data)
    return return_data


# todo - ON HOLD
def get_date_range_check_in_out_by_profile_new(request, single_profile_qs, start_date=None, end_date=None,
                                               date_list=None):
    start_date = start_date.date()
    end_date = end_date.date()
    # to fix __range or __gte/__lte : due to not include end_date so add one day
    new_end = end_date + datetime.timedelta(days=1)

    staff_at_qs = StaffAttendanceModel.objects.filter(
        # taken_for=single_profile_qs,
        attendance_type__in=['in', 'out'],
        attempt_status='success',
        attendance_date__gte=start_date,
        attendance_date__lte=new_end
    ).annotate(profile_id=F('taken_for__profile_id'),
               first_name=F('taken_for__user__first_name'),
               last_name=F('taken_for__user__last_name'),
               profile_picture=F('taken_for__profile_picture')) \
        .order_by('-attendance_date')

    # print('--staff_at_qs == == --',staff_at_qs.values())

    all_data_dict = {}

    data = []
    person_data = []
    for date_ in date_list:
        staff_at_qs_date = staff_at_qs.filter(attendance_date__date=date_)
        date = date_
        date_str = datetime.datetime.strftime(date, "%d/%m/%y")
        if staff_at_qs_date:
            staff_at_qs = staff_at_qs_date
            staff_at_qs = staff_at_qs.order_by('-attendance_date')

            is_first_checked_in = False
            is_first_checked_out = False

            for item in staff_at_qs:
                person = item.profile_id

                if person in all_data_dict:
                    person_data = all_data_dict[person]
                else:
                    all_data_dict[person] = []

                first_check_in = ""
                last_check_out = ""
                if item.attendance_type == "in" and not is_first_checked_in:
                    attendance_date = item.attendance_date
                    str_time = attendance_date.strftime("%I:%M %p")
                    first_check_in = str_time
                    is_first_checked_in = True
                elif item.attendance_type == "out" and not is_first_checked_out:
                    attendance_date = item.attendance_date
                    str_time = attendance_date.strftime("%I:%M %p")
                    last_check_out = str_time
                    is_first_checked_out = True
                date = item.attendance_date
                date_str = datetime.datetime.strftime(date, "%d/%M/%y")

                person_data.append({
                    "first_check_in": first_check_in,
                    "last_check_out": last_check_out,
                    "date": date_str
                })
                all_data_dict[person] = person_data

        # else:
        #     data.append({
        #         "first_check_in": "",
        #         "last_check_out": "",
        #         "date": date_str
        #     })

    return_data = all_data_dict
    print('--return_data--', return_data)
    return return_data


def get_date_range_check_in_check_out_attendance_success(request, profile):
    profile = profile
    # ######################

    # print('---today---', today)
    staff_at_qs = StaffAttendanceModel.objects.filter(taken_for=profile, attendance_type__in=['in', 'out'],
                                                      attempt_status='success').order_by('-attendance_date')
    # print('---staff_at_qs---', staff_at_qs)

    # todo - double check if user face in media
    # default
    is_check_in = False
    is_check_out = False

    staff_at_in_qs = staff_at_qs.filter(attendance_type='in')
    staff_at_out_qs = staff_at_qs.filter(attendance_type='out')

    in_count = staff_at_in_qs.count()
    out_count = staff_at_out_qs.count()

    if in_count == 0:
        is_check_in = False
    elif in_count == 1:
        is_check_in = True
    elif in_count > 1:
        is_check_in = True
        message = 'Something went wrong...!!! [more than 1 checkin]'

    if out_count == 0:
        is_check_out = False
    elif out_count == 1:
        is_check_out = True
    elif out_count > 1:
        is_check_out = True
        message = 'Something went wrong...!!! [more than 1 checkout]'

    # print('---is_check_in---', is_check_in)
    # print('---is_check_out---', is_check_out)

    # ######################

    return_data = {
        'is_check_in': is_check_in,
        'is_check_out': is_check_out,
        'staff_at_qs': staff_at_qs,
    }
    return return_data


@login_required
def timesheet_detail_view(request, **kwargs):
    profile_id = kwargs.get('profile_id')
    profile_qs = get_object_or_404(Profile, pk=profile_id)

    template_name = "app_smart_attendance/html/timesheet_overview.html"

    date_text = kwargs.get('date_text')
    #
    # is_date_range_type_valid = True
    # is_date_valid = True
    # is_next_url_valid = True
    #
    # # check if "date" and its format is correct or not
    # date_from_date_text = None
    # try:
    #     date_from_date_text = datetime.datetime.strptime(date_text, '%Y-%m-%d')
    # except Exception as e:
    #     print('--date Exception occurred--', e)
    #     is_date_valid = False
    #
    # # set a valid date to now
    # if not is_date_valid:
    #     now = datetime.datetime.now()
    # else:
    #     now = date_from_date_text
    #
    # # finding and setting first date of any type of range
    # if date_range_type == 'day':
    #     # set today
    #     now = now
    #     date_text = now.strftime('%Y-%m-%d')
    # elif date_range_type == 'week':
    #     # set first day of week
    #     now = now + datetime.timedelta(days=-now.weekday())
    #     date_text = now.strftime('%Y-%m-%d')
    # elif date_range_type == 'month':
    #     # set first day of month
    #     now = now.replace(day=1)
    #     date_text = now.strftime('%Y-%m-%d')
    #
    # current_range, previous_date, next_date = get_date_range(now, date_range_type)
    # start_date = current_range[0]
    # end_date = current_range[1]
    #
    # kwargs_val = {
    #     'date_text': date_text,
    #     'date_range_type': date_range_type
    # }
    # redirect_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)
    #
    # print('redirect_url---', redirect_url)
    # if not is_date_valid or not is_date_range_type_valid:
    #     print('PAGE REDIRECTED----')
    #     return redirect(redirect_url)
    #
    # #######################################
    #
    # school_id = request.user.profile.school_id
    # role_list = ['admin', 'staff', 'teacher', 'non_teacher', 'school_admin']
    #
    # all_profile_qs = Profile.objects.filter(school_id=school_id, role__in=role_list).exclude(
    #     profile_id=request.user.profile.profile_id)
    #
    # userprofile = request.user.profile
    # all_profile_qs = [userprofile] + list(all_profile_qs)
    #
    # return_data = get_timesheet_by_date_range(
    #     request=request,
    #     all_profile_qs=all_profile_qs,
    #     start_date=start_date,
    #     end_date=end_date,
    #     date_range_type=date_range_type
    # )
    #
    # """
    # day = OVERVIEW_URL/CURRENT_DATE/day
    # week = OVERVIEW_URL/CURRENT_DATE/week
    # month = OVERVIEW_URL/CURRENT_DATE/month
    #
    # previous = OVERVIEW_URL/previous_first_date_of_range/CURRENT_TYPE
    # next = OVERVIEW_URL/next_first_date_of_range/CURRENT_TYPE		if next_first_date_of_range > TODAY ---- disable this button -- NO_URL
    #
    #
    # OVERVIEW_URL=http://127.0.0.1:8001/app/timesheet/overview/
    # CURRENT_DATE=current_first_date_of_range
    # CURRENT_TYPE=date_range_type
    # """
    #
    # today = datetime.datetime.now()
    #
    # today_text = today.strftime('%Y-%m-%d')
    # kwargs_val = {
    #     'date_text': today_text,
    #     'date_range_type': 'day'
    # }
    # day_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)
    #
    # today_text = (today + datetime.timedelta(days=-today.weekday())).strftime('%Y-%m-%d')
    # kwargs_val = {
    #     'date_text': today_text,
    #     'date_range_type': 'week'
    # }
    # week_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)
    #
    # today_text = (today.replace(day=1)).strftime('%Y-%m-%d')
    # kwargs_val = {
    #     'date_text': today_text,
    #     'date_range_type': 'month'
    # }
    # month_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)
    #
    # previous_date_text = previous_date.strftime('%Y-%m-%d')
    # kwargs_val = {
    #     'date_text': previous_date_text,
    #     'date_range_type': date_range_type
    # }
    # previous_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)
    #
    # if end_date > today:
    #     is_next_url_valid = False
    #
    # next_url = ""
    # if is_next_url_valid:
    #     next_date_text = next_date.strftime('%Y-%m-%d')
    #     kwargs_val = {
    #         'date_text': next_date_text,
    #         'date_range_type': date_range_type
    #     }
    #     next_url = reverse('app_smart_attendance:timesheet_custom_all_view', kwargs=kwargs_val)
    #
    # return_data['is_next_url_valid'] = is_next_url_valid
    # return_data['date_range_type'] = date_range_type
    # return_data['day_url'] = day_url
    # return_data['week_url'] = week_url
    # return_data['month_url'] = month_url
    # return_data['previous_url'] = previous_url
    # return_data['next_url'] = next_url

    return_data = {}
    context_payload = return_data

    return render(request, template_name, context_payload)


@login_required
def demo_view(request):
    template_name = "app_smart_attendance/html/toggle_camera_demo.html"
    print('request.api_url---', request.api_url)
    context_payload = {}

    return render(request, template_name, context_payload)


@login_required
def report_view(request):
    school_id = request.user.profile.school_id
    role = request.user.profile.role

    context_payload = {}

    announcement_qs = AnnouncementModel.objects.filter(school_id=school_id)
    school_profile_qs = Profile.objects.filter(school_id=school_id)

    context_payload["announcement_qs"] = announcement_qs

    if request.organisation_type == "school":
        if role == "school_admin" or role == "teacher":
            # total_student_count
            total_student_qs = school_profile_qs.filter(role="student")
            total_student_count = total_student_qs.count()

            # total_teacher_count
            total_teacher_qs = school_profile_qs.filter(role="teacher")
            total_teacher_count = total_teacher_qs.count()

            # total_school_admin_count
            total_school_admin_qs = school_profile_qs.filter(role="school_admin")
            total_school_admin_count = total_school_admin_qs.count()

            # total_class_count
            total_class_qs = PeriodModel.objects.filter(school_id=school_id).order_by('class_name').values(
                "class_name").distinct()
            total_class_count = total_class_qs.count()

            template_name = "app_smart_attendance/html/report_school.html"
            context_payload["total_student_count"] = total_student_count
            context_payload["total_teacher_count"] = total_teacher_count
            context_payload["total_school_admin_count"] = total_school_admin_count
            context_payload["total_class_count"] = total_class_count

    elif request.organisation_type == "company":
        total_admin_count = school_profile_qs.filter(role="admin").count()
        total_staff_count = school_profile_qs.filter(role="staff").count()
        context_payload["total_admin_count"] = total_admin_count
        context_payload["total_staff_count"] = total_staff_count
        template_name = "app_smart_attendance/html/report_company.html"

    elif request.organisation_type == "anganwadi":
        total_admin_count = school_profile_qs.filter(role="admin").count()
        total_staff_count = school_profile_qs.filter(role="staff").count()
        context_payload["total_admin_count"] = total_admin_count
        context_payload["total_staff_count"] = total_staff_count
        template_name = "app_smart_attendance/html/report_anganwadi.html"

    return render(request, template_name, context_payload)


@login_required
def get_report_view_ajax(request):
    message = "fail"
    if request.method == "POST":
        data = request.POST
        data._mutable = True
        data.pop("csrfmiddlewaretoken")

        date_text = data["date_text"]

        school_id = request.user.profile.school_id
        school_profile_qs = Profile.objects.filter(school_id=school_id)

        total_student_qs = school_profile_qs.filter(role="student")
        total_student_count = total_student_qs.count()

        selected_date = datetime.datetime.strptime(date_text, '%m/%d/%Y')
        print("selected_date---", selected_date)

        total_attendance_qs = AttendanceModel.objects.filter(period_id__school_id=school_id, attempt_status="success",
                                                             attendance_date=selected_date)

        #
        # Breakfast.objects.all(). \
        #     annotate(total_food=Sum(
        #     F('count_eggs ') + F('count_bacon'))
        # ). \
        #     order_by('total_food')
        #

        # total_attendance_qs = total_attendance_qs.annotate(pp=F('present')/Sum(F('absent')+F('present')))

        # print('--total_attendance_qs--',total_attendance_qs)

        chart_date = selected_date.strftime('%d-%m-%Y')

        temp_absent = {}
        temp_present = {}
        temp = {}
        total_present = 0
        total_absent = 0
        total_class = 0
        for item in total_attendance_qs:
            attendance_date = item.attendance_date

            period_id = item.period_id
            section = period_id.section
            class_name = period_id.class_name + section  # add section in class_name
            period_number = period_id.period_number

            attendance_status = item.attendance_status
            present = 0
            absent = 0
            avg = 0

            for student in attendance_status:
                if student["status"] == "present":
                    present += 1
                elif student["status"] == "absent":
                    absent += 1

            try:
                temp_absent[class_name] += absent
            except Exception as e:
                temp_absent[class_name] = absent

            try:
                temp_present[class_name] += present
            except Exception as e:
                temp_present[class_name] = present

            try:
                # 2
                temp[class_name]["no_of_periods"] += 1
                temp[class_name]["absent"] = temp[class_name]["absent"] + absent
                temp[class_name]["present"] = temp[class_name]["present"] + present
                temp[class_name]["avg"] = (temp[class_name]["avg"] + float(
                    temp[class_name]["present"] / (temp[class_name]["present"] + temp[class_name]["absent"])) * 100) / 2
            except Exception as e:
                # 1
                avg = float(present / (present + absent)) * 100
                no_of_periods = 1
                temp[class_name] = {
                    "absent": absent,
                    "present": present,
                    "avg": avg,
                    "no_of_periods": no_of_periods
                }

            total_present += present
            total_absent += absent

        school_avg_att = 0

        category = []
        p_data = []
        a_data = []
        print('--temp--', temp)
        for i, (k, v) in enumerate(temp.items()):
            # print(i, k, v)
            category.append({
                "label": k
            })
            p_data.append({
                "value": (v["present"] / (v["absent"] + v["present"])) * 100
            })
            a_data.append({
                "value": (v["absent"] / (v["absent"] + v["present"])) * 100
            })
            if school_avg_att == 0:
                school_avg_att = v["avg"]
            else:
                school_avg_att = float(school_avg_att + v["avg"]) / 2

        print('--category--', category)
        print('--p_data--', p_data)
        print('--a_data--', a_data)

        chart = {
            "caption": "Attendance Completed",
            "subCaption": str(chart_date) + " <strong><u>(" + str(
                round(school_avg_att, 1)) + "% AVG. Attendance)</u></strong>",
            "xAxisname": "Class",
            "yAxisName": "Students (%)",
            "showSum": "1",
            "numberPrefix": "",
            "theme": "fusion"
        }

        categories = [{
            "category": category
        }]

        dataset = [
            {
                "seriesname": "Present",
                "data": p_data
            }, {
                "seriesname": "Absent",
                "data": a_data
            }]

        chart_data = {
            "chart": chart,
            "categories": categories,
            "dataset": dataset
        }
        # chart_data = json.dumps(chart_data)
        print("chart_data---", chart_data)

        total_class = len(temp)
        total_attendance_taken = total_attendance_qs.count()

    message = "success"

    payload = {
        "message": message,
        "input": data,
        "data": {
            "temp_present": temp_present,
            "temp_absent": temp_absent,
            "temp": temp,
            "total_present": total_present,
            "total_absent": total_absent,
            "total_attendance_count": school_avg_att,
            "chart_data": chart_data,
        },
    }

    return JsonResponse(payload)


@login_required
def classes_view(request):
    template_name = "app_smart_attendance/html/classes.html"
    print('request.api_url---', request.api_url)
    context_payload = {}

    return render(request, template_name, context_payload)


@login_required
def settings_view(request):
    template_name = "app_smart_attendance/html/settings.html"
    print('request.api_url---', request.api_url)
    context_payload = {}

    return render(request, template_name, context_payload)


@login_required
def announcement_view(request):
    template_name = "app_smart_attendance/module/announcement/html/announcement.html"
    school_id = request.user.profile.school_id
    announcement_filter_qs = AnnouncementModel.objects.filter(school_id=school_id)
    # print('--announcement_filter_qs---', announcement_filter_qs.values())

    today = datetime.datetime.now()
    context_payload = {
        'announcement_qs': announcement_filter_qs,
        'today': today,
    }

    return render(request, template_name, context_payload)


@login_required
def people_view(request):
    template_name = "app_smart_attendance/html/people.html"
    print('request.api_url---', request.api_url)
    context_payload = {}

    return render(request, template_name, context_payload)


def create_dynamic_class_name_section(school_id):
    # class_name and sections
    class_name_and_section_qs = PeriodModel.objects.filter(school_id=school_id).values('class_name').order_by(
        'class_name').annotate(
        sections=StringAgg('section', delimiter=',', ordering='section', distinct=True)
    )
    # print('----class_name_and_section_qs---', class_name_and_section_qs)

    class_name_section_dict = {}
    for item in class_name_and_section_qs:
        class_name_section_dict[item['class_name']] = item['sections'].split(',')

    # print('----class_name_section_dict---', class_name_section_dict)

    # class_name and section and periods
    # period_qs = PeriodModel.objects.filter(school_id=school_id).values('class_name','section').order_by('class_name').annotate(
    #     period_numbers=StringAgg('period_number', delimiter=',', ordering='period_number')
    # )

    # < QuerySet[{'class_name': '1', 'section': 'a', 'new_class_name': '1a', 'periods': '1,2'}, ...] >
    # by harendra
    # period_qs = PeriodModel.objects.filter(school_id=school_id).values('class_name', 'section').order_by('class_name',
    #                                                                                                      'section') \
    #     .annotate(new_class_name=Concat('class_name', 'section', output_field=CharField())) \
    #     .annotate(
    #     periods=StringAgg('period_number', delimiter=',', ordering='period_number', distinct=True)
    # )

    # new updated
    period_qs = PeriodModel.objects.filter(school_id=school_id).values('class_name', 'section', 'day').order_by(
        'class_name',
        'section') \
        .annotate(new_class_name=Concat('class_name', 'section', output_field=CharField())) \
        .annotate(
        periods=JSONBAgg('period_number', distinct=True)
    ).annotate(
        periods_id=JSONBAgg('period_id', distinct=True)
    )
    # print('--period_qs---', period_qs)

    periods_dict = PeriodModel.objects.filter(school_id=school_id).values('class_name', 'section', 'day').order_by(
        'class_name',
        'section') \
        .annotate(new_class_name=Concat('class_name', 'section', output_field=CharField())) \
        .annotate(
        periods=JSONBAgg('period_number', distinct=True)
    ).annotate(
        periods_dict=JSONObject(period_number=F("period_number"), period_id=F("period_id"))
    )
    # print('--periods_dict---', periods_dict)

    # period_qs = PeriodModel.objects.filter(school_id=school_id).values('class_name', 'section').order_by('class_name',
    #                                                                                                      'section') \
    #     .annotate(new_class_name=Concat('class_name', 'section', output_field=CharField())) \
    #     .annotate(
    #     periods=StringAgg('period_number', delimiter=',', ordering='period_number', distinct=True))\
    #     .annotate(
    #     periods_id=StringAgg('period_id', delimiter=',', ordering='period_number', distinct=True, )
    # )

    # print('period_qs--jsonb_agg-', period_qs)

    temp = {}
    for k, v in groupby(list(period_qs), key=lambda x: x['day']):
        if k in temp:
            temp[k] = temp[k] + list(v)
        else:
            temp[k] = list(v)
        # print('---k,v---', k,v)

    # print('---temp---', temp)

    class_name_section_list = []
    class_name_section_period_dict = {}
    period_id_map_dict = {}
    # print(" -----period_qs-----", period_qs)
    for item in period_qs:
        class_name_section_period_dict[item['new_class_name'] + "_" + item["day"]] = item['periods']
        # print(" -----item 000000 -----", item)

        class_name = ""
        if item['class_name']:
            class_name = item['class_name']
        section = ""
        if item['section']:
            section = item['section']

        str_value = ""
        if class_name and section:
            str_value = class_name + '-' + section
        elif class_name:
            str_value = class_name
        else:
            print('---------- else --------------')

        # print('---str_value---',str_value)
        class_name_section_list.append(str_value)

        for index, value in enumerate(item['periods']):
            new_class_name_period = item['new_class_name'] + value + "_" + item["day"]
            period_id_map_dict[new_class_name_period] = item['periods_id'][index]
            # print(" -----new_class_name_period-----", new_class_name_period)
            # print(" -----item['periods_id'][index]-----", item['periods_id'][index])

    class_name_section_list = list(sorted(set(class_name_section_list)))

    # print('class_name_section_list---', class_name_section_list)
    # print('period_id_map_dict---', period_id_map_dict)
    # print('class_name_section_period_dict---', class_name_section_period_dict)

    # period_list = []
    # class_name_section_dict = {}
    # # class_name_section_period_dict = {}
    #
    # if period_qs:
    #     period_list = list(period_qs)
    #     for item in period_list:
    #         try:
    #             if item['class_name'] in class_name_section_dict:
    #                 temp_v = class_name_section_dict[item['class_name']]
    #                 temp_list = []
    #                 if temp_v:
    #                     temp_list = list(sorted(set(temp_v + [item['section']])))
    #
    #                 class_name_section_dict[item['class_name']] = temp_list
    #             else:
    #                 temp_v = []
    #                 class_name_section_dict[item['class_name']] = [item['section']]
    #
    #         except Exception as e:
    #             # print('Exception----', e, "---", temp_v, item['class_name'], item['section'])
    #             pass

    # print("class_name_section_period_dict---",class_name_section_period_dict)

    return class_name_section_dict, class_name_section_period_dict, period_id_map_dict, class_name_section_list


DEFAULT_PASSWORD = "Welcome@123"


@login_required
def student_register_view_ajax(request):
    if request.method == "POST":
        request.POST._mutable = True
        post_data = request.POST
        data = request.FILES

        post_data_dict = querydict_to_dict(post_data)
        form_text_data_json = post_data_dict["form_text_data_json"]
        form_data_dict = json.loads(form_text_data_json)
        print('-- type(form_data_dict) ', type(form_data_dict))
        print("-- form_data_dict: ", form_data_dict)

        media_type = form_data_dict["media_type"]
        data_dict = querydict_to_dict(data)
        file_key = list(data_dict)[0]

        # 1.
        # register student details in system
        # create student entry in USER model
        # create student entry in PROFILE model
        registered_user = register_student_with_stable_username_by_email(request, form_data_dict)

        file = request.FILES[file_key]
        filename = request.FILES[file_key].name

        # override file name with prefixes
        filename = media_type + "__" + filename

        # 2.
        # save file in MediaCapturedModel
        obj = MediaCapturedModel.objects.create(media_type=media_type)
        getattr(obj, file_key).save(filename, file)

        # notification data
        sender = request.user
        sender_uuid = sender.profile.profile_id
        school_id = request.user.profile.school_id.school_id

        notification_type = 'Notification'
        description = ''

        recipient_role = [
            "school_admin",
            "teacher",
        ]

        action_object = registered_user
        action_object_uuid = action_object.profile.profile_id

        class_name = form_data_dict["class_name"]
        section = form_data_dict["section"]
        media_type = form_data_dict["media_type"]
        role = registered_user.profile.role
        obj_id = str(registered_user.profile.profile_id)

        try:
            wait_folder = settings.FACE_RECOG_UTILS.create_media_folder_for_extraction(str(school_id), role, obj_id)
            extract_images_from_blobs(wait_folder, video_file=os.getcwd() + obj.media_file.url)
            print("wait_folder: ", wait_folder)
        except:
            logger.exception("Exception occurred during Student Registration")
        else:
            push_request_for_face_recog({
                "request_type": "train",
                "mode": "incremental",
                "sender_uuid": str(sender_uuid),
                "notification_type": notification_type,
                "description": description,
                "action_object_uuid": str(action_object_uuid),
                "class_name": class_name,
                "section": section,
                "media_type": media_type,
                "school_id": str(school_id),
                "recipient_role": json.dumps(recipient_role),
                "wait_folder": wait_folder,
                # "role":role
            })

        # 3.
        # save media file instance in profile model
        registered_user.profile.media_id = obj
        registered_user.profile.save()

        # 4.
        # save school data from teacher profile to student profile model
        logged_in_user_profile = request.user.profile
        registered_user.profile.school_id = logged_in_user_profile.school_id

        registered_user.profile.save()

        message = "User registration request submitted, please check notification after some time for more update!"
        payload = {
            "message": message
        }

    return JsonResponse(payload)


@login_required
def user_register_ajax(request):
    if request.method == "POST":
        request.POST._mutable = True
        post_data = request.POST
        data = request.FILES

        post_data_dict = querydict_to_dict(post_data)
        form_text_data_json = post_data_dict["form_text_data_json"]
        form_data_dict = json.loads(form_text_data_json)
        print('-- type(form_data_dict) ', type(form_data_dict))
        print("-- form_data_dict: ", form_data_dict)

        media_type = form_data_dict["media_type"]
        registration_type = form_data_dict["registration_type"]
        data_dict = querydict_to_dict(data)
        file_key = list(data_dict)[0]

        # 1.
        # register student details in system
        # create student entry in USER model
        # create student entry in PROFILE model
        # registered_user = register_student_with_stable_username_by_email(request, form_data_dict)

        profile_id = form_data_dict["profile_id"]
        registered_profile = Profile.objects.get(profile_id=profile_id)
        registered_user = registered_profile.user
        print('---registered_user---', registered_user)

        file = request.FILES[file_key]
        filename = request.FILES[file_key].name

        # override file name with prefixes
        filename = media_type + "__" + filename

        # 2.
        # save file in MediaCapturedModel
        obj = MediaCapturedModel.objects.create(media_type=media_type)
        getattr(obj, file_key).save(filename, file)

        # notification data
        sender = request.user
        sender_uuid = sender.profile.profile_id
        school_id = request.user.profile.school_id.school_id

        notification_type = 'Notification'
        description = ''

        recipient_role = [
            "school_admin",
            "teacher",
        ]

        action_object = registered_user
        action_object_uuid = action_object.profile.profile_id

        try:
            class_name = form_data_dict["class_name"]
        except Exception as e:
            class_name = ""

        try:
            section = form_data_dict["section"]
        except Exception as e:
            section = ""

        media_type = form_data_dict["media_type"]
        role = registered_user.profile.role
        obj_id = str(registered_user.profile.profile_id)

        try:
            wait_folder = settings.FACE_RECOG_UTILS.create_media_folder_for_extraction(str(school_id), role, obj_id)
            extract_images_from_blobs(wait_folder, video_file=os.getcwd() + obj.media_file.url)
            print("wait_folder: ", wait_folder)
        except:
            logger.exception("Exception occurred during Student Registration")
        else:
            push_request_for_face_recog({
                "request_type": "train",
                "sender_uuid": str(sender_uuid),
                "notification_type": notification_type,
                "description": description,
                "action_object_uuid": str(action_object_uuid),
                "class_name": class_name,
                "section": section,
                "media_type": media_type,
                "school_id": str(school_id),
                "recipient_role": json.dumps(recipient_role),
                "wait_folder": wait_folder,
                "registration_type": registration_type
            })

        # 3.
        # save media file instance in profile model
        registered_user.profile.media_id = obj
        registered_user.profile.save()

        # 4.
        # save school data from teacher profile to student profile model
        logged_in_user_profile = request.user.profile
        registered_user.profile.school_id = logged_in_user_profile.school_id

        registered_user.profile.save()

        message = f"{role.upper()} registration request submitted successfully!!!"
        payload = {
            "message": message

        }

    return JsonResponse(payload)


@login_required
def get_profiles_by_role_ajax(request):
    message = "fail"
    response_data = {}
    if request.method == "POST":
        data = request.POST
        data._mutable = True
        data.pop("csrfmiddlewaretoken")
        role = data["role"]

        school_id = request.user.profile.school_id
        if request.organisation_type == "school":
            old_profile_qs = Profile.objects.filter(school_id=school_id, role=role).extra(
                select={'int_class_name': 'CAST(class_name AS CHAR)'},
                order_by=['int_class_name', "section", "user__first_name"]).annotate(first_name=F("user__first_name"),
                                                                                     last_name=F("user__last_name"),
                                                                                     email=F("user__email"),
                                                                                     username=F("user__username"),
                                                                                     ).values()
        elif request.organisation_type == "company":
            old_profile_qs = Profile.objects.filter(school_id=school_id, role=role).annotate(
                first_name=F("user__first_name"),
                last_name=F("user__last_name"),
                email=F("user__email"),
                username=F("user__username"),
            ).values()
        elif request.organisation_type == "anganwadi":
            old_profile_qs = Profile.objects.filter(school_id=school_id, role=role).annotate(
                first_name=F("user__first_name"),
                last_name=F("user__last_name"),
                email=F("user__email"),
                username=F("user__username"),
            ).values()

        old_profile_json = json.loads(json.dumps(list(old_profile_qs), cls=DjangoJSONEncoder))

        message = "success"

    payload = {
        "message": message,
        "input": data,
        "data": old_profile_json,
    }

    return JsonResponse(payload)


@login_required
def get_list_of_all_students_attendance_by_class_name_section_date_ajax(request):
    message = "fail"
    data = {}
    old_attendance_json = []
    is_already_taken = False
    attendance_id = None
    attendance_choice = None
    old_attendance_choice_display = None
    last_edit_date_time = None
    date_time = None
    if request.method == "POST":
        data = request.POST
        data._mutable = True
        data.pop("csrfmiddlewaretoken")

        class_name = data["class_name"]
        section = data["section"]
        period_number = data["period_number"]
        attendance_date = data["attendance_date"]

        school_id = request.user.profile.school_id

        try:
            old_attendance_qs = AttendanceModel.objects.get(attendance_taker__school_id=school_id,
                                                            period_id__class_name=class_name,
                                                            period_id__section=section,
                                                            period_id__period_number=period_number,
                                                            attendance_date=attendance_date,
                                                            attempt_status="success"
                                                            )
            old_attendance_json = old_attendance_qs.attendance_status
            old_attendance_choice_display = old_attendance_qs.get_attendance_choice_display()
            date_time = old_attendance_qs.date_time

            diff = (old_attendance_qs.updated_at - old_attendance_qs.date_time).total_seconds()
            print('diff---', diff)
            if diff > 1:
                last_edit_date_time = old_attendance_qs.updated_at

        except Exception as e:
            old_attendance_qs = AttendanceModel.objects.none()
            print('--- No attendance found Exception ---', e)

        all_profile_qs = Profile.objects.filter(
            school_id=school_id,
            role="student",
            class_name=class_name,
            section=section,
        )

        if old_attendance_qs:
            is_already_taken = True
            attendance_id = old_attendance_qs.attendance_id
            old_attendance_list = []
            for item in old_attendance_json:
                profile_id = item["profile_id"]
                student = all_profile_qs.get(profile_id=profile_id)
                profile_picture_url = None
                if student.profile_picture:
                    profile_picture_url = student.profile_picture.url
                old_attendance_list.append({
                    "name": student.user.first_name + " " + student.user.last_name,
                    "father_name": student.father_name,
                    "profile_id": str(student.profile_id),
                    "profile_picture_url": profile_picture_url,
                    "status": item["status"]
                })
            old_attendance_json = old_attendance_list
        else:
            attendance_id = None
            new_attendance_status_list = []
            for item in all_profile_qs:
                profile_picture_url = None
                if item.profile_picture:
                    profile_picture_url = item.profile_picture.url

                new_attendance_status_list.append({
                    "name": item.user.first_name + " " + item.user.last_name,
                    "father_name": item.father_name,
                    "profile_id": str(item.profile_id),
                    "profile_picture_url": profile_picture_url,
                    "status": ""
                })
            old_attendance_json = new_attendance_status_list

        message = "success"
    else:
        message = "ERROR: Requested method is not POST"

    payload = {
        "message": message,
        "input": data,
        "data": old_attendance_json,
        "is_already_taken": is_already_taken,
        "attendance_id": attendance_id,
        "old_attendance_choice": attendance_choice,
        "old_attendance_choice_display": old_attendance_choice_display,
        "last_edit_date_time": last_edit_date_time,
        "date_time": date_time,
    }

    return JsonResponse(payload)


@login_required
def get_camera_details_ajax(request):
    message = "fail"
    camera_qs_json = {}
    input_data = {}
    if request.method == "POST":
        input_data = request.POST
        input_data._mutable = True
        input_data.pop("csrfmiddlewaretoken")
        camera_id = input_data["camera_id"]
        school_id = request.user.profile.school_id
        camera_qs = CameraModel.objects.filter(camera_id=camera_id, school_id=school_id).values()
        camera_qs_json = json.loads(json.dumps(list(camera_qs), cls=DjangoJSONEncoder))
        message = "success"
    else:
        message = "ERROR: request method not POST"

    payload = {
        "message": message,
        "input": input_data,
        "data": camera_qs_json,
    }
    return JsonResponse(payload)


@login_required
def get_attendance_status_by_profile_id_ajax(request):
    message = "fail"
    attendance_status = None
    data = None
    if request.method == "POST":
        data = request.POST
        data._mutable = True
        data.pop("csrfmiddlewaretoken")
        profile_id = data["profile_id"]

        try:
            profile = Profile.objects.get(profile_id=profile_id)
        except Exception as e:
            print('---Profile Exception---', e)
            profile = Profile.objects.none()

        if profile:
            attendance_status = profile.attendance_status
            message = "success"
        else:
            message = "failure"
    else:
        message = "Method not allowed"

    payload = {
        "message": message,
        "input": data,
        "data": attendance_status,
    }

    return JsonResponse(payload)


@login_required
def get_check_in_out_by_profile_ajax(request):
    message = "fail"
    check_in_out_data = {}
    if request.method == "POST":
        data = request.POST
        data._mutable = True
        data.pop("csrfmiddlewaretoken")
        role = data["role"]
        profile_id = data["profile_id"]

        try:
            profile = Profile.objects.get(profile_id=profile_id)
        except Exception as e:
            print('---Profile Exception---', e)
            profile = Profile.objects.none()

        if profile:
            check_in_out_data = get_old_check_in_out_by_profile(request, profile)

        else:
            message = "failure"

    payload = {
        "message": message,
        "input": data,
        "data": check_in_out_data,
    }

    return JsonResponse(payload)


def get_old_check_in_out_by_profile(request, profile):
    # return_data = get_today_check_in_check_out_attendance_success(request, profile)
    return_data = get_last_check_in_check_out_attendance_success(request, profile)
    is_check_in = return_data["is_check_in"]
    is_check_out = return_data["is_check_out"]
    staff_at_qs = return_data["staff_at_qs"]
    last_entry = None
    if staff_at_qs:
        staff_at_qs = staff_at_qs.order_by('-attendance_date')
        last_entry = staff_at_qs[0]
    print('last_entry---', last_entry)

    staff_at_success_in_qs = staff_at_qs.filter(attendance_type="in")
    staff_at_success_out_qs = staff_at_qs.filter(attendance_type="out")

    # failure_return_data = get_today_check_in_check_out_attendance_failure(request, profile)
    failure_return_data = get_last_check_in_check_out_attendance_failure(request, profile)
    staff_at_failure_qs = failure_return_data["staff_at_failure_qs"]

    staff_at_failure_in_qs = staff_at_failure_qs.filter(attendance_type="in")
    staff_at_failure_out_qs = staff_at_failure_qs.filter(attendance_type="out")
    message = "success"

    staff_at_qs_json = serializers.serialize('json', staff_at_qs)
    staff_at_success_in_qs_json = serializers.serialize('json', staff_at_success_in_qs)
    staff_at_success_out_qs_json = serializers.serialize('json', staff_at_success_out_qs)
    staff_at_failure_in_qs_json = serializers.serialize('json', staff_at_failure_in_qs)
    staff_at_failure_out_qs_json = serializers.serialize('json', staff_at_failure_out_qs)
    staff_at_failure_qs_json = serializers.serialize('json', staff_at_failure_qs)

    # staff_attendance_id
    # attendance_id
    # attendance_type
    # attempt_status
    # loggedin_user
    # taken_by
    # taken_for
    # media_id
    # attendance_status
    # attendance_date
    # staff_attendance_geolocation
    # date_time
    # updated_at

    check_in_out_data = {
        'is_check_in': is_check_in,
        'is_check_out': is_check_out,
        'staff_at_qs': staff_at_qs_json,
        'staff_at_success_in_qs': staff_at_success_in_qs_json,
        'staff_at_success_out_qs': staff_at_success_out_qs_json,
        'staff_at_failure_in_qs': staff_at_failure_in_qs_json,
        'staff_at_failure_out_qs': staff_at_failure_out_qs_json,
        'staff_at_failure_qs': staff_at_failure_qs_json,
        "last_entry": last_entry,
    }
    return check_in_out_data


# create username by email id
@login_required
def register_student_with_stable_username_by_email(request, data):
    # get fields from ajax request

    first_name = data['first_name']
    last_name = data['last_name']
    class_name = data['class_name']
    section = data['section']
    candidate_email_id = data['email']
    blood_group = data['blood_group']
    gender = data['gender']

    print('candidate_email_id--', candidate_email_id)
    print('first_name--', first_name)
    print('last_name--', last_name)
    print('class_name--', class_name)
    print('section--', section)
    print('blood_group--', blood_group)
    print('gender--', gender)

    # new sam code
    try:
        profile_id = data['profile_id']
        print('profile_id--', profile_id)
        profile_get_qs = Profile.objects.get(profile_id=profile_id)
    except Exception as e:
        print('---Exception---', e)
        profile_get_qs = Profile.objects.none()

    print('---profile_get_qs---', profile_get_qs)

    if profile_get_qs:
        candidate_user = profile_get_qs.user
        candidate_user.first_name = first_name
        candidate_user.last_name = last_name
        candidate_user.email = candidate_email_id
    else:
        candidate_user = None

        # check if user is already registered or not for creating and sending user details to user
        user_and_profile_data = {}
        # user not exist and user not invited yet
        check_user_invited = True

        # create new user
        email = candidate_email_id
        temp_username = email.split("@")[0]
        is_final_username = False

        while is_final_username == False:
            try:
                is_user_with_temp_username_exist = User.objects.get(username=temp_username)
                # logic to create dynamic stable unique username
                print("not stable temp_username --- ", temp_username)
                temp_username = str(temp_username) + "2"
            except Exception as e:
                is_final_username = True
                print("is_user_with_temp_username_exist Exception--", e)
                print("stable temp_username --- ", temp_username)

        # stable unique username
        username = temp_username
        password = DEFAULT_PASSWORD
        create_user_and_profile_data = {
            "email": email,
            "username": username,
            "password": password,
            "first_name": first_name,
            "last_name": last_name
        }
        print("create_user_and_profile_data---", create_user_and_profile_data)
        candidate_user = create_user_and_profile_df(request, create_user_and_profile_data)

    candidate_user.profile.class_name = class_name
    candidate_user.profile.section = section
    candidate_user.profile.blood_group = blood_group
    candidate_user.profile.gender = gender
    candidate_user.profile.save()
    candidate_user.save()

    return candidate_user


# create user and create profile
def create_user_and_profile_df(request, data):
    email = data["email"]
    username = data["username"]
    password = data["password"]
    first_name = data["first_name"]
    last_name = data["last_name"]

    # ### logic 1
    # new_user = User.objects.create(
    #     email=email,
    #     username=username,
    # )

    ### logic 2
    user, created = User.objects.get_or_create(
        username=username,
        email=email,
        first_name=first_name,
        last_name=last_name,
    )
    print("user, created ---", user, created)

    user.set_password(password)
    user.save()

    # sam's auto script to create django group like employer, recruiter and candidate in Group model
    # create all django group like employer, recruiter and candidate
    assigned_group = Group.objects.none()
    try:
        assigned_group = Group.objects.get(name='student')
    except Exception as e:
        print("Group.objects.get(name='candidate')---- Exception -----", e)
        print("--- create all necessary Groups ---")
        create_all_necessary_groups(request)
        assigned_group = Group.objects.get(name='student')
    user.groups.add(assigned_group)

    user.profile.role = 'student'
    user.profile.save()

    return user


# set django group
GROUPS = ['admin', 'teacher', 'student']


# create all django group
def create_all_necessary_groups(request):
    print('---def create_all_necessary_groups CALLED---')
    for group in GROUPS:
        new_group, created = Group.objects.get_or_create(name=group)
        print('new_group---', new_group)
        print('created---', created)

    return_data = {}
    return return_data


@login_required
def take_attendance_view_ajax(request):
    message = "Invalid Request"

    if request.method == "POST":
        request.POST._mutable = True
        post_data = request.POST
        data = request.FILES

        post_data_dict = querydict_to_dict(post_data)
        form_text_data_json = post_data_dict["form_text_data_json"]
        form_data_dict = json.loads(form_text_data_json)
        print('-- form_data_dict ', form_data_dict)

        teacher = request.user.profile
        media_type = form_data_dict["media_type"]
        data_dict = querydict_to_dict(data)
        file_key = list(data_dict)[0]

        file = request.FILES[file_key]
        filename = request.FILES[file_key].name

        # override file name with prefixes
        filename = media_type + "__" + filename

        # 1.
        # save file in MediaCapturedModel
        obj = MediaCapturedModel.objects.create(media_type=media_type)
        getattr(obj, file_key).save(filename, file)

        # 2.
        #############################################################
        # write here your API response logic. . . . . . . . . . .
        # create entry in AttendanceModel
        # pass (profile_id, period_id, media_id) as model instance
        # pass attendance_status ['absent','present','leave']
        #############################################################

        # UNCOMMENT START #
        # todo - include day in logic while taking attendance
        # Check if for given period of class and section attendance is taken for the provided date
        attendance_date = parse_date(form_data_dict['date'])
        day = attendance_date.weekday() + 1  # Return the day of the week as an integer, where Monday is 0 and Sunday is 6.

        attendance_date_str = attendance_date.strftime("%Y-%m-%d")

        # notification data
        sender = request.user
        sender_uuid = sender.profile.profile_id
        school_id = request.user.profile.school_id.school_id
        notification_type = 'Notification'
        description = ''

        recipient_role = [
            "school_admin",
            "teacher",
            "student"
        ]

        class_name = form_data_dict["class_name"]
        section = form_data_dict["section"]
        media_type = form_data_dict["media_type"]
        period = form_data_dict['period']
        media_id = obj.media_id
        # need to change it according to attendence type i.e. attendance for teacher / student / non-teacher or staff
        role = "student"
        file_path = os.getcwd() + obj.media_file.url

        push_request_for_face_recog({
            "request_type": "attendance",
            "sender_uuid": str(sender_uuid),
            "notification_type": notification_type,
            "description": description,
            "class_name": class_name,
            "section": section,
            "media_type": media_type,
            "school_id": str(school_id),
            "recipient_role": json.dumps(recipient_role),
            "period": period,
            "day": day,
            "attendance_date": attendance_date_str,
            "media_id": str(media_id),
            "file_path": file_path,
            "role": role
        })

    message = "Attendance request submitted, please check notification after some time for more update!"
    payload = {
        "message": message
    }

    return JsonResponse(payload)


@login_required
def take_attendance_class_view_ajax(request):
    message = ""

    if request.method == "POST":
        request.POST._mutable = True
        post_data = request.POST
        data = request.FILES
        print('-- data ', data)

        post_data_dict = querydict_to_dict(post_data)
        form_text_data_json = post_data_dict["form_text_data_json"]
        form_data_dict = json.loads(form_text_data_json)
        print('-- form_data_dict ', form_data_dict)

        self_profile = request.user.profile
        media_type = form_data_dict["media_type"]
        class_name = form_data_dict["class_name"]
        section = form_data_dict["section"]
        period = form_data_dict["period"]
        period_id = form_data_dict["period_id"]
        profile_id = form_data_dict["profile_id"]
        attendance_choice = form_data_dict["attendance_choice"]
        attendance_date = parse_date(form_data_dict['attendance_date'])
        attendance_date_str = attendance_date.strftime("%Y-%m-%d %H:%M:%S")
        day = form_data_dict["day"]
        # day = attendance_date.weekday() + 1  # Return the day of the week as an integer, where Monday is 0 and Sunday is 6.

        # need to change it according to attendance type i.e. attendance for teacher / student / non-teacher or staff
        role = "student"
        staff_attendance_type = "class_attendance"
        attendance_type = "class"

        school_id = request.user.profile.school_id

        try:
            period_id_qs = PeriodModel.objects.get(period_number=form_data_dict['period'],
                                                   section=form_data_dict['section'],
                                                   class_name=form_data_dict['class_name'],
                                                   day=day, school_id=school_id)
        except Exception as e:
            print('--- period exception ---', e)
            period_id_qs = PeriodModel.objects.none()

        try:
            taken_by = Profile.objects.get(profile_id=profile_id)
        except Exception as e:
            print('--- profile exception ---', e)
            taken_by = Profile.objects.none()

        data_dict = querydict_to_dict(data)
        file_key = list(data_dict)[0]

        file = request.FILES[file_key]
        filename = request.FILES[file_key].name

        # override file name with prefixes
        filename = media_type + "__" + filename

        loggedin_user = str(request.user.profile.profile_id)
        school_id_str = str(school_id.school_id)
        extra_data = {
            "type": attendance_type,
            "staff_attendance_type": staff_attendance_type,
            "school_id": school_id_str,
            "loggedin_user": loggedin_user,
            "taken_for": "",
            "taken_by": str(taken_by),
            "form_data_dict": form_data_dict,
        }
        print('-- extra_data ', extra_data)

        # 1.
        # save file in MediaCapturedModel
        obj = MediaCapturedModel.objects.create(
            media_type=media_type,
            extra_data=extra_data
        )
        getattr(obj, file_key).save(filename, file)

        # 2.
        #############################################################
        # write here your API response logic. . . . . . . . . . .
        # create entry in AttendanceModel
        # pass (profile_id, period_id, media_id) as model instance
        # pass attendance_status ['absent','present','leave']
        #############################################################

        # UNCOMMENT START #

        staff_attendance_list = StaffAttendanceModel.objects.filter(
            taken_for=request.user.profile,
            attendance_date=attendance_date,
            attempt_status="success",
            attendance_type=staff_attendance_type
        )

        attendance_list = AttendanceModel.objects.filter(period_id=period_id_qs,
                                                         attendance_date=attendance_date)

        students_get_list_qs = Profile.objects.filter(role=role, school_id=school_id,
                                                      class_name=form_data_dict['class_name'],
                                                      section=form_data_dict['section'])

        ############################3
        # # if attendance list is empty i.e. no attendance is taken yet
        # if attendance_list:
        #     message = "Attendance Already taken!!!"
        #     print("Attendance Already taken!!!")
        # else:
        #     print("take attendance..... here.....")
        # try:
        #     video_data = get_face_recognition_response(request, file=os.getcwd() + obj.media_file.url)[
        #         'data']
        #
        # except Exception as e:
        #     print("\n\n Take Attendence Exception Occurred: ", str(e))
        #     message = "Exception occurred during Attendence API call"
        #
        # print("video_data: ", video_data)
        # attendance_status = []
        # students = Profile.objects.filter(
        #     school_id=school_id,
        #     role='student',
        #     class_name=class_name,
        #     section=section
        # )
        # print('---students---',students)
        #
        #
        # if video_data and video_data['set_of_classes_identified']:
        #     print("API Response : \n", json.dumps(video_data, indent=4))
        #     for student in students:
        #         if str(student.profile_id) in video_data['set_of_classes_identified']:
        #             attendance_status.append({
        #                 "name": student.user.first_name + " " + student.user.last_name,
        #                 "status": "present",
        #                 "username": student.user.username
        #             })
        #         else:
        #             attendance_status.append({
        #                 "name": student.user.first_name + " " + student.user.last_name,
        #                 "status": "absent",
        #                 "username": student.user.username
        #             })
        #     if attendance_list:
        #         print("update")
        #         attendance_obj = attendance_list[0]
        #         prev_media_id = attendance_obj.media_id
        #         attendance_obj.media_id = media_id
        #         attendance_obj.attempt_status = "success"
        #         attendance_obj.attendance_status = attendance_status
        #         attendance_obj.save()
        #         attendance_get_qs = attendance_obj
        #
        #         if settings.REMOVE_ORPHAN_MEDIA:
        #             print("prev_media_id: ", prev_media_id)
        #             # todo: check if media file exists or not and then delete it
        #             if prev_media_id.media_file and os.path.isfile(
        #                     os.path.join(os.getcwd(), prev_media_id.media_file.url.lstrip("/"))):
        #                 os.remove(os.path.join(os.getcwd(), prev_media_id.media_file.url.lstrip("/")))
        #             # MediaCapturedModel.objects.filter(media_id=prev_media_id.media_id).delete()
        #             prev_media_id.delete()
        #     else:
        #         print("create")
        #         attendance_get_qs = AttendanceModel.objects.create(
        #             loggedin_user=self_profile,
        #             # taken_by=taken_by,
        #             attempt_status="success",
        #             media_id=media_id,
        #             attendance_status=attendance_status,
        #             attendance_date=attendance_date,
        #             period_id=period_id_qs
        #         )
        #         staff_attendance_get_qs = StaffAttendanceModel.objects.create(
        #             attendance_id=attendance_get_qs,
        #             attendance_type=staff_attendance_type,
        #             loggedin_user=self_profile,
        #             taken_for=self_profile,
        #             taken_by=self_profile,
        #             attempt_status="success",
        #             media_id=media_id,
        #             attendance_status=attendance_status,
        #             attendance_date=attendance_date
        #         )
        #
        #     message = "Attendance Completed"
        # else:
        #     message = "No faces identified"
        #     # todo: Handle case when all students absent/ No face recognised / Mass bunk
        #     print("attendance_list: ", attendance_list)
        #     if attendance_list:
        #         print("failure media update ")
        #         staff_attendance_obj = attendance_list[0]
        #         prev_media_id = staff_attendance_obj.media_id
        #         staff_attendance_obj.media_id = media_id
        #         staff_attendance_obj.save()
        #         attendance_get_qs = staff_attendance_obj
        #         if settings.REMOVE_ORPHAN_MEDIA:
        #             print("prev_media_id: ", prev_media_id)
        #             # todo: check if media file exists or not and then delete it
        #             if prev_media_id.media_file and os.path.isfile(
        #                     os.path.join(os.getcwd(), prev_media_id.media_file.url.lstrip("/"))):
        #                 os.remove(os.path.join(os.getcwd(), prev_media_id.media_file.url.lstrip("/")))
        #             # MediaCapturedModel.objects.filter(media_id=prev_media_id.media_id).delete()
        #             prev_media_id.delete()
        #     else:
        #         print(" failure row create ")
        #         attendance_get_qs = AttendanceModel.objects.create(
        #             loggedin_user=self_profile,
        #             # taken_by=taken_by,
        #             attempt_status="failure",
        #             media_id=media_id,
        #             attendance_status=attendance_status,
        #             attendance_date=attendance_date,
        #             period_id=period_id_qs
        #         )
        #         staff_attendance_get_qs = StaffAttendanceModel.objects.create(
        #             attendance_id=attendance_get_qs,
        #             attendance_type=staff_attendance_type,
        #             loggedin_user=self_profile,
        #             taken_for=self_profile,
        #             taken_by=self_profile,
        #             attempt_status="failure",
        #             media_id=media_id,
        #             attendance_status=attendance_status,
        #             attendance_date=attendance_date
        #         )
        ############################################333

        # UNCOMMENT END #

        #################################################33

        # if attendance_get_qs:
        #     # send notification to employer and recuriter
        #     sender = request.user
        #     notification_type = 'Notification'
        #     description = ''

        # notification data
        notification_type = 'Notification'
        description = ''
        recipient_role = [
            "school_admin",
            "teacher",
            "non_teacher",
            "student"
        ]

        media_id = str(obj.media_id)
        taken_by_user_uuid = str(profile_id)
        sender_uuid = loggedin_user
        file_path = os.getcwd() + obj.media_file.url

        is_ready_to_push_to_redis = True
        if students_get_list_qs:
            if attendance_list and attendance_list[0].attempt_status == "success":
                message = "Attendance Already taken for given period and date!!!"
                logger.warning("[ATTENDANCE - CLASS] :: %s", message)
                is_ready_to_push_to_redis = False
        else:
            message = "No student registered"
            logger.warning("[ATTENDANCE - CLASS] :: %s", message)
            is_ready_to_push_to_redis = False

        if is_ready_to_push_to_redis:
            logger.info("[ATTENDANCE - CLASS] :: REDIS PUSH")
            push_request_for_face_recog({
                "request_type": "attendance",
                "attendance_type": attendance_type,
                "staff_attendance_type": staff_attendance_type,
                "sender_uuid": sender_uuid,
                'taken_by_user_uuid': taken_by_user_uuid,
                "notification_type": notification_type,
                "description": description,
                "class_name": class_name,
                "section": section,
                "period": period,
                "day": day,
                "media_type": media_type,
                "school_id": school_id_str,
                "recipient_role": json.dumps(recipient_role),
                "attendance_date": attendance_date_str,
                "media_id": media_id,
                "file_path": file_path,
                "role": role,
                "attendance_choice": attendance_choice
            })
            logger.info("[ATTENDANCE - CLASS] :: REDIS PUSH JOB CREATED")
            message = "Attendance request submitted, please check notification after some time for more update!"
    else:
        logger.warning("[ATTENDANCE - CLASS] :: INVALID REQUEST")
        message = "Invalid Request"

    payload = {
        "message": message
    }

    return JsonResponse(payload)


@login_required
def take_attendance_class_manual_view_ajax(request):
    message = "Something went wrong...!!!"
    status = "error"
    if request.method == "POST":
        request.POST._mutable = True
        post_data = request.POST
        print('-- post_data ', post_data)

        self_profile = request.user.profile
        class_name = post_data["class_name"]
        section = post_data["section"]
        period_number = post_data["period_number"]
        attendance_id = post_data["attendance_id"]
        attendance_taker_profile_id = post_data["attendance_taker"]
        attendance_choice = post_data["attendance_choice"]

        attendance_date = parse_date(post_data['attendance_date'])
        day = attendance_date.weekday() + 1  # Return the day of the week as an integer, where Monday is 0 and Sunday is 6.

        attendance_date_str = attendance_date.strftime("%Y-%m-%d %H:%M:%S")
        attendance_status = json.loads(post_data["attendance_status"])
        print('-- attendance_status ', attendance_status)
        school_id = request.user.profile.school_id
        period_get_qs = PeriodModel.objects.get(
            school_id=school_id,
            class_name=class_name,
            section=section,
            period_number=period_number,
            day=day
        )

        if attendance_id:
            attendance_qs = AttendanceModel.objects.get(
                period_id=period_get_qs,
                attendance_date=attendance_date,
            )
            attendance_qs.attendance_status = attendance_status
            attendance_qs.save()

            message = "Attendance updated successfully."
            status = "success"
        else:

            attempt_status = "success"
            loggedin_user = request.user.profile
            attendance_taker = Profile.objects.get(profile_id=attendance_taker_profile_id)
            media_id = None

            AttendanceModel.objects.create(
                period_id=period_get_qs,
                attempt_status=attempt_status,
                loggedin_user=loggedin_user,
                attendance_taker=attendance_taker,
                media_id=media_id,
                attendance_status=attendance_status,
                attendance_date=attendance_date,
                attendance_choice=attendance_choice,
            )
            message = "Attendance created successfully."
            status = "success"

    payload = {
        "message": message,
        "status": status
    }

    return JsonResponse(payload)


@login_required
def take_attendance_self_view_ajax(request):
    message = ""

    if request.method == "POST":
        request.POST._mutable = True
        post_data = request.POST
        data = request.FILES
        print('-- data ', data)

        post_data_dict = querydict_to_dict(post_data)
        form_text_data_json = post_data_dict["form_text_data_json"]
        form_data_dict = json.loads(form_text_data_json)
        print('-- form_data_dict ', form_data_dict)

        # todo: USE BACKEND TIMEZONE BUT COMPARE WITH FRONTEND TIME
        print("-- form_data_dict['date']: ", form_data_dict['date'])
        attendance_date = parse_datetime(form_data_dict['date'])
        print("-- attendance_date: ", attendance_date)
        attendance_date_str = attendance_date.strftime("%Y-%m-%d %H:%M:%S")
        print("-- front end attendance_date: ", attendance_date)
        today_day = datetime.datetime.today()
        attendance_date = today_day
        attendance_date_str = str(attendance_date)
        print("-- back end attendance_date: ", attendance_date)

        media_type = form_data_dict["media_type"]
        action_type = form_data_dict["action_type"]
        staff_attendance_geolocation = form_data_dict["staff_attendance_geolocation"]

        staff_attendance_type = None
        if action_type == "check_in":
            staff_attendance_type = "in"
        elif action_type == "check_out":
            staff_attendance_type = "out"

        data_dict = querydict_to_dict(data)
        file_key = list(data_dict)[0]

        file = request.FILES[file_key]
        filename = request.FILES[file_key].name

        # override file name with prefixes
        filename = media_type + "__" + filename

        school_id = request.user.profile.school_id
        loggedin_user = request.user.profile.profile_id
        attendance_type = "self"
        extra_data = {
            "attendance_type": attendance_type,
            "staff_attendance_type": staff_attendance_type,
            "action_type": action_type,
            "school_id": str(school_id.school_id),
            "loggedin_user": str(loggedin_user),
            "taken_for": str(loggedin_user),
            "taken_by": str(loggedin_user),
            "form_data_dict": form_data_dict,
        }
        print('-- extra_data ', extra_data)

        # 1.
        # save file in MediaCapturedModel
        media_create_qs = MediaCapturedModel.objects.create(
            media_type=media_type,
            extra_data=extra_data
        )
        getattr(media_create_qs, file_key).save(filename, file)

        # notification data
        taken_by = request.user
        taken_for = taken_by
        taken_by_uuid = str(taken_by.profile.profile_id)
        school_id = str(school_id.school_id)
        notification_type = 'Notification'
        description = ''

        recipient_role = [
            "school_admin",
            "teacher",
            "student"
        ]

        media_id = str(media_create_qs.media_id)
        # need to change it according to attendence type i.e. attendance for teacher / student / non-teacher or staff
        role = taken_for.profile.role
        file_path = os.getcwd() + media_create_qs.media_file.url

        if request.user.profile.is_registered:
            set_attendance_under_process(request.user.profile.profile_id, True)

            logger.info("[ATTENDANCE - SELF] :: PUSH REDIS")
            push_request_for_face_recog({
                "request_type": "attendance",
                "attendance_type": attendance_type,
                "staff_attendance_type": staff_attendance_type,
                "sender_uuid": taken_by_uuid,
                "notification_type": notification_type,
                "description": description,
                "media_type": media_type,
                "school_id": school_id,
                "recipient_role": json.dumps(recipient_role),
                "attendance_date": attendance_date_str,
                "media_id": media_id,
                "file_path": file_path,
                "role": role,
                "staff_attendance_geolocation": json.dumps(staff_attendance_geolocation)
            })
            logger.info("[ATTENDANCE - SELF] :: REDIS PUSH JOB CREATED")
            message = "Attendance request submitted, please check notification after some time for more update!"
        else:
            logger.info("[ATTENDANCE - SELF] :: NO FACE REGISTERED FOR GIVEN USER")
            message = "Please Registered Your Face First"
    else:
        logger.warning("[ATTENDANCE - SELF] :: INVALID REQUEST")
        message = "Invalid Request"

    payload = {
        "message": message
    }

    return JsonResponse(payload)


def set_profile_attendance_status(profile_id, attendance_status):
    is_set = False
    is_valid = False
    try:
        profile_qs = Profile.object.get(profile_id=profile_id)
    except Exception as e:
        print(e)
        profile_qs = Profile.object.none()

    if attendance_status in [item[0] for item in list(PROFILE_ATTENDANCE_STATUS_CHOICE)]:
        is_valid = True

    if profile_qs and is_valid:
        profile_qs.attendance_status = attendance_status
        profile_qs.save()
        is_set = True

    context = {
        "is_set": is_set,
        "is_valid": is_valid
    }

    return context


def set_attendance_under_process(profile_id, flag_status, attendance_status=False):
    is_set = False
    is_valid = False
    is_attendance_status_valid = False

    # check profile exist
    try:
        profile_qs = Profile.objects.get(profile_id=profile_id)
    except Exception as e:
        print(e)
        profile_qs = Profile.objects.none()

    # check old process flag_status and flag_status is valid
    if profile_qs.is_attendance_under_process == True and flag_status == True:
        is_valid = False
    elif flag_status in [True, False]:
        is_valid = True

    # check attendance status is valid
    if attendance_status and attendance_status in [item[0] for item in list(PROFILE_ATTENDANCE_STATUS_CHOICE)]:
        is_attendance_status_valid = True

    # set if data is valid
    if profile_qs and is_valid:
        profile_qs.is_attendance_under_process = flag_status

        # change attendance_status if is valid
        if is_attendance_status_valid:
            profile_qs.attendance_status = attendance_status

        profile_qs.save()
        is_set = True

    context = {
        "is_set": is_set,
        "is_valid": is_valid
    }

    return context


@login_required
def take_attendance_other_view_ajax(request):
    message = ""
    staff_attendance_get_qs = StaffAttendanceModel.objects.none()
    obj = MediaCapturedModel.objects.none()
    form_data_dict = {}

    if request.method == "POST":
        request.POST._mutable = True
        post_data = request.POST
        data = request.FILES
        print('-- data ', data)

        post_data_dict = querydict_to_dict(post_data)
        form_text_data_json = post_data_dict["form_text_data_json"]
        form_data_dict = json.loads(form_text_data_json)
        print('-- form_data_dict ', form_data_dict)

        # todo: USE BACKEND TIMEZONE BUT COMPARE WITH FRONTEND TIME
        print("-- form_data_dict['date']: ", form_data_dict['date'])
        attendance_date = parse_datetime(form_data_dict['date'])
        print("-- attendance_date: ", attendance_date)
        attendance_date_str = attendance_date.strftime("%Y-%m-%d %H:%M:%S")

        today_day = datetime.datetime.today()
        attendance_date = today_day
        attendance_date_str = str(attendance_date)

        media_type = form_data_dict["media_type"]
        action_type = form_data_dict["action_type"]
        taken_for_profile_id = form_data_dict["profile_id"]
        try:
            taken_for_profile_get_qs = Profile.objects.get(profile_id=taken_for_profile_id)
        except Exception as e:
            logger.exception()
            taken_for_profile_get_qs = Profile.objects.none()

        print('taken_for_profile_get_qs---', taken_for_profile_get_qs)

        # check if taken for profile exist or not
        if taken_for_profile_get_qs:
            staff_attendance_type = None
            if action_type == "check_in":
                staff_attendance_type = "in"
            elif action_type == "check_out":
                staff_attendance_type = "out"

            data_dict = querydict_to_dict(data)
            file_key = list(data_dict)[0]

            file = request.FILES[file_key]
            filename = request.FILES[file_key].name

            # override file name with prefixes
            filename = media_type + "__" + filename

            school_id = request.user.profile.school_id
            loggedin_user = request.user.profile.profile_id
            attendance_type = "other"
            extra_data = {
                "attendance_type": attendance_type,
                "staff_attendance_type": staff_attendance_type,
                "action_type": action_type,
                "school_id": str(school_id.school_id),
                "loggedin_user": str(loggedin_user),
                "taken_for": str(taken_for_profile_id),
                "taken_by": str(loggedin_user),
                "form_data_dict": form_data_dict,
            }
            print('-- extra_data ', extra_data)

            # 1.
            # save file in MediaCapturedModel
            media_create_qs = MediaCapturedModel.objects.create(
                media_type=media_type,
                extra_data=extra_data
            )
            getattr(media_create_qs, file_key).save(filename, file)

            """
                --- new ai logic ---
            """
            # notification data
            loggedin_user_uuid = str(loggedin_user)
            taken_by = request.user
            taken_for = taken_for_profile_get_qs.user
            taken_by_user_uuid = str(taken_by.profile.profile_id)
            taken_for_user_uuid = str(taken_for.profile.profile_id)
            school_id = str(school_id.school_id)
            notification_type = 'Notification'
            description = ''

            recipient_role = [
                "school_admin",
                "teacher",
                "non_teacher"
            ]

            media_id = str(media_create_qs.media_id)
            # need to change it according to attendence type i.e. attendance for teacher / student / non-teacher or staff
            role = taken_for.profile.role
            file_path = os.getcwd() + media_create_qs.media_file.url

            if taken_for_profile_get_qs.is_registered:
                set_attendance_under_process(taken_for.profile.profile_id, True)

                logger.info("[ATTENDANCE - OTHER] :: REDIS PUSH")
                push_request_for_face_recog({
                    "request_type": "attendance",
                    "attendance_type": attendance_type,
                    "staff_attendance_type": staff_attendance_type,
                    "sender_uuid": loggedin_user_uuid,
                    "notification_type": notification_type,
                    "description": description,
                    "media_type": media_type,
                    "school_id": school_id,
                    "recipient_role": json.dumps(recipient_role),
                    "attendance_date": attendance_date_str,
                    "media_id": media_id,
                    "file_path": file_path,
                    "role": role,
                    "taken_by_user_uuid": taken_by_user_uuid,
                    "taken_for_user_uuid": taken_for_user_uuid
                })
                logger.info("[ATTENDANCE - OTHER] :: REDIS PUSH JOB CREATED")
                """
                    --- new ai logic ---
                """
                message = "Attendance request submitted, please check notification after some time for more update!"
            else:
                logger.warning("[ATTENDANCE - OTHER] :: FACE IS NOT REGISTERED IN PICKLE FILE")
                message = "Face is not registered"
        else:
            logger.warning("[ATTENDANCE - OTHER] :: USER DOESN'T EXISTS")
            message = "User Doesn't Exsist"
    else:
        logger.warning("[ATTENDANCE - OTHER] :: INVALID REQUEST")
        message = "Invalid Request"
    payload = {
        "message": message
    }

    return JsonResponse(payload)


@login_required
def get_attendance_class_ajax(request):
    message = "fail"
    response_data = {}
    if request.method == "POST":
        data = request.POST
        print("---data--", data)

        data._mutable = True
        data.pop("csrfmiddlewaretoken")
        class_name = data["class_name"]
        section = data["section"]
        date = data["date"]
        attendance_date = parse_date(date)
        day = attendance_date.weekday() + 1  # Return the day of the week as an integer, where Monday is 0 and Sunday is 6.

        period = data["period"]

        print("---class_name--", class_name)
        print("---section--", section)
        print("---date--", date)
        print("---period--", period)

        try:
            # period_id = PeriodModel.objects.get(period_id=period_id_id)
            period_id = PeriodModel.objects.get(period_number=period,
                                                section=section,
                                                class_name=class_name,
                                                day=day)  # PeriodModel model instance

        except Exception as e:
            period_id = PeriodModel.objects.none()
            print("PeriodModel.objects.get Exception---", e)

        print('period_id---', period_id)

        if period_id:
            attendance_filter_qs = AttendanceModel.objects.filter(period_id=period_id,
                                                                  period_id__class_name=class_name,
                                                                  period_id__section=section,
                                                                  attempt_status="success",
                                                                  attendance_date=date
                                                                  ).values().annotate(
                period_number=F('period_id__period_number'),
                attendance_status_=F('attendance_status')
            ).order_by("-attendance_date")
        else:
            if period:
                attendance_filter_qs = AttendanceModel.objects.none()
            else:
                attendance_filter_qs = AttendanceModel.objects.filter(
                    period_id__class_name=class_name,
                    period_id__section=section,
                    attempt_status="success",
                    attendance_date=date
                ).values().annotate(
                    period_number=F('period_id__period_number'),
                    attendance_status_=F('attendance_status')
                ).order_by("-attendance_date")

        print('attendance_filter_qs---', attendance_filter_qs)

        # query set to dict
        # query set to json
        # blog = Blog.objects.all().values()
        # attendances_json = json.dumps(list(attendance_filter_qs), cls=DjangoJSONEncoder)
        attendances_json = json.loads(json.dumps(list(attendance_filter_qs), cls=DjangoJSONEncoder))

        message = "success"

    payload = {
        "message": message,
        "input": data,
        "data": attendances_json,
    }

    return JsonResponse(payload)


@login_required
def get_attendance_self_ajax(request):
    data = request.POST
    message = "fail"
    response_data = {}
    if request.method == "POST":
        data._mutable = True
        data.pop("csrfmiddlewaretoken")
        date = data["date"]
        attendance_date = parse_date(date)
        print("---attendance_date--", attendance_date)
        attendance_type_in = [
            'in',
            'out',
        ]
        taken_for = request.user.profile
        staff_att_qs = StaffAttendanceModel.objects.filter(
            attendance_type__in=attendance_type_in,
            taken_for=taken_for,
            attendance_date__date=attendance_date
        ).values()
        attendances_json = json.loads(json.dumps(list(staff_att_qs), cls=DjangoJSONEncoder))
        message = "success"

    payload = {
        "message": message,
        "input": data,
        "data": attendances_json,
    }

    return JsonResponse(payload)


@login_required
def get_attendance_other_ajax(request):
    data = request.POST
    message = "fail"
    response_data = {}
    if request.method == "POST":
        data._mutable = True
        data.pop("csrfmiddlewaretoken")
        date = data["date"]
        attendance_date = parse_date(date)
        day = attendance_date.weekday() + 1  # Return the day of the week as an integer, where Monday is 0 and Sunday is 6.
        print("---date--", date)
        attendance_type_in = [
            'in',
            'out',
        ]
        school_id = request.user.profile.school_id
        staff_att_qs = StaffAttendanceModel.objects.filter(
            attendance_type__in=attendance_type_in,
            taken_for__school_id=school_id,
            attendance_date__date=attendance_date
        ).values()
        attendances_json = json.loads(json.dumps(list(staff_att_qs), cls=DjangoJSONEncoder))
        message = "success"

    payload = {
        "message": message,
        "input": data,
        "data": attendances_json,
    }

    return JsonResponse(payload)


@login_required
def get_attendance_cctv_ajax(request):
    message = "fail"
    response_data = {}
    if request.method == "POST":
        data = request.POST
        print("---data--", data)

        data._mutable = True
        data.pop("csrfmiddlewaretoken")
        date = data["date"]
        attendance_date = parse_date(date)
        day = attendance_date.weekday() + 1  # Return the day of the week as an integer, where Monday is 0 and Sunday is 6.

        print("---date--", date)

        attendance_filter_qs = CameraResponseModel.objects.filter(
            cctv_attempt_status="success",
            attendance_date=date
        ).values().order_by("-attendance_date")

        print('attendance_filter_qs---', attendance_filter_qs)

        # query set to dict
        # query set to json
        # blog = Blog.objects.all().values()
        # attendances_json = json.dumps(list(attendance_filter_qs), cls=DjangoJSONEncoder)
        attendances_json = json.loads(json.dumps(list(attendance_filter_qs), cls=DjangoJSONEncoder))

        message = "success"

    payload = {
        "message": message,
        "input": data,
        "data": attendances_json,
    }

    return JsonResponse(payload)


def get_section_view_ajax(request):
    template = 'persons/city_dropdown_list_options.html'
    class_name = request.GET.get('class_name')
    school_id = request.user.profile.school_id

    sections = PeriodModel.objects.filter(school_id=school_id).all()
    context = {
        'sections': sections
    }
    return render(request, template, context)
    # return JsonResponse(list(cities.values('id', 'name')), safe=False)


class StudentsDetailView(LoginRequiredMixin, DetailView):
    model = Profile
    slug_url_kwarg = 'profile_id'
    slug_field = 'profile_id'
    template_name = 'app_smart_attendance/html/students_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Logic for profile_view_count
        profile_get_qs = context["profile"]
        profile_id = profile_get_qs.profile_id

        view_count = profile_get_qs.view_count
        view_count = view_count + 1
        if self.request.user.profile.profile_id != profile_id:
            Profile.objects.filter(profile_id=profile_id).update(view_count=view_count)

        context['profile_get_qs'] = profile_get_qs
        print('context --- ', context)

        return context


class TeacherDetailView(LoginRequiredMixin, DetailView):
    model = Profile
    slug_url_kwarg = 'profile_id'
    slug_field = 'profile_id'
    template_name = 'app_smart_attendance/html/teacher_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Logic for profile_view_count
        profile_get_qs = context["profile"]
        profile_id = profile_get_qs.profile_id

        view_count = profile_get_qs.view_count
        view_count = view_count + 1
        if self.request.user.profile.profile_id != profile_id:
            Profile.objects.filter(profile_id=profile_id).update(view_count=view_count)

        context['profile_get_qs'] = profile_get_qs
        print('context --- ', context)

        return context


class SchoolDetailView(LoginRequiredMixin, DetailView):
    model = SchoolModel
    slug_url_kwarg = 'school_id'
    slug_field = 'school_id'
    template_name = 'app_smart_attendance/html/school_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print('context --- ', context)

        profile_filter_qs = Profile.objects.filter(school_id=self.object.school_id)

        school_admin = profile_filter_qs.filter(role="school_admin")
        teacher = profile_filter_qs.filter(role="teacher")
        non_teacher = profile_filter_qs.filter(role="non_teacher")
        student = profile_filter_qs.filter(role="student")
        admin = profile_filter_qs.filter(role="admin")
        staff = profile_filter_qs.filter(role="staff")

        context["all_user"] = profile_filter_qs
        context["admin"] = admin
        context["staff"] = staff
        context["school_admins"] = school_admin
        context["teachers"] = teacher
        context["non_teachers"] = non_teacher
        context["students"] = student

        return context


def create_html_school_detail_list(request, school_get_qs):
    context = {}
    profile_filter_qs = Profile.objects.filter(school_id=school_get_qs.school_id)

    school_admin = profile_filter_qs.filter(role="school_admin")
    teacher = profile_filter_qs.filter(role="teacher")
    non_teacher = profile_filter_qs.filter(role="non_teacher")
    student = profile_filter_qs.filter(role="student")
    admin = profile_filter_qs.filter(role="admin")
    staff = profile_filter_qs.filter(role="staff")
    object = school_get_qs

    context["object"] = object
    context["all_user"] = profile_filter_qs
    context["admin"] = admin
    context["staff"] = staff
    context["school_admins"] = school_admin
    context["teachers"] = teacher
    context["non_teachers"] = non_teacher
    context["students"] = student

    template = 'app_smart_attendance/html/partial_school_detail.html'
    html_teacher_list = render_to_string(template_name=template, context=context, request=request)
    return html_teacher_list


@transaction.atomic
def register_school_admin_view(request, **kwargs):
    school_id = kwargs.get('school_id')
    school_get_qs = get_object_or_404(SchoolModel, school_id=school_id)

    template_name = 'app_root/html/partial_register_school_admin_create.html'
    data = dict()
    is_ask_password = True

    if request.method == 'POST':
        user_form = forms.AddSchoolAdminUserForm(request.POST, organisation=school_get_qs)
        profile_form = forms.AddSchoolAdminProfileForm(request.POST, organisation=school_get_qs)

        print('user_form', user_form.is_valid())
        print('profile_form', profile_form.is_valid())
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save(commit=False)
            user.is_active = True  # todo - set it true using verification email
            user.save()

            user.profile.school_id = school_get_qs
            user.profile.role = profile_form.cleaned_data['role']
            user.profile.gender = profile_form.cleaned_data['gender']
            user.profile.blood_group = profile_form.cleaned_data['blood_group']
            user.profile.save()

            html_school_detail_list = create_html_school_detail_list(request, school_get_qs)
            data['html_school_detail_list'] = html_school_detail_list

            data['form_is_valid'] = True
            data['message'] = 'School admin registered successfully.'
            data['profile'] = str(user.profile.profile_id)

        else:
            print('Error')
            data['form_is_valid'] = False
            data['message'] = 'School admin NOT registered successfully..!!'

    else:
        initial = {}
        if school_get_qs.organisation_type == "school":
            initial = {"role": "school_admin"}
        elif school_get_qs.organisation_type == "company" or school_get_qs.organisation_type == "anganwadi":
            initial = {"role": "admin"}

        user_form = forms.AddSchoolAdminUserForm(organisation=school_get_qs)
        profile_form = forms.AddSchoolAdminProfileForm(organisation=school_get_qs, initial=initial)

    context = {
        'user_form': user_form,
        'profile_form': profile_form,
        'school_id': school_id,
        'object': school_get_qs,
        'is_ask_password': is_ask_password
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


class ProfileDetailView(LoginRequiredMixin, DetailView):
    model = Profile
    slug_url_kwarg = 'profile_id'
    slug_field = 'profile_id'
    template_name = 'app_smart_attendance/module/profile/html/profile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print('context --- ', context)

        profile_filter_qs = Profile.objects.filter(school_id=self.object.school_id)

        school_admin = profile_filter_qs.filter(role="school_admin")
        teacher = profile_filter_qs.filter(role="teacher")
        non_teacher = profile_filter_qs.filter(role="non_teacher")
        student = profile_filter_qs.filter(role="student")

        context["school_admins"] = school_admin
        context["teachers"] = teacher
        context["non_teachers"] = non_teacher
        context["students"] = student

        return context


@login_required
def period_list_view(request):
    template_name = "app_smart_attendance/html/period_list.html"
    context_payload = {}
    role = request.user.profile.role
    if role == "admin" or role == "school_admin" or role == "teacher":
        school_id = request.user.profile.school_id
        period_filter_qs = PeriodModel.objects.filter(school_id=school_id)
        context_payload = {
            "period_filter_qs": period_filter_qs,
        }
        return render(request, template_name, context_payload)
    else:
        return render(request, 'access_denied/access-denied-page.html', {})


@login_required
def camera_list_view(request):
    template_name = "app_smart_attendance/module/camera/html/camera_list.html"
    context_payload = {}
    role = request.user.profile.role
    if role == "admin" or role == "school_admin" or role == "teacher":
        school_id = request.user.profile.school_id
        camera_filter_qs = CameraModel.objects.filter(school_id=school_id)
        context_payload = {
            "camera_filter_qs": camera_filter_qs,
        }
        return render(request, template_name, context_payload)
    else:
        return render(request, 'access_denied/access-denied-page.html', {})


class CameraDetailView(LoginRequiredMixin, DetailView):
    model = CameraModel
    slug_url_kwarg = 'camera_id'
    slug_field = 'camera_id'
    template_name = 'app_smart_attendance/module/camera/html/camera_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print('context --- ', context)
        return context


@login_required
def teacher_list_view(request):
    template_name = "app_smart_attendance/html/teacher_list.html"
    context_payload = {}
    userprofile = request.user.profile
    role = userprofile.role
    school_id = request.user.profile.school_id

    all_role_list = ROLE_CHOICE
    all_role_list = [item[0] for item in ROLE_CHOICE]

    if role == "admin" or role == "staff" or role == "school_admin" or role == "teacher":
        role_list = [
            "admin",
            "teacher",
            "staff",
            "non_teacher",
            "school_admin"
        ]
        profile_filter_qs = Profile.objects.filter(school_id=school_id, role__in=role_list).exclude(
            profile_id=userprofile.profile_id)
    else:
        profile_filter_qs = Profile.objects.none()

    profile_filter_qs = [userprofile] + list(profile_filter_qs)

    context_payload = {
        "profile_filter_qs": profile_filter_qs,
    }

    return render(request, template_name, context_payload)


@login_required
def location_manage_view(request):
    template_name = "app_smart_attendance/html/location_manage.html"
    context_payload = {}
    userprofile = request.user.profile
    role = userprofile.role
    school_id = request.user.profile.school_id

    if role == "admin" or role == "school_admin" or role == "teacher":
        role_list = [
            "teacher",
            "non_teacher",
            "school_admin"
        ]
        profile_filter_qs = Profile.objects.filter(school_id=school_id, role__in=role_list)
    else:
        profile_filter_qs = Profile.objects.none()

    if profile_filter_qs:
        profile_filter_qs = [userprofile] + [profile for profile in
                                             profile_filter_qs.exclude(profile_id=userprofile.profile_id)]

    context_payload = {
        "profile_filter_qs": profile_filter_qs,
    }

    return render(request, template_name, context_payload)


@login_required
def period_create_view(request):
    template_name = 'app_smart_attendance/html/partial_period_create.html'
    created_by = Profile.objects.get(profile_id=request.user.profile.profile_id)
    data = dict()

    if request.method == 'POST':
        form = PeriodCreateForm(request.POST, request.FILES, user=request.user)
        if form.is_valid():
            form_instance = form.save(commit=False)
            form_instance.created_by = request.user.profile.profile_id
            form_instance.school_id = request.user.profile.school_id

            is_period_exist = check_is_period_exist(request, form_instance)
            if is_period_exist is True:
                data['form_is_valid'] = False
                data['message'] = 'Period is already exist.'
            elif is_period_exist is False:
                form_instance.save()
                school_id = form_instance.school_id
                html_period_list = create_html_period_list(request, school_id)
                data['html_period_list'] = html_period_list
                data['form_is_valid'] = True
                data['message'] = 'Period created successfully.'
        else:
            data['form_is_valid'] = False
    else:
        form = PeriodCreateForm(user=request.user)

    context = {
        'form': form,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


@login_required
def camera_create_view(request):
    template_name = 'app_smart_attendance/module/camera/html/partial_camera_create.html'
    created_by = Profile.objects.get(profile_id=request.user.profile.profile_id)
    data = dict()

    if request.method == 'POST':
        form = CameraCreateForm(request.POST, request.FILES, user=request.user)
        if form.is_valid():
            form_instance = form.save(commit=False)
            form_instance.added_by = created_by
            form_instance.school_id = created_by.school_id

            is_camera_exist = check_is_camera_exist(request, form_instance)
            if is_camera_exist is True:
                data['form_is_valid'] = False
                data['message'] = 'Camera is already exist.'
            elif is_camera_exist is False:
                form_instance.save()
                school_id = form_instance.school_id
                html_period_list = create_html_camera_list(request, school_id)
                data['html_camera_list'] = html_period_list
                data['form_is_valid'] = True
                data['message'] = 'Camera added successfully.'
        else:
            data['form_is_valid'] = False
            data['message'] = form.is_valid()

    else:
        form = CameraCreateForm(user=request.user)

    context = {
        'form': form,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


@login_required
def camera_update_view(request, **kwargs):
    camera_id = kwargs.get('camera_id')
    camera_get_qs = get_object_or_404(CameraModel, pk=camera_id)
    template_name = 'app_smart_attendance/module/camera/html/partial_camera_update.html'
    data = dict()
    if request.method == 'POST':
        form = CameraUpdateForm(request.POST, request.FILES, instance=camera_get_qs, user=request.user)
        if form.is_valid():
            form_instance = form.save(commit=False)

            print('----form_instance.changed_data----', form.changed_data)
            is_check_camera_exist = False

            if any(x in form.changed_data for x in ['ip_address', 'url']):
                is_check_camera_exist = True
            else:
                is_check_camera_exist = False

            if is_check_camera_exist:
                is_camera_exist = check_is_camera_exist(request, form_instance)
            else:
                is_camera_exist = False

            if is_camera_exist is True:
                data['form_is_valid'] = False
                data['message'] = 'Camera is already exist.'
            elif is_camera_exist is False:
                form_instance.save()
                school_id = form_instance.school_id
                html_period_list = create_html_camera_list(request, school_id)
                data['html_camera_list'] = html_period_list
                data['form_is_valid'] = True
                data['message'] = 'Camera Updated successfully.'
        else:
            data['form_is_valid'] = False
    else:
        form = CameraUpdateForm(instance=camera_get_qs, user=request.user)
    context = {'form': form}
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "update"
    return JsonResponse(data)


@login_required
def camera_delete_view(request, **kwargs):
    data = dict()

    camera_get_qs = get_object_or_404(CameraModel, pk=kwargs.get('camera_id'))

    if request.user.profile.role == "school_admin" or request.user.profile.role == "admin":
        if request.method == 'POST':
            camera_id = kwargs['camera_id']
            camera = CameraModel.objects.filter(camera_id=camera_id)
            camera.delete()

            school_id = camera_get_qs.school_id
            after_delete_camera_filter_qs = CameraModel.objects.filter(school_id=school_id)
            data['form_is_valid'] = True
            data['html_camera_list'] = render_to_string(request=request,
                                                        template_name='app_smart_attendance/module/camera/html/partial_camera_list.html',
                                                        context={
                                                            'camera_filter_qs': after_delete_camera_filter_qs
                                                        })
            data['message'] = "Camera deleted successfully."
        else:
            context = {
                'camera_get_qs': camera_get_qs,
            }
            data['html_form'] = render_to_string(
                template_name='app_smart_attendance/module/camera/html/partial_camera_delete.html',
                context=context, request=request)
            data['action_type'] = "delete"
    else:
        data['form_is_valid'] = False
        data['message'] = "You are not authorized person to do this. Please contact your school admin"

    return JsonResponse(data)


def create_html_camera_list(request, school_id):
    camera_filter_qs = CameraModel.objects.filter(school_id=school_id)
    html_camera_list = render_to_string(
        template_name='app_smart_attendance/module/camera/html/partial_camera_list.html',
        context={
            'camera_filter_qs': camera_filter_qs,
        }, request=request)
    return html_camera_list


def check_is_camera_exist(request, form_instance):
    school_id = request.user.profile.school_id

    title = form_instance.title
    url = form_instance.url
    ip_address = form_instance.ip_address
    port = form_instance.port

    is_camera_exist = True

    is_camera_exist_count = CameraModel.objects.filter(school_id=school_id,
                                                       url=url,
                                                       ip_address=ip_address).count()

    if is_camera_exist_count:
        is_camera_exist = True
    else:
        is_camera_exist = False

    print('is_camera_exist_count---', is_camera_exist_count)
    print('is_camera_exist---', is_camera_exist)

    return is_camera_exist


@login_required
def announcement_create_view(request):
    template_name = 'app_smart_attendance/module/announcement/html/partial_announcement_create.html'
    created_by = Profile.objects.get(profile_id=request.user.profile.profile_id)
    data = dict()

    if request.method == 'POST':
        school_id = created_by.school_id
        form = AnnouncementCreateForm(request.POST, request.FILES, user=request.user)
        if form.is_valid():
            form_instance = form.save(commit=False)
            form_instance.school_id = school_id
            form_instance.created_by = created_by
            form_instance.save()

            data['form_is_valid'] = True
            data['message'] = 'Announcement created successfully.'
        else:
            data['form_is_valid'] = False
    else:
        initial = {
            "is_active": True
        }
        form = AnnouncementCreateForm(user=request.user, initial=initial)

    context = {
        'form': form,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


@login_required
def announcement_update_view(request, **kwargs):
    announcement_id = kwargs.get('announcement_id')
    announcement_qs = get_object_or_404(AnnouncementModel, announcement_id=announcement_id)
    template_name = 'app_smart_attendance/module/announcement/html/partial_announcement_update.html'
    data = dict()
    if request.method == 'POST':
        announcement_form = forms.AnnouncementUpdateForm(request.POST, user=request.user, instance=announcement_qs)
        if announcement_form.is_valid():

            announcement = announcement_form.save(commit=False)
            announcement.save()

            school_id = request.user.profile.school_id
            html_announcement_list = create_html_announcement_list(request, school_id)
            data['html_announcement_list'] = html_announcement_list
            data['form_is_valid'] = True
            data['message'] = 'Announcement updated successfully.'
        else:
            data['form_is_valid'] = False
            data['message'] = 'Announcement NOT updated successfully..!!'
    else:
        announcement_form = forms.AnnouncementUpdateForm(user=request.user, instance=announcement_qs)

    context = {
        'form': announcement_form,
        'action_type': 'update',
        'announcement_qs': announcement_qs,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "update"
    return JsonResponse(data)


@login_required
def announcement_delete_view(request, **kwargs):
    announcement_id = kwargs.get('announcement_id')
    announcement_qs = get_object_or_404(AnnouncementModel, announcement_id=announcement_id)
    template_name = 'app_smart_attendance/module/announcement/html/partial_announcement_delete.html'
    data = dict()
    if request.user.profile == announcement_qs.created_by:
        if request.method == 'POST':
            announcement_qs.delete()
            school_id = request.user.profile.school_id
            html_announcement_list = create_html_announcement_list(request, school_id)
            data['html_announcement_list'] = html_announcement_list
            data['form_is_valid'] = True
            data['message'] = 'Announcement deleted successfully.'
        else:
            context = {
                'announcement_qs': announcement_qs,
            }
            data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
            data['action_type'] = "delete"
    else:
        data['form_is_valid'] = False
        data['message'] = "You are not authorized person to do this. Please contact your admin"

    return JsonResponse(data)


def create_html_announcement_list(request, school_id):
    announcement_qs = AnnouncementModel.objects.filter(school_id=school_id.school_id)
    html_announcement_list = render_to_string(
        template_name='app_smart_attendance/module/announcement/html/partial_announcement_list.html',
        context={
            'announcement_qs': announcement_qs,
        }, request=request)
    return html_announcement_list


def check_is_period_exist(request, form_instance, is_day_not_include=False):
    school_id = request.user.profile.school_id
    class_name = form_instance.class_name
    section = form_instance.section
    is_period_exist = True

    if is_day_not_include is True:
        is_period_exist_count = PeriodModel.objects.filter(school_id=school_id, class_name=class_name,
                                                           section=section).count()
    else:
        day = form_instance.day
        period_number = form_instance.period_number
        is_period_exist_count = PeriodModel.objects.filter(school_id=school_id, day=day, class_name=class_name,
                                                           section=section, period_number=period_number).count()

    if is_period_exist_count:
        is_period_exist = True
    else:
        is_period_exist = False

    print('is_period_exist_count---', is_period_exist_count)
    print('is_period_exist---', is_period_exist)

    return is_period_exist


@transaction.atomic
def register_user_view(request):
    template_name = 'app_smart_attendance/html/partial_register_user_create.html'
    data = dict()
    is_ask_password = True
    profile_form = None

    if request.method == 'POST':
        user_form = forms.AddRegisterUserUserForm(request.POST, user=request.user)
        profile_form = forms.AddRegisterUserProfileForm(request.POST, user=request.user)
        data['face_registration'] = profile_form['face_registration'].value()

        print('user_form', user_form.is_valid())
        print('profile_form', profile_form.is_valid())
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save(commit=False)
            user.is_active = True  # todo - set it true using verification email
            user.save()

            user.profile.school_id = request.user.profile.school_id
            user.profile.role = profile_form.cleaned_data['role']
            user.profile.gender = profile_form.cleaned_data['gender']
            user.profile.blood_group = profile_form.cleaned_data['blood_group']
            user.profile.father_name = profile_form.cleaned_data['father_name']

            if user.profile.role == "student":
                user.profile.class_name = profile_form.cleaned_data['class_name']
                user.profile.section = profile_form.cleaned_data['section']

            user.profile.save()

            school_id = user.profile.school_id
            html_teacher_list = create_html_teacher_list(request, school_id)
            data['html_teacher_list'] = html_teacher_list

            data['form_is_valid'] = True
            data['message'] = 'User registered successfully.'
            data['profile'] = str(user.profile.profile_id)
            data['face_registration'] = profile_form.cleaned_data['face_registration']

            # return redirect('employer:emp-users')

        else:
            print('Error')
            data['form_is_valid'] = False
            data['message'] = 'User NOT registered successfully..!!'

    else:
        user_form = forms.AddRegisterUserUserForm(user=request.user)
        initial = {}
        if request.organisation_type == "school":
            if request.user.profile.role == "school_admin":
                initial = {}
            else:
                initial = {'role': 'student'}
        elif request.organisation_type == "company":
            initial = {'role': 'staff'}
        elif request.organisation_type == "anganwadi":
            initial = {'role': 'staff'}

        profile_form = forms.AddRegisterUserProfileForm(user=request.user, initial=initial)

    context = {
        'user_form': user_form,
        'profile_form': profile_form,
        'is_ask_password': is_ask_password
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


@transaction.atomic
def register_face_view(request):
    template_name = 'app_smart_attendance/html/partial_register_face_create.html'
    data = dict()
    profile_form = None
    user_form = None

    if request.method == 'POST':
        user_form = forms.AddRegisterFaceUserForm(request.POST, user=request.user)
        profile_form = forms.AddRegisterFaceProfileForm(request.POST, user=request.user)

        print('user_form', user_form.is_valid())
        print('profile_form', profile_form.is_valid())
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save(commit=False)
            user.is_active = True  # todo - set it true using verification email
            user.save()

            user.profile.school_id = request.user.profile.school_id
            user.profile.role = profile_form.cleaned_data['role']
            user.profile.gender = profile_form.cleaned_data['gender']
            user.profile.blood_group = profile_form.cleaned_data['blood_group']
            user.profile.save()

            school_id = user.profile.school_id
            html_teacher_list = create_html_teacher_list(request, school_id)
            data['html_teacher_list'] = html_teacher_list

            data['form_is_valid'] = True
            data['message'] = 'Face registered successfully.'

            # return redirect('employer:emp-users')

        else:
            print('Error')
            data['form_is_valid'] = False
            data['message'] = 'Face NOT registered successfully..!!'


    else:
        user_form = forms.AddRegisterFaceUserForm(user=request.user)
        if request.user.profile.role == "school_admin":
            profile_form = forms.AddRegisterFaceProfileForm(user=request.user)
        else:
            profile_form = forms.AddRegisterFaceProfileForm(user=request.user, initial={'role': 'student'})

    context = {
        'user_form': user_form,
        'profile_form': profile_form,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


@transaction.atomic
def take_attendance_class_view(request):
    template_name = 'app_smart_attendance/html/partial_take_attendance_class_create.html'
    data = dict()

    if request.method == 'POST':
        form = forms.TakeClassAttendanceForm(request.POST, user=request.user)

        print('form', form.is_valid())
        if form.is_valid():
            take_attendance_form = form.save(commit=False)
            take_attendance_form.save()

            school_id = take_attendance_form.profile.school_id
            html_teacher_list = create_html_teacher_list(request, school_id)
            data['html_teacher_list'] = html_teacher_list

            data['form_is_valid'] = True
            data['message'] = 'Face registered successfully.'

            # return redirect('employer:emp-users')

        else:
            print('Error')
            data['form_is_valid'] = False
            data['message'] = 'Face NOT registered successfully..!!'
    else:
        if request.user.profile.role == "school_admin":
            form = forms.TakeClassAttendanceForm(user=request.user)
        else:
            form = forms.TakeClassAttendanceForm(user=request.user, initial={'role': 'student'})

    context = {
        'form': form,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


@transaction.atomic
def take_attendance_class_preselected_view(request):
    template_name = 'app_smart_attendance/html/partial_take_attendance_class_create_preselected.html'
    data = dict()
    print('---request---', request)
    check_in_out_data = {}
    try:
        profile_id = request.GET['profile_id']
        is_profile = True
        profile_qs = Profile.objects.get(profile_id=profile_id)
    except Exception as e:
        print(e)
        profile_id = None
        is_profile = False
        profile_qs = Profile.objects.none()

    print('---profile_id---', profile_id)

    if request.method == 'POST':
        form = forms.TakeClassAttendanceFormPreselected(request.POST, user=request.user)

        print('form', form.is_valid())
        if form.is_valid():
            # take_attendance_form = form.save(commit=False)
            # take_attendance_form.save()
            #

            data['form_is_valid'] = False
            data['message'] = 'POST method not allowed'

            # return redirect('employer:emp-users')

        else:
            print('Error')
            data['form_is_valid'] = False
            data['message'] = 'POST method not allowed'
    else:
        if profile_qs:
            check_in_out_data = get_old_check_in_out_by_profile(request, profile_qs)

        if request.user.profile.role == "school_admin":
            form = forms.TakeClassAttendanceFormPreselected(
                user=request.user,
                attendance_taker_profile_qs=profile_qs
            )
        else:
            form = forms.TakeClassAttendanceFormPreselected(
                user=request.user,
                attendance_taker_profile_qs=profile_qs,
                initial={'role': 'student'}
            )

    context = {
        'form': form,
        'is_profile': is_profile,
        'attendance_taker_profile_qs': profile_qs,
        'old_check_in_out': check_in_out_data
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


@transaction.atomic
def take_attendance_cctv_view(request):
    template_name = 'app_smart_attendance/module/take_attendance_cctv/html/partial_take_attendance_cctv_create.html'

    data = dict()

    if request.method == 'POST':
        form = forms.TakeCCTVAttendanceForm(request.POST, user=request.user)

        print('form', form.is_valid())
        start = datetime.datetime.now()
        print('---start---', start)

        if form.is_valid():
            take_attendance_cctv_form = form.save(commit=False)
            take_attendance_cctv_form.cctv_attendance_type = "manual"
            take_attendance_cctv_form.loggedin_user = request.user.profile
            take_attendance_cctv_form.save()

            school_id = take_attendance_cctv_form.camera_id.added_by.school_id

            # ### LOGIC 1 start
            # get_cctv_stream(request, take_attendance_cctv_form)
            # ### LOGIC 1 end

            ## # LOGIC 2 start
            path = settings.BROWSE_FOLDER_ROOT + 'cctv/'
            url = take_attendance_cctv_form.camera_id.url
            get_stream_save_frame_or_video(path=path, url=url, user=None, password=None)
            ## # LOGIC 2 end

            end = datetime.datetime.now()
            print('---end---', end)
            print('---time taken---', end - start)

            data['form_is_valid'] = True
            data['message'] = 'Take attendance by CCTV submitted successfully.'
        else:
            print('Error')
            data['form_is_valid'] = False
            data['message'] = 'Take attendance by CCTV NOT submitted successfully..!!'
    else:
        form = forms.TakeCCTVAttendanceForm(user=request.user)

    context = {
        'form': form,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


def get_cctv_stream(request, take_attendance_cctv_form):
    message = ""

    # request.POST._mutable = True
    # post_data = request.POST
    # data = request.FILES
    # print('-- data ', data)
    #
    # post_data_dict = querydict_to_dict(post_data)
    # form_text_data_json = post_data_dict["form_text_data_json"]
    # form_data_dict = json.loads(form_text_data_json)
    # print('-- form_data_dict ', form_data_dict)
    #
    # self_profile = request.user.profile
    # media_type = form_data_dict["media_type"]
    # class_name = form_data_dict["class_name"]
    # section = form_data_dict["section"]
    # period = form_data_dict["period"]
    # period_id = form_data_dict["period_id"]
    # profile_id = form_data_dict["profile_id"]
    # attendance_date = parse_date(form_data_dict['attendance_date'])
    # attendance_date_str = attendance_date.strftime("%Y-%m-%d %H:%M:%S")
    # day = form_data_dict["day"]
    # # day = attendance_date.weekday() + 1  # Return the day of the week as an integer, where Monday is 0 and Sunday is 6.
    #
    # # need to change it according to attendance type i.e. attendance for teacher / student / non-teacher or staff
    # role = "student"
    # staff_attendance_type = "class_attendance"
    # attendance_type = "class"
    #
    # school_id = request.user.profile.school_id
    #
    # try:
    #     period_id_qs = PeriodModel.objects.get(period_number=form_data_dict['period'],
    #                                            section=form_data_dict['section'],
    #                                            class_name=form_data_dict['class_name'],
    #                                            day=day, school_id=school_id)
    # except Exception as e:
    #     print('--- period exception ---', e)
    #     period_id_qs = PeriodModel.objects.none()
    #
    # try:
    #     taken_by = Profile.objects.get(profile_id=profile_id)
    # except Exception as e:
    #     print('--- profile exception ---', e)
    #     taken_by = Profile.objects.none()
    #
    # data_dict = querydict_to_dict(data)
    # file_key = list(data_dict)[0]
    #
    # file = request.FILES[file_key]
    # filename = request.FILES[file_key].name
    #
    # # override file name with prefixes
    # filename = media_type + "__" + filename
    #
    # loggedin_user = str(request.user.profile.profile_id)
    # school_id_str = str(school_id.school_id)
    # extra_data = {
    #     "type": attendance_type,
    #     "staff_attendance_type": staff_attendance_type,
    #     "school_id": school_id_str,
    #     "loggedin_user": loggedin_user,
    #     "taken_for": "",
    #     "taken_by": str(taken_by),
    #     "form_data_dict": form_data_dict,
    # }
    # print('-- extra_data ', extra_data)

    # path = "/home/sam/Documents/ip/"

    # url = "http://192.168.1.4:6677/videofeed"
    # user = "CCJDLKGMF"
    # password = ""

    # # 1.
    # # save file in MediaCapturedModel
    # obj = MediaCapturedModel.objects.create(
    #     media_type=media_type,
    #     extra_data=extra_data
    # )
    # getattr(obj, file_key).save(filename, file)
    #
    # # 2.
    # #############################################################
    # # write here your API response logic. . . . . . . . . . .
    # # create entry in AttendanceModel
    # # pass (profile_id, period_id, media_id) as model instance
    # # pass attendance_status ['absent','present','leave']
    # #############################################################
    #
    # # UNCOMMENT START #
    #
    # staff_attendance_list = StaffAttendanceModel.objects.filter(
    #     taken_for=request.user.profile,
    #     attendance_date=attendance_date,
    #     attempt_status="success",
    #     attendance_type=staff_attendance_type
    # )
    #
    # attendance_list = AttendanceModel.objects.filter(period_id=period_id_qs,
    #                                                  attendance_date=attendance_date)
    #
    # students_get_list_qs = Profile.objects.filter(role=role, school_id=school_id,
    #                                               class_name=form_data_dict['class_name'],
    #                                               section=form_data_dict['section'])
    #
    # ############################3
    # # # if attendance list is empty i.e. no attendance is taken yet
    # # if attendance_list:
    # #     message = "Attendance Already taken!!!"
    # #     print("Attendance Already taken!!!")
    # # else:
    # #     print("take attendance..... here.....")
    # # try:
    # #     video_data = get_face_recognition_response(request, file=os.getcwd() + obj.media_file.url)[
    # #         'data']
    # #
    # # except Exception as e:
    # #     print("\n\n Take Attendence Exception Occurred: ", str(e))
    # #     message = "Exception occurred during Attendence API call"
    # #
    # # print("video_data: ", video_data)
    # # attendance_status = []
    # # students = Profile.objects.filter(
    # #     school_id=school_id,
    # #     role='student',
    # #     class_name=class_name,
    # #     section=section
    # # )
    # # print('---students---',students)
    # #
    # #
    # # if video_data and video_data['set_of_classes_identified']:
    # #     print("API Response : \n", json.dumps(video_data, indent=4))
    # #     for student in students:
    # #         if str(student.profile_id) in video_data['set_of_classes_identified']:
    # #             attendance_status.append({
    # #                 "name": student.user.first_name + " " + student.user.last_name,
    # #                 "status": "present",
    # #                 "username": student.user.username
    # #             })
    # #         else:
    # #             attendance_status.append({
    # #                 "name": student.user.first_name + " " + student.user.last_name,
    # #                 "status": "absent",
    # #                 "username": student.user.username
    # #             })
    # #     if attendance_list:
    # #         print("update")
    # #         attendance_obj = attendance_list[0]
    # #         prev_media_id = attendance_obj.media_id
    # #         attendance_obj.media_id = media_id
    # #         attendance_obj.attempt_status = "success"
    # #         attendance_obj.attendance_status = attendance_status
    # #         attendance_obj.save()
    # #         attendance_get_qs = attendance_obj
    # #
    # #         if settings.REMOVE_ORPHAN_MEDIA:
    # #             print("prev_media_id: ", prev_media_id)
    # #             # todo: check if media file exists or not and then delete it
    # #             if prev_media_id.media_file and os.path.isfile(
    # #                     os.path.join(os.getcwd(), prev_media_id.media_file.url.lstrip("/"))):
    # #                 os.remove(os.path.join(os.getcwd(), prev_media_id.media_file.url.lstrip("/")))
    # #             # MediaCapturedModel.objects.filter(media_id=prev_media_id.media_id).delete()
    # #             prev_media_id.delete()
    # #     else:
    # #         print("create")
    # #         attendance_get_qs = AttendanceModel.objects.create(
    # #             loggedin_user=self_profile,
    # #             # taken_by=taken_by,
    # #             attempt_status="success",
    # #             media_id=media_id,
    # #             attendance_status=attendance_status,
    # #             attendance_date=attendance_date,
    # #             period_id=period_id_qs
    # #         )
    # #         staff_attendance_get_qs = StaffAttendanceModel.objects.create(
    # #             attendance_id=attendance_get_qs,
    # #             attendance_type=staff_attendance_type,
    # #             loggedin_user=self_profile,
    # #             taken_for=self_profile,
    # #             taken_by=self_profile,
    # #             attempt_status="success",
    # #             media_id=media_id,
    # #             attendance_status=attendance_status,
    # #             attendance_date=attendance_date
    # #         )
    # #
    # #     message = "Attendance Completed"
    # # else:
    # #     message = "No faces identified"
    # #     # todo: Handle case when all students absent/ No face recognised / Mass bunk
    # #     print("attendance_list: ", attendance_list)
    # #     if attendance_list:
    # #         print("failure media update ")
    # #         staff_attendance_obj = attendance_list[0]
    # #         prev_media_id = staff_attendance_obj.media_id
    # #         staff_attendance_obj.media_id = media_id
    # #         staff_attendance_obj.save()
    # #         attendance_get_qs = staff_attendance_obj
    # #         if settings.REMOVE_ORPHAN_MEDIA:
    # #             print("prev_media_id: ", prev_media_id)
    # #             # todo: check if media file exists or not and then delete it
    # #             if prev_media_id.media_file and os.path.isfile(
    # #                     os.path.join(os.getcwd(), prev_media_id.media_file.url.lstrip("/"))):
    # #                 os.remove(os.path.join(os.getcwd(), prev_media_id.media_file.url.lstrip("/")))
    # #             # MediaCapturedModel.objects.filter(media_id=prev_media_id.media_id).delete()
    # #             prev_media_id.delete()
    # #     else:
    # #         print(" failure row create ")
    # #         attendance_get_qs = AttendanceModel.objects.create(
    # #             loggedin_user=self_profile,
    # #             # taken_by=taken_by,
    # #             attempt_status="failure",
    # #             media_id=media_id,
    # #             attendance_status=attendance_status,
    # #             attendance_date=attendance_date,
    # #             period_id=period_id_qs
    # #         )
    # #         staff_attendance_get_qs = StaffAttendanceModel.objects.create(
    # #             attendance_id=attendance_get_qs,
    # #             attendance_type=staff_attendance_type,
    # #             loggedin_user=self_profile,
    # #             taken_for=self_profile,
    # #             taken_by=self_profile,
    # #             attempt_status="failure",
    # #             media_id=media_id,
    # #             attendance_status=attendance_status,
    # #             attendance_date=attendance_date
    # #         )
    # ############################################333
    #
    # # UNCOMMENT END #
    #
    # #################################################33
    #
    # # if attendance_get_qs:
    # #     # send notification to employer and recuriter
    # #     sender = request.user
    # #     notification_type = 'Notification'
    # #     description = ''
    #
    # # notification data
    # notification_type = 'Notification'
    # description = ''
    # recipient_role = [
    #     "school_admin",
    #     "teacher",
    #     "non_teacher",
    #     "student"
    # ]
    #
    # media_id = str(obj.media_id)
    # taken_by_user_uuid = str(profile_id)
    # sender_uuid = loggedin_user
    # file_path = os.getcwd() + obj.media_file.url
    #
    # is_ready_to_push_to_redis = True
    # if students_get_list_qs:
    #     if attendance_list and attendance_list[0].attempt_status == "success":
    #         message = "Attendance Already taken for given period and date!!!"
    #         logger.warning("[ATTENDANCE - CLASS] :: %s", message)
    #         is_ready_to_push_to_redis = False
    # else:
    #     message = "No student registered"
    #     logger.warning("[ATTENDANCE - CLASS] :: %s", message)
    #     is_ready_to_push_to_redis = False
    #
    # if is_ready_to_push_to_redis:
    #     logger.info("[ATTENDANCE - CLASS] :: REDIS PUSH")
    #     push_request_for_face_recog({
    #         "request_type": "attendance",
    #         "attendance_type": attendance_type,
    #         "staff_attendance_type": staff_attendance_type,
    #         "sender_uuid": sender_uuid,
    #         'taken_by_user_uuid': taken_by_user_uuid,
    #         "notification_type": notification_type,
    #         "description": description,
    #         "class_name": class_name,
    #         "section": section,
    #         "period": period,
    #         "day": day,
    #         "media_type": media_type,
    #         "school_id": school_id_str,
    #         "recipient_role": json.dumps(recipient_role),
    #         "attendance_date": attendance_date_str,
    #         "media_id": media_id,
    #         "file_path": file_path,
    #         "role": role
    #     })
    #     logger.info("[ATTENDANCE - CLASS] :: REDIS PUSH JOB CREATED")

    attendance_type = "cctv"

    sender = None
    sender_profile_id = None
    taken_by_user_uuid = None
    school_id = None
    school_id_str = None
    if request:
        sender = request.user
        sender_profile_id = sender.profile.profile_id
        taken_by_user_uuid = str(sender_profile_id)
        school_id = sender.profile.school_id
        school_id_str = str(school_id.school_id)

    notification_type = 'Notification'
    description = ''
    recipient_role = [
        "school_admin",
    ]
    media_type = "attendance"

    today_day = datetime.datetime.today()
    attendance_date = today_day
    attendance_date_str = str(attendance_date)
    media_id = ""
    file_path = ""
    staff_attendance_type = ""

    path = settings.BROWSE_FOLDER_ROOT + 'cctv/'
    url = take_attendance_cctv_form.camera_id.url
    # getting value from frontend
    role = take_attendance_cctv_form.role
    camera_response_id = take_attendance_cctv_form.camera_response_id

    list_of_all_available_role = [
        'school_admin',
        'teacher',
        'non_teacher',
        'student'
    ]
    if role not in list_of_all_available_role:
        role = "unknown"

    # if not role in list_of_all_available_role:
    #     role = None

    print('---url---', url)
    print('---role---', role)

    push_request_for_face_recog({
        "request_type": "attendance",
        "attendance_type": attendance_type,
        "staff_attendance_type": staff_attendance_type,
        "sender_uuid": str(sender_profile_id),
        'taken_by_user_uuid': taken_by_user_uuid,
        "notification_type": notification_type,
        "description": description,
        "media_type": media_type,
        "school_id": school_id_str,
        "recipient_role": json.dumps(recipient_role),
        "attendance_date": attendance_date_str,
        "media_id": media_id,
        "file_path": file_path,
        "role": role,
        "path": path,
        "url": url,
        "camera_response_id": str(camera_response_id),
    })
    message = "Attendance request submitted, please check notification after some time for more update!"

    payload = {
        "message": message
    }

    return JsonResponse(payload)


def get_today_check_in_check_out_attendance_success(request, profile):
    profile = profile
    # ######################

    today = datetime.datetime.now().date()
    # print('---today---', today)
    staff_at_qs = StaffAttendanceModel.objects.filter(taken_for=profile, attendance_type__in=['in', 'out'],
                                                      attempt_status='success',
                                                      attendance_date__date=today).order_by('-attendance_date')
    # print('---staff_at_qs---', staff_at_qs)

    # todo - double check if user face in media
    # default
    is_check_in = False
    is_check_out = False

    staff_at_in_qs = staff_at_qs.filter(attendance_type='in')
    staff_at_out_qs = staff_at_qs.filter(attendance_type='out')

    in_count = staff_at_in_qs.count()
    out_count = staff_at_out_qs.count()

    if in_count == 0:
        is_check_in = False
    elif in_count == 1:
        is_check_in = True
    elif in_count > 1:
        is_check_in = True
        message = 'Something went wrong...!!! [more than 1 checkin]'

    if out_count == 0:
        is_check_out = False
    elif out_count == 1:
        is_check_out = True
    elif out_count > 1:
        is_check_out = True
        message = 'Something went wrong...!!! [more than 1 checkout]'

    # print('---is_check_in---', is_check_in)
    # print('---is_check_out---', is_check_out)

    # ######################

    return_data = {
        'is_check_in': is_check_in,
        'is_check_out': is_check_out,
        'staff_at_qs': staff_at_qs,
    }
    return return_data


def get_last_check_in_check_out_attendance_success(request, profile):
    profile = profile
    # ######################

    # print('---today---', today)
    staff_at_qs = StaffAttendanceModel.objects.filter(taken_for=profile, attendance_type__in=['in', 'out'],
                                                      attempt_status='success').order_by('-attendance_date')
    # print('---staff_at_qs---', staff_at_qs)

    # todo - double check if user face in media
    # default
    is_check_in = False
    is_check_out = False

    staff_at_in_qs = staff_at_qs.filter(attendance_type='in')
    staff_at_out_qs = staff_at_qs.filter(attendance_type='out')

    in_count = staff_at_in_qs.count()
    out_count = staff_at_out_qs.count()

    if in_count == 0:
        is_check_in = False
    elif in_count == 1:
        is_check_in = True
    elif in_count > 1:
        is_check_in = True
        message = 'Something went wrong...!!! [more than 1 checkin]'

    if out_count == 0:
        is_check_out = False
    elif out_count == 1:
        is_check_out = True
    elif out_count > 1:
        is_check_out = True
        message = 'Something went wrong...!!! [more than 1 checkout]'

    # print('---is_check_in---', is_check_in)
    # print('---is_check_out---', is_check_out)

    # ######################

    return_data = {
        'is_check_in': is_check_in,
        'is_check_out': is_check_out,
        'staff_at_qs': staff_at_qs,
    }
    return return_data


def get_today_check_in_check_out_attendance_failure(request, profile):
    profile = profile
    print('---profile---', profile)
    # ######################

    today = datetime.datetime.now().date()
    print('---today---', today)
    staff_at_failure_qs = StaffAttendanceModel.objects.filter(taken_for=profile, attendance_type__in=['in', 'out'],
                                                              attempt_status='failure',
                                                              attendance_date__date=today).order_by(
        '-attendance_date')
    # print('---staff_at_failure_qs---', staff_at_failure_qs)
    return_data = {
        'staff_at_failure_qs': staff_at_failure_qs,
    }
    return return_data


def get_last_check_in_check_out_attendance_failure(request, profile):
    profile = profile
    print('---profile---', profile)
    # ######################

    staff_at_failure_qs = StaffAttendanceModel.objects.filter(taken_for=profile, attendance_type__in=['in', 'out'],
                                                              attempt_status='failure').order_by(
        '-attendance_date')
    # print('---staff_at_failure_qs---', staff_at_failure_qs)
    return_data = {
        'staff_at_failure_qs': staff_at_failure_qs,
    }
    return return_data


@transaction.atomic
def take_attendance_self_view(request):
    template_name = 'app_smart_attendance/html/partial_take_attendance_self_create.html'
    data = dict()
    last_entry_dict = {}
    last_entry_attendance_date = None
    check_in_out_data = {}
    data['action_type'] = "create"

    profile_qs = request.user.profile
    is_attendance_under_process = profile_qs.is_attendance_under_process
    if is_attendance_under_process:
        data['form_is_valid'] = False
        data['message'] = 'Please wait, Your attendance under processing.'
        data['is_attendance_under_process'] = is_attendance_under_process
        context = {
            "is_attendance_under_process": is_attendance_under_process
        }
        data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
        return JsonResponse(data)

    is_profile = False
    if profile_qs:
        check_in_out_data = get_old_check_in_out_by_profile(request, profile_qs)
        is_profile = True

    last_entry = check_in_out_data.pop('last_entry', None)
    print('last_entry---', last_entry)
    if last_entry:
        last_entry_dict = {
            'staff_attendance_id': str(last_entry.staff_attendance_id),
            'attendance_id': str(last_entry.attendance_id),
            'attendance_type': last_entry.attendance_type,
            'attempt_status': last_entry.attempt_status,
            'loggedin_user': str(last_entry.loggedin_user),
            'taken_by': str(last_entry.taken_by),
            'taken_for': str(last_entry.taken_for),
            'media_id': str(last_entry.media_id.media_id),
            'attendance_status': last_entry.attendance_status,
            'attendance_date_str': str(last_entry.attendance_date),
            'staff_attendance_geolocation': last_entry.staff_attendance_geolocation,
            'date_time': str(last_entry.date_time),
            'updated_at': str(last_entry.updated_at),
        }
        last_entry_attendance_date = last_entry.attendance_date

    print('last_entry_dict---', last_entry_dict)

    return_data = get_today_check_in_check_out_attendance_success(request, profile_qs)
    is_check_in = return_data["is_check_in"]
    is_check_out = return_data["is_check_out"]
    staff_at_qs = return_data["staff_at_qs"]
    staff_at_success_in_qs = staff_at_qs.filter(attendance_type="in")
    staff_at_success_out_qs = staff_at_qs.filter(attendance_type="out")

    failure_return_data = get_today_check_in_check_out_attendance_failure(request, profile_qs)
    staff_at_failure_qs = failure_return_data["staff_at_failure_qs"]
    staff_at_failure_in_qs = staff_at_failure_qs.filter(attendance_type="in")
    staff_at_failure_out_qs = staff_at_failure_qs.filter(attendance_type="out")

    initial_set = {}
    if is_check_in and is_check_out:
        initial_set = {
            'check_in': 'in',
            'check_out': 'out',
        }
    elif is_check_in:
        initial_set = {
            'check_in': 'in',
        }
    elif is_check_out:
        initial_set = {
            'check_out': 'out',
        }

    if request.method == 'POST':
        form = forms.TakeStaffSelfAttendanceForm(request.POST, user=request.user)

        print('form------------', form.is_valid())
        if form.is_valid():
            if form.is_valid():
                # take_attendance_form = form.save(commit=False)
                # take_attendance_form.save()
                #
                # school_id = take_attendance_form.profile.school_id
                #
                data['form_is_valid'] = False
                data['message'] = 'POST method not allowed'

                # return redirect('employer:emp-users')

            else:
                print('Error')
                data['form_is_valid'] = False
                data['message'] = 'POST method not allowed'
    else:
        form = forms.TakeStaffSelfAttendanceForm(
            user=request.user,
            is_check_in=is_check_in,
            is_check_out=is_check_out,
            initial=initial_set,
        )

    context = {
        'form': form,
        'is_check_in': is_check_in,
        'is_check_out': is_check_out,
        'staff_at_qs': staff_at_qs,
        'staff_at_success_in_qs': staff_at_success_in_qs,
        'staff_at_success_out_qs': staff_at_success_out_qs,
        'staff_at_failure_in_qs': staff_at_failure_in_qs,
        'staff_at_failure_out_qs': staff_at_failure_out_qs,
        'staff_at_failure_qs': staff_at_failure_qs,

        'is_profile': is_profile,
        'attendance_for_profile_qs': profile_qs,
        'old_check_in_out': check_in_out_data,
        'last_entry': last_entry_dict,
        'last_entry_attendance_date': last_entry_attendance_date,

    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)

    return JsonResponse(data)


@transaction.atomic
def take_attendance_other_view(request):
    template_name = 'app_smart_attendance/html/partial_take_attendance_other_create.html'
    data = dict()

    if request.method == 'POST':
        form = forms.TakeStaffOtherAttendanceForm(request.POST, user=request.user)

        print('form', form.is_valid())
        if form.is_valid():
            take_attendance_form = form.save(commit=False)
            take_attendance_form.save()

            school_id = take_attendance_form.profile.school_id
            html_teacher_list = create_html_teacher_list(request, school_id)
            data['html_teacher_list'] = html_teacher_list

            data['form_is_valid'] = True
            data['message'] = 'Face registered successfully.'

            # return redirect('employer:emp-users')

        else:
            print('Error')
            data['form_is_valid'] = False
            data['message'] = 'Face NOT registered successfully..!!'
    else:
        if request.user.profile.role == "school_admin":
            form = forms.TakeStaffOtherAttendanceForm(user=request.user)
        else:
            form = forms.TakeStaffOtherAttendanceForm(user=request.user, initial={'role': 'student'})

    context = {
        'form': form,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


@transaction.atomic
def take_attendance_other_preselected_view(request, **kwargs):
    template_name = 'app_smart_attendance/html/partial_take_attendance_other_create_preselected.html'
    data = dict()
    print('---request---', request)
    last_entry_dict = {}
    last_entry_attendance_date = None
    attendance_date = {}
    check_in_out_data = {}
    try:
        profile_id = request.GET['profile_id']
        is_profile = True
        profile_qs = Profile.objects.get(profile_id=profile_id)
    except Exception as e:
        print(e)
        profile_id = None
        is_profile = False
        profile_qs = Profile.objects.none()

    print('---profile_id---', profile_id)

    if profile_qs:
        is_attendance_under_process = profile_qs.is_attendance_under_process
        if is_attendance_under_process:
            data['form_is_valid'] = False
            data['message'] = 'Please wait, Your attendance under processing.'
            data['is_attendance_under_process'] = is_attendance_under_process
            context = {
                "is_attendance_under_process": is_attendance_under_process,
                "profile_qs": profile_qs
            }
            data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
            return JsonResponse(data)

    if request.method == 'POST':
        form = forms.TakeStaffOtherAttendanceFormPreselected(request.POST, user=request.user)

        print('form', form.is_valid())
        if form.is_valid():
            # take_attendance_form = form.save(commit=False)
            # take_attendance_form.save()
            #
            # school_id = take_attendance_form.profile.school_id
            #
            data['form_is_valid'] = False
            data['message'] = 'POST method not allowed'

            # return redirect('employer:emp-users')

        else:
            print('Error')
            data['form_is_valid'] = False
            data['message'] = 'POST method not allowed'
    else:

        if profile_qs:
            check_in_out_data = get_old_check_in_out_by_profile(request, profile_qs)

        if request.user.profile.role == "school_admin":
            form = forms.TakeStaffOtherAttendanceFormPreselected(
                user=request.user,
                attendance_for_profile_qs=profile_qs
            )
        else:
            form = forms.TakeStaffOtherAttendanceFormPreselected(
                user=request.user,
                attendance_for_profile_qs=profile_qs,
                initial={'role': 'student'}
            )
    last_entry = check_in_out_data.pop('last_entry', None)
    print('last_entry---', last_entry)

    if last_entry:
        last_entry_dict = {
            'staff_attendance_id': str(last_entry.staff_attendance_id),
            'attendance_id': str(last_entry.attendance_id),
            'attendance_type': last_entry.attendance_type,
            'attempt_status': last_entry.attempt_status,
            'loggedin_user': str(last_entry.loggedin_user),
            'taken_by': str(last_entry.taken_by),
            'taken_for': str(last_entry.taken_for),
            'media_id': str(last_entry.media_id.media_id),
            'attendance_status': last_entry.attendance_status,
            'attendance_date_str': str(last_entry.attendance_date),
            'staff_attendance_geolocation': last_entry.staff_attendance_geolocation,
            'date_time': str(last_entry.date_time),
            'updated_at': str(last_entry.updated_at),
        }
        last_entry_attendance_date = last_entry.attendance_date

    print('last_entry_dict---', last_entry_dict)

    context = {
        'form': form,
        'is_profile': is_profile,
        'attendance_for_profile_qs': profile_qs,
        'old_check_in_out': check_in_out_data,
        'last_entry': last_entry_dict,
        'last_entry_attendance_date': last_entry_attendance_date,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


@login_required
def profile_update_view(request, **kwargs):
    profile_id = kwargs.get('profile_id')
    profile_qs = get_object_or_404(Profile, profile_id=profile_id)
    template_name = 'app_smart_attendance/module/profile/html/partial_profile_update.html'
    is_ask_password = False
    data = dict()
    if request.method == 'POST':
        user_form = forms.UpdateProfileUserForm(request.POST, user=request.user, instance=profile_qs.user)
        profile_form = forms.UpdateProfileProfileForm(request.POST, request.FILES, user=request.user,
                                                      instance=profile_qs)
        if user_form.is_valid() and profile_form.is_valid():
            # if profile_form.is_valid():

            user = user_form.save(commit=False)
            user.save()

            profile = profile_form.save(commit=False)
            profile.save()

            data['form_is_valid'] = True

            school_id = profile.school_id
            html_teacher_list = create_html_teacher_list(request, school_id)
            data['html_teacher_list'] = html_teacher_list

            data['message'] = 'Staff updated successfully.'
        else:
            data['form_is_valid'] = False
            data['message'] = 'Staff NOT updated successfully..!!'
    else:
        user_form = forms.UpdateProfileUserForm(user=request.user, instance=profile_qs.user)
        profile_form = forms.UpdateProfileProfileForm(user=request.user, instance=profile_qs)

    context = {
        'user_form': user_form,
        'profile_form': profile_form,
        'action_type': 'update',
        'profile_qs': profile_qs,
        'is_ask_password': is_ask_password
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "update"
    return JsonResponse(data)


@login_required
def teacher_update_view(request, **kwargs):
    profile_id = kwargs.get('profile_id')
    profile_qs = get_object_or_404(Profile, profile_id=profile_id)
    template_name = 'app_smart_attendance/html/partial_teacher_update.html'
    is_ask_password = False
    data = dict()
    if request.method == 'POST':
        user_form = forms.UpdateTeacherUserForm(data=request.POST, user=request.user, instance=profile_qs.user)
        profile_form = forms.UpdateTeacherProfileForm(data=request.POST, user=request.user, instance=profile_qs)
        if user_form.is_valid() and profile_form.is_valid():
            # if profile_form.is_valid():

            user = user_form.save(commit=False)
            user.save()

            profile = profile_form.save(commit=False)
            profile.save()

            data['form_is_valid'] = True

            # refresh the user instance value in request
            if user.username == request.user.username:
                request.user = user

            school_id = profile.school_id.school_id
            html_teacher_list = create_html_teacher_list(request, school_id)
            data['html_teacher_list'] = html_teacher_list

            data['message'] = 'Staff updated successfully.'
        else:
            data['form_is_valid'] = False
            data['message'] = 'Staff NOT updated successfully..!!'
    else:
        user_form = forms.UpdateTeacherUserForm(user=request.user, instance=profile_qs.user)
        profile_form = forms.UpdateTeacherProfileForm(user=request.user, instance=profile_qs)

    email = profile_qs.user.email

    context = {
        'user_form': user_form,
        'profile_form': profile_form,
        'action_type': 'update',
        'profile_qs': profile_qs,
        'email': email,
        'is_ask_password': is_ask_password
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "update"
    return JsonResponse(data)


def create_html_teacher_list(request, school_id):
    userprofile = request.user.profile

    role_list = []
    if request.user.profile.school_id.organisation_type == "school":
        role_list = [
            "school_admin",
            "teacher",
            "non_teacher"
        ]
    elif request.user.profile.school_id.organisation_type == "company":
        role_list = [
            "admin",
            "staff",
        ]
    elif request.user.profile.school_id.organisation_type == "anganwadi":
        role_list = [
            "admin",
            "staff",
        ]

    profile_filter_qs = Profile.objects.filter(school_id=school_id, role__in=role_list).exclude(
        profile_id=userprofile.profile_id)

    profile_filter_qs = [userprofile] + list(profile_filter_qs)

    html_teacher_list = render_to_string(template_name='app_smart_attendance/html/partial_teacher_list.html',
                                         context={
                                             'profile_filter_qs': profile_filter_qs,
                                         }, request=request)
    return html_teacher_list


@login_required
def period_update_view(request, **kwargs):
    period_id = kwargs.get('period_id')
    period_get_qs = get_object_or_404(PeriodModel, pk=period_id)
    # print('period_id---', period_id)
    template_name = 'app_smart_attendance/html/partial_period_update.html'

    data = dict()

    if request.method == 'POST':
        form = PeriodUpdateForm(request.POST, request.FILES, instance=period_get_qs, user=request.user)
        if form.is_valid():
            form_instance = form.save(commit=False)

            print('----form_instance.changed_data----', form.changed_data)
            is_check_period_exist = False

            if any(x in form.changed_data for x in ['day', 'class_name', 'section', 'period_number']):
                is_check_period_exist = True
            else:
                is_check_period_exist = False

            if is_check_period_exist:
                is_period_exist = check_is_period_exist(request, form_instance)
            else:
                is_period_exist = False

            if is_period_exist is True:
                data['form_is_valid'] = False
                data['message'] = 'Period is already exist.'
            elif is_period_exist is False:
                form_instance.save()
                school_id = form_instance.school_id
                html_period_list = create_html_period_list(request, school_id)
                data['html_period_list'] = html_period_list
                data['form_is_valid'] = True
                data['message'] = 'Period Updated successfully.'

        else:
            data['form_is_valid'] = False
    else:
        form = PeriodUpdateForm(instance=period_get_qs, user=request.user)
    context = {'form': form}
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "update"
    return JsonResponse(data)


@login_required
def period_delete_view(request, **kwargs):
    data = dict()

    period_get_qs = get_object_or_404(PeriodModel, pk=kwargs.get('period_id'))

    if request.user.profile.role == "school_admin" or request.user.profile.role == "admin":
        if request.method == 'POST':
            period_id = kwargs['period_id']
            period = PeriodModel.objects.filter(period_id=period_id)
            period.delete()

            school_id = period_get_qs.school_id
            after_delete_period_filter_qs = PeriodModel.objects.filter(school_id=school_id)
            data['form_is_valid'] = True
            data['html_period_list'] = render_to_string(request=request,
                                                        template_name='app_smart_attendance/html/partial_period_list.html',
                                                        context={
                                                            'period_filter_qs': after_delete_period_filter_qs
                                                        })
            data['message'] = "Period deleted successfully."
        else:
            context = {
                'period_get_qs': period_get_qs,
            }
            data['html_form'] = render_to_string(template_name='app_smart_attendance/html/partial_period_delete.html',
                                                 context=context, request=request)
            data['action_type'] = "delete"
    else:
        data['form_is_valid'] = False
        data['message'] = "You are not authorized person to do this. Please contact your school admin"

    return JsonResponse(data)


def create_html_period_list(request, school_id):
    period_filter_qs = PeriodModel.objects.filter(school_id=school_id)
    html_period_list = render_to_string(template_name='app_smart_attendance/html/partial_period_list.html',
                                        context={
                                            'period_filter_qs': period_filter_qs,
                                        }, request=request)
    return html_period_list


def create_html_student_list(request, school_id):
    profile_filter_qs = Profile.objects.filter(role="student", school_id=school_id)

    html_student_list = render_to_string(template_name='app_smart_attendance/html/partial_student_list.html',
                                         context={
                                             'profile_filter_qs': profile_filter_qs,
                                         }, request=request)
    return html_student_list


@login_required
def student_update_view(request, **kwargs):
    profile_id = kwargs.get('profile_id')
    profile_qs = get_object_or_404(Profile, pk=profile_id)
    print('profile_id---', profile_id)
    template_name = 'app_smart_attendance/html/partial_student_update.html'
    data = dict()
    if request.method == 'POST':
        user_form = forms.UpdateStudentUserForm(request.POST, user=request.user, instance=profile_qs.user)
        profile_form = forms.UpdateStudentProfileForm(request.POST, user=request.user, instance=profile_qs)
        print('user_form', user_form.is_valid())
        print('profile_form', profile_form.is_valid())
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save(commit=False)

            profile = profile_form.save(commit=False)

            print('----profile_form.changed_data----', profile_form.changed_data)
            is_period_exist = check_is_period_exist(request, profile, is_day_not_include=True)
            if is_period_exist is True:
                user.save()
                profile_form.save()
                school_id = profile.school_id
                html_student_list = create_html_student_list(request, school_id)
                data['html_student_list'] = html_student_list
                data['form_is_valid'] = True
                data['message'] = 'Student Updated successfully.'
            elif is_period_exist is False:
                data['form_is_valid'] = False
                data['message'] = 'Incorrect class name or section.'

        else:
            data['form_is_valid'] = False
    else:
        user_form = forms.UpdateStudentUserForm(user=request.user, instance=profile_qs.user)
        profile_form = forms.UpdateStudentProfileForm(user=request.user, instance=profile_qs)

    context = {
        'user_form': user_form,
        'profile_form': profile_form,
        'action_type': 'update',
        'profile_qs': profile_qs
    }

    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "update"
    return JsonResponse(data)


@login_required
def fetch_usernames_view_ajax(request):
    if request.is_ajax():
        results = request.POST['username']
        data_dict = {}
        data_list = []

        if request.organisation_type == "school":
            roles_list = [
                "school_admin",
                "teacher",
                "non_teacher",
            ]

        elif request.organisation_type == "company":
            roles_list = [
                "admin",
                "staff",
            ]
        elif request.organisation_type == "anganwadi":
            roles_list = [
                "admin",
                "staff",
            ]

        school_id = request.user.profile.school_id

        # search in firs_name, last_name, username
        users = User.objects.filter(
            Q(profile__role__in=roles_list),
            Q(profile__school_id=school_id),

            Q(username__icontains=results) |
            Q(first_name__icontains=results) |
            Q(last_name__icontains=results)
        )

        profile_picture_url = ''
        if results == '':
            data_list = []
        else:
            for user in users:
                if user.profile.profile_picture:
                    profile_picture_url = user.profile.profile_picture.url
                else:
                    profile_picture_url = ""
                data_dict = {
                    "username": user.username,
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "profile_picture": profile_picture_url,
                    "gender": user.profile.gender,
                    "role": user.profile.role,
                    "email": user.email,
                    "profile_id": str(user.profile.profile_id),
                }
                data_list.append(data_dict)

        data_list = json.dumps(data_list)
    else:
        data_list = 'fail'
    mimetype = 'application/json'
    # print(data_list)
    return HttpResponse(data_list, mimetype)


# working - change_password_v1
@login_required
def change_password(request):
    template = 'accounts/html/change_password_v1.html'
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('app_smart_attendance:change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)

    context = {
        'form': form
    }
    return render(request, template, context)
