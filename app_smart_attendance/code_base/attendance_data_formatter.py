import datetime
import operator

from app_root.models import Profile

def empty_entry_generator(date_list):
    data = []
    for empty_date in date_list:
        datetime_obj = datetime.datetime.strptime(empty_date, "%d-%m-%Y")
        # print(datetime_obj, datetime.datetime.today())
        data.append({'status': '',
                    # 'profile_id': key,
                    'attendance_date': empty_date,
                   })
    return data

def first_transformation(data):
    transformed_data = []
    transformed_data_dict = {}

    for item in data:
        if 'formatted_date' in item:
            attendance_date = item['attendance_date']
            formatted_date = item['formatted_date']
            temp_attendance = item['attendance_status']

            for attend in temp_attendance:
                if 'profile_id' in attend and attend['profile_id'] in transformed_data_dict:
                    attend['formatted_date'] = formatted_date
                    attend['attendance_date'] = attendance_date
                    transformed_data_dict[attend['profile_id']].append(attend)
                elif 'profile_id' in attend and attend['profile_id'] not in transformed_data_dict:
                    attend['formatted_date'] = formatted_date
                    attend['attendance_date'] = attendance_date
                    transformed_data_dict[attend['profile_id']] = [attend]
    return transformed_data_dict


def remove_duplicates(data):
    # print('---data.keys()---',data.keys())
    return_data = {}
    for key, value in data.items():
        if value:
            data[key] = [dict(t) for t in {tuple(d.items()) for d in value}]

    return data


def format_for_rendering(data_dict):
    data_list = []
    for key, value in data_dict.items():
        temp_dict = {}

        profile = Profile.objects.get(profile_id=key)
        # Add additional fields for rendering
        temp_dict['first_name'] = profile.user.first_name
        temp_dict['last_name'] = profile.user.last_name
        temp_dict['father_name'] = profile.father_name

        profile_url = ""
        if profile.profile_picture:
            profile_url = profile.profile_picture.url
        temp_dict['profile_url'] = profile_url

        temp_dict['profile_id'] = key
        temp_dict['data'] = value
        data_list.append(temp_dict)
    return data_list


def delete(lst, to_delete):
    if to_delete in lst:
        lst.remove(to_delete)
    return lst


def second_transformation(data, date_list, all_profile_list):
    # print('--- data.keys() ---',data.keys())
    return_data = {}
    for key, value in data.items():
        # 1. Load profile model
        # profile = Profile.objects.get(profile_id=key)

        # print("\n\n----", key, "----")
        new_date_list = date_list.copy()
        # print("--- new_date_list : ", new_date_list, "----")
        temp_dict = {}
        ##2.  Handle cases - First take care of present attendance separately
        for ele in value:
            if ele['status'] == 'present':
                if ele['formatted_date'] not in temp_dict:
                    temp_dict[ele['formatted_date']] = 'present'
                    new_date_list = delete(new_date_list, ele['formatted_date'])
                    # print("--- present --- delete date : ", ele['formatted_date'], new_date_list)


        new_val = value.copy()
        # print('---new_val---1---',new_val)

        ##3.1  Handle cases - Leave attendance for a given date
        # todo - handle leave cases
        for ele in value:
            if ele['status'] == 'leave':
                if ele['formatted_date'] not in temp_dict:
                    temp_dict[ele['formatted_date']] = 'leave'
                    new_date_list = delete(new_date_list, ele['formatted_date'])
                    # print("--- absent --- delete date: ", ele['formatted_date'], new_date_list)

                else:
                    new_date_list = delete(new_date_list, ele['formatted_date'])
                    new_val = delete(new_val, ele)
                    # print("--- absent --- delete date in ELSE block : ", ele['formatted_date'], new_date_list)

            if 'profile_id' in ele:
                ele.pop('profile_id')

        ##3.2  Handle cases - Then take care of absent attendance separately
        for ele in value:
            if ele['status'] == 'absent':
                if ele['formatted_date'] not in temp_dict:
                    temp_dict[ele['formatted_date']] = 'absent'
                    new_date_list = delete(new_date_list, ele['formatted_date'])
                    # print("--- absent --- delete date: ", ele['formatted_date'], new_date_list)

                else:
                    new_date_list = delete(new_date_list, ele['formatted_date'])
                    new_val = delete(new_val, ele)
                    # print("--- absent --- delete date in ELSE block : ", ele['formatted_date'], new_date_list)

            if 'profile_id' in ele:
                ele.pop('profile_id')



        ##4.  Handle cases - No Attendance available for a given date
        # print("--- new_date_list: ", new_date_list)
        for empty_date in new_date_list:
            datetime_obj = datetime.datetime.strptime(empty_date, "%d-%m-%Y")
            # print('------', datetime_obj, datetime.datetime.today())

            # if datetime_obj.date() < datetime.datetime.today().date():
            #     new_val.append({'status': '',
            #                     # 'profile_id': key,
            #                     'formatted_date': empty_date,
            #                     'attendance_date': datetime_obj.date(),
            #                     })
            # else:
            #     new_val.append({'status': '',
            #                     # 'profile_id': key,
            #                     'formatted_date': empty_date,
            #                     'attendance_date': datetime_obj.date(),
            #                     })

            #
            new_val.append({'status': '',
                            # 'profile_id': key,
                            'formatted_date': empty_date,
                            'attendance_date': datetime_obj.date(),
                            })

        ##5. Sort by date <String>
        # print('---operator.itemgetter("formatted_date")---',operator.itemgetter('formatted_date'))
        # print('---type operator.itemgetter("formatted_date")---',type(operator.itemgetter('formatted_date')))
        # print('---new_val---2---',new_val)

        # new_val.sort(key=operator.itemgetter('attendance_date'))
        new_val.sort(key=lambda item: item['attendance_date'])

        return_data[key] = new_val
        try:
            all_profile_list.remove(key)
        except Exception as e:
            print('---all_profile_list.remove(key) Exception---',e)

    for profile in all_profile_list:
        return_data[profile] = empty_entry_generator(date_list)


    return return_data


def data_processor(attendance_data, date_list, all_profile_list):
    return_data = []
    # print("--- attendance_data --\n", attendance_data)

    transform1_data = first_transformation(attendance_data)
    # print("--- transform_data --\n", transform1_data)

    duplicate_removed_data = remove_duplicates(transform1_data)
    # print("--- Remove duplicates --\n", duplicate_removed_data)

    date_list = [item.strftime("%d-%m-%Y") for item in date_list]
    # print("--- date_list --\n", date_list)

    transform_2_data = second_transformation(duplicate_removed_data, date_list, all_profile_list)
    # print("--- transform_2_data -- ", transform_2_data)

    # 6. Render formatting
    return_data = format_for_rendering(transform_2_data)
    # print("--- return_data -- ", return_data)

    return return_data


def main():
    # Temporary hardcode date_list
    date_list = [datetime.datetime(2023, 1, 2, 0, 0), datetime.datetime(2023, 1, 3, 0, 0),
                 datetime.datetime(2023, 1, 4, 0, 0), datetime.datetime(2023, 1, 5, 0, 0),
                 datetime.datetime(2023, 1, 6, 0, 0), datetime.datetime(2023, 1, 7, 0, 0),
                 datetime.datetime(2023, 1, 8, 0, 0)]

    attendance_data = [
        {
            'formatted_date': '02-01-2023',
            'attendance_status': [{
                'status': 'absent',
                'profile_id': '10936583-57e8-45cd-abb0-3834807d7918'
            }, {
                'status': 'present',
                'profile_id': '0760a904-5098-4553-bd48-ac958b1aea37'
            }]
        },
        {
            'formatted_date': '03-01-2023',
            'attendance_status': [{
                'status': 'absent',
                'profile_id': '10936583-57e8-45cd-abb0-3834807d7918'
            }, {
                'status': 'present',
                'profile_id': '0760a904-5098-4553-bd48-ac958b1aea37'
            }]
        },
        {
            'formatted_date': '02-01-2023',
            'attendance_status': [{
                'status': 'absent',
                'profile_id': '10936583-57e8-45cd-abb0-3834807d7918'
            }, {
                'status': 'absent',
                'profile_id': '0760a904-5098-4553-bd48-ac958b1aea37'
            }]
        },
        {
            'formatted_date': '03-01-2023',
            'attendance_status': [{
                'status': 'present',
                'profile_id': '10936583-57e8-45cd-abb0-3834807d7918'
            }, {
                'status': 'absent',
                'profile_id': '0760a904-5098-4553-bd48-ac958b1aea37'
            }]
        }]

    # Main entry point to the codebase
    data_processor(attendance_data, date_list)


# if __name__ == '__main__':
#     main()
