def get_date_range_presence_by_profile_class(
        request,
        single_profile_qs,
        attendance_filter_qs,
        date_list=None,
):
    data = []
    for date_ in date_list:
        date_str = str(date_.date())
        date_only = date_.date()
        at_qs_date = attendance_filter_qs.filter(attendance_date=date_only)

        # get consolidated attendance of a day in all given periods or entries
        presence_status = get_consolidated_attendance_in_qs_by_profile_id(at_qs_date, single_profile_qs)

        # create consolidated data for all cases like: present, absent, leave, empty("")
        data.append({
            "status": presence_status,
            "date": date_str
        })

    return_data = data
    print('---return_data---', return_data)

    # return consolidated data of given date range of a profile
    return return_data


def get_consolidated_attendance_in_qs_by_profile_id(at_qs_date, single_profile_qs):
    presence_status = ""
    for item in at_qs_date:
        attendance_status = item["attendance_status"]
        profile_id_str = str(single_profile_qs.profile_id)

        # check attendance in each period or entry
        profile_presence = check_presence_in_list_of_dict_by_profile_id(attendance_status, profile_id_str)

        # for first time
        if not presence_status:
            presence_status = profile_presence

        # override absent to present
        elif presence_status == "absent" and profile_presence == "present":
            presence_status = profile_presence

        # no change absent == absent
        elif presence_status == "absent" and profile_presence == "absent":
            pass

        # override absent to leave
        elif presence_status == "absent" and profile_presence == "leave":
            presence_status = profile_presence

        # no change present == present
        elif presence_status == "present" and profile_presence == "present":
            pass

        # ignore this entry
        elif presence_status == "present" and profile_presence == "absent":
            pass

        # stay on present
        elif presence_status == "present" and profile_presence == "leave":
            pass

        # override leave to present
        elif presence_status == "leave" and profile_presence == "present":
            presence_status = profile_presence

        # stay on leave
        elif presence_status == "leave" and profile_presence == "absent":
            pass

        # no change leave == leave
        elif presence_status == "leave" and profile_presence == "leave":
            pass

        else:
            pass

    # return consolidated presence_status in a date(day)
    return presence_status


def check_presence_in_list_of_dict_by_profile_id(attendance_status, profile_id_str):
    return next((item["status"] for item in attendance_status if item["profile_id"] == profile_id_str), None)
