from datetime import datetime, timedelta, time
from dateutil.relativedelta import relativedelta


def min_time_of_day(date):
    min_pub_date_time = datetime.combine(date, time.min)
    return min_pub_date_time


def max_time_of_day(date):
    max_pub_date_time = datetime.combine(date, time.max)
    return max_pub_date_time


def day_range_of_week(date):
    start = date + timedelta(days=-date.weekday())
    end = start + timedelta(days=6)
    return [min_time_of_day(start), max_time_of_day(end)]


def day_range_of_month(date):
    start = date.replace(day=1)

    if date.month == 12:
        end = date.replace(day=31)
    else:
        end = date.replace(month=date.month + 1, day=1) - timedelta(days=1)
    return [min_time_of_day(start), max_time_of_day(end)]


def get_date_range(request_date, range_type):
    # print("Input date : ", request_date, type(request_date))
    request_datetime = datetime.combine(request_date, datetime.min.time())
    # print(request_datetime)
    request_datetime = request_datetime.astimezone()
    current_date_range = []
    previous_date_range = []
    next_date_range = []

    if range_type == "day":
        # print("Process daily codebase")
        current_date_range = [min_time_of_day(request_datetime), max_time_of_day(request_datetime)]
        previous_date_range = min_time_of_day(request_datetime - timedelta(days=1))
        next_date_range = min_time_of_day(request_datetime + timedelta(days=1))
        return current_date_range, previous_date_range, next_date_range

    elif range_type == "week":
        # print("Process weekly codebase")
        current_date_range = day_range_of_week(request_datetime)
        previous_date_range = day_range_of_week(request_datetime - timedelta(days=7))[0]
        next_date_range = day_range_of_week(request_datetime + timedelta(days=7))[0]
        return current_date_range, previous_date_range, next_date_range

    elif range_type == "month":
        # print("process monthly codebase")
        current_date_range = day_range_of_month(request_datetime)
        previous_date_range = day_range_of_month(request_datetime + relativedelta(months=-1))[0]
        next_date_range = day_range_of_month(request_datetime + relativedelta(months=+1))[0]
        return current_date_range, previous_date_range, next_date_range
    else:
        # print("Wrong date selection")
        request_datetime = datetime.combine(datetime.today(), datetime.min.time())
        request_datetime = request_date.astimezone()
        current_date_range = day_range_of_week(request_datetime)
        previous_date_range = day_range_of_week(request_datetime - timedelta(days=7))[0]
        next_date_range = day_range_of_week(request_datetime + timedelta(days=7))[0]
        return current_date_range, previous_date_range, next_date_range


# print(get_date_range(datetime.today().date(), "day"))
#
print(get_date_range(datetime.today().date()- timedelta(days=6), "month"))
#
# print(get_date_range(datetime.today().date(), "month"))

# date = datetime.today().date()
#
# print(min_time_of_day(date))

