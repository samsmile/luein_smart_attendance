"""luein_smart_attendance URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

import notifications.urls
from app_root import views as root_views
from app_smart_attendance import views as app_smart_attendance_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('app_root.urls', namespace='root')),
    path('app/', include('app_smart_attendance.urls', namespace='app_smart_attendance')),

    # Captcha URL
    path('captcha/', include('captcha.urls'), name='captcha-image'),


    # Auth URLs
    path('login/', root_views.custom_login_view, name='login'),
    path('logout/', root_views.logout_view, name='logout'),

    # FACE RECOG APIS
    path('api/', include('api_face_recog.urls', namespace='face_recog')),


    # file manager
    path('file_manager/', include('app_file_manager.urls', namespace='app_file_manager')),

    # notifications
    path('inbox/notifications/', include(notifications.urls, namespace='notifications')),

    # pwa
    path('', include('pwa.urls')),

    # django-webpush
    path(r'webpush/', include('webpush.urls')),

    path('ajax/search/', app_smart_attendance_views.fetch_usernames_view_ajax, name='search'),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


