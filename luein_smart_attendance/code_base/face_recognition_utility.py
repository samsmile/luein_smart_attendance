import logging as log_print
import os
import pickle
import traceback
from pathlib import Path
import boto3
import pandas as pd


def setup_custom_logger(name):
    formatter = log_print.Formatter(fmt='%(asctime)s - %(process)d - %(levelname)s - %(message)s')
    handler = log_print.StreamHandler()
    handler.setFormatter(formatter)
    logger = log_print.getLogger(name)
    logger.setLevel(log_print.INFO)
    logger.addHandler(handler)
    return logger


log = setup_custom_logger("face_recog_utils")


class TrainingUtils:
    def __init__(self, bucket):
        self.db_path = None
        session = boto3.Session(profile_name="dev")
        self.s3 = session.client('s3')
        self.BUCKET_NAME = bucket

    """
        This method is used for creating folder for to-be-trained images inside media
        Args:
            - school_id: string representation of school uuid
            - role: profile role
            - profile_id: string representation of profile uuid
    """

    def create_media_folder_for_extraction(self, school_id, role, profile_id, root_folder=None, mode="incremental"):
        root_path = os.getcwd()
        if root_folder:
            root_path = root_folder
        media_path = os.path.join(root_path, "media")
        folder_path = os.path.join(media_path, "face_recog_train_extract")
        resources = Path(media_path)

        school_id = str(school_id)
        role = str(role)
        profile_id = str(profile_id)

        folders = [
            f"face_recog_train_extract/waiting_folder/{school_id}/{role}/{profile_id}",
            f"face_recog_train_extract/training_folder/{school_id}/{role}/{profile_id}",
        ]
        if mode == "scratch":
            folders = [f"face_recog_train_extract/training_folder/{school_id}/{role}/{profile_id}"]

        for folder in folders:
            folder_p = Path.joinpath(resources, folder)
            folder_p.mkdir(parents=True, exist_ok=True)

        waiting_folder_path = os.path.join(folder_path, 'waiting_folder', school_id, role, profile_id)
        if mode == "scratch":
            waiting_folder_path = waiting_folder_path.replace("waiting_folder", "training_folder")
        return waiting_folder_path

    """
        Save training representations
    """

    def save_representations(self, pickle_folder, file_name, representations):
        prev_representations = list()
        if os.path.exists(os.path.join(pickle_folder, file_name)):
            with open(os.path.join(pickle_folder, file_name), 'rb+') as f:
                prev_representations = pickle.load(f)

        with open(os.path.join(pickle_folder, file_name), 'wb') as f:
            combined_data = prev_representations
            for x in representations:
                combined_data.append(x)
            pickle.dump(combined_data, f)

    """
        Create pickle save / load hierarchy
    """

    def create_pickle_folder(self, school_id, root_folder=None):
        root_path = os.getcwd()
        if root_folder:
            root_path = root_folder
        resources_path = os.path.join(root_path, "resources")
        resources = Path(resources_path)

        folders = [f"attendance_prod_models/face_recog_db/{school_id}"]
        for folder in folders:
            folder_path = Path.joinpath(resources, folder)
            folder_path.mkdir(parents=True, exist_ok=True)

        db_path = os.path.join(resources_path, 'attendance_prod_models', 'face_recog_db', school_id)
        return db_path

    def load_representations(self, files_to_re_download, root_folder=None):
        is_data_present_in_s3 = False
        list_of_school_trained = set()
        # -------------REPRESENTATIONS FILE------------
        representation_file = "representations.pkl"
        # ---------------------------------------------

        root_path = os.getcwd()
        if root_folder:
            root_path = root_folder
        resources = os.path.join(root_path, "resources")

        bucket = self.s3.list_objects(Bucket=self.BUCKET_NAME, Prefix="representation")
        # print(bucket)
        if bucket['ResponseMetadata']['HTTPStatusCode'] == 200:
            if 'Contents' in bucket:
                data = {}
                for file in bucket['Contents']:
                    url = file['Key']
                    txt_split = url.split('/')
                    if len(txt_split) == 3 and ".pkl" not in txt_split[1]:
                        if txt_split[2] and txt_split[1] in files_to_re_download:
                            data.setdefault(txt_split[1], list()).append(
                                {"file": txt_split[2], "datetime": file['LastModified'], "key": url})
                            list_of_school_trained.add(txt_split[1])

                print("data", data)

                path = os.path.join(resources, 'attendance_prod_models', 'face_recog_db')
                for key, val in data.items():
                    files = sorted(val, key=lambda d: d['datetime'], reverse=True)
                    if files:
                        is_data_present_in_s3 = True
                        file_key = files[0]['key']
                        if not os.path.exists(os.path.join(path, key)):
                            os.mkdir(os.path.join(path, key))
                        log.warning("downloading : %s", file_key)
                        self.s3.download_file(self.BUCKET_NAME, file_key,
                                              os.path.join(resources, 'attendance_prod_models', 'face_recog_db', key,
                                                           representation_file))
            else:
                log.error("-- NO DATA AVAILABLE IN S3 WITH PROVIDED PREFIX ---")
        return is_data_present_in_s3, list(list_of_school_trained)

    def load_api_config_data(self, root_folder=None, pattern=None, files_to_skip=None):
        is_downladed = False
        file_names = []
        folders = []

        default_kwargs = {
            "Bucket": self.BUCKET_NAME,
            "Prefix": "api"
        }

        next_token = ""

        while next_token is not None:
            updated_kwargs = default_kwargs.copy()
            if next_token != "":
                updated_kwargs["ContinuationToken"] = next_token

            response = self.s3.list_objects_v2(**default_kwargs)
            contents = response.get("Contents")
            if contents:
                for result in contents:
                    key = result.get("Key")
                    if key[-1] == "/":
                        folders.append(key)
                    else:
                        file_names.append(key)

                next_token = response.get("NextContinuationToken")

        if file_names:
            file_names_updated = []
            if files_to_skip:
                for file in file_names:
                    if pattern in file:
                        file_name = file.split("/")[-1]
                        # print("file:",file)
                        if file_name not in files_to_skip:
                            file_names_updated.append(file)
            else:
                if pattern:
                    for key in file_names:
                        if pattern in key:
                            file_names_updated.append(key)
                else:
                    file_names_updated = file_names.copy()

            root_path = os.getcwd()
            if root_folder:
                root_path = root_folder
            local_path = os.path.join(root_path, "resources")

            local_path = Path(local_path)

            for folder in folders:
                folder_path = Path.joinpath(local_path, folder)
                folder_path.mkdir(parents=True, exist_ok=True)

            for file_name in file_names_updated:
                log.warning("downloading : %s", file_name)
                file_path = Path.joinpath(local_path, file_name)
                file_path.parent.mkdir(parents=True, exist_ok=True)
                self.s3.download_file(
                    self.BUCKET_NAME,
                    file_name,
                    str(file_path)
                )
            is_downladed = True
        else:
            log.error("-- NO DATA AVAILABLE IN S3 WITH PROVIDED PREFIX ---")
        return is_downladed

    def train_school_data_from_scratch(self, to_be_trained_from_scratch, root_folder):
        from django.conf import settings
        from app_smart_attendance.src.training.train_utils import update_database
        from app_smart_attendance.src.utils import extract_images_from_blobs

        # print(root_folder)
        list_of_images = list()
        log.info("--- FROM SCRATCH TRAINING STARTED ---")
        try:
            for profile in to_be_trained_from_scratch:
                school_id = str(profile.school_id.school_id)
                print("-- school_id : ", school_id)
                pickle_folder = self.create_pickle_folder(school_id, root_folder)
                training_images_path = ""
                profile_id = str(profile.profile_id)
                role = profile.role
                if not training_images_path:
                    training_images_path = os.path.join(root_folder, "media", "face_recog_train_extract",
                                                        'training_folder', school_id)
                media_file = root_folder + profile.media_id.media_file.url
                print("media_file: ", media_file)
                if os.path.exists(media_file):
                    waiting_folder_path = self.create_media_folder_for_extraction(school_id, role, profile_id,
                                                                                  root_folder, mode="scratch")
                    extract_images_from_blobs(waiting_folder_path, video_file=media_file)
                    for file in os.listdir(waiting_folder_path):
                        list_of_images.append(os.path.join(waiting_folder_path, file))
                print("-- training_images_path: ", training_images_path)
                print("-- total list_of_images: ", len(list_of_images))
                if list_of_images:
                    update_database(list_of_images=list_of_images, training_images_path=training_images_path,
                                    pickle_folder=pickle_folder,
                                    model_name=settings.RECOGNIION_MODEL_NAME,
                                    detector_backend=settings.RECOGNIION_DETECTOR,
                                    model=settings.RECOGNIION_MODEL,
                                    distance_metric=settings.RECOGNIION_METRICS, disable_prog_bar=False)
        except:
            traceback.print_exc()
            log.exception("EXCEPTION OCCURRED during train FROM SCRATCH!!! ")
        else:
            log.info("MODEL UPDATED SUCCESSFULLY!!!")
        log.info("--- FROM SCRATCH TRAINING COMPLETED ---")

    def update_representations_file_after_download(self, list_of_school_trained, roles, root_folder):
        from app_root.models import Profile
        from django.conf import settings
        from app_smart_attendance.src.training.train_utils import update_database
        from app_smart_attendance.src.utils import extract_images_from_blobs

        resources_path = os.path.join(root_folder, "resources")
        pickle_folder_root = os.path.join(resources_path, 'attendance_prod_models', 'face_recog_db')
        profiles = Profile.objects.filter(school_id__in=list_of_school_trained, role__in=roles, is_registered=True)
        # print("-- profiles: ",profiles)
        list_of_images = list()

        for profile in profiles:
            school_id = str(profile.school_id.school_id)
            profile_id = str(profile.profile_id)
            role = profile.role
            pickle_folder = os.path.join(pickle_folder_root, school_id)
            training_images_path = ""

            if os.path.exists(pickle_folder):
                try:
                    with open(os.path.join(pickle_folder, "representations.pkl"), 'rb') as f:
                        representations = pickle.load(f)
                        df = pd.DataFrame(representations, columns=['identity', 'representations'])
                        if "\\" in pickle_folder:
                            sep = '\\'
                        else:
                            sep = '/'
                        df['profile_id'] = df['identity'].apply(lambda x: x.split(sep)[-2])
                        # print(df)
                        available_profiles = df.groupby('profile_id').groups
                        print("-- available_profiles: ", available_profiles)
                        if profile_id not in available_profiles:
                            print("-- profile: ", profile)
                            if not training_images_path:
                                training_images_path = os.path.join(root_folder, "media", "face_recog_train_extract",
                                                                    'training_folder', school_id)
                            media_file = root_folder + profile.media_id.media_file.url
                            print("media_file: ", media_file)
                            if os.path.exists(media_file):
                                waiting_folder_path = self.create_media_folder_for_extraction(school_id, role,
                                                                                              profile_id,
                                                                                              root_folder,
                                                                                              mode="scratch")
                                extract_images_from_blobs(waiting_folder_path, video_file=media_file)
                                for file in os.listdir(waiting_folder_path):
                                    list_of_images.append(os.path.join(waiting_folder_path, file))
                            print("-- training_images_path: ", training_images_path)
                            print("-- total list_of_images: ", len(list_of_images))
                            if list_of_images:
                                update_database(list_of_images=list_of_images,
                                                training_images_path=training_images_path,
                                                pickle_folder=pickle_folder,
                                                model_name=settings.RECOGNIION_MODEL_NAME,
                                                detector_backend=settings.RECOGNIION_DETECTOR,
                                                model=settings.RECOGNIION_MODEL,
                                                distance_metric=settings.RECOGNIION_METRICS, disable_prog_bar=False)
                except:
                    traceback.print_exc()
                    log.exception("Exception occurred in 'update_representations_file_after_download' call!!!")
                else:
                    if list_of_images:
                        log.info(" Representation file updated!!! ")
                    else:
                        log.info(" Representation file up to date!!! ")
            else:
                log.error("%s folder doesn't exists!!", pickle_folder)

    def check_resources_exists(self, root_folder=None):

        from app_root.models import SchoolModel, Profile

        root_path = os.getcwd()
        if root_folder:
            root_path = root_folder

        resources_path = os.path.join(root_path, "resources")
        resources = Path(resources_path)
        schools = SchoolModel.objects.all()

        # -------------- API_CONFIG ----------------
        log.info("DOWNLOADING RESOURCES API_CONFIG DATA")
        folders = ["api_face_recog"]
        for folder in folders:
            folder_path = Path.joinpath(resources, folder)
            folder_path.mkdir(parents=True, exist_ok=True)
        for sub_dir in ['config', 'model/.deepface/weights']:
            files_to_skip = []
            for dirpath, dirnames, files in os.walk(
                    os.path.join(resources_path, "api_face_recog", sub_dir)):
                if files:
                    # print(dirpath)
                    for x in files:
                        files_to_skip.append(x)
            print("-- FILES ALREADY AVAILABLE : ", files_to_skip)
            is_downladed = False
            if not files_to_skip:
                is_downladed = self.load_api_config_data(root_folder=root_path, pattern=sub_dir,
                                                         files_to_skip=files_to_skip)
            if is_downladed:
                log.info("DOWNLOADING RESOURCES API_CONFIG DATA COMPLETED")
            else:
                log.info("FILES ALREADY EXISTS")
        # -------------- API_CONFIG ----------------

        # -------------- REPRESENTATIONS ----------------
        log.info("DOWNLOADING RESOURCES ATTENDANCE REPRESENTATIONS DATA")
        folders = ["attendance_prod_models/face_recog_db"]
        for folder in folders:
            folder_path = Path.joinpath(resources, folder)
            folder_path.mkdir(parents=True, exist_ok=True)

        files_to_re_download = set()
        count = 0
        for dirpath, dirnames, files in os.walk(
                os.path.join(resources_path, "attendance_prod_models", "face_recog_db")):
            for school in schools:
                if str(school.school_id) in dirpath:
                    if not files:
                        files_to_re_download.add(str(school.school_id))
            count += 1
        files_to_re_download = list(files_to_re_download)
        print("-- REPRESENTATIONS FILES TO RE-DOWNLOAD : ", files_to_re_download)

        is_data_present_in_s3 = False
        list_of_school_trained = list()
        if files_to_re_download:
            is_data_present_in_s3, list_of_school_trained = self.load_representations(root_folder=root_path,
                                                                                      files_to_re_download=files_to_re_download)

        roles = ['teacher', 'non_teacher', 'student']

        if is_data_present_in_s3:
            self.update_representations_file_after_download(list_of_school_trained, roles, root_folder=root_path)
            log.info("DOWNLOADING RESOURCES ATTENDANCE REPRESENTATIONS DATA COMPLETED")
        else:
            log.info("ATTENDANCE REPRESENTATIONS ALREADY EXISTS!!!")

        res_path_1 = os.path.join(resources_path, "attendance_prod_models", "face_recog_db")
        available_school_representations = os.listdir(res_path_1)
        print("-- available_school_representations : ", available_school_representations)

        profiles = Profile.objects.filter(school_id__in=schools, role__in=roles,
                                          is_registered=True)
        # print("-- profiles: ", profiles)

        to_be_trained_from_scratch = list()
        for profile in profiles:
            if str(profile.school_id.school_id) not in available_school_representations:
                folder_path = os.path.join(res_path_1, str(profile.school_id.school_id))
                if not os.path.exists(folder_path):
                    os.mkdir(folder_path)
                if len(os.listdir(folder_path)) == 0:
                    to_be_trained_from_scratch.append(profile)

        print("-- to_be_trained_from_scratch: ", to_be_trained_from_scratch)
        if to_be_trained_from_scratch:
            self.train_school_data_from_scratch(to_be_trained_from_scratch, root_folder=root_path)
        # -------------- REPRESENTATIONS ----------------
