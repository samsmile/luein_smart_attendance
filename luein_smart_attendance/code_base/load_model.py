from deepface import DeepFace
import json
import pickle
import logging

"""
    API_FACE_RECOGNITION
"""


class LazyLoadDeepFaceModel:
    def __init__(self, name):
        self.NAME = name
        self.MODEL_USE = None

    def get_model_obj(self):
        if not self.MODEL_USE:
            print('**** LOADING MODEL: ', self.NAME, " ****")
            self.MODEL_USE = DeepFace.build_model(self.NAME)
            return self.MODEL_USE
        else:
            print(f"----- Loading existing {self.NAME} model !! ------")
        return self.MODEL_USE
