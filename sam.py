# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class ApiFaceRecogFacerecogmodel(models.Model):
    user_id = models.CharField(primary_key=True, max_length=512)
    status = models.CharField(max_length=20)
    is_valid = models.BooleanField(blank=True, null=True)
    message = models.CharField(max_length=512, blank=True, null=True)
    logs = models.JSONField(blank=True, null=True)
    input_file = models.CharField(max_length=500)
    metadata = models.JSONField()
    date_time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'api_face_recog_facerecogmodel'


class AppFileManagerFile(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=128)
    file = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    folder = models.ForeignKey('AppFileManagerFolder', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_file_manager_file'


class AppFileManagerFolder(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=128)
    text_color = models.CharField(max_length=32)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    parent = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_file_manager_folder'


class AppRootAnnouncementmodel(models.Model):
    announcement_id = models.UUIDField(primary_key=True)
    title = models.CharField(max_length=512, blank=True, null=True)
    content = models.CharField(max_length=512, blank=True, null=True)
    extra = models.JSONField(blank=True, null=True)
    is_active = models.BooleanField(blank=True, null=True)
    expire_date = models.DateTimeField(blank=True, null=True)
    date_time = models.DateTimeField()
    updated_at = models.DateTimeField()
    created_by = models.ForeignKey('AppRootProfile', models.DO_NOTHING, blank=True, null=True)
    school_id = models.ForeignKey('AppRootSchoolmodel', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_root_announcementmodel'


class AppRootAttendancemodel(models.Model):
    attendance_id = models.UUIDField(primary_key=True)
    attempt_status = models.CharField(max_length=20, blank=True, null=True)
    attendance_status = models.JSONField(blank=True, null=True)
    attendance_date = models.DateField(blank=True, null=True)
    date_time = models.DateTimeField()
    updated_at = models.DateTimeField()
    media_id = models.ForeignKey('AppRootMediacapturedmodel', models.DO_NOTHING, blank=True, null=True)
    period_id = models.ForeignKey('AppRootPeriodmodel', models.DO_NOTHING, blank=True, null=True)
    loggedin_user = models.ForeignKey('AppRootProfile', models.DO_NOTHING, blank=True, null=True)
    staff_attendance_id = models.CharField(max_length=512, blank=True, null=True)
    attendance_taker = models.ForeignKey('AppRootProfile', models.DO_NOTHING, blank=True, null=True)
    attendance_choice = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_root_attendancemodel'


class AppRootCameramodel(models.Model):
    camera_id = models.UUIDField(primary_key=True)
    title = models.CharField(max_length=512, blank=True, null=True)
    url = models.CharField(max_length=512, blank=True, null=True)
    ip_address = models.CharField(max_length=512, blank=True, null=True)
    port = models.CharField(max_length=512, blank=True, null=True)
    extra = models.JSONField(blank=True, null=True)
    is_active = models.BooleanField(blank=True, null=True)
    status = models.CharField(max_length=512, blank=True, null=True)
    user = models.CharField(max_length=512, blank=True, null=True)
    password = models.CharField(max_length=512, blank=True, null=True)
    type = models.CharField(max_length=512, blank=True, null=True)
    mac_address = models.CharField(max_length=512, blank=True, null=True)
    resolution = models.CharField(max_length=512, blank=True, null=True)
    active_date_time = models.DateTimeField(blank=True, null=True)
    date_time = models.DateTimeField()
    updated_at = models.DateTimeField()
    added_by = models.ForeignKey('AppRootProfile', models.DO_NOTHING, blank=True, null=True)
    school_id = models.ForeignKey('AppRootSchoolmodel', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_root_cameramodel'


class AppRootCameraresponsemodel(models.Model):
    camera_response_id = models.UUIDField(primary_key=True)
    role = models.CharField(max_length=30, blank=True, null=True)
    cctv_attendance_status = models.JSONField(blank=True, null=True)
    cctv_attempt_status = models.CharField(max_length=20, blank=True, null=True)
    attendance_date = models.DateField(blank=True, null=True)
    cctv_attendance_type = models.CharField(max_length=20, blank=True, null=True)
    date_time = models.DateTimeField()
    updated_at = models.DateTimeField()
    camera_id = models.ForeignKey(AppRootCameramodel, models.DO_NOTHING, blank=True, null=True)
    loggedin_user = models.ForeignKey('AppRootProfile', models.DO_NOTHING, blank=True, null=True)
    media_id = models.ForeignKey('AppRootMediacapturedmodel', models.DO_NOTHING, blank=True, null=True)
    school_id = models.ForeignKey('AppRootSchoolmodel', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_root_cameraresponsemodel'


class AppRootGeolocationmodel(models.Model):
    geolocation_id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=512, blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    coordinates = models.JSONField(blank=True, null=True)
    geofence = models.JSONField(db_column='geoFence', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(max_length=20, blank=True, null=True)
    is_active = models.BooleanField()
    extra_data = models.JSONField(blank=True, null=True)
    date_time = models.DateTimeField()
    updated_at = models.DateTimeField()
    school_id = models.ForeignKey('AppRootSchoolmodel', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_root_geolocationmodel'


class AppRootMediacapturedmodel(models.Model):
    media_id = models.UUIDField(primary_key=True)
    media_file = models.CharField(max_length=100, blank=True, null=True)
    media_directory = models.CharField(max_length=512, blank=True, null=True)
    media_type = models.CharField(max_length=10)
    extra_data = models.JSONField(blank=True, null=True)
    date_time = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'app_root_mediacapturedmodel'


class AppRootPeriodmodel(models.Model):
    period_id = models.UUIDField(primary_key=True)
    day = models.CharField(max_length=1)
    class_name = models.CharField(max_length=10, blank=True, null=True)
    section = models.CharField(max_length=100, blank=True, null=True)
    period_number = models.CharField(max_length=2, blank=True, null=True)
    subject = models.CharField(max_length=100, blank=True, null=True)
    created_by = models.CharField(max_length=512, blank=True, null=True)
    date_time = models.DateTimeField()
    updated_at = models.DateTimeField()
    school_id = models.ForeignKey('AppRootSchoolmodel', models.DO_NOTHING, blank=True, null=True)
    teacher_id = models.ForeignKey('AppRootProfile', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_root_periodmodel'


class AppRootProfile(models.Model):
    profile_id = models.UUIDField(primary_key=True)
    role = models.CharField(max_length=30, blank=True, null=True)
    profile_picture = models.CharField(max_length=100, blank=True, null=True)
    contact_number = models.CharField(max_length=128, blank=True, null=True)
    gender = models.CharField(max_length=20, blank=True, null=True)
    blood_group = models.CharField(max_length=20, blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    postal_code = models.CharField(max_length=100, blank=True, null=True)
    class_name = models.CharField(max_length=10, blank=True, null=True)
    section = models.CharField(max_length=100, blank=True, null=True)
    view_count = models.IntegerField(blank=True, null=True)
    date_time = models.DateTimeField()
    updated_at = models.DateTimeField()
    media_id = models.ForeignKey(AppRootMediacapturedmodel, models.DO_NOTHING, blank=True, null=True)
    school_id = models.ForeignKey('AppRootSchoolmodel', models.DO_NOTHING, blank=True, null=True)
    user = models.OneToOneField('AuthUser', models.DO_NOTHING)
    is_registered = models.BooleanField(blank=True, null=True)
    unique_id = models.CharField(max_length=100, blank=True, null=True)
    father_name = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_root_profile'


class AppRootSchoolmodel(models.Model):
    school_id = models.UUIDField(primary_key=True)
    branch = models.CharField(max_length=512, blank=True, null=True)
    name = models.CharField(max_length=512, blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    postal_code = models.CharField(max_length=100, blank=True, null=True)
    is_verified = models.BooleanField()
    date_time = models.DateTimeField()
    updated_at = models.DateTimeField()
    school_logo = models.CharField(max_length=100, blank=True, null=True)
    school_geolocation = models.JSONField(blank=True, null=True)
    is_manual_entry_allowed = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'app_root_schoolmodel'


class AppRootStaffattendancemodel(models.Model):
    staff_attendance_id = models.UUIDField(primary_key=True)
    attempt_status = models.CharField(max_length=20, blank=True, null=True)
    attendance_status = models.JSONField(blank=True, null=True)
    attendance_date = models.DateTimeField(blank=True, null=True)
    date_time = models.DateTimeField()
    updated_at = models.DateTimeField()
    loggedin_user = models.ForeignKey(AppRootProfile, models.DO_NOTHING, blank=True, null=True)
    media_id = models.ForeignKey(AppRootMediacapturedmodel, models.DO_NOTHING, blank=True, null=True)
    taken_by = models.ForeignKey(AppRootProfile, models.DO_NOTHING, blank=True, null=True)
    attendance_id = models.ForeignKey(AppRootAttendancemodel, models.DO_NOTHING, blank=True, null=True)
    attendance_type = models.CharField(max_length=20, blank=True, null=True)
    taken_for = models.ForeignKey(AppRootProfile, models.DO_NOTHING, blank=True, null=True)
    staff_attendance_geolocation = models.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_root_staffattendancemodel'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class CaptchaCaptchastore(models.Model):
    challenge = models.CharField(max_length=32)
    response = models.CharField(max_length=32)
    hashkey = models.CharField(unique=True, max_length=40)
    expiration = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'captcha_captchastore'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class NotificationsNotification(models.Model):
    id = models.BigAutoField(primary_key=True)
    level = models.CharField(max_length=20)
    unread = models.BooleanField()
    actor_object_id = models.CharField(max_length=255)
    verb = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    target_object_id = models.CharField(max_length=255, blank=True, null=True)
    action_object_object_id = models.CharField(max_length=255, blank=True, null=True)
    timestamp = models.DateTimeField()
    public = models.BooleanField()
    deleted = models.BooleanField()
    emailed = models.BooleanField()
    data = models.TextField(blank=True, null=True)
    notification_type = models.CharField(max_length=30, blank=True, null=True)
    action_object_content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING, blank=True, null=True)
    actor_content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING)
    recipient = models.ForeignKey(AuthUser, models.DO_NOTHING)
    target_content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notifications_notification'


class WebpushGroup(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=255)

    class Meta:
        managed = False
        db_table = 'webpush_group'


class WebpushPushinformation(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(WebpushGroup, models.DO_NOTHING, blank=True, null=True)
    subscription = models.ForeignKey('WebpushSubscriptioninfo', models.DO_NOTHING)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'webpush_pushinformation'


class WebpushSubscriptioninfo(models.Model):
    id = models.BigAutoField(primary_key=True)
    browser = models.CharField(max_length=100)
    endpoint = models.CharField(max_length=500)
    auth = models.CharField(max_length=100)
    p256dh = models.CharField(max_length=100)
    user_agent = models.CharField(max_length=500)

    class Meta:
        managed = False
        db_table = 'webpush_subscriptioninfo'
