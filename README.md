## UI

One Time Setup (Assuming all installation has been done)

Delete / Drop DB:

    DROP database luein_smart_attendance_ui_db_dev;

Create DB:

    sudo su - postgres
    psql
    CREATE DATABASE luein_smart_attendance_ui_db_dev;
    CREATE USER luein_user WITH PASSWORD ‘**********’;
    ALTER ROLE luein_user SET client_encoding TO 'utf8';
    ALTER ROLE luein_user SET default_transaction_isolation TO 'read committed';
    ALTER ROLE luein_user SET timezone TO 'Asia/Kolkata';
    GRANT ALL PRIVILEGES ON DATABASE luein_smart_attendance_ui_db_dev TO luein_user;
    \q
    Exit
    - to restore :
      pg_restore -h localhost -p 5432 -U luein_user -d luein_smart_attendance_ui_db_dev luein_smart_attendance_ui_db_dev_dump_v1.tar
    - to dump:
      pg_dump -U luein_user luein_smart_attendance_ui_db_dev -h localhost -p 5432 -c -Ft -f luein_smart_attendance_ui_db_dev_1_0.tar
1. create superuser
2. assign them role = "admin" in ProfileModel
3. Add school
4. Add user as teacher
5. Add profile as teacher user

- SET ENVIRONMENT VARIABLE:
  - DEEPFACE_HOME = {luein_smart_attendence_path}/resources/api_face_recog/model/
  - LUEIN_SM_ATT_PROJ_PATH = {luein_smart_attendence_path}

- COMMAND
  - sudo nano ~/.bashrc
  - source ~/.bashrc

- RESOURCES: (NO NEED FOR MANUAL DOWNLOAD....)
  - ~~https://drive.google.com/file/d/1TH1GbFJtN-hEfyyp0rhOkvMEIudSqpvj/view?usp=sharing~~
  - ~~gdown https://drive.google.com/uc?id=1TH1GbFJtN-hEfyyp0rhOkvMEIudSqpvj~~

- REDIS INSTALLATION
  - sudo apt install lsb-release
  - curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg
  - echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

  - sudo apt-get update
  - sudo apt-get install redis
  - `START REDIS SERVER`
    - sudo redis-server --daemonize yes
    - To test server: `sudo redis-cli`

- RUST INSTALLATION( FOR DJANGO-WEBPUSH):
  - https://www.rust-lang.org/tools/install
  - https://www.digitalocean.com/community/tutorials/install-rust-on-ubuntu-linux

    - DJANGO-WEBPUSH
      - git clone https://github.com/safwanrahman/django-webpush.git
      - cd django-webpush
      - pip install -e .
      - Replace __./admin.py__ with the below code:
      - /.../LueinSmartAttendance/venv/lib/python3.7/site-packages/webpush/admin.py
      - 
        ```
        import json
        from django.contrib import admin
        from .models import PushInformation, SubscriptionInfo, Group
        from .utils import _send_notification
        
        class SubscriptionInfoAdmin(admin.ModelAdmin):
            list_display = ("browser", "user_agent", "endpoint", "auth","p256dh")
        
        class GroupAdmin(admin.ModelAdmin):
            list_display = ("name",)
        
        class PushInfoAdmin(admin.ModelAdmin):
            list_display = ("__str__", "user", "subscription", "group")
            actions = ("send_test_message",)
        
        def send_test_message(self, request, queryset):
          payload = {"head": "Hey", "body": "Hello World"}
          for device in queryset:
              notification = _send_notification(device.subscription, json.dumps(payload), 0)
              if notification:
                  self.message_user(request, "Test sent successfully")
              else:
                  self.message_user(request, "Deprecated subscription deleted")
        admin.site.register(PushInformation, PushInfoAdmin)
        admin.site.register(SubscriptionInfo, SubscriptionInfoAdmin)
        admin.site.register(Group, GroupAdmin)
      ```

- EXECUTION:
  - `DJANGO EXECUTION`
    - Copy the __staticfiles__ folder one directory above the root folder
      - `cp -R staticfiles ../`
    - python manage.py collectstatic --clear
    - python manage.py makemigrations api_* app_* noifications
    - python manage.py migrate
    - python manage.py runserver
  - `REDIS SCRIPT EXECUTION`
    - Once the server is started , execute the steps below
    - cd app_smart_atendance/src/redis_calls
    - Start `redis-server`
    - First time run / One time execution:
      - python redis_async_face_recog_upload_prerun.py
    - 'RUN'
      - python read_async_face_recog_upload_run.py
  - `PICKLE FILE BACKUP`
    - SET ENVIRONMENT VARIABLE:
      - AWS_CONFIG_FILE="%LUEIN_SM_ATT_PROJ_PATH%\resources\api_face_recog\config\config.ini"
    - cd app_smart_attendance/src
    - python aws_s3_backup.py
  - `CRON JOB EXECUTION`:
    - https://pypi.org/project/django-crontab/
    - python manage.py crontab add
    - python manage.py crontab show