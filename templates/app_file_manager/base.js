
"use strict"

var tree_hierarchy = [];
{% if tree_hierarchy %}
    tree_hierarchy = {{tree_hierarchy|safe}}
{% endif %}

var new_list_for_tree;
{% if new_list_for_tree %}
    new_list_for_tree = {{new_list_for_tree|safe}}
{% endif %}

$(document).ready(function() {
    base_event();
    render_tree(tree_hierarchy);
});


function render_tree(tree_hierarchy){
	$('#sidebar_tree').jstree({
		'core' : {
			'data' : tree_hierarchy
		},
		 "types" : {
             "#" : { "max_children" : 1, "max_depth" : 4, "valid_children" : ["root"] },
             "root" : { "icon" : "/static/tree2/tree_icon.png", "valid_children" : ["default"] },
             "default" : { "valid_children" : ["default","file"] },
             "file" : { "icon" : "glyphicon glyphicon-file", "valid_children" : [] }
		 },
		 "plugins" : ["types" ]
	});


//    $('#sidebar_tree').jstree({
//        "core" : {
//            "animation" : 0,
//            "check_callback" : true,
//            "force_text" : true,
//            "themes" : { "stripes" : true },
//            "data" : tree_hierarchy
//        },
//        "types" : {
//            "#" : { "max_children" : 1, "max_depth" : 4, "valid_children" : ["root"] },
//            "root" : { "icon" : "/static/3.3.12/assets/images/tree_icon.png", "valid_children" : ["default"] },
//            "default" : { "valid_children" : ["default","file"] },
//            "file" : { "icon" : "glyphicon glyphicon-file", "valid_children" : [] }
//        },
//        "plugins" : [ "contextmenu", "dnd", "search", "state", "types", "wholerow" ]
//    });



}


function base_event(){
    $(".add_folder").unbind("click.add_folder", add_folder_fn)
    $(".add_folder").bind("click.add_folder", add_folder_fn)

    $(".add_file").unbind("click.add_file", add_file_fn)
    $(".add_file").bind("click.add_file", add_file_fn)
}

function add_folder_fn(){
    var parent = $(this).data("id");
    console.warn(parent)

}

function add_file_fn(){
    var parent = $(this).data("id");
    console.warn(parent)
}