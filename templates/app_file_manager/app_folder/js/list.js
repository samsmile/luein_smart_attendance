
"use strict"

$(document).ready(function() {
    list_events()
});

var edit_fafa  = `<svg version="1.1" width="24" height="24" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 172 172"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#bababa"><path d="M112.445,6.88c-2.76812,0 -5.53625,1.02125 -7.6325,3.1175l-80.9475,80.9475c-4.1925,4.20594 -4.20594,11.18 0,15.3725l1.6125,1.6125l-18.1675,45.6875c-0.04031,0.1075 -0.08062,0.215 -0.1075,0.3225c-0.90031,3.09063 -0.01344,6.42313 2.2575,8.7075c2.15,2.15 4.66281,2.74125 7.4175,2.4725c0.04031,0 0.06719,0 0.1075,0c0.28219,0.04031 0.57781,0.04031 0.86,0h140.395c1.23625,0.01344 2.39188,-0.63156 3.02344,-1.70656c0.61813,-1.075 0.61813,-2.39187 0,-3.46687c-0.63156,-1.075 -1.78719,-1.72 -3.02344,-1.70656h-123.0875l29.1325,-11.5025l1.3975,1.3975c2.10969,2.10969 4.86438,3.225 7.6325,3.225c2.76813,0 5.64375,-1.11531 7.74,-3.225l80.9475,-80.9475c4.16563,-4.1925 4.03125,-10.95156 -0.1075,-15.1575c-0.02687,-0.02687 0.02688,-0.08062 0,-0.1075l-41.8175,-41.925c-2.09625,-2.09625 -4.86437,-3.1175 -7.6325,-3.1175zM112.445,13.76c1.02125,0 2.01563,0.40313 2.795,1.1825l41.8175,41.8175c1.54531,1.54531 1.57219,4.01781 0,5.59l-55.04,55.04l-47.4075,-47.4075l55.04,-55.04c0.77938,-0.77937 1.77375,-1.1825 2.795,-1.1825zM49.7725,74.82l47.4075,47.4075l-7.525,7.525l-47.4075,-47.4075zM37.41,87.1825l47.4075,47.4075l-8.7075,8.7075c-0.77937,0.77938 -1.77375,1.1825 -2.795,1.1825c-1.02125,0 -2.01562,-0.40312 -2.795,-1.1825l-38.27,-38.1625c-0.13437,-0.16125 -0.26875,-0.29562 -0.43,-0.43l-3.1175,-3.225c-1.54531,-1.54531 -1.55875,-4.03125 0,-5.59zM30.745,113.1975l28.165,28.165l-42.2475,16.8775c-0.02687,0 -0.06719,0 -0.1075,0c-0.81969,0.04031 -2.58,-0.73906 -2.2575,-0.43c-0.51062,-0.51062 -0.63156,-1.24969 -0.43,-1.935z"></path></g></g></svg>`;

function list_events(){
    $(".action_container .action_btn_div").unbind('click.action_btn_div',open_dropdown_fn)
    $(".action_container .action_btn_div").bind('click.action_btn_div',open_dropdown_fn)

    $(".card-text").unbind('click.card_text',card_text_fn)
    $(".card-text").bind('click.card_text',card_text_fn)


}

var load_fafa_icon = `<svg class="load_fafa_icon mt40 mb20" version="1.1" width="100%" height="50" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 172 172"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#bababa"><path d="M85.98656,6.88c-3.02295,0 -5.96886,0.19978 -8.85531,0.52406l0.77265,6.83969c2.68634,-0.3018 5.36769,-0.48375 8.08266,-0.48375c39.93679,0 72.24,32.3032 72.24,72.24c0,39.9368 -32.30321,72.24 -72.24,72.24c-2.70237,0 -5.36707,-0.17905 -8.02891,-0.47703l-0.76594,6.83969c2.86968,0.32123 5.7937,0.51735 8.79485,0.51735c43.6552,0 79.12,-35.4648 79.12,-79.12c0,-43.6552 -35.4648,-79.12 -79.12,-79.12zM59.87078,11.38156c-5.63667,1.97355 -10.97331,4.5549 -15.95031,7.69297l3.66844,5.81844c4.53803,-2.86129 9.40836,-5.21753 14.55953,-7.02109zM30.03281,30.05969c-4.20033,4.20033 -7.8444,8.89418 -10.99188,13.89438l5.82516,3.66172c2.90692,-4.61804 6.23687,-8.89749 10.03109,-12.69172zM82.56,44.72l-0.01344,66.08563l-19.35672,-19.35672l-4.86438,4.8711l27.6611,27.65437l27.66109,-27.65437l-4.86437,-4.8711l-19.35672,19.35672l0.01344,-66.08563zM11.36141,59.9111c-1.91698,5.48938 -3.30328,11.24938 -3.97078,17.26047l6.83969,0.75922c0.6053,-5.45099 1.86431,-10.70469 3.62812,-15.75547zM14.23703,94.11625l-6.83969,0.75922c0.66767,6.00902 2.06368,11.77117 3.98422,17.26047l6.49031,-2.27094c-1.76714,-5.05086 -3.02971,-10.30257 -3.63484,-15.74875zM24.89297,124.41781l-5.81844,3.66844c3.15177,5.00133 6.79622,9.68889 10.99859,13.88765l4.86437,-4.86437c-3.79907,-3.7958 -7.13502,-8.07481 -10.04453,-12.69172zM47.62922,147.1339l-3.66172,5.82516c4.97404,3.12541 10.31825,5.70883 15.95703,7.67953l2.26422,-6.49703c-5.14906,-1.79954 -10.01853,-4.15434 -14.55953,-7.00766z"></path></g></g></svg>`;


$(".card-text").each(function(i, obj) {
    var elem = $(this);
    var src = $(this).data("src");
    var icon = $(this).data("icon");

    if(icon == "-image"){
        elem.html(load_fafa_icon)
    }
    else if(icon == "-pkl"){

    }
    else{
        elem.html(load_fafa_icon)
    }

});

function card_text_fn(){
    var elem = $(this);
    var src = $(this).data("src");
    var icon = $(this).data("icon");
    if(icon == "-image"){
        var display = `<img class="display" width="100%" src="${src}" title="file" >`;
    }
    else if(icon == "-pkl"){

    }
    else{
        var display = `<video class="display" controls width="100%" src="${src}"></video>`;
    }
    elem.html(display);
}

//function a(){
//    `<img class="display" width="100%" src="" title="file" >`;
//    `<video class="display" controls width="100%" src=""></video>`;
//}



function open_dropdown_fn(){
    if($(this).hasClass("o")){
        hide_dropdown_fn();
    }else{
        show_dropdown_fn.bind(this)();
    }
}
function show_dropdown_fn(){
    hide_dropdown_fn();
    var parent = $(this).closest('.action_container');
    var action_dropdown = parent.children(".action_dropdown");
    action_dropdown.removeClass("dn");
    $(this).addClass("o");

    var action_btn_div  = $(".action_btn_div");
    var action_dropdown = $(".action_dropdown");
    // show hide div on click outside container
    $(document).unbind('click.action_dropdown');
    $(document).bind('click.action_dropdown',function(e)
    {
       e.stopPropagation();
        // if the target of the click isn't the container nor a descendant of the container
        if (
            (!action_btn_div.is(e.target) && action_btn_div.has(e.target).length === 0)&&
            (!action_dropdown.is(e.target) && action_dropdown.has(e.target).length === 0)
        ){
            // calling "action_close" function on closing overlay by click outside
            if($(this).hasClass("o")){
                hide_dropdown_fn();
            }
            else{
                show_dropdown_fn();
            }
        }
    });
}
function hide_dropdown_fn(){
    $(".action_dropdown").addClass("dn");
    $(".action_btn_div").removeClass("o");
    $(document).unbind('click.action_dropdown');
}





function ajax(){
    var url ='';
    var data ='';
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function(data) {
            ajax_success(data);
            dynamic_loader_end();
        },
        error:function(data) {
            dynamic_loader_end();
        }
    });
}
function ajax_success(data){
    alert(data)
}



function copyToClipboard(text) {

   var textArea = document.createElement( "textarea" );
   textArea.value = text;
   document.body.appendChild( textArea );
   textArea.select();

   try {
      var successful = document.execCommand( 'copy' );
      var msg = successful ? 'successful' : 'unsuccessful';
      console.log('Copying text command was ' + msg);
   } catch (err) {
      console.log('Oops, unable to copy',err);
   }
   document.body.removeChild( textArea );
}



