

/* Functions */

var loadFormFolder = function () {

    console.warn("load_folder_form");

    var btn = $(this);

    if(btn.attr("data-action_type")=="delete"){

    }else{
        luein_modal_open();
        luein_modal_content_clear();
    }

    $.ajax({
        url: btn.attr("data-url"),
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function (data) {
            successFormFolder(data);
            dynamic_loader_end();
        },
        error: function (data) {
            dynamic_loader_end();

        }
    });
};


var successFormFolder = function(data){
    if(data.action_type == "delete"){
        var confirmation_obj = {
            "icon":"",
            "head":"Delete folder?",
            "body":data.html_form,
            "width":"400px",
            "action_type":"delete",
            "action_yes":action_yes,
            "action_no":action_no,
            "action_close":action_close,
            "position":"2",
            "cancel_text":"No, keep it",
            "submit_text":"Yes, delete it",
            "effect":"shake"
        }

        dynamic_confirmation(confirmation_obj);

        function action_yes(){
            //here define the code execute on action_yes
            saveFormFolder();
        }
        function action_no(){
            //here define the code execute on action_no
        }
        function action_close(){
            //here define the code execute on action_close
        }
    }
    else
    {
        if(data.html_form){
            $(".luein-modal-content").html(data.html_form);
        }else{
            displayAlert( data.message, 'error', 2000);
        }
    }
}


var saveFormFolder = function () {
    var form = $('#folder_crud_form');
    var url = form.attr("action");
    var form_data = new FormData($('#folder_crud_form').get(0));
    $.ajax({
        url: url,
        data: form_data,
        async: true,
        cache: false,
        processData: false,
        contentType: false,
        enctype: 'multipart/form-data',
        type: "POST",
        //        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function (data) {
            if (data.form_is_valid) {
                $("#folder_list_table tbody").html(data.html_folder_list);
                luein_modal_close();
                displayAlert( data.message, 'success', 2000);
                setTimeout(function(){location.reload();}, 2000);
            }
            else {
                $(".luein-modal-content").html(data.html_form);
                displayAlert( data.message, 'error', 2000);
            }
            dynamic_loader_end();
        },
        error: function (xhr, textStatus, error) {
            dynamic_loader_end();
            displayAlert("Something went wrong",'error',5000)
        }

    });
    return false;
};



/* Un-Binding */

// Create folder
$(".js_create_folder").unbind('click');
$("#folder_list_table").unbind('click');



/* Binding */
// Create folder
$(".js_create_folder").click(loadFormFolder);

// Update folder
$(".action_container").on("click", ".js_update_folder", loadFormFolder);

// move folder
$(".action_container").on("click", ".js_move_folder", loadFormFolder);

// add folder
$(".action_container").on("click", ".js_add_folder", loadFormFolder);

// Delete folder
$(".action_container").on("click", ".js_delete_folder", loadFormFolder);


// Link folder
$(".action_container").on("click", ".js_link_folder", get_folder_link);


function get_folder_link(){

    var html_form = `{% include 'app_file_manager/app_folder/html/partial_folder_link.html' %}`;

     var confirmation_obj = {
            "icon":"",
            "head":`Public Link for Folder`,
            "body":html_form,
            "width":"400px",
            "action_type":"form",
            "action_yes":action_yes,
            "action_no":action_no,
            "action_close":action_close,
            "position":"2",
            "cancel_text":"Discard",
            "submit_text":"Copy",
            "effect":""
        }

    dynamic_confirmation(confirmation_obj);

    function action_yes(){
        //here define the code execute on action_yes
         var clipboardText = "";
         clipboardText = $( '#id_full_link' ).val();
         copyToClipboard( clipboardText );
         displayAlert( "Link Copied...!!!","info",2000 );
    }
    function action_no(){
        //here define the code execute on action_no
    }
    function action_close(){
        //here define the code execute on action_close
    }
}



