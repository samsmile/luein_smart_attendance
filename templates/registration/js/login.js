"use-strict"
// login.js start

var all_image_array = [];

{% if all_image_array %}
    all_image_array = {{all_image_array|safe}};
{% endif %}

if(all_image_array){
    var image_slide_dot = '';
    for (var i in all_image_array){
        image_slide_dot += `<div class="image_slide_dot" id="image_slide_dot_${i}" data-id="${i}"></div>`;
    }
    $(".image_slide_dot_container").html(image_slide_dot)
}

$(document).ready(function(){

    var current_image_index = 0;
    show_image(current_image_index)


    $(".arrow_div").click(function(){
        // left arrow clicked
        if($(this).hasClass('left_arrow')){
            show_previous_image();
        }
        // right arrow clicked
        else if($(this).hasClass('right_arrow')){
           show_next_image();
        }
        //
        else{
            alert("something went wrong")
        }
    })


    $(".image_slide_dot").click(function(){
        var index = $(this).data("id");
        show_image(index)
    })

    function show_previous_image(){
        var current_image_index_val = parseInt(current_image_index)-1;
        show_image(current_image_index_val);

    }
    function show_next_image(){
        var current_image_index_val = parseInt(current_image_index)+1;
        show_image(current_image_index_val);
    }
    function active_image_slide_dot(index){
        $(".image_slide_dot").removeClass('active');
        $(`.image_slide_dot[data-id="${index}"]`).addClass('active');
    }
    function show_image(index){
        clearInterval(change_image_setInterval);

        var len = all_image_array.length;
        if(index<0){
            current_image_index = len-1;
        }else if(index>=len){
            current_image_index = 0;
        }
        else{
            current_image_index = index;
        }
        var path = `url("${all_image_array[current_image_index]}")`;
        $('#login_image').css('backgroundImage',path);

        if(all_image_array){
            create_new_interval();
        }
        active_image_slide_dot(current_image_index)

    }

    var change_image_setInterval;
    function create_new_interval(){
        change_image_setInterval = setInterval(function () {
                                        show_next_image();
                                    },7000);
    }

});



// login.js end