

/* Functions */

var loadFormRegisterUser = function () {

    console.warn("load_register_user_crud_form");

    var btn = $(this);

    if(btn.attr("data-action_type")=="delete"){

    }else{
        luein_modal_open();
        luein_modal_content_clear();
    }

    $.ajax({
        url: btn.attr("data-url"),
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function (data) {
            successFormRegisterUser(data);
            dynamic_loader_end();
        },
        error: function (data) {
            dynamic_loader_end();

        }
    });
};

var successFormRegisterUser = function(data){
    if(data.action_type == "delete"){
        var confirmation_obj = {
            "icon":"",
            "head":"Archive Job?",
            "body":data.html_form,
            "width":"400px",
            "action_type":"delete",
            "action_yes":action_yes,
            "action_no":action_no,
            "action_close":action_close,
            "position":"2",
            "cancel_text":"No, keep it",
            "submit_text":"Yes, archived it",
            "effect":"shake"
        }

        dynamic_confirmation(confirmation_obj);

        function action_yes(){
            //here define the code execute on action_yes
            saveFormRegisterUser();
        }
        function action_no(){
            //here define the code execute on action_no
        }
        function action_close(){
            //here define the code execute on action_close
        }
    }
    else
    {
        if(data.html_form){
            $(".luein-modal-content").html(data.html_form);
        }else{
            displayAlert( data.message, 'error', 2000);
        }
    }
}

var saveFormRegisterUser = function () {
    var form = $('#register_user_crud_form');
    var url = form.attr("action");
    var form_data = new FormData($('#register_user_crud_form').get(0));
    var temp_formData = $('#register_user_crud_form').serializeArray()

    $.ajax({
        url: url,
        data: form_data,
        async: true,
        cache: false,
        processData: false,
        contentType: false,
        enctype: 'multipart/form-data',
        type: "POST",
        //        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function (data) {
            if (data.form_is_valid) {
                $("#school_detail_list_div").html(data.html_school_detail_list);
                displayAlert( data.message, 'success', 2000);
//                setTimeout(function(){location.reload();}, 2000);

                main_formData = temp_formData;
                main_formData.push({name: 'profile_id', value:  data.profile})

//                open_register_user();

                luein_modal_close();
            }
            else {
                $(".luein-modal-content").html(data.html_form);
            }
            dynamic_loader_end();
        },
        error: function (xhr, textStatus, error) {
            dynamic_loader_end();
            displayAlert("Something went wrong",'error',5000)
        }

    });
    return false;
};

function open_register_user(){
    hide_student_register_frm_container();
    show_student_register_video_container();

    // open device selection
    reset_recording_btn();
    ct_init_devices_access_permissions_fn();
}


/* Un-Binding */

// Add register_user
$(".js_create_register_user").unbind('click');
$("#register_user_list_table").unbind('click');

/* Binding */
// Add register_user
$(".js_create_register_user").click(loadFormRegisterUser);
// Update Staff
$("#register_user_list_table").on("click", ".js_update_register_user", loadFormRegisterUser);






