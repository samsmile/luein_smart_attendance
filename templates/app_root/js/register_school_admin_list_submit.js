
$(".luein-modal-container").unbind('submit');


$(".luein-modal-container").on("submit", ".js_register_user_create_form", saveFormRegisterUser);

$(".luein-modal-container").on("submit", ".js_register_user_update_form", saveFormRegisterUser);

$(".luein-modal-container").on("submit", ".js_register_user_delete_form", saveFormRegisterUser);


$(".luein-modal-button.submit").unbind("click.submit", check_form_is_valid);
$(".luein-modal-button.submit").bind("click.submit", check_form_is_valid);

function check_form_is_valid(){

    if(!$(`[name='role']:checked`).val()){
        displayAlert("User group is required...!!!", "error", 2000)
    }

}
