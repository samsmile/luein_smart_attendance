"use strict"

{% load static %}

var installationSteps = {
    firefox: {
        mobile: {
            ios:`<div>
                   <div>Step 1:</div>
                   <div>Go to <b>⋮</b> at bottom right</div>
                </div>
                <div>
                   <div>Step 2:</div>
                   <div>Click on <b>Install</b> option.</div>
                </div>
                `,
            android: `<div>
                   <div>Step 1:</div>
                   <div>Go to <b>⋮</b> at bottom right</div>
                </div>
                <div>
                   <div>Step 2:</div>
                   <div>Click on <b>Install</b> option.</div>
                </div>
                `
        },
        desktop: `<div>
                   <div>App installation is not supported on Firefox Browsers</div>
                </div>`
    },
    safari: {
        mobile: {
            ios: `<div>
                   <div>Step 1:</div>
                   <div>Open the <b>Share</b> menu, available at the bottom or top of the browser.</div>
                </div>
                <div>
                   <div>Step 2:</div>
                   <div>Click <b>Add to Home Screen</b>.</div>
                </div>
                <div>
                   <div>Step 3:</div>
                   <div>Confirm the name of the app; the name is user-editable.</div>
                </div>
                <div>
                   <div>Step 4:</div>
                   <div>Click Add.</div>
                </div>
                `
        },
        desktop: `<div>
                   <div>App installation is not supported on Safari Browser</div>
                </div>`
    }
};

const getBrowserType = () => {
     let userAgent = navigator.userAgent;
     let browserName;

     if(userAgent.match(/chrome|chromium|crios/i)){
         browserName = "chrome";
       }else if(userAgent.match(/firefox|fxios/i)){
         browserName = "firefox";
       }  else if(userAgent.match(/safari/i)){
         browserName = "safari";
       }else if(userAgent.match(/opr\//i)){
         browserName = "opera";
       } else if(userAgent.match(/edg/i)){
         browserName = "edge";
       }else{
         browserName="No browser detection";
       }
       return browserName;
};

function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {
        return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "iOS";
    }

    return "unknown";
}

const getDeviceType = () => {
      const ua = navigator.userAgent;
      if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
        return "tablet";
      }
      if (
        /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
          ua
        )
      ) {
        return "mobile";
      }
      return "desktop";
};

const getIfStandalone = () => {
    let is_standalone = false;
    if('standalone' in window.navigator){
        is_standalone = window.navigator.standalone;
    }
    else{
        is_standalone = window.matchMedia('(display-mode: standalone)').matches
    }
    return is_standalone;
};



//// Code to handle install prompt
var deferredPrompt;
function installPWA(){
//    console.log("onbeforeinstallprompt: ",window.onbeforeinstallprompt);
    const a2hs_btn = $("#installApp");
            a2hs_btn.show();
    if(window.onbeforeinstallprompt !== undefined){
//    debugger;
        window.addEventListener('beforeinstallprompt', (e) => {
            e.preventDefault();
            deferredPrompt = e;
            a2hs_btn.click(async function(){
                if (deferredPrompt !== null) {
                    deferredPrompt.prompt();
                    const { outcome } = await deferredPrompt.userChoice;
                    if (outcome === 'accepted') {
                        deferredPrompt = null;
                    }else{
                        console.warn("outcome === 'accepted' ---- else ")
                    }
                }else{
                    console.warn("deferredPrompt !== null ---- else ")
                }
            });
        });
    }
    else{
        a2hs_btn.unbind('click.a2hs_btn', show_installation_steps_fn)
        a2hs_btn.bind('click.a2hs_btn', show_installation_steps_fn)
    }
}

function installPWA2(){
    let deferredPrompt;
    var btnAdd = document.getElementById('installApp')
    var btnOpen = document.getElementById('openApp')




    btnAdd.style.display = 'flex';
//    btnOpen.style.display = 'none';

    if(window.onbeforeinstallprompt === null){
        btnAdd.style.display = 'none';
//        btnOpen.style.display = 'none';


        window.addEventListener('beforeinstallprompt', function(event) {



            btnAdd.style.display = 'flex';

            // Prevent Chrome 67 and earlier from automatically showing the prompt
            event.preventDefault();
            // Stash the event so it can be triggered later.
            deferredPrompt = event;

            // Installation must be done by a user gesture! Here, the button click
            btnAdd.addEventListener('click', (e) => {
                // hide our user interface that shows our A2HS button
                btnAdd.style.display = 'none';


                // Show the prompt
                deferredPrompt.prompt();
                // Wait for the user to respond to the prompt
                deferredPrompt.userChoice
                .then((choiceResult) => {
                    if (choiceResult.outcome === 'accepted') {
                        console.log('User accepted the A2HS prompt');
                        localStorage.pwa = true;
                    } else {
                        btnAdd.style.display = 'flex';
                        console.log('User dismissed the A2HS prompt');
                        localStorage.pwa = false;
                    }
                    deferredPrompt = null;
                });
            });

            });
    }
    else{
        btnAdd.style.display = 'flex';
        $(btnAdd).unbind('click.a2hs_btn', show_installation_steps_fn)
        $(btnAdd).bind('click.a2hs_btn', show_installation_steps_fn)
    }


////Logic 1 start
//    if(window.getComputedStyle(document.getElementById('openApp')).display === "none") {
//        document.getElementById('openApp').style.display = 'flex';
//    }else{
//        document.getElementById('openApp').style.display = 'none';
//    }
////Logic 1 end
//
////Logic 2 start
//    if($('#installApp').css('display') == 'none'){
//        document.getElementById('openApp').style.display = 'flex';
//    }else{
//        document.getElementById('openApp').style.display = 'none';
//    }
////Logic 2 end



//    // check if app running as standalone in desktop or not
//    if (window.matchMedia('(display-mode: standalone)').matches) {
//        console.warn("This is running as standalone in desktop");
//        // hide open in app btn in desktop
//        btnOpen.style.display = 'none';
//    }else{
//        // show open in app btn in desktop
//        btnOpen.style.display = 'flex';
//        console.warn("This is NOT running as standalone in desktop");
//    }
//
//    // check if running as pwa
//    const pwaModeEnabled = (
//        window.navigator.standAlone ||           // Safari
//        window.fullScreen ||                     // FireFox
//        ( !window.screenTop && !window.screenY ) // Chrome
//    );
//
//    if(pwaModeEnabled){
//        // hide open in app btn in mobile
//        btnOpen.style.display = 'none';
//    }


}

function updateBodyForInstallAppOverlay(){
    let device = getDeviceType();
    var mobile_type = null;
    var is_mobile = false;
    if(device === 'mobile'){
        is_mobile = true;
        mobile_type = getMobileOperatingSystem();
    }
    let browser = getBrowserType();
//    let is_standalone = getIfStandalone();
    console.log("browser: ",browser)
    console.log("device: ",device)
    console.log("is_mobile: ",is_mobile)
    console.log("mobile_type: ",mobile_type)

    var body = "";
    // check if app is not installed
    if(browser in installationSteps){
        var val1 = installationSteps[browser];
//        console.log("val1: ",val1)
        if(device in val1){
            var val2 = val1[device];
//            console.log("val2: ",val2)
            if(is_mobile && mobile_type.toLowerCase() in val2){
                body = val2[mobile_type.toLowerCase()]
            }
            else{
                body = val2;
            }
//            console.log("body: ",body)
        }
    }
    return body;
}

function show_installation_steps_fn(){
    var body = updateBodyForInstallAppOverlay();

    var confirmation_obj = {
        "icon":"",
        "head":`How to install app in your system`,
        "body":body,
        "width":"80%",
        "action_type":"alert",
        "action_yes":action_yes,
        "action_no":action_no,
        "action_close":action_close,
        "position":"2",
        "alert_button_text":"Ok",
    }

    dynamic_confirmation(confirmation_obj);

    function action_yes(){
        //here define the code execute on action_yes
    }
    function action_no(){
        //here define the code execute on action_no
    }
    function action_close(){
        //here define the code execute on action_close
    }
}



const install_link = async() => {
    if ('serviceWorker' in navigator) {
        const service_worker = await navigator.serviceWorker.getRegistration('/');
        if(!(service_worker.waiting && service_worker.installing)){
//            installPWA();
            installPWA2();
        }
    } else {
        console.warn("No service worker has been registered yet.");
    }
}

install_link();