

/* Functions */

var loadFormAnnouncement = function () {

    console.warn("load_announcement_form");

    var btn = $(this);

    if(btn.attr("data-action_type")=="delete"){

    }else{
        luein_modal_open();
        luein_modal_content_clear();
    }

    $.ajax({
        url: btn.attr("data-url"),
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function (data) {
            successFormAnnouncement(data);
            dynamic_loader_end();
        },
        error: function (data) {
            dynamic_loader_end();
            displayAlert( "Something went wrong...!!!", 'error', 2000);
        }
    });
};


var successFormAnnouncement = function(data){
    if(data.action_type == "delete"){
        var confirmation_obj = {
            "icon":"",
            "head":"Delete Announcement?",
            "body":data.html_form,
            "width":"400px",
            "action_type":"delete",
            "action_yes":action_yes,
            "action_no":action_no,
            "action_close":action_close,
            "position":"2",
            "cancel_text":"No, keep it",
            "submit_text":"Yes, delete it",
            "effect":"shake"
        }

        dynamic_confirmation(confirmation_obj);

        function action_yes(){
            //here define the code execute on action_yes
            saveFormAnnouncement();
        }
        function action_no(){
            //here define the code execute on action_no
        }
        function action_close(){
            //here define the code execute on action_close
        }
    }
    else
    {
        if(data.html_form){
            $(".luein-modal-content").html(data.html_form);
        }else{
            displayAlert( data.message, 'error', 2000);
        }
    }
}


var saveFormAnnouncement = function () {
    var form = $('#announcement_crud_form');
    var url = form.attr("action");
    var form_data = new FormData($('#announcement_crud_form').get(0));
    $.ajax({
        url: url,
        data: form_data,
        async: true,
        cache: false,
        processData: false,
        contentType: false,
        enctype: 'multipart/form-data',
        type: "POST",
        //        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function (data) {
            if (data.form_is_valid) {
                $("#announcement_list_table").html(data.html_announcement_list);
                displayAlert( data.message, 'success', 2000);
//                setTimeout(function(){location.reload();}, 2000);
                luein_modal_close();
            }
            else {
                $(".luein-modal-content").html(data.html_form);
                displayAlert( data.message, 'error', 2000);
            }
            dynamic_loader_end();
        },
        error: function (xhr, textStatus, error) {
            dynamic_loader_end();
            displayAlert("Something went wrong",'error',5000)
        }

    });
    return false;
};



/* Un-Binding */

// Create announcement
$(".js_create_announcement").unbind('click');
$("#announcement_list_table").unbind('click');



/* Binding */
// Create announcement
$(".js_create_announcement").click(loadFormAnnouncement);

// Update announcement
$("#announcement_list_table").on("click", ".js_update_announcement", loadFormAnnouncement);

// Delete announcement
$("#announcement_list_table").on("click", ".js_delete_announcement", loadFormAnnouncement);



