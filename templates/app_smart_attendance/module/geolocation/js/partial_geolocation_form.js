"use strict"

$(document).ready(function() {
    geolocation_form_events();
});

var place =  {
 	"address_components": [
 	{
 		"long_name": "G27-14",
 		"short_name": "G27-14",
 		"types": ["street_number"]
 	}, {
 		"long_name": "Deenbandhu Chotooram Marg",
 		"short_name": "Deenbandhu Chotooram Marg",
 		"types": ["route"]
 	}, {
 		"long_name": "Pocket G-27",
 		"short_name": "Pocket G-27",
 		"types": ["sublocality_level_3", "sublocality", "political"]
 	}, {
 		"long_name": "Sector 3",
 		"short_name": "Sector 3",
 		"types": ["sublocality_level_2", "sublocality", "political"]
 	}, {
 		"long_name": "Rohini",
 		"short_name": "Rohini",
 		"types": ["sublocality_level_1", "sublocality", "political"]
 	}, {
 		"long_name": "Delhi",
 		"short_name": "Delhi",
 		"types": ["locality", "political"]
 	}, {
 		"long_name": "North West Delhi",
 		"short_name": "North West Delhi",
 		"types": ["administrative_area_level_3", "political"]
 	}, {
 		"long_name": "Delhi",
 		"short_name": "DL",
 		"types": ["administrative_area_level_1", "political"]
 	}, {
 		"long_name": "India",
 		"short_name": "IN",
 		"types": ["country", "political"]
 	}, {
 		"long_name": "110085",
 		"short_name": "110085",
 		"types": ["postal_code"]
 	}
 	],
 	"adr_address": "G27-14, Deenbandhu Chotooram Marg, Pocket G-27, Sector 3, Rohini, Delhi, 110085, India",
 	"business_status": "OPERATIONAL",
 	"current_opening_hours": {
 		"open_now": true,
 		"periods": [{
 			"close": {
 				"date": "2023-01-02",
 				"day": 1,
 				"time": "1430"
 			},
 			"open": {
 				"date": "2023-01-02",
 				"day": 1,
 				"time": "0730"
 			}
 		}, {
 			"close": {
 				"date": "2022-12-27",
 				"day": 2,
 				"time": "1430"
 			},
 			"open": {
 				"date": "2022-12-27",
 				"day": 2,
 				"time": "0730"
 			}
 		}, {
 			"close": {
 				"date": "2022-12-28",
 				"day": 3,
 				"time": "1430"
 			},
 			"open": {
 				"date": "2022-12-28",
 				"day": 3,
 				"time": "0730"
 			}
 		}, {
 			"close": {
 				"date": "2022-12-29",
 				"day": 4,
 				"time": "1430"
 			},
 			"open": {
 				"date": "2022-12-29",
 				"day": 4,
 				"time": "0730"
 			}
 		}, {
 			"close": {
 				"date": "2022-12-30",
 				"day": 5,
 				"time": "1430"
 			},
 			"open": {
 				"date": "2022-12-30",
 				"day": 5,
 				"time": "0730"
 			}
 		}, {
 			"close": {
 				"date": "2022-12-31",
 				"day": 6,
 				"time": "1200"
 			},
 			"open": {
 				"date": "2022-12-31",
 				"day": 6,
 				"time": "0730"
 			}
 		}],
 		"weekday_text": ["Monday: 7:30 AM – 2:30 PM", "Tuesday: 7:30 AM – 2:30 PM", "Wednesday: 7:30 AM – 2:30 PM", "Thursday: 7:30 AM – 2:30 PM", "Friday: 7:30 AM – 2:30 PM", "Saturday: 7:30 AM – 12:00 PM", "Sunday: Closed"]
 	},
 	"formatted_address": "G27-14, Deenbandhu Chotooram Marg, Pocket G-27, Sector 3, Rohini, Delhi, 110085, India",
 	"formatted_phone_number": "011 2752 4641",
 	"geometry": {
 		"location": {
 			"lat": 28.7048991,
 			"lng": 77.10180299999999
 		},
 		"viewport": {
 			"south": 28.7035671697085,
 			"west": 77.1004291197085,
 			"north": 28.7062651302915,
 			"east": 77.10312708029151
 		}
 	},
 	"icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/school-71.png",
 	"icon_background_color": "#7B9EB0",
 	"icon_mask_base_uri": "https://maps.gstatic.com/mapfiles/place_api/icons/v2/school_pinlet",
 	"international_phone_number": "+91 11 2752 4641",
 	"name": "St. Giri Senior Secondary School",
 	"opening_hours": {
 		"open_now": true,
 		"periods": [{
 			"close": {
 				"day": 1,
 				"time": "1430",
 				"hours": 14,
 				"minutes": 30,
 				"nextDate": 1672650000000
 			},
 			"open": {
 				"day": 1,
 				"time": "0730",
 				"hours": 7,
 				"minutes": 30,
 				"nextDate": 1672624800000
 			}
 		}, {
 			"close": {
 				"day": 2,
 				"time": "1430",
 				"hours": 14,
 				"minutes": 30,
 				"nextDate": 1672131600000
 			},
 			"open": {
 				"day": 2,
 				"time": "0730",
 				"hours": 7,
 				"minutes": 30,
 				"nextDate": 1672711200000
 			}
 		}, {
 			"close": {
 				"day": 3,
 				"time": "1430",
 				"hours": 14,
 				"minutes": 30,
 				"nextDate": 1672218000000
 			},
 			"open": {
 				"day": 3,
 				"time": "0730",
 				"hours": 7,
 				"minutes": 30,
 				"nextDate": 1672192800000
 			}
 		}, {
 			"close": {
 				"day": 4,
 				"time": "1430",
 				"hours": 14,
 				"minutes": 30,
 				"nextDate": 1672304400000
 			},
 			"open": {
 				"day": 4,
 				"time": "0730",
 				"hours": 7,
 				"minutes": 30,
 				"nextDate": 1672279200000
 			}
 		}, {
 			"close": {
 				"day": 5,
 				"time": "1430",
 				"hours": 14,
 				"minutes": 30,
 				"nextDate": 1672390800000
 			},
 			"open": {
 				"day": 5,
 				"time": "0730",
 				"hours": 7,
 				"minutes": 30,
 				"nextDate": 1672365600000
 			}
 		}, {
 			"close": {
 				"day": 6,
 				"time": "1200",
 				"hours": 12,
 				"minutes": 0,
 				"nextDate": 1672468200000
 			},
 			"open": {
 				"day": 6,
 				"time": "0730",
 				"hours": 7,
 				"minutes": 30,
 				"nextDate": 1672452000000
 			}
 		}],
 		"weekday_text": ["Monday: 7:30 AM – 2:30 PM", "Tuesday: 7:30 AM – 2:30 PM", "Wednesday: 7:30 AM – 2:30 PM", "Thursday: 7:30 AM – 2:30 PM", "Friday: 7:30 AM – 2:30 PM", "Saturday: 7:30 AM – 12:00 PM", "Sunday: Closed"]
 	},
 	"photos": [
 	{
 		"height": 3096,
 		"html_attributions": ["Amit Kumar"],
 		"width": 4128
 	}, {
 		"height": 1200,
 		"html_attributions": ["Rahul S"],
 		"width": 1600
 	}, {
 		"height": 1600,
 		"html_attributions": ["Rahul S"],
 		"width": 720
 	}, {
 		"height": 720,
 		"html_attributions": ["Sandeep Jha"],
 		"width": 1062
 	}, {
 		"height": 720,
 		"html_attributions": ["govind goyal"],
 		"width": 1280
 	}, {
 		"height": 1920,
 		"html_attributions": ["Seraj Alam"],
 		"width": 1080
 	}, {
 		"height": 1920,
 		"html_attributions": ["Pankaj Keswani"],
 		"width": 1080
 	}, {
 		"height": 720,
 		"html_attributions": ["Rahul S"],
 		"width": 1600
 	}, {
 		"height": 768,
 		"html_attributions": ["rita singh"],
 		"width": 1280
 	}, {
 		"height": 3264,
 		"html_attributions": ["Poonam. Totally West of Money. Rani"],
 		"width": 2448
 	}
 	],
 	"place_id": "ChIJpYDRsaoGDTkR91P22W_bGtM",
 	"plus_code": {
 		"compound_code": "P432+XP Delhi, India",
 		"global_code": "7JWVP432+XP"
 	},
 	"rating": 3.5,
 	"reference": "ChIJpYDRsaoGDTkR91P22W_bGtM",
 	"reviews": [
 	{
 		"author_name": "Madhuri Garg",
 		"author_url": "https://www.google.com/maps/contrib/104760596936764365007/reviews",
 		"language": "en",
 		"profile_photo_url": "https://lh3.googleusercontent.com/a-/AD5-WCk2w-B-uqKkfQcBxacArbsbWPA2j_WGpRB3m_i0Hg=s128-c0x00000000-cc-rp-mo",
 		"rating": 5,
 		"relative_time_description": "3 months ago",
 		"text": "Excellent school. All teachers are very nice..All facilities are available here ex..drinking water, washrooms , labs and libraries etc.Studies are doing good. Wonderful work place for students...must visit place.",
 		"time": 1662697006
 	}, {
 		"author_name": "Geetanjali Khatri",
 		"author_url": "https://www.google.com/maps/contrib/110944696093734789459/reviews",
 		"language": "en",
 		"profile_photo_url": "https://lh3.googleusercontent.com/a/AEdFTp4AOfGpRXyrG_JtIh73hJIhWyD6oheflUy9mGRs=s128-c0x00000000-cc-rp-mo",
 		"rating": 1,
 		"relative_time_description": "2 months ago",
 		"text": "Worst school of my life..dirty washroom for girls no handwash. School fees increased by 65.7% in the second quarter without notice. So that children do not change school in the middle of the year. please Do not take admission here to spoil the life of your children. Here the queries of illiterate teachers and parents and children are not resolved and you cannot talk to the principal.",
 		"time": 1664391639
 	}, {
 		"author_name": "NITIN KUMAR",
 		"author_url": "https://www.google.com/maps/contrib/118320801582663272374/reviews",
 		"language": "en",
 		"profile_photo_url": "https://lh3.googleusercontent.com/a/AEdFTp5Mh2ovHk9RBkY-JqpPchesuzir82Tqz-0n5l6T=s128-c0x00000000-cc-rp-mo",
 		"rating": 5,
 		"relative_time_description": "3 months ago",
 		"text": "ST GIRI SENIOR SECONDARY SCHOOL\n,\nIS THE\nbetter environment for development of\nthe students.\n*Teachers are highly qualified and polite. *Teachers are a great encouragement\nand and the academic program is\ngreat.\n*They teach value lessons,\ncommunication and leadership skills.",
 		"time": 1663080155
 	}, {
 		"author_name": "Garima Singh",
 		"author_url": "https://www.google.com/maps/contrib/116306158333630884290/reviews",
 		"language": "en",
 		"profile_photo_url": "https://lh3.googleusercontent.com/a/AEdFTp5nScKbhx2HNK29_oLoais1oP4nG9dK1OS0A8Lz=s128-c0x00000000-cc-rp-mo",
 		"rating": 5,
 		"relative_time_description": "3 months ago",
 		"text": "I visited school previous month for a workshop. The school and teaching staff is very cooperative. The school environment was peaceful and students were disciplined.",
 		"time": 1662719092
 	}, {
 		"author_name": "Nidhi Kakar",
 		"author_url": "https://www.google.com/maps/contrib/105602969530145710196/reviews",
 		"language": "en",
 		"profile_photo_url": "https://lh3.googleusercontent.com/a/AEdFTp7P49BgSwjcGjI7DTcd5nxz2Qt4B_Ph9pzBo6JF=s128-c0x00000000-cc-rp-mo",
 		"rating": 5,
 		"relative_time_description": "a month ago",
 		"text": "My experience with the school is wonderful. Staff is very helpful as well as they nurture the student in all aspects",
 		"time": 1667615831
 	}
 	],
 	"types": ["school", "point_of_interest", "establishment"],
 	"url": "https://maps.google.com/?cid=15211711964841923575",
 	"user_ratings_total": 214,
 	"utc_offset": 330,
 	"vicinity": "G27-14, Deenbandhu Chotooram Marg, Pocket G-27, Sector 3, Rohini, Delhi",
 	"website": "http://www.stgirirohini.in/",
 	"html_attributions": [],
 	"utc_offset_minutes": 330
 }
var radius_choice_list = {
	"m": [
		300,
		400,
		500,
		1000
	],
	"ft": [
		1000,
		1350,
		1650,
		3300
	]
}
var radius_unit = [
    'ft',
    'm'
]
var radius_unit_display = {
    'ft':"feet",
    'm':"meter"
}
var SPACE = " ";
const meter2feet = 3.2808;
function get_feet(value){
    return Math.round(value*meter2feet)
}
function get_meter(value){
    return Math.round(value/meter2feet)
}

function geolocation_form_events(){
    render_form_data(place)
}

function render_form_data(place){
    var name = place.name;
    var unit = radius_unit[1];
    var radius = radius_choice_list[unit][0];
    var coordinates_str = place.geometry.location.lat + ',' + place.geometry.location.lng;

    $("#id_name").val(name)

    render_radius_input_html(unit)
    set_radius_hidden_value(radius)
    set_radius_display_value(radius,unit)
    set_radius_unit_value(unit)

    $("#id_coordinates_str").val(coordinates_str)

    var address_html = get_popup_content(place)
    $(".address_html").html(address_html)
}

function get_popup_content(place){
    var name = place.name;
    var adr_address = place.adr_address;
    var formatted_address = place.formatted_address;
    var content = `
        <div class="popup_div">
            <div class="popup_name">${name}</div>
            <div class="popup_add">${formatted_address}</div>
            <div class="add_address_div">
                <div class="add_address"></div>
            </div>
        </div>
    `;
    return content;
}

radius_unit_fn_event()
function radius_unit_fn_event(){
    $(`#id_radius_unit input[name='radius_unit']`).unbind("change.id_radius_unit", radius_unit_fn)
    $(`#id_radius_unit input[name='radius_unit']`).bind("change.id_radius_unit", radius_unit_fn)
}

function radius_unit_fn(){
    var unit = this.value;
    var radius_list = radius_choice_list[unit];
    radius_list.forEach(function (value, i) {
        var elem = $(`.radius_input_div input[name='radius']`)[i];
        elem.value = value;
        $(elem).closest(".radius_input_div").find("label").text(value + SPACE + radius_unit_display[unit])
    })

    set_radius_hidden_value(get_radius_display_value())
}

function radius_input_fn_event(){
    $(".radius_input_div input[name='radius']").unbind("change.radius", radius_input_fn)
    $(".radius_input_div input[name='radius']").bind("change.radius", radius_input_fn)
}

function radius_input_fn(){
    var value = this.value;
    set_radius_hidden_value(value)
    if(value==0){
        show_custom_radius_input()
    }else{
        hide_custom_radius_input()
    }
}
function show_custom_radius_input(){
    var custom_radius_input_html = `<div class="custom_radius_div">
                                        <div class="partitioned_form_field ">
                                            <input type="number" min="0" name="radius" id="id_radius_custom">
                                        </div>
                                    </div>`;
    var custom_radius_info_html = `<div class="custom_radius_info">
                                        <div>custom_radius_info</div>
                                    </div>`;
    $(".radius_input_outer_div").append(custom_radius_input_html);
    $(".partitioned_form").append(custom_radius_info_html)
    document.getElementById('id_radius_custom').focus();
    id_radius_custom_event();
}
function hide_custom_radius_input(){
    $("#id_radius_custom").closest(".custom_radius_div").remove()
    $(".custom_radius_info").remove();
}
function id_radius_custom_event(){
    $("#id_radius_custom").unbind("input", custom_radius_input_change_fn)
    $("#id_radius_custom").bind("input", custom_radius_input_change_fn)
}
function custom_radius_input_change_fn(){
    var value = this.value;
    set_radius_hidden_value(value)
}
function set_radius_hidden_value(value){
    $("#id_radius").val(value)
}
function set_radius_display_value(value, unit){
    if((radius_choice_list[unit].indexOf(value))!=-1){
        $(`.radius_input_div input[name='radius'][value='${value}']`).prop("checked", true);
    }else{
        $(`.radius_input_div input[name='radius'][value='0']`).prop("checked", true);
    }
}
function get_radius_display_value(){
    return $(`.radius_input_div input[name='radius']:checked`).val()
}
function set_radius_unit_value(unit){
    $(`#id_radius_unit input[name='radius_unit'][value='${unit}']`).prop("checked", true);
}

function render_radius_input_html(unit){
    var radius = radius_choice_list[unit];
    var radius_input_html = ``;
    radius.forEach(function (value, i) {
        var recommended = "";
        if(i == 0){
            recommended = `<div class="recommended">Recommended</div>`
        }
        radius_input_html += `<div class="radius_input_div">
                                <input type="radio" name="radius" value="${value}">
                                <label>${value + SPACE + radius_unit_display[unit]}</label>
                                ${recommended}
                            </div>`;
    });
    radius_input_html += `<div class="radius_input_div">
                            <input type="radio" name="radius" value="0">
                            <label>Custom radius</label>
                        </div>`;

    $(".radius_input_outer_div").html(radius_input_html);
    radius_input_fn_event();
//    return radius_input_html;
}