"use strict"

$(document).ready(function(){
    partial_take_attendance_cctv_form()
    check_cctv_validation_init();
})

var NO_CAMERA_ERROR = "First add camera"

function check_cctv_validation_init(){
    if($("#id_camera_id").find("option").length <= 2){
        var error_html = `<div class="flx non_field_errors">
                            <div class="non_field_errors_div">
                                ${NO_CAMERA_ERROR}
                            </div>
                          </div>`;
        $("#take_attendance_cctv_crud_form").prepend(error_html)
    }
}

function partial_take_attendance_cctv_form(){
    $("#id_camera_id").unbind('change.id_camera_id', id_camera_id_fn)
    $("#id_camera_id").bind('change.id_camera_id', id_camera_id_fn)
}

function id_camera_id_fn(){
    var id = $(this).val();
    get_camera_details_ajax();
}

function show_camera_preview(data){
    hide_camera_preview();

    var preview_html = ``;
    var url = data[0]['url'];
//    debugger;
//    var url = "";
    preview_html = `<div class="preview_div p10">
                        <div class="flx">Preview</div>
                        <img id="img" src="${url}" class="camera_feed">
                    </div>`;
    $("#take_attendance_cctv_crud_form").append(preview_html)
}

function hide_camera_preview(){
    $(".preview_div").remove()

}

function get_camera_details_ajax(){
    var data = get_camera_details_ajax_data()
    $.ajax({
        type: "POST",
        url: "{% url 'app_smart_attendance:get_camera_details_ajax' %}",
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
            hide_camera_preview();
        },
        success: function (data) {
            get_camera_details_ajax_success(data);
            dynamic_loader_end();
        },
        error: function (data) {
            dynamic_loader_end();

        }
    });
}

function get_camera_details_ajax_success(data){
    var camera_data = data["data"];
    show_camera_preview(camera_data)
}

function get_camera_details_ajax_data(){
    var camera_id =  $("#id_camera_id").val();
    var camera_details_ajax_data = {'csrfmiddlewaretoken': '{{ csrf_token }}'};
    camera_details_ajax_data['camera_id'] = camera_id;

    return camera_details_ajax_data;
}




