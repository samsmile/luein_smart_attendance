"use strict"

$(document).ready(function(){

});



function get_profiles_by_role_event(){
    $("#id_person").unbind('change.id_profile_face',id_profile_face_change_fn)
    $("#id_person").bind('change.id_profile_face',id_profile_face_change_fn)
}

function get_profiles_by_role_ajax(){

    var data = get_profiles_by_role_ajax_data();
    $.ajax({
        type: "POST",
        url: "{% url 'app_smart_attendance:get_profiles_by_role_ajax' %}",
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function(data) {
            get_profiles_by_role_ajax_success(data);
            dynamic_loader_end();
        },
        error:function(data) {
            dynamic_loader_end();
            alert('error');
        }
    })
}

var old_users = [];
function get_profiles_by_role_ajax_success(data){
    console.warn(data)
    old_users = [];
    if(data["data"]){
        old_users = data["data"];
        $("#id_person").html("")
        var options = `<option value="">---------</option>`;
        for(var i in data["data"]){
            var obj = data["data"][i]
            if(obj['role'] == "student"){
                options += `<option value="${i}">
                            (Class:${obj["class_name"]}${obj["section"]}) -
                            ${obj["first_name"]} ${obj["last_name"]}
                        </option>`;
            }
            else if(obj['role'] == "teacher"){
                options += `<option value="${i}">
                                ${obj["first_name"]} ${obj["last_name"]}
                            </option>`;
            }
            else if(obj['role'] == "non_teacher"){
                options += `<option value="${i}">
                                ${obj["first_name"]} ${obj["last_name"]}
                            </option>`;
            }
            else if(obj['role'] == "school_admin"){
                options += `<option value="${i}">
                                ${obj["first_name"]} ${obj["last_name"]}
                            </option>`;
            }
        }
        $("#id_person").append(options)
        get_profiles_by_role_event();
        show_person_select();
    }else{
        hide_person_select();
    }
}

function id_profile_face_change_fn(){
    var index = $(this).val();
    if(index){
        var data = old_users[index]
        render_old_person_data(data);
        show_face_form_fn();
    }else{
        reset_old_person_data();
        hide_face_form_fn();
        reset_face_form();
    }
}

function render_old_person_data(data){
    var old_details = data;
    var class_name = old_details["class_name"];
    register_face_profile_id =  old_details["profile_id"];
    var first_name = data["first_name"];
    var last_name = data["last_name"];
    var email = data["email"];
    var gender = data["gender"];
    var gender_display = '';
    if(gender){
        if(gender == "male"){
            gender_display = "(M)";
        }else if(gender == "female"){
            gender_display = "(F)";
        }
    }

    var display_data = "";
    if(data){
        display_data = `
                        <div class="flx flxc">
                            <div class="flx p5">${first_name} ${last_name} ${gender_display}</div>
                            <div class="flx p5 c6">${email}</div>
                        </div>
                        `;
    }

    $(".display_person_data").html(display_data);
}

function reset_old_person_data(){
    $(".display_person_data").html("");
}

// reset section
function reset_face_section(class_name){
    $("#id_section").html(`<option value="">---Select---</option>`);
    for(var i in class_name_section_dict[class_name]){
        var value = class_name_section_dict[class_name][i];
        var text = section_choice[class_name_section_dict[class_name][i]];
        $("#id_section").append(`<option value="${value}">${text}</option>`)
    }
}

var face_form_ids = [
    "id_profile",
    "id_username",
    "id_email",
    "id_first_name",
    "id_last_name",
    "id_gender",
    "id_blood_group",
    "id_class_name",
    "id_section",
]
var face_form_select_ids = [
    "id_gender",
    "id_blood_group",
    "id_class_name",
    "id_section",
]

function reset_face_form(){
    for(var i in face_form_ids){
        var id = face_form_ids[i];
        $(`#${id}`).val(``);
    }
    remove_select_options();
}
function remove_select_options(){
    for(var i in face_form_select_ids){
        var id = face_form_select_ids[i];
        $(`#${id}`).html(``);
    }
}

function show_face_form_fn(){
    $(".all_detail").removeClass("dn");
}
function hide_face_form_fn(){
    $(".all_detail").addClass("dn");
    reset_face_form();
}




function show_person_select(){
    $(".person").removeClass("dn");
}
function hide_person_select(){
    $(".person").addClass("dn");
}


function get_profiles_by_role_ajax_data(){
    var role =  $("#id_role").find('input[name=role]:checked').val();
    var profiles_by_role_ajax_data = {'csrfmiddlewaretoken': '{{ csrf_token }}'};
    profiles_by_role_ajax_data['role'] = role;

    return profiles_by_role_ajax_data;
}