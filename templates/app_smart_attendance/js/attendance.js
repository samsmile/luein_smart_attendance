
"use strict"

$(document).ready(function() {
    attendance_events();
    reset_class();
});
var None = null;

var class_name_section_dict = {}
{% if request.organisation_type == "school" %}
    {% if class_name_section_dict %}
        class_name_section_dict = {{class_name_section_dict|safe}};
    {% else %}
        {% if request.user.profile.role in 'school_admin,admin' %}
        $(".no_data").html(`<div class="red pl10 pr10 pl5  m10">
                                        <div class="flx p5">
                                            * No class record available
                                        </div>
                                    </div>
                                    <div class="red pl10 pr10  m10">
                                        <div class="flx p5">
                                            <div class="flx mr5">* For initial setup, go to</div>
                                            <div class="flx mr5"><a href="{% url 'app_smart_attendance:settings' %}">settings</a></div>
                                            <div class="flx">:</div>
                                        </div>
                                        <div class="pl20">
                                            <div class="flx p5">1. Add teachers</div>
                                            <div class="flx p5">2. Create periods</div>
                                        </div>
                                        <div class="flx p5">
                                            <div class="flx mr5">* Next steps :</div>
                                         </div>
                                       <div class="pl20">
                                            <div class="flx p5">3. Student register</div>
                                            <div class="flx p5">4. Ready to take attendance</div>
                                        </div>
                                    </div>
                        `);
        {% elif request.user.profile.role == 'teacher' %}
                $(".no_data").html(`<div class="red pl10 pr10  m10">
                                    * No class record available
                                </div>
                                <div class="red pl10 pr10  m10">
                                    * Please contact your school admin
                                </div>
                            `);
        {% endif %}
        $(".all_content_container").remove()
    {% endif %}
{% elif request.organisation_type == "company" %}
// write here
{% elif request.organisation_type == "anganwadi" %}
// write here
{% endif %}


{% if attendance_filter_qs %}
    var attendances_json = {{attendance_filter_qs|safe}};
    $(document).ready(function() {
        render_attendance_status_for_student(attendances_json);
        show_attendance_table_div();
    });
{% endif %}

var class_name_section_period_dict = {}
{% if class_name_section_period_dict %}
    class_name_section_period_dict = {{class_name_section_period_dict|safe}};
{% endif %}

var period_id_map_dict = {}
{% if period_id_map_dict %}
    period_id_map_dict = {{period_id_map_dict|safe}};
{% endif %}

var class_choice = {}
{% if class_choice %}
    class_choice = {{class_choice|safe}};
{% endif %}

var section_choice = {}
{% if section_choice %}
    section_choice = {{section_choice|safe}};
{% endif %}

var period_choice = {}
{% if period_choice %}
    period_choice = {{period_choice|safe}};
{% endif %}

function take_attendance_events(){

}
$('#id_class').on('change', function() {
  var class_name = this.value
  reset_section(class_name)
});

$('#id_section').on('change', function() {
  var section = this.value;
  var class_name = $('#id_class').val();
  var class_name_section_val = class_name+section;
  reset_period(class_name_section_val)
  reset_date();
});

function reset_class(){
    $("#id_class").html(`<option value="">---Select---</option>`);
    for(var i in class_name_section_dict){
        var value = i;
        var text = class_choice[i];
        $("#id_class").append(`<option value="${value}">${text}</option>`)
    }
}

function reset_section(class_name){
    $("#id_section").html(`<option value="">---Select---</option>`);
    for(var i in class_name_section_dict[class_name]){
        var value = class_name_section_dict[class_name][i];
        var text = section_choice[class_name_section_dict[class_name][i]];
        $("#id_section").append(`<option value="${value}">${text}</option>`)
    }
}

function reset_period(class_name_section_val){
    $("#id_period").html(`<option value="">---Select---</option>`);
    console.warn("--class_name_section_val--",class_name_section_val)

    for(var i in class_name_section_period_dict[class_name_section_val]){
        var value = class_name_section_period_dict[class_name_section_val][i];
        var text = period_choice[class_name_section_period_dict[class_name_section_val][i]];
        $("#id_period").append(`<option value="${value}">${text}</option>`)
    }
}

function reset_date(){
    $("#id_date").val("")

}

function attendance_events(){
    $("#attendance_frm_class").unbind("submit.attendance_frm_class", get_attendance_class_ajax)
    $("#attendance_frm_class").bind("submit.attendance_frm_class", get_attendance_class_ajax)


    $("#attendance_frm_cctv").unbind("submit.attendance_frm_cctv", get_attendance_cctv_ajax)
    $("#attendance_frm_cctv").bind("submit.attendance_frm_cctv", get_attendance_cctv_ajax)



    $("#attendance_frm_self").unbind("submit.attendance_frm_self", get_attendance_self_ajax)
    $("#attendance_frm_self").bind("submit.attendance_frm_self", get_attendance_self_ajax)

    $("#attendance_frm_other").unbind("submit.attendance_frm_other", get_attendance_other_ajax)
    $("#attendance_frm_other").bind("submit.attendance_frm_other", get_attendance_other_ajax)

    $("#id_date").unbind("change.id_date", id_date_fn)
    $("#id_date").bind("change.id_date", id_date_fn)
}

function id_date_fn(){
    var date_val = $("#id_date").val();
    var date_inst = new Date(date_val);
    var day = date_inst.getDay();

    var section = $('#id_section').val();
    var class_name = $('#id_class').val();
    var class_name_section_val = class_name+section+"_"+day;
    reset_period(class_name_section_val)
}

function get_day_from_date(){
    var date_val = $("#id_date").val();
    var date_inst = new Date(date_val);
    var day = date_inst.getDay();
    return day;
}

function get_class_ajax_data(){
    var form_text_data = get_attendance_class_ajax_data();
    form_text_data["csrfmiddlewaretoken"] = '{{ csrf_token }}';
    return form_text_data;
}
function get_attendance_class_ajax_data(){
    var return_data = {}
    var formData = $('#attendance_frm_class').serializeArray()
    for(var data of formData){
        return_data[data["name"]]=data["value"];
    }
    // add extra filed in formData
    return return_data;
}
function get_attendance_class_ajax(){
    var data = get_class_ajax_data();
    $.ajax({
        type: "POST",
        url: "{% url 'app_smart_attendance:get_attendance_class_ajax' %}",
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function(data) {
            get_attendance_class_ajax_success(data);
            dynamic_loader_end();
        },
        error:function(data) {
            dynamic_loader_end();
            alert('error');
        }
    })
}
function get_attendance_class_ajax_success(data){
    console.warn(data)
    $(".result_of_class_div").html(`
    <div class="small c6 flx">
        <div class="mr5">Showing result of: - </div>
        <div class="mr5">Class name: ${class_choice[data.input.class_name]}</div>
        <div class="mr5">,</div>
        <div class="mr5">Section: ${section_choice[data.input.section]}</div>
        <div class="mr5">,</div>
        <div class="mr5">Date: ${data.input.date}</div>
    </div>
    `)

    var attendance_json ={};
    if(data.data.length){
        attendance_json = data.data;
    }
    render_class_attendance_status(attendance_json);
    if(!data.data.length){
        $("#attendance_table_date").html(`Date: ${data.input.date}`)
    }

    show_attendance_table_div();
}
function render_class_attendance_status(attendance_json){
    if(attendance_json.length){
        $(".attendance_table_div").html("");
        for(var j in attendance_json){
            var table = `
            <div class="small c3 attendance_table_date" id="attendance_table_date${j}">Period: ${attendance_json[j].period_number}</div>
            <table class="table405 attendance_table" id="attendance_table${j}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>User name</th>
                                <th>Student name</th>
                                <th>
                                    <div class="flx a-i-c">
                                        <div>Status</div>
                                         ${at_st_label}
                                     </div>
                                 </th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>`;
            $(".attendance_table_div").append(table);
            var tbody = ``;
            var attendance_status = attendance_json[j].attendance_status;
            var at_st_counter = 0;
            for(var i in attendance_status){
                at_st_counter++;
                var username = attendance_status[i]["username"];
                var name = attendance_status[i]["name"];
                var status = attendance_status[i]["status"];
                tbody += `<tr>
                            <td>${at_st_counter}</td>
                            <td>${username}</td>
                            <td>${name}</td>
                            <td><div class="at_st ${status}">${at_st[status]}</div></td>
                        </tr>`;
            }
            $(`#attendance_table${j} tbody`).html(tbody);
        }
    }
    else{
        var table = `
            <div class="small c3 attendance_table_date" id="attendance_table_date"></div>
            <table class="table405 attendance_table" id="attendance_table0">
            <thead>
            <tr>
                <th>Student name</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="red p10" colspan="2">No data to show...!!!</td>
                </tr>
            </tbody>
        </table>`;
        $(".attendance_table_div").html(table);
    }
}
function show_attendance_table_div(){
    $(".attendance_table_div").removeClass("dn");

}
function hide_attendance_table_div(){
    $(".attendance_table_div").addClass("dn");

}


function get_cctv_ajax_data(){
    var form_text_data = get_attendance_cctv_ajax_data();
    form_text_data["csrfmiddlewaretoken"] = '{{ csrf_token }}';
    return form_text_data;
}
function get_attendance_cctv_ajax_data(){
    var return_data = {}
    var formData = $('#attendance_frm_cctv').serializeArray()
    for(var data of formData){
        return_data[data["name"]]=data["value"];
    }
    // add extra filed in formData
    return return_data;
}
function get_attendance_cctv_ajax(){
    var data = get_cctv_ajax_data();
    $.ajax({
        type: "POST",
        url: "{% url 'app_smart_attendance:get_attendance_cctv_ajax' %}",
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function(data) {
            get_attendance_cctv_ajax_success(data);
            dynamic_loader_end();
        },
        error:function(data) {
            dynamic_loader_end();
            alert('error');
        }
    })
}
function get_attendance_cctv_ajax_success(data){
    console.warn(data)
    $(".result_of_cctv_div").html(`
    <div class="small c6 flx">
        <div class="mr5">Showing result of: - </div>
        <div class="mr5">Date: ${data.input.date}</div>
    </div>
    `)

    var attendance_json ={};
    if(data.data.length){
        attendance_json = data.data;
    }
    render_cctv_attendance_status(attendance_json);
    if(!data.data.length){
        $("#cctv_attendance_table_date").html(`Date: ${data.input.date}`)
    }

    show_cctv_attendance_table_div();
}
function render_cctv_attendance_status(attendance_json){
    var thead ;
    thead = `<thead>
            <tr>
                <th>#</th>
                <th>User name</th>
                <th>Name</th>
                <th>
                    <div class="flx a-i-c">
                        <div>Status</div>
                         ${at_st_label}
                     </div>
                 </th>
            </tr>
            </thead>`
    if(attendance_json.length){
        $(".cctv_attendance_table_div").html("");

        for(var j in attendance_json){
            var table = `
            <div class="small c3 cctv_attendance_table_date" id="cctv_attendance_table_date${j}">Time: ${get_time_format(attendance_json[j].updated_at)}</div>
            <table class="table405 cctv_attendance_table" id="cctv_attendance_table${j}">
                            ${thead}
                            <tbody>

                            </tbody>
                        </table>`;
            $(".cctv_attendance_table_div").append(table);
            var tbody = ``;
            var cctv_attendance_status = attendance_json[j].cctv_attendance_status;
            var at_st_counter = 0;
            for(var i in cctv_attendance_status){
                at_st_counter++;
                var username = cctv_attendance_status[i]["username"];
                var name = cctv_attendance_status[i]["name"];
                var status = cctv_attendance_status[i]["status"];
                tbody += `<tr>
                            <td>${at_st_counter}</td>
                            <td>${username}</td>
                            <td>${name}</td>
                            <td><div class="at_st ${status}">${at_st[status]}</div></td>
                        </tr>`;
            }
            $(`#cctv_attendance_table${j} tbody`).html(tbody);
        }
    }
    else{
        var table = `
            <div class="small c3 cctv_attendance_table_date" id="cctv_attendance_table_date"></div>
            <table class="table405 cctv_attendance_table" id="cctv_attendance_table0">
            ${thead}
            <tbody>
                <tr>
                    <td class="red p10" colspan="2">No data to show...!!!</td>
                </tr>
            </tbody>
            </table>`;
        $(".cctv_attendance_table_div").html(table);
    }
}
function show_cctv_attendance_table_div(){
    $(".cctv_attendance_table_div").removeClass("dn");

}
function hide_cctv_attendance_table_div(){
    $(".cctv_attendance_table_div").addClass("dn");

}




function get_self_ajax_data(){
    var form_text_data = get_attendance_self_ajax_data();
    form_text_data["csrfmiddlewaretoken"] = '{{ csrf_token }}';
    return form_text_data;
}
function get_attendance_self_ajax_data(){
    var return_data = {}
    var formData = $('#attendance_frm_self').serializeArray()
    for(var data of formData){
        return_data[data["name"]]=data["value"];
    }
    // add extra filed in formData

    return return_data;
}
function get_attendance_self_ajax(){
    var data = get_self_ajax_data();
    $.ajax({
        type: "POST",
        url: "{% url 'app_smart_attendance:get_attendance_self_ajax' %}",
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function(data) {
            get_attendance_self_ajax_success(data);
            dynamic_loader_end();
        },
        error:function(data) {
            dynamic_loader_end();
            alert('error');
        }
    })
}
function get_attendance_self_ajax_success(data){
    $(".result_of_self_div").html(`
    <div class="small c6 flx">
        <div class="mr5">Showing result of: - </div>
        <div class="mr5">Date: ${data.input.date}</div>
    </div>
    `)

    var attendance_json ={};
    if(data.data.length){
        attendance_json = data.data;
    }
    render_self_attendance_status(attendance_json);
    if(!data.data.length){
        $("#attendance_table_date").html(`Date: ${data.input.date}`)
    }

    show_self_attendance_table_div();
}
function render_self_attendance_status(attendance_json){
    if(attendance_json.length){
        var tbody = ``;
        for(var j in attendance_json){
            var data = attendance_json[j]
            var attempt_status = data.attempt_status;
            var attendance_date = data.attendance_date;
            var attendance_type = data.attendance_type;
            var loggedin_user_id = data.loggedin_user_id;
            var taken_by_id = data.taken_by_id;
            var taken_for_id = data.taken_for_id;
            var taken_by_user = '';
            if(taken_by_id == taken_for_id){
                taken_by_user = 'self';
            }else{
                taken_by_user = 'other';
            }
            tbody += `<tr>
                        <td>#</td>
                        <td>${attempt_status}</td>
                        <td>${attendance_date}</td>
                        <td>${attendance_type}</td>
                        <td>${taken_by_user}</td>
                    </tr>`;
            $(`#self_attendance_table tbody`).html(tbody);
        }
    }
    else{
        var tbody = `
                <tr>
                    <td class="red p10" colspan="2">No data to show...!!!</td>
                </tr>`;
        $(`#self_attendance_table tbody`).html(tbody);
    }
}
function show_self_attendance_table_div(){
    $(".self_attendance_table_div").removeClass("dn");

}
function hide_self_attendance_table_div(){
    $(".self_attendance_table_div").addClass("dn");

}


function get_other_ajax_data(){
    var form_text_data = get_attendance_other_ajax_data();
    form_text_data["csrfmiddlewaretoken"] = '{{ csrf_token }}';
    return form_text_data;
}
function get_attendance_other_ajax_data(){
    var return_data = {}
    var formData = $('#attendance_frm_other').serializeArray()
    for(var data of formData){
        return_data[data["name"]]=data["value"];
    }
    // add extra filed in formData
    return return_data;
}
function get_attendance_other_ajax(){
    var data = get_other_ajax_data();
    $.ajax({
        type: "POST",
        url: "{% url 'app_smart_attendance:get_attendance_other_ajax' %}",
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function(data) {
            get_attendance_other_ajax_success(data);
            dynamic_loader_end();
        },
        error:function(data) {
            dynamic_loader_end();
            alert('error');
        }
    })
}
function get_attendance_other_ajax_success(data){
    $(".result_of_other_div").html(`
    <div class="small c6 flx">
        <div class="mr5">Showing result of: - </div>
        <div class="mr5">Date: ${data.input.date}</div>
    </div>
    `)

    var attendance_json ={};
    if(data.data.length){
        attendance_json = data.data;
    }
    render_other_attendance_status(attendance_json);
    if(!data.data.length){
        $("#attendance_table_date").html(`Date: ${data.input.date}`)
    }

    show_other_attendance_table_div();
}
function render_other_attendance_status(attendance_json){
    if(attendance_json.length){
        var tbody = ``;
        for(var j in attendance_json){
            var data = attendance_json[j]
            var attempt_status = data.attempt_status;
            var attendance_date = data.attendance_date;
            var attendance_type = data.attendance_type;
            var loggedin_user_id = data.loggedin_user_id;
            var taken_by_id = data.taken_by_id;
            var taken_for_id = data.taken_for_id;
            var taken_by_user = '';
            if(taken_by_id == taken_for_id){
                taken_by_user = 'self';
            }else{
                taken_by_user = 'other';
            }
            tbody += `<tr>
                        <td>#</td>
                        <td>${attempt_status}</td>
                        <td>${attendance_date}</td>
                        <td>${attendance_type}</td>
                        <td>${taken_by_user}</td>
                    </tr>`;
            $(`#other_attendance_table tbody`).html(tbody);
        }
    }
    else{
        var tbody = `
                <tr>
                    <td class="red p10" colspan="2">No data to show...!!!</td>
                </tr>`;
        $(`#other_attendance_table tbody`).html(tbody);
    }
}
function show_other_attendance_table_div(){
    $(".other_attendance_table_div").removeClass("dn");

}
function hide_other_attendance_table_div(){
    $(".other_attendance_table_div").addClass("dn");

}



const at_st = {
    "present":"P",
    "absent":"A",
    "leave":"L"
}
const at_st_label = ` <div class="pr cp info-fafa-div-status wfc">
            <div class="p2 info-fafa-small">
                <svg x="0px" y="0px" width="16" height="16" viewBox="0 0 172 172" style=" fill:#000000;">
                    <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
                       stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
                       font-family="none" font-weight="none" font-size="none" text-anchor="none"
                       style="mix-blend-mode: normal">
                        <path d="M0,172v-172h172v172z" fill="none"></path>
                        <g fill="#440055">
                            <path d="M86,21.5c-35.62237,0 -64.5,28.87763 -64.5,64.5c0,35.62237 28.87763,64.5 64.5,64.5c35.62237,0 64.5,-28.87763 64.5,-64.5c0,-35.62237 -28.87763,-64.5 -64.5,-64.5z"
                                  opacity="0.3"></path>
                            <path d="M93.16667,121.83333h-14.33333v-43h14.33333zM93.16667,64.5h-14.33333v-14.33333h14.33333z"></path>
                            <path d="M86,157.66667c-39.41667,0 -71.66667,-32.25 -71.66667,-71.66667c0,-39.41667 32.25,-71.66667 71.66667,-71.66667c39.41667,0 71.66667,32.25 71.66667,71.66667c0,39.41667 -32.25,71.66667 -71.66667,71.66667zM86,28.66667c-31.53333,0 -57.33333,25.8 -57.33333,57.33333c0,31.53333 25.8,57.33333 57.33333,57.33333c31.53333,0 57.33333,-25.8 57.33333,-57.33333c0,-31.53333 -25.8,-57.33333 -57.33333,-57.33333z"></path>
                        </g>
                    </g>
                </svg>
            </div>
            <div class="dnone info-fafa-info-status">
                <div class="flx flxc">
                    <div class="flx m2 a-i-c">
                        <div class="at_st present mr5">P</div>
                        <div>Present</div>
                    </div>
                    <div class="flx m2 a-i-c">
                        <div class="at_st absent mr5">A</div>
                        <div>Absent</div>
                    </div>
                    <div class="flx m2 a-i-c">
                        <div class="at_st leave mr5">L</div>
                        <div>Leave</div>
                    </div>
                </div>
            </div>
        </div>`;


function render_attendance_status_for_student(attendance_json){
    var tbody = ``;
    if(attendance_json.length){
        for(var i in attendance_json){
            var obj = get_object_from_array_by_key_value(attendance_json[i]["attendance_status"],"username","{{request.user.username}}");
            var name = obj[0]["name"];
            var status = obj[0]["status"];
            var username = obj[0]["username"];

            var attendance_id = attendance_json[i]["attendance_id"];
            var loggedin_user_id = attendance_json[i]["loggedin_user_id"];
            var period_id_id = attendance_json[i]["period_id_id"];
            var media_id_id = attendance_json[i]["media_id_id"];
            var attendance_status = JSON.stringify(obj)
            var attendance_date = attendance_json[i]["attendance_date"];
            tbody += `<tr>
                <td>${name}</td>
                <td>${status}</td>
                <td>${attendance_date}</td>
            </tr>`;
        }
    }else{
        tbody = `<tr><td class="red p10" colspan="2">No data to show...!!!</td></tr>`;
    }

    $(".attendance_table tbody").html(tbody);
}
function get_object_from_array_by_key_value(object, key,value){
    const found_object = object.filter(v => v[key] === value);
    return found_object;
}


// filter start
$(".js_view_attendance").unbind('click.js_view_attendance', js_view_attendance_fn)
$(".js_view_attendance").bind('click.js_view_attendance', js_view_attendance_fn)

function js_view_attendance_fn(){
    var elem = $(this);
    var id = elem.data('id');
    if(id == "class"){
        show_view_attendance_class()
    }
    else if(id == "cctv"){
        show_view_attendance_cctv()
    }
    else if(id == "self"){
        show_view_attendance_self()
    }
    else if(id == "other"){
        show_view_attendance_other()
    }
    else{

    }
}
function show_view_attendance_class(){
    $(".attendance_filter").addClass('dn');
    $(".attendance_filter[data-id='class']").removeClass('dn');

    $(".js_view_attendance").removeClass('active');
    $(".js_view_attendance[data-id='class']").addClass('active');
}
function show_view_attendance_cctv(){
    $(".attendance_filter").addClass('dn');
    $(".attendance_filter[data-id='cctv']").removeClass('dn');

    $(".js_view_attendance").removeClass('active');
    $(".js_view_attendance[data-id='cctv']").addClass('active');
}
function show_view_attendance_self(){
    $(".attendance_filter").addClass('dn');
    $(".attendance_filter[data-id='self']").removeClass('dn');

    $(".js_view_attendance").removeClass('active');
    $(".js_view_attendance[data-id='self']").addClass('active');
}
function show_view_attendance_other(){
    $(".attendance_filter").addClass('dn');
    $(".attendance_filter[data-id='other']").removeClass('dn');

    $(".js_view_attendance").removeClass('active');
    $(".js_view_attendance[data-id='other']").addClass('active');
}
// filter end


function get_date_time_format(date_str) {
    var d = new Date(date_str) // 26.3.2020
    var dtfUK = new Intl.DateTimeFormat('UK', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    }); //
    var dtfUS = new Intl.DateTimeFormat('en', {
        year: 'numeric',
        month: 'short',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
//        second: '2-digit'
    }); //
//     console.warn(dtfUS.format(d)); // 08/05/2010 11:45:00 PM
//     console.warn(dtfUK.format(d)); // 05.08.2010 23:45:00
    return dtfUS.format(d);
}
function get_time_format(date_str) {
    var d = new Date(date_str) // 26.3.2020
    var dtfUK = new Intl.DateTimeFormat('UK', {
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    }); //
    var dtfUS = new Intl.DateTimeFormat('en', {
        hour: '2-digit',
        minute: '2-digit',
//        second: '2-digit'
    }); //
//     console.warn(dtfUS.format(d)); // 08/05/2010 11:45:00 PM
//     console.warn(dtfUK.format(d)); // 05.08.2010 23:45:00
    return dtfUS.format(d);
}
function get_date_format(date_str) {
    var d = new Date(date_str) // 26.3.2020
    var dtfUK = new Intl.DateTimeFormat('UK', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'
    }); //
    var dtfUS = new Intl.DateTimeFormat('en', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'
    }); //
//     console.warn(dtfUS.format(d)); // 08/05/2010 11:45:00 PM
//     console.warn(dtfUK.format(d)); // 05.08.2010 23:45:00
    return dtfUK.format(d);
}











