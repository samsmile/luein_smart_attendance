"use strict"

/////////////////////////////////////////////
//////  EMPLOYER MANAGE USER PAGE START ////////////
/////////////////////////////////////////////
var all_div = $(".all_div");
var admin_div = $(".admin_div");
var staff_div = $(".staff_div");
var school_admin_div = $(".school_admin_div");
var teacher_div =$(".teacher_div");
var non_teacher_div = $(".non_teacher_div");
var student_div = $(".student_div");
var tab_div = $(".tab_div");


var toggle_profile_button = $(".toggle-profile-button");
toggle_profile_button.click(function(e){
    var id = $(this).data('id');
    sessionStorage.sd_tab = id;
    render_school_details_tab();
});

render_school_details_tab();
function render_school_details_tab(){
    if(sessionStorage.sd_tab=="all"){
        all_fn();
    }
    {% if object.organisation_type == "company" or object.organisation_type == "anganwadi" %}
        else if(sessionStorage.sd_tab=="admin"){
            admin_fn();
        }
        else if(sessionStorage.sd_tab=="staff"){
            staff_fn();
        }
        else{
            admin_fn();
        }
    {% elif object.organisation_type == "school" %}
        else if(sessionStorage.sd_tab=="school_admin"){
            school_admin_fn();
        }
        else if(sessionStorage.sd_tab=="teacher"){
            teacher_fn();
        }
        else if(sessionStorage.sd_tab=="non_teacher"){
            non_teacher_fn();
        }
        else if(sessionStorage.sd_tab=="student"){
            student_fn();
        }
        else{
            school_admin_fn();
        }
    {% endif %}
}

function all_fn(){
    tab_div.addClass('dn');
    all_div.removeClass('dn');

    $(".toggle-profile-button").removeClass("active");
    $(".toggle-profile-button[data-id='all']").addClass("active");
    $(".sub-menu-title").text("Manage users");
}
function admin_fn(){
    tab_div.addClass('dn');
    admin_div.removeClass('dn');

    $(".toggle-profile-button").removeClass("active");
    $(".toggle-profile-button[data-id='admin']").addClass("active");
    $(".sub-menu-title").text("Manage users");
}
function staff_fn(){
    tab_div.addClass('dn');
    staff_div.removeClass('dn');

    $(".toggle-profile-button").removeClass("active");
    $(".toggle-profile-button[data-id='staff']").addClass("active");
    $(".sub-menu-title").text("Manage users");
}
function school_admin_fn(){
    tab_div.addClass('dn');
    school_admin_div.removeClass('dn');

    $(".toggle-profile-button").removeClass("active");
    $(".toggle-profile-button[data-id='school_admin']").addClass("active");
    $(".sub-menu-title").text("Manage users");
}
function teacher_fn(){
    tab_div.addClass('dn');
    teacher_div.removeClass('dn');

    $(".toggle-profile-button").removeClass("active");
    $(".toggle-profile-button[data-id='teacher']").addClass("active");
    $(".sub-menu-title").text("Manage roles");
}
function non_teacher_fn(){
    tab_div.addClass('dn');
    non_teacher_div.removeClass('dn');

    $(".toggle-profile-button").removeClass("active");
    $(".toggle-profile-button[data-id='non_teacher']").addClass("active");
    $(".sub-menu-title").text("Manage roles");
}
function student_fn(){
    tab_div.addClass('dn');
    student_div.removeClass('dn');

    $(".toggle-profile-button").removeClass("active");
    $(".toggle-profile-button[data-id='student']").addClass("active");
    $(".sub-menu-title").text("Manage roles");
}
//


