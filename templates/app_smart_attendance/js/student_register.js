"use strict"

$(document).ready(function() {
    student_register_form_event();
    reset_register_class();
});

var None = "";
var old_students = {}
{% if old_students_qs %}
    {% for item in old_students_qs %}
    old_students["{{item.profile_id}}"]=
    {
        "first_name":"{{item.user.first_name}}",
        "last_name":"{{item.user.last_name}}",
        "gender":"{{item.gender}}",
        "class_name":"{{item.class_name}}",
        "section":"{{item.section}}",
        "email":"{{item.user.email}}",
        "blood_group":"{{item.blood_group}}"
    }
    {% endfor %}
{% endif %}

var class_name_section_dict = {}
{% if request.organisation_type == "school" %}

    {% if class_name_section_dict %}
        class_name_section_dict = {{class_name_section_dict|safe}};
    {% else %}
        {% if request.user.profile.role in 'school_admin,admin' %}
            $(".no_data").html(`<div class="red pl10 pr10 pl5  m10">
                                        <div class="flx p5">
                                            * No class record available
                                        </div>
                                    </div>
                                    <div class="red pl10 pr10  m10">
                                        <div class="flx p5">
                                            <div class="flx mr5">* For initial setup:</div>
                                            <div class="flx">:</div>
                                        </div>
                                        <div class="pl20">
                                            <div class="flx p5">1. Register teachers</div>
                                            <div class="flx p5">2. Create periods</div>
                                        </div>
                                        <div class="flx p5">
                                            <div class="flx mr5">* Next steps :</div>
                                         </div>
                                       <div class="pl20">
                                            <div class="flx p5">3. Register student </div>
                                            <div class="flx p5">4. Ready to take attendance</div>
                                        </div>
                                    </div>
                            `);
        {% elif request.user.profile.role == 'teacher' %}
            $(".no_data").html(`<div class="red pl10 pr10  m10">
                                    * No class record available
                                </div>
                                <div class="red pl10 pr10  m10">
                                    * Please contact your school admin
                                </div>
                            `);
            $(".all_content_container").remove()
        {% endif %}
    {% endif %}
{% elif request.organisation_type == "company" %}
// write here
{% elif request.organisation_type == "anganwadi" %}
// write here
{% endif %}

var class_choice = {}
{% if class_choice %}
    class_choice = {{class_choice|safe}};
{% endif %}

var section_choice = {}
{% if section_choice %}
    section_choice = {{section_choice|safe}};
{% endif %}

var blood_group_choice = {}
{% if blood_group_choice %}
    blood_group_choice = {{blood_group_choice|safe}};
{% endif %}

var gender_choice = {}
{% if gender_choice %}
    gender_choice = {{gender_choice|safe}};
{% endif %}



function student_register_form_event(){
    $('#id_register_profile').unbind('change.id_profile',id_profile_change_fn)
    $('#id_register_profile').bind('change.id_profile',id_profile_change_fn)

    $('#id_register_select_task').unbind('change.id_select_task',id_select_task_change_fn)
    $('#id_register_select_task').bind('change.id_select_task',id_select_task_change_fn)
}

function id_select_task_change_fn(){
    var value = $(this).val();
    if(value == "new"){
        hide_old_student_dropdown();
        reset_only_right_form();
        show_form_fn();
    }else if(value == "old"){
        show_old_student_dropdown();
        hide_form_fn();
    }
    else{
        hide_old_student_dropdown();
        hide_form_fn();
        reset_only_right_form();
    }
}

const right_form_ids = [
    "id_profile",
    "id_first_name",
    "id_last_name",
    "id_gender",
    "id_class_name",
    "id_section",
    "id_email",
    "id_blood_group",
]

function reset_only_right_form(){
    for(var i in right_form_ids){
        var id = right_form_ids[i];
        $(`#${id}`).val("");
    }
}

function show_old_student_dropdown(){
    $('.old_dropdown_div').removeClass("dn");

}

function hide_old_student_dropdown(){
    $('.old_dropdown_div').addClass("dn");

}

function show_form_fn(){
    $(".form-right").removeClass("dn");

}

function hide_form_fn(){
    $(".form-right").addClass("dn");

}

function id_profile_change_fn(){
    var profile_id = $(this).val();
//    var profile_id = $(this).data("id");
    render_old_student_data(profile_id);
    show_form_fn();
}

function render_old_student_data(profile_id){
    var old_details = old_students[profile_id];
    var class_name = old_details["class_name"];
    reset_register_section(class_name);
    for(var i in old_details){
        $(`#id_register_${i}`).val(`${old_details[i]}`)
    }
}



function reset_register_class(){
    $("#id_class_name").html(`<option value="">---Select---</option>`);
    for(var i in class_name_section_dict){
        var value = i;
        var text = class_choice[i];
        $("#id_class_name").append(`<option value="${value}">${text}</option>`)
    }
}

function reset_register_section(class_name){
    $("#id_section").html(`<option value="">---Select---</option>`);
    for(var i in class_name_section_dict[class_name]){
        var value = class_name_section_dict[class_name][i];
        var text = section_choice[class_name_section_dict[class_name][i]];
        $("#id_section").append(`<option value="${value}">${text}</option>`)
    }
}


$("#student_register_frm").submit(function(){
    hide_student_register_frm_container();
    show_student_register_video_container();

//    // open device selection
//    show_connectivity_test()
    reset_recording_btn();
    ct_init_devices_access_permissions_fn();
})

function show_student_register_frm_container(){
    $(".student_register_frm_container").removeClass("dn");

}

function hide_student_register_frm_container(){
    $(".student_register_frm_container").addClass("dn");

}


// connectivity.js recorder config start
var AJAX_URL = "{% url 'app_smart_attendance:user_register_ajax' %}";
function get_ajax_data(){
    var file_key = '';
    file_key = 'media_file';
    var form_data = new FormData();
    form_data.append('csrfmiddlewaretoken', '{{ csrf_token }}');
    form_data.append(file_key, upblob);

    var form_text_data = get_register_face_form_data()
    var form_text_data_json = JSON.stringify(form_text_data)
    form_data.append("form_text_data_json", form_text_data_json);

    return form_data
}
function ajax_success_fn(event){
    var response = JSON.parse(event.currentTarget.response)
    displayAlert(response.message,"success",2000)
    show_student_register_frm_container();
    $("#student_register_frm").trigger('reset')
}
function ajax_error_fn(event){

}
function cancel_recording_stream_callback(){
    show_student_register_frm_container();
    hide_student_register_video_container();
}
// connectivity.js recorder config end

// get form data in object
function get_student_register_form_data(){
    var formData = $('#student_register_frm').serializeArray()
    formData.push({
        "name":"media_type",
        "value":"register",
    },{
        "name":"registration_type",
        "value":"new_user",
    })
    var return_data = {}
    for(var data of formData){
        return_data[data["name"]]=data["value"]
    }
    return return_data;
}

var main_formData;
function get_register_face_form_data(){
    var formData = main_formData;
    formData.push({
        "name":"media_type",
        "value":"register"
    },{
        "name":"registration_type",
        "value":"face",
    })
    var return_data = {}
    for(var data of formData){
        return_data[data["name"]]=data["value"]
    }
    return return_data;
}




