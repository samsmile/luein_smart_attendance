//
//$(".luein-modal-container").unbind('submit');
//
//
//$(".luein-modal-container").on("submit", ".js_register_face_create_form", saveFormRegisterFace);
//
//$(".luein-modal-container").on("submit", ".js_register_face_update_form", saveFormRegisterFace);
//
//$(".luein-modal-container").on("submit", ".js_register_face_delete_form", saveFormRegisterFace);
//

$(".luein-modal-button.submit").unbind("click.submit", check_form_is_valid);
$(".luein-modal-button.submit").bind("click.submit", check_form_is_valid);

$(".js_register_face_create_form").unbind("submit.js_register_face_create_form", open_register_face);
$(".js_register_face_create_form").bind("submit.js_register_face_create_form", open_register_face);


function check_form_is_valid(){
    if(!$(`[name='role']:checked`).val()){
        displayAlert("User group is required...!!!", "error", 2000)
    }
}


var register_face_profile_id = "";
function open_register_face(){
    main_formData = $('#register_face_crud_form').serializeArray()
    main_formData.push({name: 'profile_id', value:  register_face_profile_id})

    luein_modal_close();

    hide_student_register_frm_container();
    show_student_register_video_container();

    // open device selection
    reset_recording_btn();
    ct_init_devices_access_permissions_fn();
}



