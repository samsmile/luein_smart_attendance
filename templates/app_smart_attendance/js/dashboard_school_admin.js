'use strict'

$(document).ready(function() {
    dashboard_events();
});

var time_div = document.getElementById('time_div');
var date_div = document.getElementById('date_div');
var class_form_interval_preselected;
formatAMPM();

function dashboard_events(){
    show_date();
    class_form_interval_preselected = setInterval(formatAMPM, 1000);
}


// /////////////
function formatAMPM() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = `<div class="flx">
                    <div class="time_inner_div">${hours}:${minutes}</div>
                    <div class="time_ampm_div">${ampm}</div>
                </div>`;
    time_div.innerHTML = strTime;
}

// Attaching a new function  toShortFormat()  to any instance of Date() class
Date.prototype.toShortFormat = function() {

    const monthNames = ["Jan", "Feb", "Mar", "Apr",
                        "May", "Jun", "Jul", "Aug",
                        "Sep", "Oct", "Nov", "Dec"];

    const day = this.getDate();

    const monthIndex = this.getMonth();
    const monthName = monthNames[monthIndex];

    const year = this.getFullYear();

    return `${day}-${monthName}-${year}`;
}

function show_date(){
    let today = new Date();
    date_div.textContent = today.toShortFormat();
}

////////////