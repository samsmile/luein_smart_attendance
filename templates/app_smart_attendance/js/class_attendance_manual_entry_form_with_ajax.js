"use strict"

$(document).ready(function(){

});


function open_manual_entry_form_init(attendance_choice){
    get_list_of_all_students_attendance_by_class_name_section_date_ajax(attendance_choice)
}

function get_list_of_all_students_attendance_by_class_name_section_date_ajax(attendance_choice){
    var data = get_list_of_all_students_attendance_by_class_name_section_date_ajax_data(attendance_choice);
    $.ajax({
        type: "POST",
        url: "{% url 'app_smart_attendance:get_list_of_all_students_attendance_by_class_name_section_date_ajax' %}",
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function(data) {
            get_list_of_all_students_attendance_by_class_name_section_date_ajax_success(data);
            dynamic_loader_end();
        },
        error:function(data) {
            dynamic_loader_end();
            alert('error');
        }
    })
}

function get_list_of_all_students_attendance_by_class_name_section_date_ajax_data(attendance_choice){
    var class_name = $(`[name='class_name']`).val();
    var section = $(`[name='section']`).val();
    var period_id = $(`[name='period_id']`).val()
    var attendance_date = $(`[name='attendance_date']`).val()

    var list_of_all_students_ajax_data = {'csrfmiddlewaretoken': '{{ csrf_token }}'};
    list_of_all_students_ajax_data['class_name'] = class_name;
    list_of_all_students_ajax_data['section'] = section;
    list_of_all_students_ajax_data['period_number'] = period_id;
    list_of_all_students_ajax_data['attendance_date'] = attendance_date;
    list_of_all_students_ajax_data['attendance_choice'] = attendance_choice;


    return list_of_all_students_ajax_data;
}

var old_attendance_data = []
function get_list_of_all_students_attendance_by_class_name_section_date_ajax_success(data){
    var want_to_edit = false;
    var is_already_taken = data['is_already_taken'];
    var input = data["input"];
    var attendance_choice = input['attendance_choice'];
    var class_name = input["class_name"];
    var section = input["section"];
    var attendance_date = input["attendance_date"];
    old_attendance_data = data['data'];

    var old_attendance_choice_display_html = "";
    var old_attendance_choice_display = data['old_attendance_choice_display'];
    if(old_attendance_choice_display != null){
        old_attendance_choice_display_html = `<div class="small mt10">Attendance initiated "${old_attendance_choice_display}"</div>`;
    }

    var date_time_html = "";
    var date_time = data['date_time'];
    if(date_time != null){
        date_time_html = `<div class="small">Taken at "${get_date_time_format(date_time)}"</div>`;
    }

    var last_edit_date_time_html = "";
    var last_edit_date_time = data['last_edit_date_time'];
    if(last_edit_date_time != null){
        last_edit_date_time_html = `<div class="small">Last edited at "${get_date_time_format(last_edit_date_time)}"</div>`;
    }



    if(is_already_taken){
        let text = `<div class="">
                         Attendance already taken for Class
                         <strong>
                            ${data['input']["class_name"]}
                            ${section_choice[data['input']["section"]]}
                            (${period_choice[data['input']["period_number"]]} Period)
                        </strong>
                        for Date:
                        <strong>
                            ${attendance_date}
                         </strong>.
                    </div>
                    ${old_attendance_choice_display_html}
                    ${date_time_html}
                    ${last_edit_date_time_html}
                    <div class="mt10">
                        Do you want to edit the attendance?
                    </div>`;

        var confirmation_obj = {
            "icon":"",
            "head":"Edit Attendance",
            "body":text,
            "width":"400px",
            "height":"100%",
            "action_type":"confirm",
            "action_yes":action_yes,
            "action_no":action_no,
            "action_close":action_close,
            "position":"2",
            "cancel_text":"No",
            "submit_text":"Yes, View and Edit",
            "effect":"shake"
        }

        dynamic_confirmation(confirmation_obj);

        function action_yes(){
            //here define the code execute on action_yes
             want_to_edit = true;
             after()
        }
        function action_no(){
            //here define the code execute on action_no
            want_to_edit = false;
        }
        function action_close(){
            //here define the code execute on action_close
            want_to_edit = false;
        }
    }
    else{
        after()
    }

    function after(){
        if(!is_already_taken){
            if(attendance_choice == 'face_recognition'){
                open_register_face();
            }
            else if(attendance_choice == 'manual'){
               open_manual_entry(data)
            }
        }
        else if(is_already_taken && want_to_edit){
            open_manual_entry(data)
        }
        luein_modal_close();
    }
}

function open_manual_entry(data){
    if(data["data"]){
    var table;
    var thead = `<tr>
                    <th>Student detail</th>
                    <th>Attendance status</th>
                </tr>`;
    var tbody = "";
    var all_list = data["data"];

    for(var i in all_list){
        var student_data = all_list[i];
        var row = get_row_html(student_data, i)
        tbody += row;
    }
    table = `<table class="table405">
                ${tbody}
            </table>`;

    open_manual_entry_form(table, data)
}
    else{
        alert(`No student found in ${class_name}-section.`)
    }
}

var student_fafa_icon = `<svg width="50" height="50" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 172 172"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#bababa"><path d="M87.54715,8.17667c-0.44692,0.00134 -0.89312,0.09913 -1.30766,0.29263l-41.0349,19.16747c-0.95977,0.44848 -1.62519,1.35425 -1.76695,2.40405c-0.1421,1.04947 0.2593,2.09926 1.06555,2.78592l11.48289,9.78418c0.04789,1.13026 0.12443,2.31961 0.20143,3.48929c0.13034,1.97968 0.26466,4.02695 0.24014,5.60989c-0.00004,0.00265 0.00069,0.00523 0.00066,0.00787c-0.02631,0.70347 -0.01921,1.43313 -0.0105,2.14095c0.00437,0.35139 0.00918,0.70622 0.00918,1.0603c0,16.74111 10.37099,46.64545 31.03551,46.64545c6.93073,0 13.55708,-3.49129 19.16287,-10.0965c8.82599,-10.39852 13.97685,-27.4818 12.06554,-39.89717c0.02562,-1.29499 0.31054,-2.86269 0.5892,-4.3849c0.21981,-1.2005 0.44265,-2.4293 0.55443,-3.60674l6.28964,-5.91893v16.69911c-2.94,1.2243 -5.01282,4.12681 -5.01282,7.50479c0,4.4804 3.64507,8.12614 8.12614,8.12614c4.48073,0 8.12549,-3.64541 8.12549,-8.12614c0,-3.37824 -2.07225,-6.28058 -5.01216,-7.50479v-22.57342c0,-0.22887 -0.02661,-0.45117 -0.07349,-0.66597c0.08263,-0.378 0.09634,-0.77255 0.03215,-1.16528c-0.16696,-1.02159 -0.83092,-1.89329 -1.77155,-2.32597l-41.68052,-19.16747c-0.41572,-0.19115 -0.86337,-0.2861 -1.31029,-0.28476zM87.56683,14.72089l36.17365,16.63416l-3.81867,3.59296c0.00465,-0.51571 0.00442,-1.01672 0.01247,-1.5419v-0.00066c0.00122,-0.07958 -0.00048,-0.15833 -0.00525,-0.23686c-0.0143,-0.2356 -0.05507,-0.46624 -0.12007,-0.68762c-0.02166,-0.07379 -0.04588,-0.14616 -0.07283,-0.21783c-0.02695,-0.07168 -0.05652,-0.14264 -0.08858,-0.21193c-0.06403,-0.13841 -0.13843,-0.27165 -0.22177,-0.39893v-0.00066c-0.08338,-0.12728 -0.17625,-0.24818 -0.27754,-0.36218v-0.00066c-0.05071,-0.05706 -0.10385,-0.11201 -0.15878,-0.16534c-0.05494,-0.05333 -0.11226,-0.10486 -0.17125,-0.15419c-0.17696,-0.148 -0.37175,-0.27731 -0.58199,-0.38449c-0.07008,-0.03573 -0.14179,-0.06908 -0.21521,-0.09973c-1.34866,-0.56299 -2.69548,-1.09107 -4.04043,-1.58455c-1.34496,-0.49348 -2.68788,-0.95217 -4.02797,-1.3759c-0.67004,-0.21187 -1.33914,-0.41516 -2.00775,-0.60954c-6.01751,-1.74942 -11.97432,-2.79011 -17.81715,-3.1107c-0.6492,-0.03562 -1.29713,-0.06222 -1.94345,-0.08005c-0.64632,-0.01783 -1.29096,-0.0269 -1.93426,-0.0269c-5.38569,0 -10.76508,0.62523 -16.09154,1.86603c-0.59182,0.13786 -1.18306,0.28328 -1.77351,0.43633c-1.77135,0.45916 -3.53635,0.98656 -5.29298,1.58192c-1.17108,0.39688 -2.3385,0.82428 -3.50175,1.28142c-0.58144,0.22852 -1.16274,0.46443 -1.74202,0.70796c-0.07099,0.02984 -0.14077,0.0625 -0.20865,0.09711c-0.06787,0.03462 -0.13487,0.07174 -0.19946,0.11089c-0.12933,0.07842 -0.25226,0.16604 -0.36743,0.26114c-0.11517,0.09513 -0.22312,0.19852 -0.32281,0.30838c-0.09978,0.10999 -0.19132,0.22692 -0.27426,0.34972c-0.08294,0.1228 -0.15698,0.2515 -0.22177,0.38515c-0.19437,0.40093 -0.30448,0.84629 -0.31166,1.30963c-0.01008,0.66079 -0.01663,1.32035 -0.021,1.98282l-3.694,-3.1481zM86.24998,29.92337c0.55344,0 1.10837,0.00716 1.6646,0.02165c0.55623,0.0145 1.11355,0.03599 1.67247,0.06496c2.23567,0.11588 4.49238,0.34766 6.76665,0.69418c0.56857,0.08663 1.13851,0.18049 1.70921,0.28148c2.85354,0.50494 5.73321,1.18894 8.63202,2.05106c1.15952,0.34485 2.32208,0.71834 3.48732,1.12001c1.16523,0.40167 2.333,0.83165 3.50307,1.28995c-0.01277,1.79962 -0.00928,3.53619 -0.00525,5.23065v0.00066l0.00263,1.48941v0.00066c0.0009,1.01179 -0.25515,2.41289 -0.52687,3.89609c-0.10078,0.54993 -0.20252,1.10529 -0.29591,1.66329c-8.27683,-2.24709 -16.36686,-3.34363 -24.62186,-3.34363c-8.22778,0 -16.67303,1.10186 -25.7084,3.36003c-0.03998,-0.68229 -0.08556,-1.37109 -0.13057,-2.05237c-0.11557,-1.75395 -0.2246,-3.41056 -0.23555,-4.79958c-0.00002,-0.00395 0.00069,-0.00786 0.00066,-0.01181c-0.0168,-2.23466 -0.02981,-4.30393 -0.01772,-6.33098c0.49425,-0.1919 0.98913,-0.37831 1.48482,-0.55837c1.98284,-0.72015 3.9765,-1.34538 5.97864,-1.87521c1.00105,-0.26493 2.00425,-0.50594 3.009,-0.72305c1.00477,-0.21711 2.01113,-0.41022 3.01885,-0.57936c1.0077,-0.16914 2.01682,-0.31466 3.02672,-0.43567c1.51486,-0.18153 3.03192,-0.30839 4.54959,-0.38121c1.01178,-0.04852 2.02381,-0.07283 3.0359,-0.07283zM88.23805,50.60977c8.19485,0 16.23019,1.16456 24.50179,3.55097c0.9616,10.51438 -3.5978,24.72135 -10.86089,33.27881c-3.05804,3.60327 -8.04171,7.89847 -14.41579,7.89847c-16.05311,0 -24.81017,-26.70045 -24.81017,-40.41879c0,-0.25921 -0.00368,-0.51765 -0.00656,-0.7762c9.06696,-2.37361 17.46167,-3.53325 25.59161,-3.53325zM71.99167,57.98334c-1.71933,0 -3.11333,1.39334 -3.11333,3.11267c0,1.71933 1.394,3.11333 3.11333,3.11333h6.226c1.71933,0 3.11333,-1.394 3.11333,-3.11333c0,-1.71933 -1.394,-3.11267 -3.11333,-3.11267zM96.89566,57.98334c-1.71933,0 -3.11332,1.39334 -3.11332,3.11267c0,1.71933 1.39433,3.11333 3.11332,3.11333h6.22534c1.71933,0 3.11333,-1.394 3.11333,-3.11333c0,-1.71933 -1.394,-3.11267 -3.11333,-3.11267zM129.23752,59.96419c1.04745,0 1.90015,0.85236 1.90015,1.90015c0,1.04779 -0.85269,1.90015 -1.90015,1.90015c-1.04779,0 -1.90015,-0.85236 -1.90015,-1.90015c0,-1.04779 0.85236,-1.90015 1.90015,-1.90015zM81.20633,76.66395c-0.69443,0.02733 -1.38414,0.28628 -1.94411,0.78407c-1.2853,1.14219 -1.4007,3.10913 -0.25851,4.39409c0.37827,0.4253 3.83892,4.15788 8.78358,4.15788c2.11036,0 5.26699,-0.72158 8.32167,-4.15788c1.14219,-1.28496 1.02645,-3.2519 -0.25851,-4.39409c-1.28463,-1.14152 -3.25222,-1.02678 -4.39475,0.25851c-1.22852,1.38205 -2.41969,2.05871 -3.64216,2.06812h-0.02559c-1.82918,0 -3.60761,-1.49935 -4.1441,-2.08321c-0.64362,-0.71504 -1.54467,-1.06263 -2.43752,-1.0275zM70.38415,96.584c-1.02192,0.00202 -1.97744,0.50551 -2.55693,1.34703l-5.54625,8.05397l-37.63156,8.01067c-1.43714,0.30604 -2.46507,1.57504 -2.46507,3.04443v43.66991c0,1.71933 1.394,3.11333 3.11333,3.11333h24.90399h24.89481c0.00326,0.00001 0.00658,0.00066 0.00984,0.00066c0.00528,0 0.01047,-0.00063 0.01575,-0.00066h21.75589c0.00527,0.00003 0.01047,0.00066 0.01575,0.00066c0.00327,0 0.00658,-0.00065 0.00984,-0.00066h24.89481h24.90399c1.71933,0 3.11301,-1.39368 3.11267,-3.11267v-40.20161c0,-1.33804 -0.85505,-2.52597 -2.12389,-2.95126l-32.74209,-10.96849l-7.48904,-8.89709c-0.60502,-0.71891 -1.50619,-1.12837 -2.44211,-1.10755c-0.93928,0.01814 -1.82069,0.46028 -2.39749,1.20203l-15.71492,20.21399l-13.95781,-20.0808c-0.58218,-0.83716 -1.53704,-1.33588 -2.55627,-1.33588zM105.17403,104.64519l3.92824,4.66704l-0.54524,0.68172l-12.55501,15.69392l-4.72806,-3.16254zM70.40253,105.16812l11.92906,17.16168l-5.1657,3.18616l-9.35769,-16.58035zM62.55327,112.29302l10.78149,19.10119c0.15596,0.27639 0.35179,0.52419 0.5787,0.73749c0.07564,0.0711 0.1546,0.13896 0.23686,0.20209c0.00013,0.0001 0.00052,-0.0001 0.00066,0c0.08213,0.063 0.16713,0.12193 0.25523,0.1765c0.00015,0.00009 0.0005,-0.00009 0.00066,0c0.08811,0.05455 0.17885,0.10526 0.27229,0.15091c0.00017,0.00008 0.00048,-0.00008 0.00066,0c0.09345,0.04563 0.18922,0.08645 0.28738,0.12269c0.09854,0.0364 0.19916,0.06802 0.30182,0.09448c0.11822,0.03051 0.23723,0.05193 0.35693,0.06824c0.03674,0.00502 0.07342,0.00746 0.11023,0.01115c0.07935,0.00794 0.15877,0.01327 0.23817,0.01509c0.02384,0.00055 0.04769,0.00459 0.07152,0.00459c0.02219,0 0.0441,-0.00412 0.06627,-0.00459c0.09819,-0.0021 0.19592,-0.00899 0.29329,-0.02034c0.02865,-0.00333 0.05742,-0.0044 0.08595,-0.00853c0.12473,-0.01805 0.24759,-0.04434 0.3694,-0.07742c0.03227,-0.00875 0.06379,-0.01973 0.0958,-0.02953c0.09257,-0.02836 0.18369,-0.06139 0.2736,-0.09842c0.03668,-0.01509 0.07341,-0.02942 0.10957,-0.04593c0.10669,-0.04878 0.21151,-0.10227 0.31297,-0.16338c0.0092,-0.00553 0.01906,-0.00946 0.02822,-0.01509l2.26889,-1.39952l-7.22266,26.48264h-19.41286v-19.74486c0,-1.71933 -1.39334,-3.11333 -3.11267,-3.11333c-1.71966,0 -3.11333,1.39434 -3.11333,3.11333v19.74486h-18.67799v-38.03705zM114.20235,112.90453l29.38666,9.8452v34.84761h-18.67734v-19.74486c0,-1.71933 -1.39334,-3.11333 -3.11267,-3.11333c-1.71933,0 -3.11332,1.39434 -3.11332,3.11333v19.74486h-19.41286l-7.41622,-27.19257l3.06018,2.04712h0.00066c0.00775,0.00518 0.01584,0.00934 0.02362,0.01444c0.06964,0.04562 0.14002,0.08854 0.21193,0.12795c0.02361,0.01295 0.04769,0.02446 0.07152,0.03674c0.05735,0.02954 0.11537,0.05698 0.17387,0.08267c0.02645,0.01162 0.05274,0.02328 0.07939,0.03412c0.06069,0.02466 0.12207,0.04695 0.18372,0.06758c0.0241,0.00806 0.04794,0.01683 0.07217,0.02428c0.02111,0.00649 0.04245,0.01103 0.06364,0.01706c0.11467,0.03257 0.23057,0.05774 0.34709,0.07677c0.02808,0.0046 0.05583,0.00997 0.08398,0.01378c0.12802,0.01732 0.25605,0.02623 0.38449,0.02756c0.01095,0.00012 0.02186,0.00262 0.0328,0.00262c0.02652,0 0.0529,-0.00392 0.07939,-0.00459c0.06648,-0.0017 0.13265,-0.00522 0.19881,-0.01115c0.04126,-0.00369 0.08229,-0.0078 0.12335,-0.01312c0.06622,-0.0086 0.13195,-0.01934 0.19749,-0.03215c0.03973,-0.00776 0.07934,-0.01497 0.11876,-0.02427c0.0713,-0.01684 0.14188,-0.03786 0.21193,-0.05971c0.0316,-0.00985 0.06383,-0.01735 0.09514,-0.02821c0.00065,-0.00023 0.00132,-0.00043 0.00197,-0.00066c0.09356,-0.03254 0.18515,-0.07003 0.27558,-0.11154c0.04252,-0.01951 0.08361,-0.04216 0.12532,-0.06364c0.05332,-0.02747 0.10626,-0.0553 0.15813,-0.08595c0.04116,-0.02432 0.08122,-0.05045 0.12138,-0.07677c0.05123,-0.03358 0.10145,-0.06883 0.15091,-0.10564c0.03775,-0.02808 0.07491,-0.05666 0.11154,-0.08661c0.05267,-0.04307 0.10334,-0.08892 0.15354,-0.13582c0.02898,-0.02708 0.0585,-0.05301 0.08661,-0.08136c0.07745,-0.07815 0.15291,-0.15966 0.22308,-0.24736zM86,132.59165l6.81979,25.00569h-13.63959z"></path></g></g></svg>`
function get_row_html(student_data, index){
    var student_name = student_data["name"];
    var father_name = student_data["father_name"];
    var father_name_html = "";
    if(father_name){
        father_name_html = `${father_name}`;
    }
    var profile_id = student_data["profile_id"];
    var attendance_status = student_data["status"];

    var profile_picture_url_html = "";
    var profile_picture_url = student_data["profile_picture_url"];
    if(profile_picture_url != null){
        profile_picture_url_html = `<img class="student_profile_pic" src="${profile_picture_url}">`;
    }else{
        profile_picture_url_html = `${student_fafa_icon}`;
    }

    var attendance_status_html = get_pal_html(attendance_status, index)
    var row_html = `<tr class="student_row" data-id="${profile_id}">
                        <td>
                            <div class="flx">
                                <div class="profile_pic_div">${profile_picture_url_html}</div>
                                <div class="flx flxc a-s-c pl5 pr5">
                                    <div class="student_name">${student_name}</div>
                                    <div class="father_name">${father_name_html}</div>
                                <div>
                            </div>
                        </td>
                        <td>
                            ${attendance_status_html}
                        </td>
                    </tr>`;

    return row_html;
}

function attendance_status_event(){
    $(".attendance_status_card_item input[type='radio']").unbind('change.attendance_status_card_item', attendance_status_change_fn)
    $(".attendance_status_card_item input[type='radio']").bind('change.attendance_status_card_item', attendance_status_change_fn)
}

function get_pal_html(attendance_status, index){
    var p_html, a_html, l_html;
    if(attendance_status == "present"){
        p_html = `<div class="attendance_status_card_item">
                    <label>
                        <input type="radio" name="attendance_status${index}" value="P" checked/>
                        <div class="attendance_flag">P</div>
                    </label>
                </div>`;
    }else{
        p_html = `<div class="attendance_status_card_item">
                    <label>
                        <input type="radio" name="attendance_status${index}" value="P" />
                        <div class="attendance_flag">P</div>
                    </label>
                </div>`;
    }
    if(attendance_status == "absent"){
        a_html = `<div class="attendance_status_card_item">
                    <label>
                        <input type="radio" name="attendance_status${index}" value="A" checked/>
                        <div class="attendance_flag">A</div>
                    </label>
                </div>`;
    }
    else{
        a_html = `<div class="attendance_status_card_item">
                    <label>
                        <input type="radio" name="attendance_status${index}" value="A" />
                        <div class="attendance_flag">A</div>
                    </label>
                </div>`;
    }

    if(attendance_status == "leave"){
        l_html = `<div class="attendance_status_card_item">
                        <label>
                            <input type="radio" name="attendance_status${index}" value="L" checked/>
                            <div class="attendance_flag">L</div>
                        </label>
                    </div>`;
    }else{
        l_html = `<div class="attendance_status_card_item">
                        <label>
                            <input type="radio" name="attendance_status${index}" value="L" />
                            <div class="attendance_flag">L</div>
                        </label>
                    </div>`;
    }

    var pal_html = `<div class="attendance_status_card_container">
        ${p_html}
        ${a_html}
        ${l_html}
    </div>`

    return pal_html;
}


function open_manual_entry_form(table, data){
    var class_name = data["input"]["class_name"]
    var section = data["input"]["section"]
    var period_number = data["input"]["period_number"]
    var attendance_date = data["input"]["attendance_date"]
    var body = `
                <div class="flx">
                    <div>Class Attendance:</div>
                    <div class="pl5">${class_name}</div>
                    <div>${section_choice[section]}</div>
                    <div class="small pl5">(${period_choice[period_number]} Period)</div>
                    <div class="mla">Date: ${attendance_date}</div>
                </div>
                <div class="oa h100 mt10 bdrt">
                    ${table}
                </div>`;

    var confirmation_obj = {
        "icon":"",
        "head":"Mark Attendance",
        "body":body,
        "width":"80%",
        "height":"100%",
        "action_type":"form",
        "action_yes":action_yes,
        "action_no":action_no,
        "action_close":action_close,
        "position":"2",
        "cancel_text":"Cancel",
        "submit_text":"Submit",
        "effect":"fade",
        "before_action_yes":before_action_yes
    }

    dynamic_confirmation(confirmation_obj);
    attendance_status_event();

    function action_yes(){
        //here define the code execute on action_yes
        var student_attendance_json = get_student_attendance_json()
        console.warn('---student_attendance_json---',student_attendance_json)
        take_attendance_class_manual_view_ajax(data, student_attendance_json);
    }
    function action_no(){
        //here define the code execute on action_no
    }
    function action_close(){
        //here define the code execute on action_close
    }
    function before_action_yes(){
        //here define the code execute on before_action_yes
        var is_valid = check_is_student_attendance_valid(get_student_attendance_json())
        if(!is_valid){
            displayAlert("Mark all student attendance before submit", "error", 3000)
        }
        return is_valid
    }
}
var attendance_status_map = {
    "P":"present",
    "A":"absent",
    "L":"leave"
}
function get_student_attendance_json(){
    var student_rows = $(".student_row")
    var student_attendance_json = []
    student_rows.each(function() {
        var name = $(this).find('.student_name').text();
        var status = attendance_status_map[$(this).find('input[type="radio"]:checked').val()];
        var profile_id = $(this).data('id');
        student_attendance_json.push({
            "status": status,
            "profile_id": profile_id
        })
    });
    return student_attendance_json;
}

function attendance_status_change_fn(){
    var attendance_status = $(this).val();
    var id = $(this).closest('.student_row').data('id');

    //Find index of specific object using findIndex method.
    var objIndex = old_attendance_data.findIndex((obj => obj.profile_id == id));

    //Update object's name property.
    old_attendance_data[objIndex]["status"] = attendance_status_map[attendance_status]
}

function check_is_student_attendance_hacked(){
    var is_hacked = false;
    if(JSON.stringify(old_attendance_data) === JSON.stringify(get_student_attendance_json())){
        is_hacked = false;
    }else{
        is_hacked = true;
    }
    return is_hacked;
}


function check_is_student_attendance_valid(data){
    var attendance_json = data;
    var is_valid = true;
    if(attendance_json){
        for(var i in attendance_json){
            if(is_valid){
                if(swap_json_obj(attendance_status_map)[attendance_json[i]['status']]){
                    // check if attendance flags is valid
                }else{
                    //
                    is_valid = false;
                }
            }else{
                //
            }
        }
    }else{
        //
        is_valid = true;
    }
    return is_valid;
}

function swap_json_obj(json){
  var ret = {};
  for(var key in json){
    ret[json[key]] = key;
  }
  return ret;
}



function take_attendance_class_manual_view_ajax(data, student_attendance_json){
    var data = take_attendance_class_manual_view_ajax_data(data, student_attendance_json);
    $.ajax({
        type: "POST",
        url: "{% url 'app_smart_attendance:take_attendance_class_manual_view_ajax' %}",
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function(data) {
            take_attendance_class_manual_view_ajax_success(data);
            dynamic_loader_end();
        },
        error:function(data) {
            dynamic_loader_end();
            alert('error');
        }
    })
}

function take_attendance_class_manual_view_ajax_data(data, student_attendance_json){
    var class_name = data["input"]["class_name"]
    var section = data["input"]["section"]
    var period_number = data["input"]["period_number"]
    var attendance_date = data["input"]["attendance_date"]
    var attendance_choice = data["input"]["attendance_choice"]
    var attendance_id = data["attendance_id"]
    var attendance_status = JSON.stringify(student_attendance_json)

    var list_of_all_students_ajax_data = {'csrfmiddlewaretoken': '{{ csrf_token }}'};
    list_of_all_students_ajax_data['class_name'] = class_name;
    list_of_all_students_ajax_data['section'] = section;
    list_of_all_students_ajax_data['period_number'] = period_number;
    list_of_all_students_ajax_data['attendance_date'] = attendance_date;
    list_of_all_students_ajax_data['attendance_choice'] = attendance_choice;
    list_of_all_students_ajax_data['attendance_status'] = attendance_status;
    list_of_all_students_ajax_data['attendance_id'] = attendance_id;
    list_of_all_students_ajax_data['attendance_taker'] = profile_id_selected_assignee;


    return list_of_all_students_ajax_data;
}

function take_attendance_class_manual_view_ajax_success(data){
    displayAlert(data.message,data.status,2000)
}




function get_date_time_format(date_str) {
    var d = new Date(date_str) // 26.3.2020
    var dtfUK = new Intl.DateTimeFormat('UK', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    }); //
    var dtfUS = new Intl.DateTimeFormat('en', {
        year: 'numeric',
        month: 'short',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
//        second: '2-digit'
    }); //
//     console.warn(dtfUS.format(d)); // 08/05/2010 11:45:00 PM
//     console.warn(dtfUK.format(d)); // 05.08.2010 23:45:00
    return dtfUS.format(d);
}
function get_time_format(date_str) {
    var d = new Date(date_str) // 26.3.2020
    var dtfUK = new Intl.DateTimeFormat('UK', {
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    }); //
    var dtfUS = new Intl.DateTimeFormat('en', {
        hour: '2-digit',
        minute: '2-digit',
//        second: '2-digit'
    }); //
//     console.warn(dtfUS.format(d)); // 08/05/2010 11:45:00 PM
//     console.warn(dtfUK.format(d)); // 05.08.2010 23:45:00
    return dtfUS.format(d);
}
function get_date_format(date_str) {
    var d = new Date(date_str) // 26.3.2020
    var dtfUK = new Intl.DateTimeFormat('UK', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'
    }); //
    var dtfUS = new Intl.DateTimeFormat('en', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'
    }); //
//     console.warn(dtfUS.format(d)); // 08/05/2010 11:45:00 PM
//     console.warn(dtfUK.format(d)); // 05.08.2010 23:45:00
    return dtfUK.format(d);
}
