"use strict"

//$(".luein-modal-container").unbind('submit');
//$(".luein-modal-container").on("submit", ".js_take_attendance_create_form", saveFormTakeAttendance);
//$(".luein-modal-container").on("submit", ".js_take_attendance_update_form", saveFormTakeAttendance);
//$(".luein-modal-container").on("submit", ".js_take_attendance_delete_form", saveFormTakeAttendance);


$(".luein-modal-button.submit").unbind("click.submit", check_form_is_valid);
$(".luein-modal-button.submit").bind("click.submit", check_form_is_valid);

function cancel_device_selection(){
    reset_action_type_other()
}
function reset_action_type_other_preselected(){
    action_type_other_preselected = "";
}
function check_form_is_valid(){
    var is_form_valid = false;

    if(!is_geolocation_allowed){
        is_form_valid = false;
        displayAlert("Location required: Please allow geolocation permission...!!!", "error", 2000);
    }

    {% if not is_profile %}
        else if(!$(`[name='role']:checked`).val()){
            displayAlert("Attendance for role is required...!!!", "error", 2000)
        }
        else if(!$(`[name='person']`).val()){
            displayAlert("Attendance for is required...!!!", "error", 2000)
        }
    {% endif %}


    else if(action_type_other_preselected == "check_in"){
        is_form_valid = true;
    }
    else if(action_type_other_preselected == "check_out"){
        is_form_valid = true;
    }
    else{
        is_form_valid = false;
        displayAlert("Please select your preferred action...!!!", "error", 2000)
    }

//    if(!$(`[name='check_in']:checked`).val()){
//        displayAlert("Please select your preferred action...!!!", "error", 2000)
//    }
//    else if(!$(`[name='check_out']:checked`).val()){
//        displayAlert("Please select your preferred action...!!!", "error", 2000)
//    }


    if(is_form_valid){
        open_register_face();
    }
}

//$(".luein-modal-container").on("submit", ".js_take_attendance_create_form", open_register_face);

var profile_id_other_selected = "";
var main_formData;
function open_register_face(){
    main_formData = $('#take_attendance_crud_form').serializeArray()
    main_formData.push({name: 'profile_id', value:  profile_id_other_preselected})

    luein_modal_close();

    show_student_register_video_container();

    // open device selection
    reset_recording_btn();
    ct_init_devices_access_permissions_fn();

    // clear other time interval - 1 on submit
    clearInterval(other_form_interval_preselected)
}

{% if not is_attendance_under_process %}
    // clear other time interval - 2 on close
    $(".luein-modal-close").click(function(){
        clearInterval(other_form_interval_preselected)
    });

    // clear other time interval - 3 on cancel
    $(".luein-modal-button.cancel").click(function(){
        clearInterval(other_form_interval_preselected)
    });
{% endif %}

function get_take_attendance_other_form_data(){
    var formData = $('#take_attendance_crud_form').serializeArray()
    formData.push({
        "name":"media_type",
        "value":"attendance"
    })
    var return_data = {}
    for(var data of formData){
        return_data[data["name"]]=data["value"]
    }

    var date = new Date();
    var role = role_other_preselected;
    var profile_id = profile_id_other_preselected;
    // add extra filed in formData
    return_data["action_type"] = action_type_other_preselected;
    return_data["date"] = date;
    return_data["role"] = role;
    return_data["profile_id"] = profile_id;

    return return_data;
}



// connectivity.js recorder config start
var AJAX_URL = "{% url 'app_smart_attendance:take_attendance_other_view_ajax' %}";
function get_ajax_data(){
    var file_key = '';
    file_key = 'media_file';
    var form_data = new FormData();
    form_data.append('csrfmiddlewaretoken', '{{ csrf_token }}');
    form_data.append(file_key, upblob);

    var form_text_data = get_take_attendance_other_form_data()


    // add extra fields
    var staff_attendance_geolocation = {}
    staff_attendance_geolocation["distance_meter"]=position_distance_meter_val;
    staff_attendance_geolocation["msg"]=position_msg;
    staff_attendance_geolocation["lat"]=position_val.coords.latitude;
    staff_attendance_geolocation["long"]=position_val.coords.longitude;
    staff_attendance_geolocation["accuracy"]=position_val.coords.accuracy;

    form_text_data["staff_attendance_geolocation"]=staff_attendance_geolocation;

    var form_text_data_json = JSON.stringify(form_text_data)
    form_data.append("form_text_data_json", form_text_data_json);


    return form_data
}
function ajax_success_fn(event){
    var response = JSON.parse(event.currentTarget.response)
    displayAlert(response.message,"success",2000)
    show_student_attendance_frm_container();
    $("#take_attendance_frm").trigger('reset')
}
function ajax_error_fn(event){

}
function cancel_recording_stream_callback(){
    reset_action_type_other_preselected();
    show_student_attendance_frm_container();
    hide_student_register_video_container();
}
// connectivity.js recorder config end




