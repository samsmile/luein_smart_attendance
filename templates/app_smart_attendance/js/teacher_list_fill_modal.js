

/* Functions */

var loadFormTeacher = function () {

    console.warn("load_teacher_crud_form");

    var btn = $(this);

    if(btn.attr("data-action_type")=="delete"){

    }else{
        luein_modal_open();
        luein_modal_content_clear();
    }

    $.ajax({
        url: btn.attr("data-url"),
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function (data) {
            successFormTeacher(data);
            dynamic_loader_end();
        },
        error: function (data) {
            dynamic_loader_end();

        }
    });
};

var successFormTeacher = function(data){
    if(data.action_type == "delete"){
        var confirmation_obj = {
            "icon":"",
            "head":"Archive Job?",
            "body":data.html_form,
            "width":"400px",
            "action_type":"delete",
            "action_yes":action_yes,
            "action_no":action_no,
            "action_close":action_close,
            "position":"2",
            "cancel_text":"No, keep it",
            "submit_text":"Yes, archived it",
            "effect":"shake"
        }

        dynamic_confirmation(confirmation_obj);

        function action_yes(){
            //here define the code execute on action_yes
            saveFormTeacher();
        }
        function action_no(){
            //here define the code execute on action_no
        }
        function action_close(){
            //here define the code execute on action_close
        }
    }
    else
    {
        if(data.html_form){
            $(".luein-modal-content").html(data.html_form);
        }else{
            displayAlert( data.message, 'error', 2000);
        }
    }
}

var saveFormTeacher = function () {
    var form = $('#teacher_crud_form');
    var url = form.attr("action");
    var form_data = new FormData($('#teacher_crud_form').get(0));
    $.ajax({
        url: url,
        data: form_data,
        async: true,
        cache: false,
        processData: false,
        contentType: false,
        enctype: 'multipart/form-data',
        type: "POST",
        //        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function (data) {
            if (data.form_is_valid) {
                $("#teacher_list_table tbody").html(data.html_teacher_list);
                displayAlert( data.message, 'success', 2000);
//                setTimeout(function(){location.reload();}, 2000);
                luein_modal_close();
            }
            else {
                $(".luein-modal-content").html(data.html_form);
            }
            dynamic_loader_end();
        },
        error: function (xhr, textStatus, error) {
            dynamic_loader_end();
            displayAlert("Something went wrong",'error',5000)
        }

    });
    return false;
};


/* Un-Binding */

// Add teacher
$(".js_create_teacher").unbind('click');
$("#teacher_list_table").unbind('click');

/* Binding */
// Add teacher
$(".js_create_teacher").click(loadFormTeacher);
// Update Staff
$("#teacher_list_table").on("click", ".js_update_teacher", loadFormTeacher);






