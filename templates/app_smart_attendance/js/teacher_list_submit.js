
$(".luein-modal-container").unbind('submit');


$(".luein-modal-container").on("submit", ".js_teacher_create_form", saveFormTeacher);

$(".luein-modal-container").on("submit", ".js_teacher_update_form", saveFormTeacher);

$(".luein-modal-container").on("submit", ".js_teacher_delete_form", saveFormTeacher);


$(".luein-modal-button.submit").unbind("click.submit", check_form_is_valid);
$(".luein-modal-button.submit").bind("click.submit", check_form_is_valid);


function check_form_is_valid(){
    if(!$(`[name='role']:checked`).val()){
        displayAlert("User group is required...!!!", "error", 2000)
    }
}

