"use strict"

$(document).ready(function(){

});



function get_profiles_by_role_event(){
    $("#id_person").unbind('change.id_profile_face',id_profile_face_change_fn)
    $("#id_person").bind('change.id_profile_face',id_profile_face_change_fn)
}

function get_profiles_by_role_take_attendance_class_ajax(){
    var data = get_profiles_by_role_ajax_take_attendance_class_data();
    $.ajax({
        type: "POST",
        url: "{% url 'app_smart_attendance:get_profiles_by_role_ajax' %}",
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
            reset_old_person_data();
        },
        success: function(data) {
            get_profiles_by_role_take_attendance_class_ajax_success(data);
            dynamic_loader_end();
        },
        error:function(data) {
            dynamic_loader_end();
            alert('error');
        }
    })
}

var old_users = [];
function get_profiles_by_role_take_attendance_class_ajax_success(data){
    console.warn(data)
    old_users = [];
    if(data["data"]){
        old_users = data["data"];
        $("#id_person").html("")
        var options = `<option value="">---------</option>`;
        for(var i in data["data"]){
            var obj = data["data"][i]
            if(obj['role'] == "student"){
                options += `<option value="${i}">
                            (Class:${obj["class_name"]}${obj["section"]}) -
                            ${obj["first_name"]} ${obj["last_name"]}
                        </option>`;
            }
            else if(obj['role'] == "teacher"){
                options += `<option value="${i}">
                                ${obj["first_name"]} ${obj["last_name"]}
                            </option>`;
            }
            else if(obj['role'] == "non_teacher"){
                options += `<option value="${i}">
                                ${obj["first_name"]} ${obj["last_name"]}
                            </option>`;
            }
            else if(obj['role'] == "school_admin"){
                options += `<option value="${i}">
                                ${obj["first_name"]} ${obj["last_name"]}
                            </option>`;
            }
        }
        $("#id_person").append(options)
        get_profiles_by_role_event();
        show_person_select();
    }else{
        hide_person_select();
    }
}

function id_profile_face_change_fn(){
    var index = $(this).val();
    if(index){
        var data = old_users[index];
        render_old_person_data(data);
        show_face_form_fn();
    }else{
        reset_old_person_data();
        hide_face_form_fn();
        reset_face_form();
    }
}

var fafa_icon_size = 50;
var profile_picture_default_fafa_icon = `<svg width="${fafa_icon_size}" height="${fafa_icon_size}" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  viewBox="0,0,256,256"><g fill="#bababa" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(2,2)"><path d="M64,1c-34.74,0 -63,28.26 -63,63c0,12.01 3.39055,23.68953 9.81055,33.76953c0.89,1.4 2.73867,1.80992 4.13867,0.91992c1.4,-0.89 1.80992,-2.73867 0.91992,-4.13867c-5.8,-9.12 -8.86914,-19.69078 -8.86914,-30.55078c0,-31.43 25.57,-57 57,-57c31.43,0 57,25.57 57,57c0,10.96 -3.11953,21.60906 -9.01953,30.78906c-0.9,1.39 -0.48961,3.25039 0.90039,4.15039c0.5,0.32 1.05914,0.48047 1.61914,0.48047c0.99,0 1.9493,-0.49086 2.5293,-1.38086c6.52,-10.15 9.9707,-21.91906 9.9707,-34.03906c0,-34.74 -28.26,-63 -63,-63zM64,31c-12.68,0 -23,10.32 -23,23c0,12.68 10.32,23 23,23c12.68,0 23,-10.32 23,-23c0,-12.68 -10.32,-23 -23,-23zM64,37c9.37,0 17,7.63 17,17c0,9.37 -7.63,17 -17,17c-9.37,0 -17,-7.63 -17,-17c0,-9.37 7.63,-17 17,-17zM64,88.59766c-15.255,0 -30.50914,5.80688 -42.11914,17.42187c-0.04,0.04 -0.07156,0.07133 -0.10156,0.11133l-0.28906,0.31836c-1.11,1.23 -1.02102,3.12023 0.20898,4.24023c11.6,10.52 26.62078,16.31055 42.30078,16.31055c15.68,0 30.70078,-5.79031 42.30078,-16.32031c1.23,-1.11 1.31899,-3.01024 0.20899,-4.24024l-0.15039,-0.15039c-0.07,-0.09 -0.16024,-0.18953 -0.24024,-0.26953c-11.61,-11.615 -26.86414,-17.42187 -42.11914,-17.42187zM64,94.59766c12.8425,0 25.68484,4.57742 35.83984,13.73242c-10.12,8.19 -22.72984,12.66992 -35.83984,12.66992c-13.11,0 -25.71984,-4.47992 -35.83984,-12.66992c10.155,-9.155 22.99734,-13.73242 35.83984,-13.73242z"></path></g></g></svg>`;
function render_old_person_data(data){
    var old_details = data;
    var class_name = old_details["class_name"];
    profile_id_class =  old_details["profile_id"];
    var first_name = data["first_name"];
    var last_name = data["last_name"];
    var email = data["email"];
    var gender = data["gender"];
    var profile_picture = data["profile_picture"];
    var gender_display = '';
    if(gender){
        if(gender == "male"){
            gender_display = "(M)";
        }else if(gender == "female"){
            gender_display = "(F)";
        }else if(gender == "other"){
            gender_display = "(O)";
        }else{
            gender_display = "";
        }
    }

    var profile_picture_html = "";
    if(profile_picture){
        profile_picture_html = `<div class="flx mr10">
                                    <div class="at_img_div">
                                        <img class="at_img" src="/media/${profile_picture}">
                                    </div>
                                </div>`;
    }else{
        profile_picture_html = `<div class="flx mr10">
                                    ${profile_picture_default_fafa_icon}
                               </div>`;
    }

    var display_data = "";
    if(data){
        display_data = `
                        <div class="flx">
                            ${profile_picture_html}
                            <div class="flx flxc a-s-f-e">
                                <div class="flx t-t-c">${first_name} ${last_name} ${gender_display}</div>
                                <div class="flx c6">${email}</div>
                            </div>
                        </div>
                        `;
    }

    $(".display_person_data").html(display_data);
}

function reset_old_person_data(){
    $(".display_person_data").html("");
}

// reset section
function reset_face_section(class_name){
    $("#id_section").html(`<option value="">---Select---</option>`);
    for(var i in class_name_section_dict[class_name]){
        var value = class_name_section_dict[class_name][i];
        var text = section_choice[class_name_section_dict[class_name][i]];
        $("#id_section").append(`<option value="${value}">${text}</option>`)
    }
}



var face_form_ids = [
    "id_profile",
    "id_username",
    "id_email",
    "id_first_name",
    "id_last_name",
    "id_gender",
    "id_blood_group",
    "id_class_name",
    "id_section",
]
var face_form_select_ids = [
    "id_gender",
    "id_blood_group",
    "id_class_name",
    "id_section",
]

function reset_face_form(){
    for(var i in face_form_ids){
        var id = face_form_ids[i];
        $(`#${id}`).val(``);
    }
    remove_select_options();
}
function remove_select_options(){
    for(var i in face_form_select_ids){
        var id = face_form_select_ids[i];
        $(`#${id}`).html(``);
    }
}

function show_face_form_fn(){
    $(".all_detail").removeClass("dn");
}
function hide_face_form_fn(){
    $(".all_detail").addClass("dn");
//    reset_face_form();
}




function show_person_select(){
    $(".person").removeClass("dn");
}
function hide_person_select(){
    $(".person").addClass("dn");
}


function get_profiles_by_role_ajax_take_attendance_class_data(){
    var role =  $("#id_role").find('input[name=role]:checked').val();
    var profiles_by_role_ajax_data = {'csrfmiddlewaretoken': '{{ csrf_token }}'};
    profiles_by_role_ajax_data['role'] = role;

    return profiles_by_role_ajax_data;
}