"use strict"

$(document).ready(function() {
     model_training_test_events();
})
var AJAX_URL = "{% url 'face_recog:train' %}";
// var AJAX_URL = "http://216.48.177.111:8084/api/train/"

function model_training_test_events(){
    $("#btn-train-model").unbind("click.train-model", start_model_training)
    $("#btn-train-model").bind("click.train-model", start_model_training)
    $("#btn-retrain-model").unbind("click.retrain-model", start_model_retraining)
    $("#btn-retrain-model").bind("click.retrain-model", start_model_retraining)
}

// INCREMENTAL TRAINING
function start_model_training() {
    var choice = "incremental";
    this.disabled = true;
    document.getElementById('btn-train-model').disabled = false;
    $("#loading-overlay").show();
    trainer_ajax(choice);
};

// TRAINING FROM SCRATCH
function start_model_retraining() {
    var choice = "from_scratch";
    this.disabled = true;
    document.getElementById('btn-retrain-model').disabled = false;
    $("#loading-overlay").show();
    trainer_ajax(choice);
};

// AJAX METHODS
function trainer_ajax(choice){
    // config in settings.js
    var form_data =get_trainer_ajax_data(choice)

    var ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST", AJAX_URL);
    ajax.send(form_data);
};

function get_trainer_ajax_data(choice){
    var file_key = '';
    file_key = 'training_choices';
    var form_data = new FormData();
    form_data.append('csrfmiddlewaretoken', '{{ csrf_token }}');
    form_data.append(file_key, choice);
    return form_data
}

function ajax_success_fn(event){
    console.warn(event)
    var response = JSON.parse(event.currentTarget.response)
    displayAlert(response.message,"success",2000)
}

function ajax_error_fn(event){

}













