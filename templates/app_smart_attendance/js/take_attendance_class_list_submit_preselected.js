"use strict"

//$(".luein-modal-container").unbind('submit');
//$(".luein-modal-container").on("submit", ".js_register_face_create_form", saveFormRegisterFace);
//$(".luein-modal-container").on("submit", ".js_register_face_update_form", saveFormRegisterFace);
//$(".luein-modal-container").on("submit", ".js_register_face_delete_form", saveFormRegisterFace);


$(".luein-modal-button.submit").unbind("click.submit", check_form_is_valid);
$(".luein-modal-button.submit").bind("click.submit", check_form_is_valid);

function cancel_device_selection(){
    reset_action_type_class()
}
function reset_action_type_class(){
    action_type_class_preselected = "";
}
function check_form_is_valid(){
    var is_form_valid = false;
    var attendance_choice = $(`[name='attendance_choice']`).val();
    var class_name = $(`[name='class_name']`).val();
    var section = $(`[name='section']`).val();
    var period_id = $(`[name='period_id']`).val()
    var attendance_date = $(`[name='attendance_date']`).val()

    if(!class_name){
        is_form_valid = false;
        displayAlert("Class name is required...!!!", "error", 2000)
    }
    else if(!section){
        is_form_valid = false;
        displayAlert("Section is required...!!!", "error", 2000)
    }
    else if(!attendance_date){
        is_form_valid = false;
        displayAlert("Attendance date is required...!!!", "error", 2000)
    }
    else if(!period_id){
        is_form_valid = false;
        displayAlert("Period is required...!!!", "error", 2000)
    }
    else if(!$(`[name='attendance_choice']`).val()){
        is_form_valid = false;
        displayAlert("Attendance Choice is required...!!!", "error", 2000)
    }
    else{
        is_form_valid = true;
    }

    if(is_form_valid){
//        if(attendance_choice == 'face_recognition'){
//            open_register_face();
//        }
//        else if(attendance_choice == 'manual'){
//            open_manual_entry_form_init();
//            luein_modal_close();
//        }
        open_manual_entry_form_init(attendance_choice);
    }
}



//$(".luein-modal-container").on("submit", ".js_register_face_create_form", open_register_face);

//var profile_id_class_preselected = "";
var main_formData_class_preselected;
function open_register_face(){

    main_formData_class_preselected = $('#take_attendance_class_crud_form').serializeArray()
    main_formData_class_preselected.push({name: 'profile_id', value:  profile_id_class_preselected})

    luein_modal_close();

//    hide_student_register_frm_container();
    show_student_register_video_container();

    // open device selection
    reset_recording_btn();
    ct_init_devices_access_permissions_fn();
}

// clear class time interval - 2 on close
$(".luein-modal-close").click(function(){
    clearInterval(class_form_interval_preselected)
});

// clear class time interval - 3 on cancel
$(".luein-modal-button.cancel").click(function(){
    clearInterval(class_form_interval_preselected)
});


function get_take_attendance_class_form_data(){
    var formData = main_formData_class_preselected;
    formData.push({
        "name":"media_type",
        "value":"attendance"
    })
    var return_data = {}
    for(var data of formData){
        return_data[data["name"]]=data["value"]
    }
    // replace index value to original value

    // period_id start
    var class_name = return_data["class_name"];
    var section = return_data["section"];
    var period = return_data["period_id"];
    var attendance_date = return_data["attendance_date"];
    var day = get_day_from_date_class(attendance_date);
    var period_id = get_period_id_by_class_name_section_period_day(class_name, section, period, day)
    return_data["day"] = day;
    return_data["period"] = period;
    return_data["period_id"] = period_id;
    // period_id end



    // add extra filed in formData

    return return_data;
}



// connectivity.js recorder config start
var AJAX_URL = "{% url 'app_smart_attendance:take_attendance_class_view_ajax' %}";
function get_ajax_data(){
    var file_key = '';
    file_key = 'media_file';
    var form_data = new FormData();
    form_data.append('csrfmiddlewaretoken', '{{ csrf_token }}');
    form_data.append(file_key, upblob);

    var form_text_data = get_take_attendance_class_form_data()
    var form_text_data_json = JSON.stringify(form_text_data)
    form_data.append("form_text_data_json", form_text_data_json);

    return form_data
}
function ajax_success_fn(event){
    var response = JSON.parse(event.currentTarget.response)
    displayAlert(response.message,"success",2000)
    show_student_attendance_frm_container();
    $("#take_attendance_frm").trigger('reset')
}
function ajax_error_fn(event){

}
function cancel_recording_stream_callback(){
    reset_action_type_class();
    show_student_attendance_frm_container();
    hide_student_register_video_container();
}
// connectivity.js recorder config end


