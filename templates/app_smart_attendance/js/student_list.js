var class_name_section_dict = {}
{% if class_name_section_dict %}
    class_name_section_dict = {{class_name_section_dict|safe}};
{% endif %}

var class_choice = {}
{% if class_choice %}
    class_choice = {{class_choice|safe}};
{% endif %}

var section_choice = {}
{% if section_choice %}
    section_choice = {{section_choice|safe}};
{% endif %}