function getWeek(d) {
  // https://stackoverflow.com/a/6117889/383904
  d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
  d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
  var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
  return {year:d.getUTCFullYear(), number: weekNo};
}

var currentWeekNumber = getWeek(new Date()).number;

var date_range_type = '{{date_range_type|safe}}';
if(date_range_type){
    $(".date_range_type").val(date_range_type)
}



var class_name_section = '{{class_name_section|safe}}';
if(class_name_section=='None'){
    class_name_section="";
}
if(class_name_section){
    $(".class_name_section").val(class_name_section)
}

var class_name_section_dict = {{class_name_section_dict|safe}};




$(".date_range_type").unbind('change.date_range_type',date_range_type_fn)
$(".date_range_type").bind('change.date_range_type',date_range_type_fn)

$(".class_name_section").unbind('change.class_name_section',class_name_section_fn)
$(".class_name_section").bind('change.class_name_section',class_name_section_fn)


function date_range_type_fn(){
    if (this.value){
        window.location.href=$(this).find(':selected').data('url');
    }
}

function class_name_section_fn(){
    if (this.value){
        dynamic_loader_start()
        window.location.href=$(this).find(':selected').data('url');
    }
}