"use strict"


$(document).ready(function() {
    render_fafa_icon();
})

var check_in = `<svg class="r180" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16px" height="16px" viewBox="0,0,256,256"> <g fill="#bababa" fill-rule="evenodd" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"> <g transform="scale(16,16)"> <path d="M8,1.25c-0.414,0 -0.75,0.336 -0.75,0.75c0,0.414 0.336,0.75 0.75,0.75h5c0.066,0 0.13073,0.02527 0.17773,0.07227c0.047,0.047 0.07227,0.11173 0.07227,0.17773v10c0,0.066 -0.02527,0.13073 -0.07227,0.17773c-0.047,0.047 -0.11173,0.07227 -0.17773,0.07227h-5c-0.414,0 -0.75,0.336 -0.75,0.75c0,0.414 0.336,0.75 0.75,0.75h5c0.464,0 0.90833,-0.18467 1.23633,-0.51367c0.329,-0.328 0.51367,-0.77233 0.51367,-1.23633v-10c0,-0.464 -0.18467,-0.90833 -0.51367,-1.23633c-0.328,-0.329 -0.77233,-0.51367 -1.23633,-0.51367zM8.5,4.75c-0.192,0 -0.3833,0.0742 -0.5293,0.2207c-0.293,0.292 -0.293,0.76659 0,1.05859l1.21875,1.2207h-7.18945c-0.414,0 -0.75,0.336 -0.75,0.75c0,0.414 0.336,0.75 0.75,0.75h7.18945l-1.21875,1.2207c-0.293,0.292 -0.293,0.76659 0,1.05859c0.292,0.293 0.76659,0.293 1.05859,0l2.5,-2.5c0.293,-0.293 0.293,-0.76559 0,-1.05859l-2.5,-2.5c-0.146,-0.1465 -0.3373,-0.2207 -0.5293,-0.2207z"></path> </g> </g> </svg>`;
var check_out = `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16px" height="16px" viewBox="0,0,256,256"> <g fill="#bababa" fill-rule="evenodd" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"> <g transform="scale(16,16)"> <path d="M3,1.25c-0.464,0 -0.90833,0.18467 -1.23633,0.51367c-0.329,0.328 -0.51367,0.77233 -0.51367,1.23633v10c0,0.464 0.18467,0.90833 0.51367,1.23633c0.328,0.329 0.77233,0.51367 1.23633,0.51367h5c0.414,0 0.75,-0.336 0.75,-0.75c0,-0.414 -0.336,-0.75 -0.75,-0.75h-5c-0.066,0 -0.13073,-0.02527 -0.17773,-0.07227c-0.047,-0.047 -0.07227,-0.11173 -0.07227,-0.17773v-10c0,-0.066 0.02527,-0.13073 0.07227,-0.17773c0.047,-0.047 0.11173,-0.07227 0.17773,-0.07227h5c0.414,0 0.75,-0.336 0.75,-0.75c0,-0.414 -0.336,-0.75 -0.75,-0.75zM11.5,4.75c-0.192,0 -0.3833,0.0742 -0.5293,0.2207c-0.293,0.292 -0.293,0.76659 0,1.05859l1.21875,1.2207h-7.18945c-0.414,0 -0.75,0.336 -0.75,0.75c0,0.414 0.336,0.75 0.75,0.75h7.18945l-1.21875,1.2207c-0.293,0.292 -0.293,0.76659 0,1.05859c0.292,0.293 0.76659,0.293 1.05859,0l2.5,-2.5c0.293,-0.293 0.293,-0.76559 0,-1.05859l-2.5,-2.5c-0.146,-0.1465 -0.3373,-0.2207 -0.5293,-0.2207z"></path> </g> </g> </svg>`;
function render_fafa_icon(){
    $(".check_in_span").html(check_in)
    $(".check_out_span").html(check_out)
}


function getWeek(d) {
  // https://stackoverflow.com/a/6117889/383904
  d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
  d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
  var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
  return {year:d.getUTCFullYear(), number: weekNo};
}

var currentWeekNumber = getWeek(new Date()).number;

var date_range_type = '{{date_range_type|safe}}';
if(date_range_type){
    $(".date_range_type").val(date_range_type)
}

$(".date_range_type").unbind('change.date_range_type',date_range_type_fn)
$(".date_range_type").bind('change.date_range_type',date_range_type_fn)

function date_range_type_fn(){
    if (this.value){
        window.location.href=$(this).find(':selected').data('url');
    }
}