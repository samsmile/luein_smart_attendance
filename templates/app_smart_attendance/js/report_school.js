
"use strict"

$(document).ready(function() {
    report_events();
});

function report_events(){
    $(".view_announcement").unbind("click.view_a", view_announcement_fn)
    $(".view_announcement").bind("click.view_a", view_announcement_fn)
}

//FusionCharts.ready(function(){
//
//});
var chartObj;

var d_chart_data = {
    "chart": {
        "caption": "Attendance",
        "subCaption": "19-10-2022",
        "xAxisname": "Class",
        "yAxisName": "Students (In number)",
        "showSum": "1",
        "numberPrefix": "",
        "theme": "fusion"
    },
    "categories": [{
        "category": [{
            "label": "V"
        }, {
            "label": "VI"
        }, {
            "label": "X"
        }, {
            "label": "XI"
        }]
    }],
    "dataset": [
    {
        "seriesname": "Present",
        "data": [{
            "value": "20"
        }, {
            "value": "21"
        }, {
            "value": "25"
        }, {
            "value": "30"
        }]
    }, {
        "seriesname": "Absent",
        "data": [{
            "value": "10"
        }, {
            "value": "9"
        }, {
            "value": "5"
        }, {
            "value": "0"
        }]
    }]
}


//init_render_chart(chart_data)
function init_render_chart(chart_data){
    $.ajax({
       url:render_chart(chart_data),
       success:function(){
            const myTimeout = setTimeout(remove_water_mark, 2000);
        }
    })
}



function render_chart(chart_data){
    chartObj = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'chart-container',
        width: '100%',
        height: '100%',
        dataFormat: 'json',
        dataSource: chart_data
    });
    chartObj.render();
}
function remove_water_mark(){
    $(".water_mark").addClass("dn")
    $(".water_mark").parent().addClass("dn")
}



var eventDates = [],
$picker = $('#custom-cells'),
$content = $('#custom-cells-events'),
sentences = [ "asdf" ];

// Make Sunday and Saturday disabled
var disabledDays = [0, 6];

$picker.datepicker({
    language: 'en',
    onRenderCell: function (date, cellType) {
        var currentDate = date.getDate();
        // Add extra element, if `eventDates` contains `currentDate`
        if (cellType == 'day' && eventDates.indexOf(currentDate) != -1) {
            return {
                html: currentDate + '<span class="dp-note"></span>'
            }
        }
        if (cellType == 'day') {
            var day = date.getDay(),
                isDisabled = disabledDays.indexOf(day) != -1;

            return {
                disabled: isDisabled
            }
        }
    },
    onSelect: function onSelect(fd, date) {
        var title = '', content = ''
        // If date with event is selected, show it
        if (date && eventDates.indexOf(date.getDate()) != -1) {
            title = fd;
            content = sentences[0];
        }
        $content.html(`<strong>${title}</strong>`)
        $content.append(`<div>${content}</div>`)
        get_report_ajax(fd)
    }
})

// Select initial date from `eventDates`
var currentDate = currentDate = new Date();
$picker.data('datepicker').selectDate(new Date())




function get_report_ajax(date_text){

    var data = get_report_ajax_data(date_text);
    $.ajax({
        type: "POST",
        url: "{% url 'app_smart_attendance:get_report_ajax' %}",
        data: data,
        dataType: 'json',
        beforeSend: function () {
            dynamic_loader_start();
        },
        success: function(data) {
            get_report_ajax_success(data);
            dynamic_loader_end();
        },
        error:function(data) {
            dynamic_loader_end();
            alert('error');
        }
    })
}


function get_report_ajax_data(date_text){
    var form_text_data = {};
    form_text_data["csrfmiddlewaretoken"] = '{{ csrf_token }}';
    form_text_data["date_text"] = date_text;

    return form_text_data;
}


function get_report_ajax_success(data){
    console.warn(data)
    var chart_data = data["data"]["chart_data"];
    var total_attendance_count = data["data"]["total_attendance_count"]
    init_render_chart(chart_data)
//    $(".count_card_no.attendance").html(`${total_attendance_count}%`)
}


function view_announcement_fn(){
    var body = $(".announcement_container").html()
    var confirmation_obj = {
            "icon":"",
            "head":"Announcement",
            "body":body,
            "width":"90%",
            "height":"100%",
            "action_type":"overlay",
            "action_close":null,
            "position":"2",
            "cancel_text":"",
            "submit_text":"",
            "effect":"fade",
            "close_click_outside":"true",
            "show_close_btn":"true",
            "auto_close":"true",
        }

    dynamic_confirmation(confirmation_obj)
}

function view_announcement_open(){

}
function view_announcement_close(){

}