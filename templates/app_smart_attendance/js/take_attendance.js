
"use strict"

$(document).ready(function() {
    take_attendance_events();
    reset_class();
});



var class_name_section_dict = {}
{% if request.organisation_type == "school" %}
    {% if class_name_section_dict %}
        class_name_section_dict = {{class_name_section_dict|safe}};
    {% else %}
        {% if request.user.profile.role in 'school_admin,admin' %}
            $(".no_data").html(`<div class="red pl10 pr10 pl5  m10">
                                        <div class="flx p5">
                                            * No class record available
                                        </div>
                                    </div>
                                    <div class="red pl10 pr10  m10">
                                        <div class="flx p5">
                                            <div class="flx mr5">* For initial setup, go to</div>
                                            <div class="flx mr5"><a href="{% url 'app_smart_attendance:settings' %}">settings</a></div>
                                            <div class="flx">:</div>
                                        </div>
                                        <div class="pl20">
                                            <div class="flx p5">1. Add teachers</div>
                                            <div class="flx p5">2. Create periods</div>
                                        </div>
                                        <div class="flx p5">
                                            <div class="flx mr5">* Next steps :</div>
                                         </div>
                                       <div class="pl20">
                                            <div class="flx p5">3. Student register</div>
                                            <div class="flx p5">4. Ready to take attendance</div>
                                        </div>
                                    </div>
                            `);
        {% elif request.user.profile.role == 'teacher' %}
            $(".no_data").html(`<div class="red pl10 pr10  m10">
                                    * No class record available
                                </div>
                                <div class="red pl10 pr10  m10">
                                    * Please contact your school admin
                                </div>
                            `);
        {% endif %}
        $(".all_content_container").remove()
    {% endif %}
{% elif request.organisation_type == "company" %}
// write here
{% elif request.organisation_type == "anganwadi" %}
// write here
{% endif %}


var class_name_section_period_dict = {}
{% if class_name_section_period_dict %}
    class_name_section_period_dict = {{class_name_section_period_dict|safe}};
{% endif %}


var period_id_map_dict = {}
{% if period_id_map_dict %}
    period_id_map_dict = {{period_id_map_dict|safe}};
{% endif %}



var class_choice = {}
{% if class_choice %}
    class_choice = {{class_choice|safe}};
{% endif %}

var section_choice = {}
{% if section_choice %}
    section_choice = {{section_choice|safe}};
{% endif %}

var period_choice = {}
{% if period_choice %}
    period_choice = {{period_choice|safe}};
{% endif %}


var blood_group_choice = {}
{% if blood_group_choice %}
    blood_group_choice = {{blood_group_choice|safe}};
{% endif %}

var gender_choice = {}
{% if gender_choice %}
    gender_choice = {{gender_choice|safe}};
{% endif %}

var True = true;
var False = false;

// todo - change this to dynamic
const is_check_in = {{is_check_in|safe}};
const is_check_out = {{is_check_out|safe}};

function take_attendance_events(){
    $("#id_date").unbind("change.id_date", id_date_fn)
    $("#id_date").bind("change.id_date", id_date_fn)

}

function id_date_fn(){
    var date_val = $(this).val();
    var date_inst = new Date(date_val);
    var day = date_inst.getDay();

    var section = $('#id_section').val();
    var class_name = $('#id_class').val();
    var class_name_section_val = class_name+section+"_"+day;
    reset_period(class_name_section_val)

}

$('#id_class').on('change', function() {
  var class_name = this.value
  reset_section(class_name)
});

$('#id_section').on('change', function() {
  var section = this.value;
  var class_name = $('#id_class').val();
  var class_name_section_val = class_name+section;
  reset_period(class_name_section_val)
});


function reset_class(){
    $("#id_class").html(`<option value="">---Select---</option>`);
    for(var i in class_name_section_dict){
        var value = i;
        var text = class_choice[i];
        $("#id_class").append(`<option value="${value}">${text}</option>`)
    }
}

function reset_section(class_name){
    $("#id_section").html(`<option value="">---Select---</option>`);
    for(var i in class_name_section_dict[class_name]){
        var value = class_name_section_dict[class_name][i];
        var text = section_choice[class_name_section_dict[class_name][i]];
        $("#id_section").append(`<option value="${value}">${text}</option>`)
    }
}

function reset_period(class_name_section_val){
    $("#id_period").html(`<option value="">---Select---</option>`);
    for(var i in class_name_section_period_dict[class_name_section_val]){
        var value = class_name_section_period_dict[class_name_section_val][i];
        var text = period_choice[class_name_section_period_dict[class_name_section_val][i]];
        $("#id_period").append(`<option value="${value}">${text}</option>`)
    }
}




$(".take_attendance").click(function(){
    // open device selection
//    show_connectivity_test();
    show_student_register_video_container();
    ct_init_devices_access_permissions_fn();
})

$("#take_attendance_frm").submit(function(){
    hide_student_attendance_frm_container();
    show_student_register_video_container();

//    // open device selection
//    show_connectivity_test()
    reset_recording_btn();
    ct_init_devices_access_permissions_fn();
})


function show_student_attendance_frm_container(){
    $(".student_attendance_frm_container").removeClass("dn");
}
function hide_student_attendance_frm_container(){
    $(".student_attendance_frm_container").addClass("dn");
}


// connectivity.js recorder config start
var AJAX_URL = "{% url 'app_smart_attendance:take_attendance_ajax' %}";
function get_ajax_data(){
    var file_key = '';
    file_key = 'media_file';
    var form_data = new FormData();
    form_data.append('csrfmiddlewaretoken', '{{ csrf_token }}');
    form_data.append(file_key, upblob);

    var form_text_data = get_take_attendance_form_data()
    var form_text_data_json = JSON.stringify(form_text_data)
    form_data.append("form_text_data_json", form_text_data_json);


    return form_data
}
function ajax_success_fn(event){
    var response = JSON.parse(event.currentTarget.response)
    displayAlert(response.message,"success",2000)
    show_student_attendance_frm_container();
    $("#take_attendance_frm").trigger('reset')
}
function ajax_error_fn(event){

}
function cancel_recording_stream_callback(){
    show_student_attendance_frm_container();
    hide_student_register_video_container();
}
// connectivity.js recorder config end

//function get_take_attendance_form_data(){
//    var data ={
//        "media_type":"attendance"
//    }
//    return data
//}

function get_day_from_date(){
    var date_val = $("#id_date").val();
    var date_inst = new Date(date_val);
    var day = date_inst.getDay();
    return day;
}

function get_take_attendance_form_data(){
    var formData = $('#take_attendance_frm').serializeArray()
    formData.push({
        "name":"media_type",
        "value":"attendance"
    })
    var return_data = {}
    for(var data of formData){
        return_data[data["name"]]=data["value"]
    }

    // add extra filed in formData
    var class_name = return_data["class_name"];
    var section = return_data["section"];
    var period = return_data["period"];
    var day = get_day_from_date();

//    var all_csp = class_name + section + period +"_"+day;
//    return_data["period_id"] = period_id_map_dict[all_csp];

    return return_data;
}



//    show_connectivity_test();




// /////////////
function formatAMPM() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = `<div class="flx">
                    <div class="time_inner_div">${hours}:${minutes}</div>
                    <div class="time_ampm_div">${ampm}</div>
                </div>`;
    time_div.innerHTML = strTime;
}

// Attaching a new function  toShortFormat()  to any instance of Date() class
Date.prototype.toShortFormat = function() {

    const monthNames = ["Jan", "Feb", "Mar", "Apr",
                        "May", "Jun", "Jul", "Aug",
                        "Sep", "Oct", "Nov", "Dec"];

    const day = this.getDate();

    const monthIndex = this.getMonth();
    const monthName = monthNames[monthIndex];

    const year = this.getFullYear();

    return `${day}-${monthName}-${year}`;
}

function show_date(){
    let today = new Date();
    date_div.textContent = today.toShortFormat();
}

////////////


