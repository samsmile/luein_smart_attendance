import datetime
import glob
import itertools
import json
import os

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import BadHeaderError, send_mail
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
# from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.generic.base import TemplateView

# Create your views here.

from app_root.forms import UserLoginForm, SchoolCreateForm, SchoolUpdateForm, GeolocationCreateForm
from app_root.models import SchoolModel, AttendanceModel, StaffAttendanceModel, Profile
from app_smart_attendance.views import get_old_check_in_out_by_profile
from luein_smart_attendance.code_base.utility import is_valid_user


# Home Page View
class HomePageView(TemplateView):
    template_name = "app_root/html/home.html"

    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated:
            pass

    def get(self, request):
        webpush_settings = getattr(settings, 'WEBPUSH_SETTINGS', {})
        vapid_key = webpush_settings.get('VAPID_PUBLIC_KEY')

        payload = {
            "vapid_key": vapid_key
        }

        if self.request.user.is_authenticated:
            return redirect('app_root:dashboard_view')
        else:
            return render(request, self.template_name, payload)


@login_required
def dashboard_view(request):
    if request.user.is_superuser:
        context_payload = {"side_nav_bar": True}
        template_name = "access_denied/access-denied-page.html"
        return render(request, template_name, context_payload)

    template_name = "app_smart_attendance/html/dashboard_school_admin.html"
    check_in_out_data = {}
    last_entry_dict = {}
    last_entry_attendance_date = None
    profile_qs = request.user.profile
    school_id = profile_qs.school_id
    check_in_out_data = get_old_check_in_out_by_profile(request, profile_qs)
    # print('check_in_out_data---', check_in_out_data)

    last_entry = check_in_out_data.pop('last_entry', None)

    if last_entry:
        last_entry_dict = {
            'staff_attendance_id': str(last_entry.staff_attendance_id),
            'attendance_id': str(last_entry.attendance_id),
            'attendance_type': last_entry.attendance_type,
            'attempt_status': last_entry.attempt_status,
            'loggedin_user': str(last_entry.loggedin_user),
            'taken_by': str(last_entry.taken_by),
            'taken_for': str(last_entry.taken_for),
            'media_id': str(last_entry.media_id.media_id),
            'attendance_status': last_entry.attendance_status,
            'attendance_date_str': str(last_entry.attendance_date),
            'staff_attendance_geolocation': last_entry.staff_attendance_geolocation,
            'date_time': str(last_entry.date_time),
            'updated_at': str(last_entry.updated_at),
        }
        last_entry_attendance_date = last_entry.attendance_date

    role_list = []
    if request.organisation_type == "company" or request.organisation_type == "anganwadi":
        role_list = ['admin', 'staff']
    elif request.organisation_type == "school":
        role_list = ['teacher', 'non_teacher', 'school_admin']

    today_date = datetime.datetime.now().date()

    all_profile_qs = Profile.objects.filter(school_id=school_id)
    all_staff_attendance_qs = StaffAttendanceModel.objects.filter(
        attempt_status='success',
        attendance_date__date=today_date,
        taken_for__school_id=school_id,
        taken_for__role__in=role_list
    )
    total_staff = all_profile_qs.count()

    total_in = all_staff_attendance_qs.filter(attendance_type="in").count()
    total_out = all_staff_attendance_qs.filter(attendance_type="out").count()

    inside = all_profile_qs.filter(attendance_status="in").count()
    outside = all_profile_qs.filter(attendance_status="out").count()

    total_pending = total_staff - total_in

    context_payload = {
        'old_check_in_out': check_in_out_data,
        'last_entry': last_entry_dict,
        'last_entry_attendance_date': last_entry_attendance_date,
        'total_in': total_in,
        'total_out': total_out,
        'total_pending': total_pending,
        'total_staff': total_staff,

        'inside': inside,
        'outside': outside,
    }

    return render(request, template_name, context_payload)


@login_required
def school_create_view(request):
    template_name = 'app_root/html/partial_school_create.html'
    data = dict()

    if request.method == 'POST':
        form = SchoolCreateForm(request.POST, request.FILES, user=request.user)
        if form.is_valid():
            form_instance = form.save(commit=False)
            form_instance.save()
            html_school_list = create_html_school_list(request)
            data['html_school_list'] = html_school_list
            data['form_is_valid'] = True
            data['message'] = 'School created successfully.'
        else:
            data['form_is_valid'] = False
    else:
        form = SchoolCreateForm(user=request.user)

    context = {
        'form': form,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


def create_html_school_list(request):
    school_filter_qs = SchoolModel.objects.filter()
    html_school_list = render_to_string(template_name='app_root/html/partial_school_list.html',
                                        context={
                                            'school_filter_qs': school_filter_qs,
                                        }, request=request)
    return html_school_list


@login_required
def school_update_view(request, **kwargs):
    school_id = kwargs.get('school_id')
    school_get_qs = get_object_or_404(SchoolModel, pk=school_id)
    template_name = 'app_root/html/partial_school_update.html'

    data = dict()

    if request.method == 'POST':
        form = SchoolUpdateForm(request.POST, request.FILES, instance=school_get_qs, user=request.user)
        if form.is_valid():
            form_instance = form.save(commit=False)
            form_instance.save()
            html_school_list = create_html_school_list(request)
            data['html_school_list'] = html_school_list
            data['form_is_valid'] = True
            data['message'] = 'School Updated successfully.'

        else:
            data['form_is_valid'] = False
    else:
        form = SchoolUpdateForm(instance=school_get_qs, user=request.user)
    context = {'form': form}
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "update"
    return JsonResponse(data)


@login_required
def geolocation_create_view(request):
    template_name = 'app_smart_attendance/module/geolocation/html/partial_geolocation_create.html'
    data = dict()

    if request.method == 'POST':
        form = GeolocationCreateForm(request.POST, request.FILES, user=request.user)
        if form.is_valid():
            form_instance = form.save(commit=False)
            # add extra data

            form_instance.save()

            data['form_is_valid'] = True
            data['message'] = 'Geolocation created successfully.'
        else:
            data['form_is_valid'] = False
    else:
        form = GeolocationCreateForm(user=request.user)

    context = {
        'form': form,
    }
    data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)
    data['action_type'] = "create"

    return JsonResponse(data)


def custom_login_view(request):
    form = UserLoginForm(request.POST)
    print(form.is_valid())
    if form.is_valid():
        user_data = form.cleaned_data["username"]
        password = form.cleaned_data["password"]

        # create data to check a valid user if user_data is a username or email
        is_valid_user_data = {
            'user_data': user_data
        }
        return_is_valid_user = is_valid_user(is_valid_user_data)
        is_user_exist = return_is_valid_user.get('is_valid')
        if is_user_exist:
            user = return_is_valid_user.get('user')
            username = user.username
        else:
            username = user_data

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                if request.GET.get('next'):
                    return HttpResponseRedirect(request.GET.get('next'))

    # logic to getting all files from staticfiles folder and create appropriate path list for UI
    LOGIN_STATIC_IMAGE_FULL_PATHS = os.getcwd() + '/staticfiles/website/login/'
    LOGIN_STATIC_IMAGE_PATHS = '/static/website/login/'

    from os import listdir
    from os.path import isfile, join
    onlyfiles = [f for f in listdir(LOGIN_STATIC_IMAGE_FULL_PATHS) if isfile(join(LOGIN_STATIC_IMAGE_FULL_PATHS, f))]

    all_image_array = []
    for item in onlyfiles:
        all_image_array.append(LOGIN_STATIC_IMAGE_PATHS + item)
    print("onlyfils: ", all_image_array)

    context = {
        'form': form,
        'all_image_array': all_image_array
    }

    return render(request, "registration/login.html", context)


@login_required
def logout_view(request):
    logout(request)
    return redirect('root:home')


@login_required
def organisation_list_view(request):
    if request.user.is_superuser:
        template_name = "app_root/html/school_list.html"
        school_filter_qs = SchoolModel.objects.filter()
        context_payload = {
            'school_filter_qs': school_filter_qs,
        }
        return render(request, template_name, context_payload)
    else:
        template_name = 'access_denied/access-denied-page.html'
        context_payload = {
            'side_nav_bar': True
        }
        return render(request, template_name, context_payload)


def validate_username_view_ajax(request):
    user_data = request.GET.get('username', None)

    is_username_taken = User.objects.filter(username__iexact=user_data).exists()
    is_email_taken = User.objects.filter(email__iexact=user_data).exists()

    is_taken = False
    if is_username_taken or is_email_taken:
        is_taken = True

    data = {
        'is_taken': is_taken,
    }

    if data['is_taken']:
        data['error_message'] = 'A user with this username already exists.'
    return JsonResponse(data)


def validate_email_view_ajax(request):
    email = request.GET.get('email', None)
    data = {
        'is_taken': User.objects.filter(email__iexact=email).exists(),
    }
    if data['is_taken']:
        data['error_message'] = 'A user with this email already exists.'
    return JsonResponse(data)


# working app_smart_attendance.change_password - change_password_v1
# not in use - app_root.password_change_ajax_view
def password_change_ajax_view(request):
    new_password1 = ''
    new_password2 = ''
    existing_pwd = ''
    message = ""

    try:
        new_password1 = request.POST['new_password1']
        new_password2 = request.POST['new_password2']
        existing_pwd = request.POST['existing_pwd']
    except:
        message = 'fail'

    if new_password1 and new_password2 and existing_pwd:
        user_instance = User.objects.get(username=request.user.username)
        try:
            validate_password(new_password1, user=request.user)
            message = 'Validation successful'
        except Exception as e:
            message = str(e)
            print(e)
        if new_password1 != new_password2:
            message = 'Passwords do not match.'
        if not user_instance.check_password(existing_pwd):
            message = 'Please enter correct existing password.'
        if new_password1 == new_password2 and user_instance.check_password(
                existing_pwd) and message == 'Validation successful':
            user_instance.set_password(new_password2)
            user_instance.save()
            message = 'Password changed successfully.'
    print(message)
    return JsonResponse({'message': message})
