from captcha.fields import CaptchaField
from ckeditor.widgets import CKEditorWidget
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User

# Contact Us FORM
from app_root.models import SchoolModel, GeolocationModel, RADIUS_UNIT_CHOICE
from luein_smart_attendance.code_base.utility import is_valid_user
from luein_smart_attendance.settings import EMAIL_HOST_USER


class ContactUsForm(forms.Form):
    name = forms.CharField(required=True)
    sender_email = forms.EmailField(required=True)
    subject = forms.CharField(required=True)
    message = forms.CharField(widget=CKEditorWidget(), required=True)
    captcha = CaptchaField()
    required_css_class = 'required'

    def clean(self, *args, **kwargs):
        sender_email = self.cleaned_data.get("sender_email")

        if sender_email == EMAIL_HOST_USER:
            print('ERROR: contact us sender email is same as host email')

            raise forms.ValidationError("Invalid email")


class UserLoginForm(forms.Form):
    username = forms.CharField(required=True, label="Username Or Email")
    password = forms.CharField(required=True, widget=forms.PasswordInput)
    captcha = CaptchaField()
    required_css_class = 'required'

    def clean(self, *args, **kwargs):
        user_data = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        if user_data and password:
            # create data to check a valid user if user_data is a username or email
            is_valid_user_data = {
                'user_data': user_data
            }
            return_is_valid_user = is_valid_user(is_valid_user_data)
            is_user_exist = return_is_valid_user.get('is_valid')
            if is_user_exist:
                user = return_is_valid_user.get('user')
                username = user.username
            else:
                username = user_data

            user = authenticate(username=username, password=password)
            if User.objects.filter(username=username, is_active=False):
                raise forms.ValidationError("Account inactive, please contact your administrator for more details.")
            if not user:
                raise forms.ValidationError("Username or password is incorrect.")

        return super(UserLoginForm, self).clean(*args, **kwargs)

    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].error_messages = {'required': ''}
        self.fields['password'].error_messages = {'required': ''}
        self.fields['captcha'].error_messages = {'required': ''}


class SchoolCreateForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = SchoolModel
        fields = (
            'organisation_type',
            'school_logo',
            'branch',
            'name',
            'address',
            'city',
            'state',
            'country',
            'postal_code',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(SchoolCreateForm, self).__init__(*args, **kwargs)
        profile = user.profile

        self.fields['organisation_type'].required = True
        self.fields['name'].required = True
        self.fields['city'].required = True
        self.fields['state'].required = True
        self.fields['country'].required = True
        self.fields['postal_code'].required = True

        self.fields['school_logo'].label = "Organisation Logo"


class SchoolUpdateForm(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = SchoolModel
        fields = (
            'organisation_type',
            'school_logo',
            'branch',
            'name',
            'address',
            'city',
            'state',
            'country',
            'postal_code',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(SchoolUpdateForm, self).__init__(*args, **kwargs)
        profile = user.profile

        self.fields['organisation_type'].required = True
        self.fields['name'].required = True
        self.fields['city'].required = True
        self.fields['state'].required = True
        self.fields['country'].required = True
        self.fields['postal_code'].required = True

        self.fields['school_logo'].label = "Organisation Logo"


class GeolocationCreateForm(forms.ModelForm):
    required_css_class = 'required'
    coordinates_str = forms.CharField(required=True, label="Coordinates")
    radius = forms.CharField(required=True, label="Radius")
    radius_unit = forms.CharField(required=True, label="Unit")

    class Meta:
        model = GeolocationModel
        fields = (
            'name',
            'address',
            'extra_data',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(GeolocationCreateForm, self).__init__(*args, **kwargs)
        profile = user.profile

        self.fields['name'].required = True
        self.fields['coordinates_str'].required = True
        self.fields['address'].required = True
        self.fields['radius'].required = True
        self.fields['radius_unit'].required = True
        self.fields['extra_data'].required = True

        self.fields['name'].label = "Location Name"

        # self.fields['radius'].widget = self.fields['radius'].hidden_widget()
        # self.fields['radius_unit'].widget = self.fields['radius_unit'].hidden_widget()

        self.fields['radius_unit'] = forms.ChoiceField(choices=RADIUS_UNIT_CHOICE, label='Unit', required=True,
                                                widget=forms.RadioSelect)

