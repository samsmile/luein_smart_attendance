from django.urls import path, re_path

from app_root import views
from app_smart_attendance.views import SchoolDetailView, register_school_admin_view, ProfileDetailView

app_name = 'app_root'

urlpatterns = [

    # Base URLs
    path('', views.HomePageView.as_view(), name='home'),

    path('dashboard/', views.dashboard_view, name='dashboard_view'),
    path('profile/<slug:profile_id>', ProfileDetailView.as_view(), name='profile_view'),

    path('organisation/create/', views.school_create_view, name='school_create'),
    path('organisation/<slug:school_id>/', SchoolDetailView.as_view(), name='school_detail'),
    path('organisation/<slug:school_id>/update/', views.school_update_view, name='school_update'),

    path('organisation_list/', views.organisation_list_view, name='school_list'),

    path('geolocation/create/', views.geolocation_create_view, name='geolocation_create'),

    path('register_school_admin/<slug:school_id>', register_school_admin_view, name='register_school_admin_view'),


    path('ajax/validate_username/', views.validate_username_view_ajax, name='validate_username'),
    path('ajax/validate_email/', views.validate_email_view_ajax, name='validate_email'),
    # not in use - # working - change_password_v1
    path('ajax/pass-change/', views.password_change_ajax_view, name='pass-change'),

    # path('terms-conditions/', views.TermsConditionsView.as_view(), name='terms'),

    # Contact Us URLs
    # path('contact-us/', views.contact_us_view, name='contact-us'),
    # path('query-mail-sent/', views.contact_us_success_view, name='query-mail-sent'),

    # Account Activation URLs
    # path('account_activation_sent/', views.account_activation_sent, name='account_activation_sent'),
    # re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate,
    #         name='activate'),
    # path('account-activated/', views.AccountActivatedView.as_view(), name='account-activated'),

    # Sitemap URL
    # path('sitemap/', views.SiteMap.as_view(), name='sitemap'),
    # path('faq/', views.faq_page_view, name='faq'),

    # check if user is alive
    # path('is_alive', views.is_alive, name='is_alive'),

]
