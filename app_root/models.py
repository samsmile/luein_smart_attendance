import datetime

from django.db import models
from django.contrib.auth.models import User
import uuid

# from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError
from django.db.models import JSONField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField


def media_uuid_generate():
    return 'M_' + str(uuid.uuid4())


def validate_image(fieldfile_obj):
    filesize = fieldfile_obj.file.size
    megabyte_limit = 2.0
    if filesize > megabyte_limit * 1024 * 1024:
        raise ValidationError("Max file size is %sMB" % str(megabyte_limit))


ORGANISATION_CHOICE = (
    ('school', 'School'),
    ('company', 'Company'),
    ('anganwadi', 'Anganwadi'),
)

ORGANISATION_ROLES_MAP = {
    "school": ["school_admin", "teacher", "non_teacher", "student"],
    "company": ["admin", "staff"],
    "anganwadi": ["admin", "staff"]
}

ROLE_CHOICE = (
    ('admin', 'Admin'),
    ('school_admin', 'School admin'),
    ('teacher', 'Teacher'),
    ('non_teacher', 'Non Teacher'),
    ('student', 'Student'),
    ('staff', 'Staff'),
)

GENDER_CHOICE = (
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other'),
)

BLOOD_GROUP_CHOICE = (
    ('ab+', 'AB+'),
    ('ab-', 'AB-'),
    ('a+', 'A+'),
    ('a-', 'A-'),
    ('b+', 'B+'),
    ('b-', 'B-'),
    ('o+', 'O+'),
    ('o-', 'O-'),
)

DAY_CHOICE = (
    ('1', 'Monday'),
    ('2', 'Tuesday'),
    ('3', 'Wednesday'),
    ('4', 'Thursday'),
    ('5', 'Friday'),
    ('6', 'Saturday'),
)
CLASS_NAME_CHOICE = (
    ('pre', 'Pre'),
    ('1', 'I'),
    ('2', 'II'),
    ('3', 'III'),
    ('4', 'IV'),
    ('5', 'V'),
    ('6', 'VI'),
    ('7', 'VII'),
    ('8', 'VIII'),
    ('9', 'IX'),
    ('10', 'X'),
    ('11', 'XI'),
    ('12', 'XII'),
)
SECTION_CHOICE = (
    ('a', 'A'),
    ('b', 'B'),
    ('c', 'C'),
    ('d', 'D'),
    ('e', 'E'),
    ('f', 'F'),
)
PERIOD_CHOICE = (
    ("0", 'Zero'),
    ("1", 'First'),
    ("2", 'Second'),
    ("3", 'Third'),
    ("4", 'Forth'),
    ("5", 'Fifth'),
    ("6", 'Sixth'),
    ("7", 'Seventh'),
    ("8", 'Eighth'),
)

MEDIA_TYPE_CHOICE = (
    ('register', 'Register'),
    ('attendance', 'Attendance'),
)
ATTENDANCE_STATUS_CHOICE = (
    ('absent', 'Absent'),
    ('present', 'Present'),
    ('leave', 'Leave'),
)
ATTEMPT_STATUS_CHOICE = (
    ('success', 'Success'),
    ('failure', 'Failure'),
)

STAFF_ATTENDANCE_TYPE_CHOICE = (
    ('class_attendance', 'Class attendance'),
    ('in', 'In'),
    ('out', 'Out'),
)
GEOLOCATION_STATUS_CHOICE = (
    ('active', 'Active'),
    ('inactive', 'Inactive'),
)


class MediaCapturedModel(models.Model):
    media_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    media_file = models.FileField(upload_to='media_file', null=True, blank=True)
    media_directory = models.CharField(max_length=512, null=True, blank=True)
    media_type = models.CharField(max_length=10, choices=MEDIA_TYPE_CHOICE)
    extra_data = JSONField(null=True, blank=True, default=list)
    date_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_time',)


class GeolocationModel(models.Model):
    geolocation_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=512, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    coordinates = JSONField(null=True, blank=True, default=dict)
    geoFence = JSONField(null=True, blank=True, default=dict)
    status = models.CharField(choices=GEOLOCATION_STATUS_CHOICE, null=True, blank=True, max_length=20)
    is_active = models.BooleanField(default=False)
    extra_data = JSONField(null=True, blank=True, default=list)
    date_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_time',)


class SchoolModel(models.Model):
    school_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    organisation_type = models.CharField(max_length=128, choices=ORGANISATION_CHOICE, null=True, blank=True, )
    name = models.CharField(max_length=512, null=True, blank=True)
    branch = models.CharField(max_length=512, null=True, blank=True)
    school_logo = models.FileField(upload_to='school_logo', validators=[validate_image], null=True, blank=True,
                                   help_text='Maximum file size allowed is 2Mb')
    address = models.TextField(null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    state = models.CharField(max_length=100, null=True, blank=True)
    country = models.CharField(max_length=100, null=True, blank=True)
    postal_code = models.CharField(max_length=100, null=True, blank=True)
    school_geolocation = JSONField(null=True, blank=True, default=list)
    geolocation = models.ManyToManyField(GeolocationModel, blank=True, null=True)
    is_verified = models.BooleanField(default=False)
    is_manual_entry_allowed = models.BooleanField(default=False)
    date_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_time',)


RADIUS_UNIT_CHOICE = (
    ('m', 'Meter'),
    ('ft', 'Feet'),
)

PROFILE_ATTENDANCE_STATUS_CHOICE = (
    ('in', 'In'),
    ('out', 'Out'),
)
# Create your models here.
class Profile(models.Model):
    # common fileds
    profile_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    unique_id = models.CharField(max_length=100, null=True, blank=True)

    # admin, teacher, student
    role = models.CharField(max_length=30, choices=ROLE_CHOICE, null=True, blank=True)

    media_id = models.ForeignKey(MediaCapturedModel, on_delete=models.CASCADE, null=True, blank=True)
    school_id = models.ForeignKey(SchoolModel, on_delete=models.CASCADE, null=True, blank=True)
    # media_id = models.CharField(max_length=512, null=True, blank=True)
    # school_id = models.CharField(max_length=512, null=True, blank=True)

    profile_picture = models.FileField(upload_to='profile_picture', validators=[validate_image], null=True, blank=True,
                                       help_text='Maximum file size allowed is 2Mb')
    father_name = models.CharField(max_length=100, null=True, blank=True)
    contact_number = PhoneNumberField(null=True, blank=True)
    gender = models.CharField(max_length=20, choices=GENDER_CHOICE, null=True, blank=True)
    blood_group = models.CharField(max_length=20, choices=BLOOD_GROUP_CHOICE, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    state = models.CharField(max_length=100, null=True, blank=True)
    country = models.CharField(max_length=100, null=True, blank=True)
    postal_code = models.CharField(max_length=100, null=True, blank=True)

    # student fields only
    class_name = models.CharField(max_length=10, choices=CLASS_NAME_CHOICE, null=True, blank=True)
    section = models.CharField(max_length=100, choices=SECTION_CHOICE, null=True, blank=True)

    view_count = models.IntegerField(null=True, blank=True, default=0)
    is_registered = models.BooleanField(default=False, editable=True)

    attendance_status = models.CharField(max_length=100, choices=PROFILE_ATTENDANCE_STATUS_CHOICE, default="out",)
    is_attendance_under_process = models.BooleanField(default=False,)

    date_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_time',)

    def __str__(self):
        return self.user.username


# auto create Profile model entry on every User create
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class PeriodModel(models.Model):
    period_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    school_id = models.ForeignKey(SchoolModel, on_delete=models.CASCADE, null=True, blank=True)
    day = models.CharField(max_length=1, choices=DAY_CHOICE)
    class_name = models.CharField(max_length=10, choices=CLASS_NAME_CHOICE, null=True, blank=True)
    section = models.CharField(max_length=100, choices=SECTION_CHOICE, null=True, blank=True)
    period_number = models.CharField(max_length=2, choices=PERIOD_CHOICE, null=True, blank=True)
    subject = models.CharField(max_length=100, null=True, blank=True)
    teacher_id = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, blank=True)
    created_by = models.CharField(max_length=512, null=True, blank=True)
    date_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_time',)


ATTENDANCE_CHOICE = [
    ('face_recognition', 'By Face Recognition'),
    ('manual', 'By Manual Entry'),
]


# Only for class attendance role == student
class AttendanceModel(models.Model):
    attendance_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    period_id = models.ForeignKey(PeriodModel, on_delete=models.CASCADE, null=True, blank=True)

    attempt_status = models.CharField(choices=ATTEMPT_STATUS_CHOICE, null=True, blank=True, max_length=20)
    loggedin_user = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, blank=True)
    attendance_taker = models.ForeignKey(Profile, related_name='attendance_taker', on_delete=models.SET_NULL, null=True,
                                         blank=True)
    media_id = models.ForeignKey(MediaCapturedModel, on_delete=models.SET_NULL, null=True, blank=True)
    attendance_status = JSONField(null=True, blank=True, default=list)
    attendance_date = models.DateField(null=True, blank=True)
    attendance_choice = models.CharField(choices=ATTENDANCE_CHOICE, null=True, blank=True, max_length=20)

    date_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    staff_attendance_id = models.CharField(max_length=512, null=True, blank=True)

    class Meta:
        ordering = ('-date_time',)


# teacher / non-teacher / school_admin -  + [Class attendance]
class StaffAttendanceModel(models.Model):
    staff_attendance_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    attendance_id = models.ForeignKey(AttendanceModel, on_delete=models.SET_NULL, null=True, blank=True)
    # period_id = models.ForeignKey(PeriodModel, on_delete=models.CASCADE, null=True, blank=True)
    attendance_type = models.CharField(choices=STAFF_ATTENDANCE_TYPE_CHOICE, null=True, blank=True, max_length=20)
    attempt_status = models.CharField(choices=ATTEMPT_STATUS_CHOICE, null=True, blank=True, max_length=20)
    loggedin_user = models.ForeignKey(Profile, related_name='loggedin_user', on_delete=models.SET_NULL, null=True,
                                      blank=True)
    taken_by = models.ForeignKey(Profile, related_name='taken_by', on_delete=models.SET_NULL, null=True, blank=True)
    taken_for = models.ForeignKey(Profile, related_name='taken_for', on_delete=models.CASCADE, null=True, blank=True)
    media_id = models.ForeignKey(MediaCapturedModel, on_delete=models.SET_NULL, null=True, blank=True)
    attendance_status = JSONField(null=True, blank=True, default=list)
    attendance_date = models.DateTimeField(null=True, blank=True, default=datetime.datetime.now)
    staff_attendance_geolocation = JSONField(null=True, blank=True, default=list)
    date_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_time',)


class AnnouncementModel(models.Model):
    announcement_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    school_id = models.ForeignKey(SchoolModel, on_delete=models.CASCADE, null=True, blank=True)
    created_by = models.ForeignKey(Profile, related_name='created_by', on_delete=models.SET_NULL, null=True, blank=True)

    title = models.CharField(max_length=512, null=True, blank=True)
    content = models.CharField(max_length=5000, null=True, blank=True)
    extra = JSONField(null=True, blank=True, default=list)
    is_active = models.BooleanField(default=False)
    expire_date = models.DateTimeField(null=True, blank=True)

    date_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_time',)


class CameraModel(models.Model):
    camera_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    school_id = models.ForeignKey(SchoolModel, on_delete=models.CASCADE, null=True, blank=True)
    added_by = models.ForeignKey(Profile, related_name='added_by', on_delete=models.SET_NULL, null=True, blank=True)
    title = models.CharField(max_length=512, null=True, blank=True)
    url = models.CharField(max_length=512, null=True, blank=True)
    ip_address = models.CharField(max_length=512, null=True, blank=True)
    port = models.CharField(max_length=512, null=True, blank=True)
    extra = JSONField(null=True, blank=True, default=list)
    is_active = models.BooleanField(null=True, blank=True, default=False)
    status = models.CharField(max_length=512, null=True, blank=True)
    user = models.CharField(max_length=512, null=True, blank=True)
    password = models.CharField(max_length=512, null=True, blank=True)
    type = models.CharField(max_length=512, null=True, blank=True)
    mac_address = models.CharField(max_length=512, null=True, blank=True)
    resolution = models.CharField(max_length=512, null=True, blank=True)
    active_date_time = models.DateTimeField(null=True, blank=True)
    date_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_time',)


CCTV_ATTENDANCE_TYPE_CHOICE = (
    ('auto', 'Auto'),
    ('manual', 'Manual'),
)


class CameraResponseModel(models.Model):
    camera_response_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    school_id = models.ForeignKey(SchoolModel, on_delete=models.CASCADE, null=True, blank=True)
    camera_id = models.ForeignKey(CameraModel, on_delete=models.SET_NULL, null=True, blank=True)

    role = models.CharField(max_length=30, choices=ROLE_CHOICE, null=True, blank=True)
    loggedin_user = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, blank=True)
    media_id = models.ForeignKey(MediaCapturedModel, on_delete=models.SET_NULL, null=True, blank=True)
    cctv_attendance_status = JSONField(null=True, blank=True, default=list)
    cctv_attempt_status = models.CharField(choices=ATTEMPT_STATUS_CHOICE, null=True, blank=True, max_length=20)
    attendance_date = models.DateField(null=True, blank=True)

    cctv_attendance_type = models.CharField(choices=CCTV_ATTENDANCE_TYPE_CHOICE, null=True, blank=True, max_length=20)
    date_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_time',)
