from django.contrib import admin

# Register your models here.
from app_root.models import Profile, PeriodModel, AttendanceModel, MediaCapturedModel, SchoolModel, AnnouncementModel, \
    StaffAttendanceModel, CameraModel, CameraResponseModel, GeolocationModel


class MediaCapturedModelAdminReadOnly(admin.ModelAdmin):
    readonly_fields = ('date_time', 'updated_at',)
    list_display = (
        'media_id',
        'media_directory',
        'media_type',
        'date_time',
    )
    list_filter = (
        'media_id',
        'media_directory',
        'media_type',
        'date_time',
    )


class SchoolModelAdminReadOnly(admin.ModelAdmin):
    readonly_fields = ('date_time', 'updated_at',)
    list_display = (
        'school_id',
        'organisation_type',
        'branch',
        'name',
        'address',
        'city',
        'state',
        'country',
        'postal_code',
        'is_verified',
        'date_time',
    )
    list_filter = (
        'school_id',
        'branch',
        'name',
        'address',
        'city',
        'state',
        'country',
        'postal_code',
        'is_verified',
        'date_time',
    )


class ProfileAdminReadOnly(admin.ModelAdmin):
    readonly_fields = ('date_time', 'updated_at',)
    list_display = (
        'user',
        'profile_id',
        'gender',
        'role',
        'blood_group',
        'date_time',
        'is_registered',
    )
    list_filter = (
        'role',
        'gender',
        'blood_group',
    )


class PeriodModelAdminReadOnly(admin.ModelAdmin):
    readonly_fields = ('date_time', 'updated_at',)
    list_display = (
        'period_id',
        'school_id',
        'teacher_id',
        'class_name',
        'section',
        'period_number',
        'day',
        'subject',
        'date_time',
    )
    list_filter = (
        'date_time',
        'school_id',
    )


class AttendanceModelAdminReadOnly(admin.ModelAdmin):
    readonly_fields = ('date_time', 'updated_at',)
    list_display = (
        'attendance_id',
        'loggedin_user',
        'period_id',
        'media_id',
        'attempt_status',
        'attendance_date',
        'attendance_choice',
        'updated_at',
        'date_time',
    )
    list_filter = (
        'loggedin_user',
        'attendance_date',
    )


class StaffAttendanceModelAdminReadOnly(admin.ModelAdmin):
    readonly_fields = ('date_time', 'updated_at',)
    list_display = (
        'staff_attendance_id',
        'attendance_type',
        'loggedin_user',
        'taken_by',
        'taken_for',
        'media_id',
        'attempt_status',
        'attendance_status',
        'attendance_date',
        'updated_at',
        'date_time',
    )
    list_filter = (
        'taken_for',
        'loggedin_user',
        'attendance_status',
        'attendance_date',
    )


class AnnouncementModelAdminReadOnly(admin.ModelAdmin):
    readonly_fields = ('date_time', 'updated_at',)
    list_display = (
        'announcement_id',
        'title',
        'content',
        'is_active',
        'expire_date',
        'date_time',
    )
    list_filter = (
        'announcement_id',
        'date_time',
    )


class CameraModelAdminReadOnly(admin.ModelAdmin):
    readonly_fields = ('date_time', 'updated_at',)
    list_display = (
        'camera_id',
        'school_id',
        'title',
        'ip_address',
        'user',
        'password',
        'url',
        'date_time',
        'updated_at',
    )
    list_filter = (
        'school_id',
        'status',
        'type',
        'is_active',
        'resolution',
    )


class CameraResponseModelAdminReadOnly(admin.ModelAdmin):
    readonly_fields = ('date_time', 'updated_at',)
    list_display = (
        'camera_response_id',
        'camera_id',
        'role',
        'loggedin_user',
        'cctv_attendance_status',
        'cctv_attempt_status',
        'cctv_attendance_type',
        'date_time',
        'updated_at',
    )
    list_filter = (
        'cctv_attempt_status',
        'cctv_attendance_type',
    )


class GeolocationModelAdminReadOnly(admin.ModelAdmin):
    readonly_fields = ('date_time', 'updated_at',)
    list_display = (
        'geolocation_id',
        'name',
        'address',
        'coordinates',
        'geoFence',
        'status',
        'is_active',
        'extra_data',
    )
    list_filter = (
        'is_active',
    )


admin.site.register(MediaCapturedModel, MediaCapturedModelAdminReadOnly)
admin.site.register(SchoolModel, SchoolModelAdminReadOnly)
admin.site.register(Profile, ProfileAdminReadOnly)
admin.site.register(PeriodModel, PeriodModelAdminReadOnly)
admin.site.register(AttendanceModel, AttendanceModelAdminReadOnly)
admin.site.register(StaffAttendanceModel, StaffAttendanceModelAdminReadOnly)
admin.site.register(AnnouncementModel, AnnouncementModelAdminReadOnly)
admin.site.register(CameraModel, CameraModelAdminReadOnly)
admin.site.register(CameraResponseModel, CameraResponseModelAdminReadOnly)
admin.site.register(GeolocationModel, GeolocationModelAdminReadOnly)
