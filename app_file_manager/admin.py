from django.contrib import admin

from app_file_manager.models import Folder, File

admin.site.register(Folder)
admin.site.register(File)
