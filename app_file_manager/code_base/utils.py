import os
import shutil
from wsgiref.util import FileWrapper
from django.http import HttpResponse

############# FILE MANAGEMENT OPTIONS ################

# List folder and files in a directory (one level tree). Example: parent_path = settings.MEDIA_ROOT+"/documents/"
from django.urls import reverse
from datetime import datetime


def icon(extension):
    if extension == 'txt':
        return '-alt'
    elif extension == 'pdf':
        return '-pdf'
    elif extension in ['doc', 'docs', 'docx', 'odt', 'dot', 'dotm', 'dotx']:
        return '-word'
    elif extension in ['xls', 'xlsx', 'xlt']:
        return '-excel'
    elif extension in ['ppt', 'pptx', 'ppsx']:
        return '-powerpoint'
    elif extension in ['png', 'jpg', 'jpeg']:
        return '-image'
    elif extension in ['mp4', 'mkv', 'flv', 'vob', 'avi', 'wmv']:
        return '-video'
    elif extension in ['mp3', 'aa', 'aac', 'act', 'mmf', 'mpc']:
        return '-audio'
    elif extension in ['py', 'js', 'java', 'php', 'rb', 'html', 'css', 'htm']:
        return '-code'
    elif extension in ['zip', 'rar', 'iso', '7z', 'apk']:
        return '-archive'
    elif extension == 'csv':
        return '-csv'
    elif extension == 'pkl':
        return '-pkl'

    else:
        return ''


def list_current_dir_tree(request, base_path, parent):
    parent_path = base_path + parent

    breadcrumb = parent.split("/")
    breadcrumb = list(filter(None, breadcrumb))

    # print('---breadcrumb---',breadcrumb)
    breadcrumb_list = []
    prefix_url = ""
    for item in breadcrumb:
        prefix_url += item + '/'
        breadcrumb_list.append({
            'text': item,
            'link': '?parent=' + prefix_url
        })

    # print('---breadcrumb_list---',breadcrumb_list)

    # print('---list_current_dir_tree---parent_path---',parent_path)
    folders = []
    files = []
    tree = {}
    message = ''
    try:
        contents = os.listdir(parent_path)
        for item in contents:
            if os.path.isdir(parent_path + item):
                folders.append({
                    "parent_name": parent[:-1],
                    "folder_name": item,
                    # "folder_full_path": parent_path + item,
                    "folder_path": parent + item + "/"
                })

            if os.path.isfile(parent_path + item):
                get_download_link = reverse('app_file_manager:file_download', kwargs={'file_name': item})
                link = "/media/" + parent + item

                # set API URL here
                SCHEME = request.scheme
                HOST = request.get_host()
                API_URL = SCHEME + '://' + HOST
                request.api_url = API_URL
                #

                get_full_link = API_URL + link

                file_path = parent_path + item
                created = os.path.getctime(file_path)
                modified = os.path.getmtime(file_path)

                is_modified = False
                if created != modified:
                    is_modified = True

                ccreated = datetime.fromtimestamp(created).strftime('%Y-%m-%d %H:%M:%S')
                mmodified = datetime.fromtimestamp(modified).strftime('%Y-%m-%d %H:%M:%S')

                ext = item.split('.')[-1]
                icon_ = icon(ext)
                files.append({
                    "file_name": item,
                    "get_download_link": get_download_link,
                    "get_full_link": get_full_link,
                    "link": link,
                    "created": created,
                    "ccreated": ccreated,
                    "modified": modified,
                    "mmodified": mmodified,
                    "is_modified": is_modified,
                    "ext": ext,
                    "icon": icon_
                })

        files = sorted(files, key=lambda d: d['created'], reverse=True)

        message = 'SUCCESS'
    except Exception as e:
        print(e)
        message = 'FAILED'

    tree['Folders'] = folders
    tree['Files'] = files
    tree['Parent'] = parent
    tree['message'] = message
    tree['breadcrumb_list'] = breadcrumb_list
    return tree


# create new folder
def create_folder(parent_path, new_folder_name):
    message = ''
    try:
        if not os.path.exists(parent_path + new_folder_name):
            os.makedirs(parent_path + new_folder_name)
            message = 'SUCCESS'
        else:
            message = 'FAILED'
    except Exception as e:
        print(e)
        message = 'FAILED'
    return message


# Rename file. rename_file(settings.MEDIA_ROOT+"/", '111', '111111') where 111 directory name exist
def rename_folder(parent_path, old_folder_name, new_folder_name):
    parent_path = parent_path + "/"

    message = ''
    try:
        if os.path.exists(parent_path + old_folder_name):
            os.rename(parent_path + old_folder_name, parent_path + new_folder_name)
            message = 'SUCCESS'
        else:
            message = 'FAILED'
    except Exception as e:
        print(e)
        message = 'FAILED'
    return message


# Rename file. rename_file(settings.MEDIA_ROOT+"/", '111', '111111') where 111 directory name exist
def delete_folder(parent_path, folder_name):
    message = ''
    try:
        if os.path.exists(parent_path + folder_name):
            shutil.rmtree(parent_path + folder_name)
            message = 'SUCCESS'
        else:
            message = 'FAILED'
    except Exception as e:
        print(e)
        message = 'FAILED'
    return message


############# FILE MANAGEMENT OPTIONS ################
# Rename file. rename_file(settings.MEDIA_ROOT+"/111/", '1663935463814_28.jpg', 'new_1663935463814_28.jpg')
def rename_file(parent_path, old_file_name, new_file_name):
    parent_path = parent_path + "/"
    print('---parent_path---', parent_path)
    print('---old_file_name---', old_file_name)
    print('---new_file_name---', new_file_name)

    message = ''
    try:
        if os.path.exists(parent_path + old_file_name):
            os.rename(parent_path + old_file_name, parent_path + new_file_name)
            message = 'SUCCESS'
        else:
            message = 'FAILED'
    except Exception as e:
        print(e)
        message = 'FAILED'
    return message


# Rename file. rename_file(settings.MEDIA_ROOT+"/", '111') where 111 directory name exist
def delete_file(parent_path, filename):
    parent_path = parent_path + "/"

    print('---parent_path---', parent_path)
    print('---filename---', filename)

    message = ''
    try:
        if os.path.exists(parent_path + filename):
            os.remove(parent_path + filename)
            message = 'SUCCESS'
        else:
            message = 'FAILED'
    except Exception as e:
        print(e)
        message = 'FAILED'
    return message


# Download file download_file(settings.MEDIA_ROOT+"/111/", 'new_1663935463814_28.jpg')
def download_file(parent_path, filename):
    file = open(parent_path + filename, 'rb')
    wrapper = FileWrapper(file)
    response = HttpResponse(wrapper, content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=%s' % file.name
    response['Content-Length'] = os.path.getsize(filename)
    return response


# Move file or entire folder to a new location. here
def move_file_or_folder(old_path, new_path):
    print('---old_path---', old_path)
    print('---new_path---', new_path)

    message = ''
    is_valid = False

    try:
        if os.path.isdir(new_path):
            if os.path.exists(old_path):
                if old_path != new_path:
                    if new_path != old_path + '/':
                        shutil.move(old_path, new_path)
                        message = 'SUCCESS'
                        is_valid = True
                    else:
                        message = 'YOU CANNOT MOVE A FOLDER INTO ITSELF'
                else:
                    message = 'DESTINATION PATH CAN NOT BE SAME AS THE SOURCE PATH'
            else:
                message = 'SOURCE PATH IS NOT EXIST'
        else:
            message = 'DESTINATION SHOULD BE A DIRECTORY'
    except Exception as e:
        print(e)
        message = 'FAILED'

    return message, is_valid


def walk_dir_tree(request, base_path):
    parent_path = base_path

    return_list = []
    tree = {}
    message = ''

    all_possible_root = []
    try:
        if os.path.exists(parent_path):
            for root, dirs, dir_files in os.walk(parent_path):
                all_possible_root.append(root.replace(base_path, "") + "/")
            message = 'SUCCESS'
        else:
            message = 'FAILED'
    except Exception as e:
        print('----Exception----', Exception)
        message = 'FAILED'

    tree['Tree'] = all_possible_root
    tree['message'] = message
    return tree


import json

import pdb


# tree
def path_to_dict(path, new_list=None):
    d = {'text': os.path.basename(path)}
    if os.path.isdir(path):
        d['type'] = "folder"
        # d['icon'] = "jstree-icon"
        if new_list:
            breadcrumb_list = new_list["breadcrumb_list"]
            if breadcrumb_list:
                text = breadcrumb_list[-1]["text"]
                if text == os.path.basename(path):
                    d['state'] = {
                        'opened': True,
                        'selected': True
                    }
        d['children'] = [path_to_dict(os.path.join(path, x), new_list) for x in os.listdir(path)]
    else:
        d['type'] = "file"


    return d
