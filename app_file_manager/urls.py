from django.urls import path, re_path
# from file_manager.folder import views
from app_file_manager import views

app_name = 'app_file_manager'


urlpatterns = [
    # folder
    path('folder/add/', views.FolderCreateView.as_view(), name='old_folder_add'),
    path('folder/', views.FolderListView.as_view(), name='old_folder_list'),
    re_path(r'^folder/delete/(?P<folder_name>[\w|\W]+)/$', views.folder_delete, name='folder_delete'),
    re_path(r'^folder/update/(?P<folder_name>[\w|\W]+)/$', views.folder_update, name='folder_update'),
    re_path(r'^folder/link/(?P<folder_name>[\w|\W]+)/$', views.folder_link, name='folder_link'),
    # re_path(r'^move/(?P<folder_name>[\w|\W]+)/$', views.folder_move, name='folder_move'),
    path('folder/add_folder/', views.folder_add_view, name='folder_add'),
    path('folder/test/', views.test_view, name='test'),



    # file
    path('file/add/', views.FileCreateView.as_view(), name='old_file_add'),
    path('file/list/', views.FileListView.as_view(), name='old_file_list'),
    path('file/<int:pk>/download/', views.FileDownloadView.as_view(), name='download'),
    re_path(r'^file/delete/(?P<file_name>[\w|\W]+)/$', views.file_delete, name='file_delete'),
    re_path(r'^file/update/(?P<file_name>[\w|\W]+)/$', views.file_update, name='file_update'),
    re_path(r'^file/link/(?P<file_name>[\w|\W]+)/$', views.file_link, name='file_link'),
    re_path(r'^file/download/(?P<file_name>[\w|\W]+)/$', views.FileDownloadNewView.as_view(), name='file_download'),
    re_path(r'^file/move/(?P<file_name>[\w|\W]+)/$', views.file_and_folder_move, name='file_move'),
    path('file/upload/', views.file_upload_view, name='file_upload'),


]
