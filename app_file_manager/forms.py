from django import forms
from django.forms import ModelForm

from app_file_manager.models import Folder


class FolderForm(ModelForm):
    class Meta:
        model = Folder
        fields = ['name', 'text_color', 'parent']


class FolderRenameForm(forms.Form):
    required_css_class = 'required'
    old_name = forms.CharField(max_length=50)
    new_name = forms.CharField(max_length=50)

    class Meta:
        fields = ['old_name', 'new_name']

    def __init__(self, *args, **kwargs):
        folder_name = kwargs.pop('folder_name', None)
        super(FolderRenameForm, self).__init__(*args, **kwargs)

        for field_name in ['old_name', 'new_name']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

        self.fields['new_name'].required = True
        self.fields['new_name'].widget.attrs['value'] = folder_name
        self.fields['old_name'].widget.attrs['value'] = folder_name
        self.fields['old_name'].widget.attrs['readonly'] = True

class FolderAddForm(forms.Form):
    required_css_class = 'required'

    all_possible_root = forms.ChoiceField()
    folder_name = forms.CharField(max_length=50)

    class Meta:
        fields = ['all_possible_root', 'folder_name']

    def __init__(self, *args, **kwargs):
        parent_root = kwargs.pop('parent_root', None)
        all_possible_root = kwargs.pop('all_possible_root', None)

        super(FolderAddForm, self).__init__(*args, **kwargs)

        for field_name in ['all_possible_root', 'folder_name']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

        self.fields['all_possible_root'].required = True
        self.fields['folder_name'].required = True

        parent_root_for_remove = "/" + parent_root
        self.initial['all_possible_root'] = parent_root_for_remove

        # ALL_POSSIBLE_ROOT = sorted(set([(item, item) for item in all_possible_root]))
        ALL_POSSIBLE_ROOT = [(item, item) for item in all_possible_root]

        self.fields['all_possible_root'] = forms.ChoiceField(choices=ALL_POSSIBLE_ROOT, label='Add to',
                                                             required=True)
        self.fields['all_possible_root'].widget = forms.HiddenInput()

    def clean(self):
        cleaned_data = super(FolderAddForm, self).clean()

        selected_root = self.cleaned_data['all_possible_root']
        folder_name = self.cleaned_data['folder_name']
        # print('---form selected_root---', selected_root)
        # print('---form folder_name---', folder_name)

        # return self.cleaned_data
        return cleaned_data



from django.forms import ModelForm, Form
from django import forms

from app_file_manager.models import File


class FileForm(ModelForm):
    class Meta:
        model = File
        fields = ['folder', 'file']


class FileRenameForm(Form):
    required_css_class = 'required'
    old_name = forms.CharField(max_length=50)
    new_name = forms.CharField(max_length=50)

    class Meta:
        fields = ['old_name', 'new_name']

    def __init__(self, *args, **kwargs):
        file_name = kwargs.pop('file_name', None)
        super(FileRenameForm, self).__init__(*args, **kwargs)

        for field_name in ['old_name', 'new_name']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

        self.fields['new_name'].required = True
        self.fields['new_name'].widget.attrs['value'] = file_name
        self.fields['old_name'].widget.attrs['value'] = file_name
        self.fields['old_name'].widget.attrs['readonly'] = True


class FileMoveForm(Form):
    required_css_class = 'required'

    all_possible_root = forms.ChoiceField()

    class Meta:
        fields = ['all_possible_root']

    def __init__(self, *args, **kwargs):
        file_name = kwargs.pop('file_name', None)
        parent_root = kwargs.pop('parent_root', None)
        all_possible_root = kwargs.pop('all_possible_root', None)

        super(FileMoveForm, self).__init__(*args, **kwargs)

        for field_name in ['all_possible_root']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

        self.fields['all_possible_root'].required = True
        # self.fields['all_possible_root'].widget.attrs['value'] = file_name

        # print('---form parent_root---', parent_root)

        parent_root_for_remove = "/" + parent_root
        if parent_root_for_remove in all_possible_root:
            all_possible_root.remove(parent_root_for_remove)  # user cannot move folder in the same path

        itself_folder_remove = "/" + parent_root + file_name + '/'
        if itself_folder_remove in all_possible_root:
            all_possible_root.remove(itself_folder_remove)  # user cannot move folder into itself

        # ALL_POSSIBLE_ROOT = sorted(set([(item, item) for item in all_possible_root]))
        ALL_POSSIBLE_ROOT = [(item, item) for item in all_possible_root]

        self.fields['all_possible_root'] = forms.ChoiceField(choices=ALL_POSSIBLE_ROOT, label='Move to', required=True)


class FileUploadForm(Form):
    required_css_class = 'required'

    all_possible_root = forms.ChoiceField()
    file = forms.FileField()

    class Meta:
        fields = ['all_possible_root', 'file']

    def __init__(self, *args, **kwargs):
        file_name = kwargs.pop('file_name', None)
        parent_root = kwargs.pop('parent_root', None)
        all_possible_root = kwargs.pop('all_possible_root', None)

        super(FileUploadForm, self).__init__(*args, **kwargs)

        for field_name in ['all_possible_root', 'file']:
            self.fields[field_name].help_text = None
            self.fields[field_name].widget.attrs.update({
                'autocomplete': 'off'
            })

        self.fields['all_possible_root'].required = True
        self.fields['file'].required = True

        parent_root_for_remove = "/" + parent_root
        self.initial['all_possible_root'] = parent_root_for_remove

        # print('---form parent_root---', parent_root)
        # print('---form parent_root_for_remove---', parent_root_for_remove)

        # ALL_POSSIBLE_ROOT = sorted(set([(item, item) for item in all_possible_root]))
        ALL_POSSIBLE_ROOT = [(item, item) for item in all_possible_root]
        # print('---form ALL_POSSIBLE_ROOT---', ALL_POSSIBLE_ROOT)

        self.fields['all_possible_root'] = forms.ChoiceField(choices=ALL_POSSIBLE_ROOT, label='Upload to',
                                                             required=True)
        self.fields['all_possible_root'].widget = forms.HiddenInput()

    def clean(self):
        cleaned_data = super(FileUploadForm, self).clean()

        selected_root = self.cleaned_data['all_possible_root']
        file = self.cleaned_data['file']
        print('---form selected_root---', selected_root)
        print('---form file---', file)

        # return self.cleaned_data
        return cleaned_data
