import json
import os

from django.shortcuts import get_object_or_404, render
from wsgiref.util import FileWrapper
from django.core.files.storage import default_storage, FileSystemStorage
from django.http import HttpResponse, JsonResponse
from django.template.loader import render_to_string
from django.urls import reverse
from django.views.generic import CreateView, ListView
from django.views.generic.base import View

from app_file_manager.code_base.utils import delete_file, rename_file, move_file_or_folder, walk_dir_tree
from app_file_manager.code_base.utils import list_current_dir_tree, delete_folder, rename_folder, path_to_dict
from app_file_manager import forms
from app_file_manager.models import Folder, File
from luein_smart_attendance import settings


class FolderCreateView(CreateView):
    form_class = forms.FolderForm
    template_name = 'app_file_manager/app_folder/html/add.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['submit_url'] = reverse('app_file_manager:old_folder_add')
        context['add_folder'] = reverse('app_file_manager:old_folder_add')
        context['add_file'] = reverse('app_file_manager:old_file_add')
        return context

    def get_success_url(self):
        return reverse('app_file_manager:old_folder_list')


class FolderListView(ListView):
    context_object_name = 'folder_list'
    template_name = 'app_file_manager/app_folder/html/list.html'

    def get_queryset(self):
        pk = self.request.GET.get('id')
        queryset = Folder.objects.all()
        if pk:
            queryset = queryset.filter(parent__pk=pk)
        else:
            queryset = queryset.filter(parent=None)

        order_by = self.request.GET.get('sort')
        if order_by == 'time':
            queryset = queryset.order_by('-created_at')
        elif order_by == 'name':
            queryset = queryset.order_by('name')

        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)

        pk = self.request.GET.get('id')

        context['file_list'] = self.get_file_query(pk)
        # context['add_folder'] = reverse('folder:add')
        # context['add_file'] = reverse('file:add')

        context['add_folder'] = reverse('app_file_manager:old_folder_add')
        context['add_file'] = reverse('app_file_manager:old_file_add')

        context['parent_folder'] = self.get_parent_folder_link(pk)

        context['new_parent_folder'] = [{
            'text': 'documents',
            'link': '/?parent=' + 'documents/'
        }, {
            'text': 'child_1',
            'link': '/?parent=' + 'documents/' + 'child_1/'
        }, {
            'text': 'child_2',
            'link': '/?parent=' + 'documents/' + 'child_1/' + 'child_2/'
        }]

        parent = self.request.GET.get('parent')
        # base_path = settings.MEDIA_ROOT + "/"
        base_path = settings.BROWSE_FOLDER_ROOT

        # print('---URL parent---', parent)
        # print('---base_path---', base_path)

        if parent:
            parent_path = base_path + parent
        else:
            parent_path = base_path
            parent = ""

        tree_list = list_current_dir_tree(self.request, base_path, parent)
        context['new_list'] = tree_list
        context['new_list_for_tree'] = json.dumps(tree_list)
        context['all_folders'] = walk_dir_tree(self.request, base_path)

        context['tree_hierarchy'] = json.dumps([path_to_dict(base_path, tree_list)])

        context['parent_name'] = parent

        return context

    def get_file_query(self, pk):
        file_query = File.objects.all()
        if pk:
            file_query = file_query.filter(folder__pk=pk)
        else:
            file_query = file_query.filter(folder=None)
        order_by = self.request.GET.get('sort')
        if order_by == 'time':
            file_query = file_query.order_by('-created_at')
        elif order_by == 'name':
            file_query = file_query.order_by('name')
        return file_query

    @staticmethod
    def get_parent_folder_link(pk):
        parent_folder = []
        if pk:
            folder = Folder.objects.get(pk=pk)
            parent_folder.append({
                'text': folder.name,
                'link': reverse('app_file_manager:old_folder_list') + f'?id={folder.pk}',
            })
            while folder.parent:
                parent_folder.append({
                    'text': folder.parent.name,
                    'link': reverse('app_file_manager:old_folder_list') + f'?id={folder.parent.pk}',
                })
                folder = folder.parent
            parent_folder.reverse()
        return parent_folder

    @staticmethod
    def get_new_parent_folder_link(parent_path):
        parent_folder = []
        if pk:
            folder = Folder.objects.get(pk=pk)
            parent_folder.append({
                'text': folder.name,
                'link': reverse('app_file_manager:old_folder_list') + f'?id={folder.pk}',
            })
            while folder.parent:
                parent_folder.append({
                    'text': folder.parent.name,
                    'link': reverse('app_file_manager:old_folder_list') + f'?id={folder.parent.pk}',
                })
                folder = folder.parent
            parent_folder.reverse()
        return parent_folder


# @login_required
def folder_delete(request, **kwargs):
    # os.path.exists(path)

    parent = request.GET.get('parent')
    folder_name = kwargs.get('folder_name')
    base_path = settings.MEDIA_ROOT
    absolute_path = base_path + '/' + parent + folder_name
    dir_path = base_path + '/' + parent
    folder_path = absolute_path.replace(base_path, "")

    # print('---parent---', parent)
    # print('---folder_name---', folder_name)
    # print('---base_path---', base_path)
    # print("---absolute_path---", absolute_path)
    # print("---dir_path---", dir_path)
    # print("---folder_path---", folder_path)

    data = dict()
    if request.method == 'POST':

        # delete row from DB
        # folder_get_qs.delete()

        parent_path = dir_path
        del_msg = delete_folder(parent_path, folder_name)

        # company_users_list = create_company_users_list(request)
        # html_emp_jobs_list = create_html_emp_jobs_list(request, company_users_list)
        # data['html_emp_jobs_list'] = html_emp_jobs_list

        data['form_is_valid'] = True
        data['message'] = del_msg
    else:
        # context = {'folder_get_qs': folder_get_qs}
        context = {
            'folder': {
                'parent_name': parent,
                'folder_name': folder_name,
                'folder_path': folder_path
            }
        }
        template_name = 'app_file_manager/app_folder/html/partial_folder_delete.html'
        data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)

    data['action_type'] = "delete"
    return JsonResponse(data)


def folder_update(request, **kwargs):
    template = 'app_file_manager/app_folder/html/partial_folder_update.html'

    # os.path.exists(path)
    folder_name = kwargs.get('folder_name')
    print('---folder_name---', folder_name)

    parent = request.GET.get('parent')
    base_path = settings.MEDIA_ROOT
    absolute_path = base_path + '/' + parent
    dir_path = base_path + '/' + parent
    folder_path = absolute_path.replace(base_path, "")

    print('---parent---', parent)
    print('---base_path---', base_path)
    print("---absolute_path---", absolute_path)
    print("---dir_path---", dir_path)
    print("---folder_path---", folder_path)

    data = dict()
    if request.method == 'POST':
        folder_rename_form = forms.FolderRenameForm(data=request.POST, folder_name=folder_name)
        if folder_rename_form.is_valid():
            # delete row from DB
            # folder_get_qs.delete()

            parent_path = dir_path
            old_name = folder_name
            new_name = folder_rename_form.cleaned_data['new_name']
            print('---new_name---', new_name)

            del_msg = rename_folder(parent_path, old_name, new_name)

            # company_users_list = create_company_users_list(request)
            # html_emp_jobs_list = create_html_emp_jobs_list(request, company_users_list)
            # data['html_emp_jobs_list'] = html_emp_jobs_list

            data['form_is_valid'] = True
            data['message'] = del_msg
        else:
            data['form_is_valid'] = False
            data['message'] = "failed"
    else:
        # context = {'folder_get_qs': folder_get_qs}
        folder_rename_form = forms.FolderRenameForm(folder_name=folder_name)

        context = {
            'folder': folder_name,
            "parent_name": parent,
            "form": folder_rename_form
        }
        data['html_form'] = render_to_string(template_name=template, context=context, request=request)

    data['action_type'] = "update"
    return JsonResponse(data)


def folder_link(request, **kwargs):
    template = 'app_file_manager/app_folder/html/partial_folder_update.html'

    # os.path.exists(path)
    folder_name = kwargs.get('folder_name')
    print('---folder_name---', folder_name)

    parent = request.GET.get('parent')
    base_path = settings.MEDIA_ROOT
    absolute_path = base_path + parent
    dir_path = base_path + parent
    folder_path = absolute_path.replace(base_path, "")

    # print('---parent---', parent)
    # print('---base_path---', base_path)
    # print("---absolute_path---", absolute_path)
    # print("---dir_path---", dir_path)
    # print("---folder_path---", folder_path)

    data = dict()
    if request.method == 'POST':
        folder_rename_form = forms.FolderRenameForm(data=request.POST, folder_name=folder_name)
        if folder_rename_form.is_valid():
            # delete row from DB
            # folder_get_qs.delete()

            parent_path = dir_path
            old_name = folder_name
            new_name = folder_rename_form.cleaned_data['new_name']
            print('---new_name---', new_name)

            del_msg = rename_folder(parent_path, old_name, new_name)

            # company_users_list = create_company_users_list(request)
            # html_emp_jobs_list = create_html_emp_jobs_list(request, company_users_list)
            # data['html_emp_jobs_list'] = html_emp_jobs_list

            data['form_is_valid'] = True
            data['message'] = del_msg
        else:
            data['form_is_valid'] = False
            data['message'] = "failed"
    else:
        # context = {'folder_get_qs': folder_get_qs}
        folder_rename_form = forms.FolderRenameForm(folder_name=folder_name)

        context = {
            'folder': folder_name,
            "parent_name": parent,
            "form": folder_rename_form
        }
        data['html_form'] = render_to_string(template_name=template, context=context, request=request)

    data['action_type'] = "link"
    return JsonResponse(data)


# def folder_move(request, **kwargs):
#     template = 'folder/partial_folder_update.html'
#
#     # os.path.exists(path)
#     folder_name = kwargs.get('folder_name')
#     print('---folder_name---', folder_name)
#
#     parent = request.GET.get('parent')
#     base_path = settings.MEDIA_ROOT
#     absolute_path = base_path + parent
#     dir_path = base_path + parent
#     folder_path = absolute_path.replace(base_path, "")
#
#     # print('---parent---', parent)
#     # print('---base_path---', base_path)
#     # print("---absolute_path---", absolute_path)
#     # print("---dir_path---", dir_path)
#     # print("---folder_path---", folder_path)
#
#     data = dict()
#     if request.method == 'POST':
#         folder_rename_form = forms.FolderRenameForm(data=request.POST, folder_name=folder_name)
#         if folder_rename_form.is_valid():
#             # delete row from DB
#             # folder_get_qs.delete()
#
#             parent_path = dir_path
#             old_name = folder_name
#             new_name = folder_rename_form.cleaned_data['new_name']
#             print('---new_name---', new_name)
#
#             del_msg = rename_folder(parent_path, old_name, new_name)
#
#             # company_users_list = create_company_users_list(request)
#             # html_emp_jobs_list = create_html_emp_jobs_list(request, company_users_list)
#             # data['html_emp_jobs_list'] = html_emp_jobs_list
#
#             data['form_is_valid'] = True
#             data['message'] = del_msg
#         else:
#             data['form_is_valid'] = False
#             data['message'] = "failed"
#     else:
#         # context = {'folder_get_qs': folder_get_qs}
#         folder_rename_form = forms.FolderRenameForm(folder_name=folder_name)
#
#         context = {
#             'folder': folder_name,
#             "parent_name": parent,
#             "form": folder_rename_form
#         }
#         data['html_form'] = render_to_string(template_name=template, context=context, request=request)
#
#     data['action_type'] = "update"
#     return JsonResponse(data)


def folder_add_view(request, **kwargs):
    template = 'app_file_manager/app_folder/html/partial_folder_add.html'
    context = {}

    parent = request.GET.get('parent')
    base_path = settings.MEDIA_ROOT
    absolute_path = base_path + '/' + parent
    dir_path = base_path + '/' + parent
    folder_path = absolute_path.replace(base_path, "")

    # print('---parent---', parent)
    # print('---base_path---', base_path)
    # print("---absolute_path---", absolute_path)
    # print("---dir_path---", dir_path)
    # print("---folder_path---", folder_path)

    return_walk_dir_tree = walk_dir_tree(request, base_path)
    all_possible_root = []
    if return_walk_dir_tree["message"] == 'SUCCESS':
        all_possible_root = return_walk_dir_tree["Tree"]

    parent_root = parent

    data = dict()
    if request.method == 'POST':
        folder_add_form = forms.FolderAddForm(request.POST, request.FILES, all_possible_root=all_possible_root,
                                              parent_root=parent_root)

        if folder_add_form.is_valid():

            selected_root = folder_add_form.cleaned_data['all_possible_root']
            folder_name = folder_add_form.cleaned_data['folder_name']
            parent_path = base_path + selected_root

            folder_path = parent_path + folder_name
            print('---folder_path---', folder_path)
            # create the folder if it doesn't exist.
            try:
                os.mkdir(folder_path)
            except:
                pass

            upload_msg = "success"

            data['form_is_valid'] = True
            data['message'] = upload_msg
        else:
            data['form_is_valid'] = False
            data['message'] = "failed"
            context = {
                "parent_name": parent,
                "form": folder_add_form
            }

    else:
        folder_add_form = forms.FolderAddForm(all_possible_root=all_possible_root, parent_root=parent_root)
        context = {
            "parent_name": parent,
            "form": folder_add_form
        }

    data['html_form'] = render_to_string(template_name=template, context=context, request=request)
    data['action_type'] = "add"
    return JsonResponse(data)


def test_view(request):
    template_name = "test.html"
    context_payload = {
    }

    return render(request, template_name, context_payload)


# file views
class FileCreateView(CreateView):
    form_class = forms.FileForm
    template_name = 'file/add.html'
    template_name = 'app_file_manager/app_file/html/add.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['submit_url'] = reverse('app_file_manager:old_file_add')
        context['add_folder'] = reverse('app_file_manager:old_folder_add')
        context['add_file'] = reverse('app_file_manager:old_folder_add')
        return context

    def get_success_url(self):
        return reverse('app_file_manager:old_folder_list')

    def post(self, request, *args, **kwargs):
        return super().post(request=request, args=args, **kwargs)


class FileListView(ListView):
    queryset = File.objects.all()
    context_object_name = 'file_list'
    template_name = 'app_file_manager/app_file/html/list.html'


class FileDownloadView(View):

    def get(self, request, pk):
        file = File.objects.filter(pk=pk).first()
        filename = file.file.path
        wrapper = FileWrapper(file.file)
        response = HttpResponse(wrapper, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=%s' % file.name
        response['Content-Length'] = os.path.getsize(filename)
        return response


# @login_required
# def file_delete(request, **kwargs):
#     file_id = kwargs.get('file_id')
#     file_name = kwargs.get('file_name')
#
#     print('---file_id---', file_id)
#     print('---file_name---', file_name)
#
#     file_get_qs = get_object_or_404(File, pk=file_id)
#     # print('---file_get_qs---', file_get_qs)
#
#     data = dict()
#     if request.method == 'POST':
#
#         file = file_get_qs.file
#         file_url_without_media = str(file)
#         filename = file.name.split("/")[-1]
#
#         parent_path = settings.MEDIA_ROOT + '/' + file_url_without_media
#
#         del_msg = delete_file(parent_path, filename)
#         if del_msg == "SUCCESS":
#             # # # delete row from DB
#             file_get_qs.delete()
#
#         # company_users_list = create_company_users_list(request)
#         # html_emp_jobs_list = create_html_emp_jobs_list(request, company_users_list)
#         # data['html_emp_jobs_list'] = html_emp_jobs_list
#
#         data['form_is_valid'] = True
#         data['message'] = 'Folder deleted successfully.'
#     else:
#         context = {'file_get_qs': file_get_qs}
#         data['html_form'] = render_to_string(template_name='file/partial_file_delete.html',
#                                              context=context, request=request)
#     data['action_type'] = "delete"
#     return JsonResponse(data)


# @login_required
def file_delete(request, **kwargs):
    # os.path.exists(path)
    file_name = kwargs.get('file_name')
    print('---file_name---', file_name)

    parent = request.GET.get('parent')
    base_path = settings.MEDIA_ROOT
    absolute_path = base_path + parent
    absolute_path = base_path + '/' + parent
    dir_path = base_path + '/' + parent
    folder_path = absolute_path.replace(base_path, "")

    # print('---parent---', parent)
    # print('---base_path---', base_path)
    # print("---absolute_path---", absolute_path)
    # print("---dir_path---", dir_path)
    # print("---folder_path---", folder_path)

    data = dict()
    if request.method == 'POST':

        # delete row from DB
        # folder_get_qs.delete()

        parent_path = dir_path
        del_msg = delete_file(parent_path, file_name)

        # company_users_list = create_company_users_list(request)
        # html_emp_jobs_list = create_html_emp_jobs_list(request, company_users_list)
        # data['html_emp_jobs_list'] = html_emp_jobs_list

        data['form_is_valid'] = True
        data['message'] = del_msg
    else:
        # context = {'folder_get_qs': folder_get_qs}
        context = {
            'file': {
                "file_name": file_name
            },
            "parent_name": parent
        }
        template_name = 'app_file_manager/app_file/html/partial_file_delete.html'
        data['html_form'] = render_to_string(template_name=template_name, context=context, request=request)

    data['action_type'] = "delete"
    return JsonResponse(data)


def file_update(request, **kwargs):
    template = 'app_file_manager/app_file/html/partial_file_update.html'

    # os.path.exists(path)
    file_name = kwargs.get('file_name')
    print('---file_name---', file_name)

    parent = request.GET.get('parent')
    base_path = settings.MEDIA_ROOT
    absolute_path = base_path + '/' + parent
    dir_path = base_path + '/' + parent
    folder_path = absolute_path.replace(base_path, "")

    # print('---parent---', parent)
    # print('---base_path---', base_path)
    # print("---absolute_path---", absolute_path)
    # print("---dir_path---", dir_path)
    # print("---folder_path---", folder_path)

    data = dict()
    if request.method == 'POST':
        file_rename_form = forms.FileRenameForm(data=request.POST, file_name=file_name)
        if file_rename_form.is_valid():
            # delete row from DB
            # folder_get_qs.delete()

            parent_path = dir_path
            old_name = file_name
            new_name = file_rename_form.cleaned_data['new_name']
            print('---new_name---', new_name)

            del_msg = rename_file(parent_path, old_name, new_name)

            # company_users_list = create_company_users_list(request)
            # html_emp_jobs_list = create_html_emp_jobs_list(request, company_users_list)
            # data['html_emp_jobs_list'] = html_emp_jobs_list

            data['form_is_valid'] = True
            data['message'] = del_msg
        else:
            data['form_is_valid'] = False
            data['message'] = "failed"
    else:
        # context = {'folder_get_qs': folder_get_qs}
        file_rename_form = forms.FileRenameForm(file_name=file_name)

        context = {
            'file': file_name,
            "parent_name": parent,
            "form": file_rename_form
        }
        data['html_form'] = render_to_string(template_name=template, context=context, request=request)

    data['action_type'] = "update"
    return JsonResponse(data)


def file_link(request, **kwargs):
    template = 'app_file_manager/app_file/html/partial_file_link.html'

    # os.path.exists(path)
    file_name = kwargs.get('file_name')
    print('---file_name---', file_name)

    parent = request.GET.get('parent')
    base_path = settings.MEDIA_ROOT
    absolute_path = base_path + parent
    dir_path = base_path + parent
    folder_path = absolute_path.replace(base_path, "")

    data = dict()
    if request.method == 'GET':
        # context = {'folder_get_qs': folder_get_qs}
        file_rename_form = forms.FileRenameForm(file_name=file_name)

        item = file_name

        get_download_link = reverse('app_file_manager:file_download', kwargs={'file_name': item})
        link = "/media/" + parent + item
        # set API URL here
        SCHEME = request.scheme
        HOST = request.get_host()
        API_URL = SCHEME + '://' + HOST
        request.api_url = API_URL
        #
        get_full_link = API_URL + link

        context = {
            'file': {
                "file_name": item,
                "get_download_link": get_download_link,
                "get_full_link": get_full_link,
                "link": link,
            },
            "parent_name": parent,
            "get_full_link": get_full_link,
            "form": file_rename_form
        }
        data['html_form'] = render_to_string(template_name=template, context=context, request=request)

    data['action_type'] = "link"
    return JsonResponse(data)


class FileDownloadNewView(View):

    def get(self, request, file_name):
        print('---file_name---', file_name)

        file = ""
        filename = file.file.path
        wrapper = FileWrapper(file.file)
        response = HttpResponse(wrapper, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=%s' % file.name
        response['Content-Length'] = os.path.getsize(filename)
        return response


# working in use file_and_folder_move
def file_and_folder_move(request, **kwargs):
    template = 'app_file_manager/app_file/html/partial_file_move.html'

    # os.path.exists(path)
    file_name = kwargs.get('file_name')
    print('---file_name---', file_name)

    parent = request.GET.get('parent')
    base_path = settings.MEDIA_ROOT
    absolute_path = base_path + '/' + parent
    folder_path = absolute_path.replace(base_path, "")

    # print('---parent---', parent)
    # print('---base_path---', base_path)
    # print("---absolute_path---", absolute_path)
    # print("---folder_path---", folder_path)

    return_walk_dir_tree = walk_dir_tree(request, base_path)
    all_possible_root = []
    if return_walk_dir_tree["message"] == 'SUCCESS':
        all_possible_root = return_walk_dir_tree["Tree"]

    parent_root = parent
    old_path = base_path + "/" + parent_root + file_name

    file_type = ""
    try:
        if os.path.isdir(old_path):
            file_type = "Folder"
        else:
            file_type = "File"
    except Exception as e:
        print('--Exception--', e)

    data = dict()
    if request.method == 'POST':
        file_rename_form = forms.FileMoveForm(data=request.POST, file_name=file_name,
                                              all_possible_root=all_possible_root, parent_root=parent_root)
        if file_rename_form.is_valid():
            selected_root = file_rename_form.cleaned_data['all_possible_root']

            new_path = base_path + selected_root

            move_msg, is_valid = move_file_or_folder(old_path, new_path)

            data['form_is_valid'] = is_valid
            data['message'] = move_msg
        else:
            data['form_is_valid'] = False
            data['message'] = "Something went wrong...!!!"
    else:
        file_rename_form = forms.FileMoveForm(file_name=file_name, all_possible_root=all_possible_root,
                                              parent_root=parent_root)

        context = {
            'file': file_name,
            "file_type": file_type,
            "parent_name": parent,
            "form": file_rename_form
        }
        data['html_form'] = render_to_string(template_name=template, context=context, request=request)

    data['action_type'] = "move"
    return JsonResponse(data)


def file_upload_view(request, **kwargs):
    template = 'app_file_manager/app_file/html/partial_file_upload.html'
    context = {}

    parent = request.GET.get('parent')
    base_path = settings.MEDIA_ROOT
    absolute_path = base_path + '/' + parent
    dir_path = base_path + '/' + parent
    folder_path = absolute_path.replace(base_path, "")

    # print('---parent---', parent)
    # print('---base_path---', base_path)
    # print("---absolute_path---", absolute_path)
    # print("---dir_path---", dir_path)
    # print("---folder_path---", folder_path)

    return_walk_dir_tree = walk_dir_tree(request, base_path)
    all_possible_root = []
    if return_walk_dir_tree["message"] == 'SUCCESS':
        all_possible_root = return_walk_dir_tree["Tree"]
    # print('---all_possible_root---',all_possible_root)

    parent_root = parent

    data = dict()
    if request.method == 'POST':
        file_upload_form = forms.FileUploadForm(request.POST, request.FILES, all_possible_root=all_possible_root,
                                                parent_root=parent_root)

        if file_upload_form.is_valid():

            selected_root = file_upload_form.cleaned_data['all_possible_root']
            file = file_upload_form.cleaned_data['file']
            parent_path = base_path + selected_root

            # # # file save in default storage
            # file_name = default_storage.save(file.name, file)

            # file save in given location
            fs = FileSystemStorage(location=parent_path)  # defaults to   MEDIA_ROOT
            filename = fs.save(file.name, file)
            file_url = fs.url(filename)
            print('---views file_url---', file_url)

            upload_msg = "success"

            data['form_is_valid'] = True
            data['message'] = upload_msg
            data['file_url'] = file_url
        else:
            data['form_is_valid'] = False
            data['message'] = "failed"
            context = {
                "parent_name": parent,
                "form": file_upload_form
            }

    else:
        file_upload_form = forms.FileUploadForm(all_possible_root=all_possible_root, parent_root=parent_root)
        context = {
            "parent_name": parent,
            "form": file_upload_form
        }

    data['html_form'] = render_to_string(template_name=template, context=context, request=request)
    data['action_type'] = "upload"
    return JsonResponse(data)
