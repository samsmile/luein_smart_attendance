#!/usr/bin/env bash

cd /root/projects/luein_smart_attendence/git_pull_codebase/
source /root/projects/luein_smart_attendence/luein_sm_att_env/bin/activate
python /root/projects/luein_smart_attendence/git_pull_codebase/app_smart_attendance/src/aws_s3_backup.py
cd ~
